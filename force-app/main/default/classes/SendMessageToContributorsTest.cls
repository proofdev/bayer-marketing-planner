@isTest
public class SendMessageToContributorsTest {
    @isTest
    static void allContributors(){ 
        try{
            Date sDate = Date.newInstance(2019, 01, 01);
            Date eDate = Date.newInstance(2025, 01, 01);
            Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
            musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
            marketingPlan.Name = 'Marketing Plan';
            marketingPlan.musqotmp__StartDate__c = sDate;
            marketingPlan.musqotmp__EndDate__c = eDate;
            marketingPlan.musqotmp__Description__c = '';
            marketingPlan.musqotmp__CampaignTemplate__c = false;
            marketingPlan.musqotmp__Status__c = 'In Progress';
            marketingPlan.musqotmp__MarketingArea__c = 'Europe';
            marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
            marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
            marketingPlan.RecordTypeId = recordTypeId;
            insert marketingPlan;
            Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('Campaign').getDescribe().getSObjectType();
            SObject newSobjectCampaign = objectDef.newSobject();
            newSobjectCampaign.put('Name', 'Campaign');
            newSobjectCampaign.put('musqotmp__MarketingPlan__c', marketingPlan.Id);
            newSobjectCampaign.put('StartDate', sDate);
            newSobjectCampaign.put('EndDate', eDate);
            insert newSobjectCampaign;
            User userRecord = TestDataSetup.getUser('System Administrator');
            insert userRecord;
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'allContributors');
            jsonGenData.writeEndObject();
            SendMessageToContributors.allContributors(jsonGenData.getAsString(), newSobjectCampaign.Id);
            SendMessageToContributors.allContributors(jsonGenData.getAsString(), '');
            Schema.Campaign newSobjectCampaign1 = TestDataSetup.getCampaign('Campaign', sDate, eDate, 'In Progress', marketingPlan);
            insert newSobjectCampaign1;
            Task taskRecord = TestDataSetup.getTask(newSobjectCampaign1,userRecord,'Not Started',Date.Today());
            Database.SaveResult sr = Database.insert(taskRecord,false);
            SendMessageToContributors.allContributors(jsonGenData.getAsString(), newSobjectCampaign1.Id);
        }catch(exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void saveFile(){
        try{
            Date sDate = Date.newInstance(2019, 01, 01);
            Date eDate = Date.newInstance(2025, 01, 01);
            Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
            musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
            marketingPlan.Name = 'Marketing Plan';
            marketingPlan.musqotmp__StartDate__c = sDate;
            marketingPlan.musqotmp__EndDate__c = eDate;
            marketingPlan.musqotmp__Description__c = '';
            marketingPlan.musqotmp__CampaignTemplate__c = false;
            marketingPlan.musqotmp__Status__c = 'In Progress';
            marketingPlan.musqotmp__MarketingArea__c = 'Europe';
            marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
            marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
            marketingPlan.RecordTypeId = recordTypeId;
            insert marketingPlan;
            Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('Campaign').getDescribe().getSObjectType();
            SObject newSobjectCampaign = objectDef.newSobject();
            newSobjectCampaign.put('Name', 'Campaign');
            newSobjectCampaign.put('musqotmp__MarketingPlan__c', marketingPlan.Id);
            newSobjectCampaign.put('StartDate', sDate);
            newSobjectCampaign.put('EndDate', eDate);
            insert newSobjectCampaign;
            String base64Data = 'iVBORw0KGgoAAAANSUhEUgAABVYAAAMACAYAAADPPjzCAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAP%2B6SURBVHhe7P150GfZWd8J3twzK6uyFtWi0i4kgyc8GAlhA2oL7IaZMRN2AJIHbzhoexw9E2F2cIQjHNNBu8N%2FGNyG7mgi2jYe22EQZhCLjd1ghMBIRkgCtIFk0FZV2kqqfa%2FKfZ7P85zPvc%2Fv5u%2FNenOpUmUpv2%2BePOc8%2B1nvufd339%2B75xMf%2FdjZPXv2TGfOnJn27t07nT17dhKUSXv27pn27DmXtyfyvfugFw197ITwtHfY3Ldv33RmyII9zUfykA%2BgK%2BDuw9aQw9biuWQz5mYXn%2BicDnvwoOMrEcq0ASRnKGUbkB0J3QP7988xYTOz%2FH9Kf%2Bk7yrYhdYk1dIhTWvTcdOZ00KKN4szp03PsgDz1Bu3M2fKbp';
            List<String> files = new List<String>{'Screenshot (21).png','Screenshot (13).png'};
            SendMessageToContributors.saveFile(newSobjectCampaign.Id, new List<String>{'Screenshot (21).png'}, base64Data);
        }catch(exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void saveFileException(){
        try{
            SendMessageToContributors.saveFile(null, null, null);
        }catch(exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
    @isTest
    static void emailMessageToContributors(){
        try{
            Date sDate = Date.newInstance(2019, 01, 01);
            Date eDate = Date.newInstance(2025, 01, 01);
            User userRecord = TestDataSetup.getUser('System Administrator');
            insert userRecord;
            /*musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
            marketingPlan.Name = 'Marketing Plan';
            marketingPlan.musqotmp__StartDate__c = sDate;
            marketingPlan.musqotmp__EndDate__c = eDate;
            marketingPlan.musqotmp__Description__c = '';
            marketingPlan.musqotmp__CampaignTemplate__c = false;
            marketingPlan.musqotmp__Status__c = 'In Progress';
            marketingPlan.musqotmp__MarketingArea__c = 'Europe';
            marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
            marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
            insert marketingPlan;*/
            musqotmp__MarketingPlan__c marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',sDate,eDate,'In Progress','Global');
            marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
            marketingPlan.musqotmp__CampaignTemplate__c = false;
            marketingPlan.musqotmp__MarketingArea__c = 'Europe';
            insert marketingPlan;
            //Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('Campaign').getDescribe().getSObjectType();
            //SObject newSobjectCampaign = objectDef.newSobject();
            /*newSobjectCampaign.put('Name', 'Campaign');
            newSobjectCampaign.put('musqotmp__MarketingPlan__c', marketingPlan.Id);
            newSobjectCampaign.put('StartDate', sDate);
            newSobjectCampaign.put('EndDate', eDate);*/
            Schema.Campaign newSobjectCampaign = TestDataSetup.getCampaign('Campaign', sDate, eDate, 'In Progress', marketingPlan);
            insert newSobjectCampaign;
           	
            String campaignInRole = 'Contributor';
            String Subject = 'Test Email';
            String body = 'Test';
            String base64Data = 'iVBORw0KGgoAAAANSUhEUgAABVYAAAMACAYAAADPPjzCAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAP%2B6SURBVHhe7P150GfZWd8J3twzK6uyFtWi0i4kgyc8GAlhA2oL7IaZMRN2AJIHbzhoexw9E2F2cIQjHNNBu8N%2FGNyG7mgi2jYe22EQZhCLjd1ghMBIRkgCtIFk0FZV2kqqfa%2FKfZ7P85zPvc%2Fv5u%2FNenOpUmUpv2%2BePOc8%2B1nvufd339%2B75xMf%2FdjZPXv2TGfOnJn27t07nT17dhKUSXv27pn27DmXtyfyvfugFw197ITwtHfY3Ldv33RmyII9zUfykA%2BgK%2BDuw9aQw9biuWQz5mYXn%2BicDnvwoOMrEcq0ASRnKGUbkB0J3QP7988xYTOz%2FH9Kf%2Bk7yrYhdYk1dIhTWvTcdOZ00KKN4szp03PsgDz1Bu3M2fKbp';
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            ContentVersion cv = new ContentVersion();
            cv.Title = Subject;
            cv.PathOnClient = '/' + Subject;
            cv.FirstPublishLocationId = newSobjectCampaign.Id;
            cv.VersionData = EncodingUtil.base64Decode(base64Data);
            cv.IsMajorVersion = true;
            insert cv;
            SendMessageToContributors.emailMessageToContributors( newSobjectCampaign.Id, campaignInRole, Subject, body,  new List<String>{'anji59924@gmail.com'}, new List<Id>{cv.Id});
        	Task taskRecord = TestDataSetup.getTask(newSobjectCampaign,userRecord,'Not Started',Date.Today());
            Database.SaveResult sr = Database.insert(taskRecord,false);
            SendMessageToContributors.emailMessageToContributors( newSobjectCampaign.Id, campaignInRole, Subject, body,  new List<String>{'anji59924@gmail.com'}, new List<Id>{cv.Id});
        }catch(exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void emailMessageToContributorException(){
        try{
            SendMessageToContributors.emailMessageToContributors( null, null, null, null,  null, null);
        }catch(exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void DynamicObjectInfo(){
        try{
            Date sDate = Date.newInstance(2019, 01, 01);
            Date eDate = Date.newInstance(2025, 01, 01);
            Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
            musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
            marketingPlan.Name = 'Marketing Plan';
            marketingPlan.musqotmp__StartDate__c = sDate;
            marketingPlan.musqotmp__EndDate__c = eDate;
            marketingPlan.musqotmp__Description__c = '';
            marketingPlan.musqotmp__CampaignTemplate__c = false;
            marketingPlan.musqotmp__Status__c = 'In Progress';
            marketingPlan.musqotmp__MarketingArea__c = 'Europe';
            marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
            marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
            marketingPlan.recordTypeId = recordTypeId;
            insert marketingPlan;
            Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('Campaign').getDescribe().getSObjectType();
            SObject newSobjectCampaign = objectDef.newSobject();
            newSobjectCampaign.put('Name', 'Campaign');
            newSobjectCampaign.put('musqotmp__MarketingPlan__c', marketingPlan.Id);
            newSobjectCampaign.put('StartDate', sDate);
            newSobjectCampaign.put('EndDate', eDate);
            insert newSobjectCampaign;
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'DynamicObjectInfo');
            jsonGenData.writeEndObject();
            Campaign.DynamicObjectInfo(jsonGenData.getAsString(), newSobjectCampaign.Id);
            Campaign.DynamicObjectInfo(jsonGenData.getAsString(), '');
        }catch(exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void DynamicObjectInfoException(){
        try{
            Campaign.DynamicObjectInfo(null, null);
        }catch(exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
    @isTest
    static void exceptions(){
        exception ex;
        DataModel.Exceptions exceptions = new DataModel.Exceptions();
        exceptions.component = 'exceptions';
        exceptions.ex = ex;
        String param = JSON.serialize(exceptions);
        System.assertEquals('exceptions' , exceptions.component);
        Test.startTest();
        SendMessageToContributors.allContributors(param, param);
        SendMessageToContributors.DynamicObjectInfo(param, param);
        Test.stopTest();
    }
    
}