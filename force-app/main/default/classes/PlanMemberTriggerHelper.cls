/************************************************************************************************************************************
* Developer			Date				Description
* ---------------------------------------------------------------------------------------------------------------------------------------	                                                               												campaigncontributorrelatedlist 
* Abhishek          30/12/2020			Helper class for trigger PlanMemberTrigger
**************************************************************************************************************************************/
public class PlanMemberTriggerHelper {
    public static void processPlanMemberBeforeInsert(List<musqotmp__MarketingPlanMember__c> TriggerNew){
        List<Id> planIdList = new List<Id>();
        Map<String,musqotmp__MarketingPlanMember__c> uniqueIdToPlanMemberMap = new Map<String,musqotmp__MarketingPlanMember__c>();
        List<musqotmp__MarketingPlanMember__c> ObjPlanMemberList = new List<musqotmp__MarketingPlanMember__c>();
        try{
            for(musqotmp__MarketingPlanMember__c planMember : TriggerNew){
                planIdList.add(planMember.musqotmp__MarketingPlan__c);
            }
            if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__PlanAccess__c.isAccessible())
            {
            	ObjPlanMemberList = [select musqotmp__MarketingPlan__c,musqotmp__User__c , musqotmp__UniquePlanMember__c,musqotmp__Role__c 
                                                             from musqotmp__MarketingPlanMember__c
                                                             Where musqotmp__MarketingPlan__c in :planIdList ];
            }
            //System.debug('ObjPlanMemberList'+ObjPlanMemberList);
            for(musqotmp__MarketingPlanMember__c ObjPlanMember : ObjPlanMemberList){
                uniqueIdToPlanMemberMap.put(ObjPlanMember.musqotmp__UniquePlanMember__c,ObjPlanMember);
            }
            String customLabelValue = System.Label.msg_PlanMember_user ;
            String uniqueMemberId;
            for(musqotmp__MarketingPlanMember__c planMember : TriggerNew){
                //System.debug('uniqueIdToPlanMemberMap'+uniqueIdToPlanMemberMap);
                uniqueMemberId = planMember.musqotmp__MarketingPlan__c+''+planMember.musqotmp__User__c;
                //System.debug('uniqueMemberId'+uniqueMemberId);
                if(uniqueIdToPlanMemberMap.get(uniqueMemberId) != null){
                    planMember.addError(customLabelValue);
                } else {
                    planMember.musqotmp__UniquePlanMember__c = planMember.musqotmp__MarketingPlan__c+''+planMember.musqotmp__User__c; 
                }
            }
        }Catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanMemberTriggerHelper','processPlanMemberBeforeInsert',ex);
        }
    }
    Public static void processPlanMemberAfterInsert(List<musqotmp__MarketingPlanMember__c> TriggerNew){
        try{
            List<musqotmp__MarketingPlan__Share> jobShares  = new List<musqotmp__MarketingPlan__Share>();
            if(Schema.sObjectType.musqotmp__MarketingPlan__Share.isCreateable())
            {
                for(musqotmp__MarketingPlanMember__c t : TriggerNew){
                    if(t.musqotmp__PlanAccess__c != 'Owner'){
                        musqotmp__MarketingPlan__Share planRecord = new musqotmp__MarketingPlan__Share();
                        planRecord.ParentId = t.musqotmp__MarketingPlan__c;
                        planRecord.UserOrGroupId = t.musqotmp__User__c;
                        // We have Edit, Read, All sharing in apex
                        if(t.musqotmp__PlanAccess__c  == 'Read Only'){
                            planRecord.AccessLevel = 'read';
                        }else if(t.musqotmp__PlanAccess__c == 'Read Write'){
                            planRecord.AccessLevel = 'edit';
                        }
                        planRecord.RowCause = schema.musqotmp__MarketingPlan__Share.RowCause.MarketingPlanMember__c;
                        //System.debug('planRecord'+planRecord);
                        jobShares.add(planRecord);
                    }
                }
            }
            if(jobShares.size() > 0 && 
             Schema.sObjectType.musqotmp__MarketingPlan__Share.fields.RowCause.isCreateable() &&
             Schema.sObjectType.musqotmp__MarketingPlan__Share.fields.ParentId.isCreateable() &&
             Schema.sObjectType.musqotmp__MarketingPlan__Share.fields.UserOrGroupId.isCreateable() &&
             Schema.sObjectType.musqotmp__MarketingPlan__Share.fields.AccessLevel.isCreateable() ){
                Database.SaveResult[] jobShareInsertResult = Database.insert(jobShares);
            }
        }Catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanMemberTriggerHelper','processPlanMemberAfterInsert',ex);
        }
    }
    public static void processPlanMemberBeforeUpdate(List<musqotmp__MarketingPlanMember__c> TriggerNew,Map<Id,musqotmp__MarketingPlanMember__c> oldMap){
        try{
            Set<Id> planIdSet = new Set<Id>();
            List<musqotmp__MarketingPlan__c> planList = new List<musqotmp__MarketingPlan__c>();
            List<musqotmp__MarketingPlanMember__c> ownerMemberList = new List<musqotmp__MarketingPlanMember__c>();
            List<musqotmp__MarketingPlanMember__c> planUserChangeList = new List<musqotmp__MarketingPlanMember__c>();
            for(musqotmp__MarketingPlanMember__c planMember : TriggerNew){
                if((planMember.musqotmp__PlanAccess__c != oldMap.get(planMember.Id).musqotmp__PlanAccess__c &&
                   	oldMap.get(planMember.Id).musqotmp__PlanAccess__c == 'Owner' )||
                  	(planMember.musqotmp__User__c != oldMap.get(planMember.Id).musqotmp__User__c &&
                   	 planMember.musqotmp__PlanAccess__c == 'Owner'))
                {
                    planIdSet.add(planMember.musqotmp__MarketingPlan__c);
                    ownerMemberList.add(planMember);
                }else if(planMember.musqotmp__User__c != oldMap.get(planMember.Id).musqotmp__User__c &&
                          planMember.musqotmp__PlanAccess__c != 'Owner')
                {
                      planIdSet.add(planMember.musqotmp__MarketingPlan__c);
                      planUserChangeList.add(planMember); 
                }
            }
            //System.debug('ownerMemberList'+ ownerMemberList);
            if(planIdSet.size() > 0){
                Map<Id,Id> planToPlanOwnerMap = new Map<Id,Id>();
                if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.isAccessible())
                {
            		planList = [SELECT Id, ownerId, owner.Name from musqotmp__MarketingPlan__c where Id in :planIdSet];
                }
                for(musqotmp__MarketingPlan__c plan: planList){
                    planToPlanOwnerMap.put(plan.Id,plan.ownerId);
                }
            	//System.debug('planList'+ planList);
                for(musqotmp__MarketingPlanMember__c planMember: ownerMemberList){
                    if(oldMap.get(planMember.Id).musqotmp__User__c == planToPlanOwnerMap.get(planMember.musqotmp__MarketingPlan__c)){
                        String customLabelValue = System.Label.Marketing_Plan_Owner ;
                    	planMember.addError(customLabelValue); 
                    }
                }
                for(musqotmp__MarketingPlanMember__c planMember: planUserChangeList){
                    if(planMember.musqotmp__User__c == planToPlanOwnerMap.get(planMember.musqotmp__MarketingPlan__c)){
                        String customLabelValue = System.Label.Marketing_Plan_Owner;
                    	planMember.addError(customLabelValue); 
                    }
                }
            }
        }Catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanMemberTriggerHelper','processPlanMemberBeforeUpdate',ex);
        }
    }
    public static void processPlanMemberAfterUpdate(List<musqotmp__MarketingPlanMember__c> TriggerNew, Map<Id,musqotmp__MarketingPlanMember__c> oldMap){
        try{
            List<musqotmp__MarketingPlan__Share> jobShares  = new List<musqotmp__MarketingPlan__Share>();
            List<musqotmp__MarketingPlan__Share> planSharingToBeRemoved = new List<musqotmp__MarketingPlan__Share>();
            List<Id> planOwnerIdList = new List<Id>();
            Map<String,musqotmp__MarketingPlan__Share> parentPlanToPlanShareMap = new Map<String,musqotmp__MarketingPlan__Share>();
            List<musqotmp__MarketingPlanMember__c> planMemberListUpdated = new List<musqotmp__MarketingPlanMember__c>();
            Set<Id> planIdSet = new Set<Id>();
            for(musqotmp__MarketingPlanMember__c planMember : TriggerNew){
                if((planMember.musqotmp__PlanAccess__c != oldMap.get(planMember.Id).musqotmp__PlanAccess__c ||
                    planMember.musqotmp__User__c != oldMap.get(planMember.Id).musqotmp__User__c) &&
                    planMember.musqotmp__PlanAccess__c != 'Owner'){
                	planIdSet.add(planMember.musqotmp__MarketingPlan__c);
                    planMemberListUpdated.add(planMember);
     			}
            }
            //System.debug('planIdSet'+ planIdSet);
            Map<Id,musqotmp__MarketingPlan__c> planMap = new Map<Id,musqotmp__MarketingPlan__c>([SELECT Id, OwnerId from musqotmp__MarketingPlan__c where Id in :planIdSet]);
            if(planIdSet.size() > 0){
                if(Schema.sObjectType.musqotmp__MarketingPlan__share.fields.AccessLevel.isAccessible())
                {
                    for(musqotmp__MarketingPlan__Share planshare :[SELECT AccessLevel,ParentId,RowCause,UserOrGroupId 
                                         FROM musqotmp__MarketingPlan__Share 
                                         where ParentId in :planIdSet and AccessLevel != 'All' and RowCause = 'musqotmp__MarketingPlanMember__c']){
                        //System.debug('planshare'+ planshare);
                        parentPlanToPlanShareMap.put(planshare.ParentId+''+planshare.UserOrGroupId,planshare);
                    }
                }
                //System.debug('parentPlanToPlanShareMap'+ parentPlanToPlanShareMap);
                musqotmp__MarketingPlan__Share planRecord;
                for(musqotmp__MarketingPlanMember__c planMember :planMemberListUpdated){
                    if(Schema.sObjectType.musqotmp__MarketingPlan__share.isUpdateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlan__share.isDeletable())
                    {
                        if(parentPlanToPlanShareMap.get(planMember.musqotmp__UniquePlanMember__c ) != null){
                            planRecord = parentPlanToPlanShareMap.get(planMember.musqotmp__UniquePlanMember__c );
                            if(planMember.musqotmp__PlanAccess__c == 'Read Only'){
                                planRecord.AccessLevel = 'read';
                            }else if(planMember.musqotmp__PlanAccess__c == 'Read Write'){
                                planRecord.AccessLevel = 'edit';
                            }
                            jobShares.add(planRecord);
                        } else {
                            planRecord = new musqotmp__MarketingPlan__Share();
                            planRecord.UserOrGroupId = planMember.musqotmp__User__c;
                            planRecord.ParentId = planMember.musqotmp__MarketingPlan__c;
                            if(planMember.musqotmp__PlanAccess__c == 'Read Only'){
                                planRecord.AccessLevel = 'read';
                            }else if(planMember.musqotmp__PlanAccess__c == 'Read Write'){
                                planRecord.AccessLevel = 'edit';
                            }
                             planRecord.RowCause = schema.musqotmp__MarketingPlan__Share.RowCause.MarketingPlanMember__C;
                            jobShares.add(planRecord);
                        }
                        String key = planMember.musqotmp__MarketingPlan__c +''+ planMap.get(planMember.musqotmp__MarketingPlan__c).OwnerId; 
                        if(parentPlanToPlanShareMap.get(key) != null){
                            planSharingToBeRemoved.add(parentPlanToPlanShareMap.get(key));
                        }
            	}
            //System.debug('planSharingToBeRemoved'+ planSharingToBeRemoved);
        	if(jobShares.size() > 0){
            	//system.debug('jobShares'+jobShares);
            	if(Schema.sObjectType.musqotmp__MarketingPlan__Share.fields.ParentId.isCreateable() &&
                   Schema.sObjectType.musqotmp__MarketingPlan__Share.fields.ParentId.isUpdateable()){
                   	Database.upsert(jobShares);   
                   }
        	}
            if(planSharingToBeRemoved.size() > 0){
                if(musqotmp__MarketingPlan__Share.sObjectType.getDescribe().isDeletable()){
                	delete planSharingToBeRemoved ;   
                }
            }
            }
        }
        }Catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanMemberTriggerHelper','processPlanMemberAfterUpdate',ex);
        }
    }
    public static void processPlanMemberBeforeDelete(List<musqotmp__MarketingPlanMember__c> TriggerOld){
        try{
            for(musqotmp__MarketingPlanMember__c t : TriggerOld){
                if(t.musqotmp__PlanAccess__c == 'Owner'){
                    String customLabelValue = System.Label.Marketing_Plan_Owner ;
                    t.addError(customLabelValue);  
                }
            }   
        }Catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanMemberTriggerHelper','processPlanMemberBeforeDelete',ex);
        }
    }
    public static void processPlanMemberAfterDelete(List<musqotmp__MarketingPlanMember__c> TriggerOld){
         try{
            //System.debug('processPlanMemberAfterDelete');
            List<Id> parentIdList = new List<Id>();
            List<Id> userIdList = new List<Id>();
            Map<String, musqotmp__MarketingPlan__Share> parentPlanToPlanShareMap = new Map<String,musqotmp__MarketingPlan__Share>();
            List<musqotmp__MarketingPlan__Share> planShareDeleteList = new List<musqotmp__MarketingPlan__Share>();
            for(musqotmp__MarketingPlanMember__c planMem : TriggerOld){
                if(planMem.musqotmp__Role__c != 'Owner'){
                    parentIdList.add(planMem.musqotmp__MarketingPlan__c);
                    userIdList.add(planMem.musqotmp__User__c);
                }
            }
            if(parentIdList.size() > 0){
                if(Schema.sObjectType.musqotmp__MarketingPlan__share.fields.RowCause.isAccessible())
                {
                    for(musqotmp__MarketingPlan__Share planshare :[SELECT ParentId,RowCause,UserOrGroupId 
                                         FROM musqotmp__MarketingPlan__Share 
                                         WHERE ParentId in :parentIdList]){
                        parentPlanToPlanShareMap.put(planshare.ParentId+''+planshare.UserOrGroupId,planshare);
                    }   
                }
            }
             if(Schema.sObjectType.musqotmp__MarketingPlan__share.isDeletable())
             {
                for(musqotmp__MarketingPlanMember__c planMem : TriggerOld){
                    if(planMem.musqotmp__PlanAccess__c != 'Owner' && 
                       parentPlanToPlanShareMap.get(planMem.musqotmp__UniquePlanMember__c )!= null){
                        planShareDeleteList.add(parentPlanToPlanShareMap.get(planMem.musqotmp__UniquePlanMember__c ));
                    }
                }
                //System.debug('planShareDeleteList'+ planShareDeleteList);
                 if(planShareDeleteList.size()>0){
                     if(musqotmp__MarketingPlan__Share.sObjectType.getDescribe().isDeletable()){
                     	database.delete(planShareDeleteList);    
                     }  
                 }
         	}
        }Catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanMemberTriggerHelper','processPlanMemberAfterDelete',ex);
        }
    }
}