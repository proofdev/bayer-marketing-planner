Global class PostInstallDataScript {
    /*Global PostInstallDataScript(){
        try{
            Id campaignFolderRecTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Folder').getRecordTypeId();
            Id campaignExecutionRecTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Type').getRecordTypeId();
            //System.debug('inside ScratchOrgDataCreation');
            Date todaysdate = Date.today();
            Date sDateExecution = DATE.Today();
            Date eDateExecution = DATE.Today() + 30;
            Date sDateCurrentYear = DATE.newInstance(todaysdate.year(), 1, 1);
            Date eDateCurrentYear = DATE.newInstance(todaysdate.year(), 12, 31);
            Date sDateNextYear = DATE.newInstance(todaysdate.year()+1, 1, 1);
            Date eDateNextYear = DATE.newInstance(todaysdate.year()+1, 12, 31);
            Id currentUserId = UserInfo.getUserId();
            
            Integer eCurrentYear = System.Today().year();
            Integer eNextYear = System.Today().year() + 1;
            Id marketingPlanRecTypeId = Schema.SObjectType.musqotmp__MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
            Id unknownPlanRecTypeId = Schema.SObjectType.musqotmp__MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('UnknownPlan').getRecordTypeId();
            Id templatePlanRecTypeId = Schema.SObjectType.musqotmp__MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('TemplatePlan').getRecordTypeId();
            String MarketingArea = 'Europe';
            
            List<musqotmp__CampaignManagerSetup__c> setUpRecordList = new List<musqotmp__CampaignManagerSetup__c>();
            if(Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.Id.isAccessible()){
                setUpRecordList = [SELECT id from musqotmp__CampaignManagerSetup__c limit 1];
            }
            
            musqotmp__MarketingPlan__c  marketingPlan = new musqotmp__MarketingPlan__c();
            musqotmp__MarketingPlan__c  marketingPlanNE = new musqotmp__MarketingPlan__c(); 
            musqotmp__MarketingPlan__c  campaignTemplate = new musqotmp__MarketingPlan__c(); 
            musqotmp__MarketingPlan__c  unknownTemplate=new musqotmp__MarketingPlan__c(); 
            musqotmp__MarketingPlan__c  campaignTemplateNE = new musqotmp__MarketingPlan__c(); 
            musqotmp__MarketingPlan__c  unknownTemplateNE = new musqotmp__MarketingPlan__c(); 
            
            Schema.Campaign campaignEurope = new Schema.Campaign();
            Schema.Campaign subCampaignEuropeDivA = new Schema.Campaign();
            Schema.Campaign subCampaignEuropeDivAExec = new Schema.Campaign();
            Schema.Campaign subCampaignEuropeDivB = new Schema.Campaign();
            Schema.Campaign subCampaignEuropeDivBExec = new Schema.Campaign();
            
            musqotmp__Gantt__c  GanttAll = new musqotmp__Gantt__c();
            musqotmp__Gantt__c  GanttAllUn2;
            musqotmp__Gantt__c  GanttAllCY = new musqotmp__Gantt__c(); 
            musqotmp__Gantt__c  GanttAllNY = new musqotmp__Gantt__c(); 
            
            musqotmp__MyWorkspace__c  MyWorkspaceAll = new musqotmp__MyWorkspace__c(); 
            musqotmp__MyWorkspace__c  MyWorkspaceMy = new musqotmp__MyWorkspace__c(); 
            
            if(setUpRecordList.size() == 0) {
                // Create campaign manager settings
                if(Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.Name.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__UseSalesCloud__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__UseMarketingCloud__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__UseBudget__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__UseCampaignCost__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__UseQuipEnterprize__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__GanttProgressType__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__GanttProgressTypeFolder__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__BarColorCampaign__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__BarColorMarketingActivity__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__BarColorMarketingJourney__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__CampaignManagerSetup__c.fields.musqotmp__BarColorMarketingPlan__c.isCreateable()
                  ){
                      musqotmp__CampaignManagerSetup__c campaignmanager  = new musqotmp__CampaignManagerSetup__c();{
                          String orgId = UserInfo.getOrganizationId();
                          campaignmanager.Name=orgId;
                          campaignmanager.musqotmp__UseSalesCloud__c = true;
                          campaignmanager.musqotmp__UseMarketingCloud__c = false;
                          campaignmanager.musqotmp__UseBudget__c = true;
                          campaignmanager.musqotmp__UseCampaignCost__c = true;
                          campaignmanager.musqotmp__UseQuipEnterprize__c = False;
                          campaignmanager.musqotmp__GanttProgressType__c = 1;
                          campaignmanager.musqotmp__GanttProgressTypeFolder__c = 1;
                          campaignmanager.musqotmp__BarColorCampaign__c = '#0E78B4,#125E62,#484848,#118ED7,#0F7980,#23A7EC,#13969F,#7F7F7F,#44BEF8,#1CAFB9';
                          campaignmanager.musqotmp__BarColorMarketingActivity__c = '#642F88,#FC8527,#BD005F,#72498F,#FC9B48,#D11174,#875EA3,#FDB16C,#E32987,#A68BB9';
                          campaignmanager.musqotmp__BarColorMarketingJourney__c = '#A1004D';
                          //campaignmanager.musqotmp__BarColorMarketingCloudActivity__c = '#FC6D0D,#642F88,#FC8527,#BD005F,#72498F,#FC9B48,#D11174,#875EA3,#FDB16C,#E32987';
                          campaignmanager.musqotmp__BarColorMarketingPlan__c = '#DC5C07,#86003C,#551E7C,#0B5E8D,#094D51,#323232,#999999';
                          insert campaignmanager;
                      }
                }
                // Create disable settings
                if(Schema.sObjectType.musqotmp__DisableTrigger__c.fields.musqotmp__Task__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__DisableTrigger__c.fields.musqotmp__CampaignContributor__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__DisableTrigger__c.fields.musqotmp__Campaign__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__DisableTrigger__c.fields.musqotmp__MarketingActivity__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__DisableTrigger__c.fields.musqotmp__MarketingJourney__c.isCreateable()
                  ){
                      musqotmp__DisableTrigger__c disableTriggerRecord = new musqotmp__DisableTrigger__c();
                      disableTriggerRecord.musqotmp__Task__c = false;
                      disableTriggerRecord.musqotmp__CampaignContributor__c = false;
                      disableTriggerRecord.musqotmp__Campaign__c = false;
                      disableTriggerRecord.musqotmp__MarketingActivity__c = false;
                      disableTriggerRecord.musqotmp__MarketingJourney__c = false;
                      insert disableTriggerRecord;
                }
            }
            
            //Create Marketing plan method signature getNewPlan(name,recTypeId,description,planType,sDate,eDate,status)
            //Create Maketing Plan - current Year
            String description = 'Marketing Plan is the main Plan to capture marketing Campaigns and Activities for year '+ eCurrentYear;
            marketingPlan = getNewPlan('Marketing Plan '+ eCurrentYear, marketingPlanRecTypeId, description, 'Global', sDateCurrentYear, eDateCurrentYear, 'In Progress');
            insert marketingPlan;
            
            //getCampaign method signature(campaignName,startDate,endDate,status,marketingPlan,type,mktArea,bu,recordTypeId,parentId)
            //Campaigns - Europe
            campaignEurope = getCampaign('Europe',sDateCurrentYear,eDateCurrentYear,'Planned',marketingPlan,'Folder',MarketingArea,'Division A;Division B',campaignFolderRecTypeId,null);
            if(campaignEurope != null){
                insert campaignEurope;
                //SubCampaigns - Europe DivisionA
                subCampaignEuropeDivA = getCampaign('Europe - Division A',sDateCurrentYear,eDateCurrentYear,'Planned',marketingPlan,'Folder',MarketingArea,'Division A',campaignFolderRecTypeId,campaignEurope.Id);
                insert subCampaignEuropeDivA;
                
                //SubSubCampaigns - Europe DivisionA Execution
                subCampaignEuropeDivAExec = getCampaign('Advertisement Europe - Division A (Exec)',sDateExecution,eDateExecution,'In Progress',marketingPlan,'Advertisement',MarketingArea,'Division A',campaignExecutionRecTypeId,subCampaignEuropeDivA.Id);
                insert subCampaignEuropeDivAExec;
                
                //SubCampaigns - Europe DivisionB
                subCampaignEuropeDivB = getCampaign('Europe - Division B',sDateCurrentYear,eDateCurrentYear,'Planned',marketingPlan,'Folder',MarketingArea,'Division B',campaignFolderRecTypeId,campaignEurope.Id);
                insert subCampaignEuropeDivB;
                
                //SubSubCampaigns - Europe DivisionB Execution
                subCampaignEuropeDivBExec = getCampaign('Advertisement Europe - Division B (Exec)',sDateExecution,eDateExecution,'In Progress',marketingPlan,'Advertisement',MarketingArea,'Division B',campaignExecutionRecTypeId,subCampaignEuropeDivB.Id);
                insert subCampaignEuropeDivBExec;
            }
            
            //Campaign Template - current Year
            description = 'This Plan will store predefined templates for Campaigns and Activities for year '+ eCurrentYear;
            campaignTemplate = getNewPlan('Campaign Templates ' + eCurrentYear,templatePlanRecTypeId,description,'Global',sDateCurrentYear,eDateCurrentYear,'In Progress');
            campaignTemplate.musqotmp__CampaignTemplate__c = true; 
            insert campaignTemplate;
            
            //Unknown Template - current Year
            description = 'In this plan, all campaigns that do not have a plan will be defined. Unknown Campaign Plan is use for show Campaigns in the Gantt view with not has a connection to any Plan.';
            unknownTemplate = getNewPlan('Unknown Campaign '+ eCurrentYear,unknownPlanRecTypeId,description,'Unknown Plan',sDateCurrentYear,eDateCurrentYear,'In Progress');
            unknownTemplate.musqotmp__UnknownCampaigns__c=true;
            insert unknownTemplate;
            
            
            // Create Maketing Plan - Next Year
            description = 'Marketing Plan is the main Plan for capture marketing Campaigns and Activities for year '+ eNextYear;
            marketingPlanNE = getNewPlan('Marketing Plan '+ eNextYear,marketingPlanRecTypeId,description,'Global',sDateNextYear,eDateNextYear,'Planned');
            insert marketingPlanNE;
            
                                         
            //Campaign Template - Next Year
            description = 'This Plan will store predefined templates for Campaigns and Activities for year '+ eNextYear;
            campaignTemplateNE = getNewPlan('Campaign Templates '+ eNextYear,templatePlanRecTypeId,description,'Global',sDateNextYear,eDateNextYear,'Planned');
            campaignTemplateNE.musqotmp__CampaignTemplate__c = true; 
            insert campaignTemplateNE;
            
            
            //Unknown Template - Next Year
            description = 'In this plan, all campaigns that do not have a plan will be defined. Unknown Campaign Plan is use for show Campaigns in the Gantt view with not has a connection to any Plan.';
            unknownTemplateNE = getNewPlan('Unknown Campaign '+ eNextYear,unknownPlanRecTypeId,description,'Unknown Plan',sDateNextYear,eDateNextYear,'Planned');
            unknownTemplateNE.musqotmp__UnknownCampaigns__c = true; 
            insert unknownTemplateNE;
            
            
            List<musqotmp__MarketingPlanMember__c> planMemberList = new List<musqotmp__MarketingPlanMember__c>();
            List<User> adminUserList = new List<User>();
            if(Schema.sObjectType.User.fields.Name.isAccessible()){
            	adminUserList = [SELECT Id, Name FROM User where Profile.UserLicense.Name  = 'Salesforce'];    
            }
            if(adminUserList.size () > 0){
                if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__MarketingPlan__c.isCreateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__User__c.isCreateable()){
                    for(User userInst: adminUserList){
                        musqotmp__MarketingPlanMember__c planmemberunknownTemplate = new musqotmp__MarketingPlanMember__c();
                        planmemberunknownTemplate.musqotmp__MarketingPlan__c = unknownTemplate.Id;
                        planmemberunknownTemplate.musqotmp__User__c = userInst.Id;
                        planMemberList.add(planmemberunknownTemplate);
                        
                        musqotmp__MarketingPlanMember__c planmembermarketingPlanNE = new musqotmp__MarketingPlanMember__c();
                        planmembermarketingPlanNE.musqotmp__MarketingPlan__c = marketingPlanNE.Id;
                        planmembermarketingPlanNE.musqotmp__User__c = userInst.Id;
                        planMemberList.add(planmembermarketingPlanNE);
                        
                        musqotmp__MarketingPlanMember__c planmember = new musqotmp__MarketingPlanMember__c();
                        planmember.musqotmp__MarketingPlan__c = marketingPlan.Id;
                        planmember.musqotmp__User__c = currentUserId;
                        planMemberList.add(planmember);
                        
                        musqotmp__MarketingPlanMember__c planmemberunknownTemplateNE = new musqotmp__MarketingPlanMember__c();
                        planmemberunknownTemplateNE.musqotmp__MarketingPlan__c = unknownTemplateNE.Id;
                        planmemberunknownTemplateNE.musqotmp__User__c = currentUserId;
                        planMemberList.add(planmemberunknownTemplateNE);
                     }
                     if(planMemberList.size() > 0){
                     	//Database.insert(planMemberList);          
                     }     
            	}
            }
            //method signature getNewGanttFilter(name, dateRange, barText, planType, ganttView, mktActivity, journey)
            //Gantt filter this year
            GanttAll = getNewGanttFilter('Unknown Campaigns Current Year', 'THIS_YEAR', 'Name;OwnerId', 'Unknown Plan', 'Compact', false, false);
            insert GanttAll;
            
            //Gantt filter next year
            GanttAllUn2 = getNewGanttFilter('Unknown Campaigns Next Year', 'NEXT_YEAR', 'Name;OwnerId', 'Unknown Plan', 'Compact', false, false);
            insert GanttAllUn2;
            
            //Gantt filter this year
            GanttAllCY = getNewGanttFilter('Marketing Campaigns Current Year', 'THIS_YEAR', 'Name;OwnerId','Marketing Plan;Global;Local;Other', 'Compact', false, false);
            insert GanttAllCY;
            
            //Gantt filter next year
            GanttAllNY = getNewGanttFilter('Marketing Campaigns Next Year', 'NEXT_YEAR', 'Name;OwnerId', 'Marketing Plan;Global;Local;Other', 'Compact', false, false);
            insert GanttAllNY;
            
            //method signature getWorkspace(name, dateRange, barText, planType, showMe)
            //Campaign Builder Filter All Campaigns
            MyWorkspaceAll = getWorkspace('All Campaigns, Current and Next Quater', 'THIS_QUARTER,NEXT_QUARTER', 'Name;OwnerId', 'Marketing Plan;Global;Local;Other', 'All Campaigns');
            insert MyWorkspaceAll;
            
            //Campaign Builder Filter MY Campaigns
            MyWorkspaceMy = getWorkspace('My Campaigns, Current and Next Quater', 'THIS_QUARTER,NEXT_QUARTER', 'Name;OwnerId', 'Marketing Plan;Global;Local;Other', 'My Campaigns');
            insert MyWorkspaceMy;
            
            //Campaign Optimaizer
            date enddt=system.today();
            enddt = enddt + 366;
            List <musqotmp__CampaignOptimizer__c> COList = new List<musqotmp__CampaignOptimizer__c>();
            List <Period> recordQuater = new List <Period>();
            recordQuater = [SELECT Id,Number,startDate FROM Period where type = 'Quarter' and startDate < :enddt and endDate > Today order by startDate limit 4];
            if(Schema.sObjectType.musqotmp__CampaignOptimizer__c.fields.Name.isCreateable() &&
              Schema.sObjectType.musqotmp__CampaignOptimizer__c.fields.musqotmp__MarketingPlan__c.isCreateable() &&
              Schema.sObjectType.musqotmp__CampaignOptimizer__c.fields.musqotmp__AcquisitionRetention__c.isCreateable() &&
              Schema.sObjectType.musqotmp__CampaignOptimizer__c.fields.musqotmp__MoreDealsBiggerDeals__c.isCreateable() &&
              Schema.sObjectType.musqotmp__CampaignOptimizer__c.fields.musqotmp__RevenueCashflow__c.isCreateable() &&
              Schema.sObjectType.musqotmp__CampaignOptimizer__c.fields.musqotmp__Period__c.isCreateable() &&
              Schema.sObjectType.musqotmp__CampaignOptimizer__c.fields.musqotmp__FiscalYear__c.isCreateable()
              ){
                    for(Period p :recordQuater){
                    integer q  = p.Number ;
                    integer eCYear = p.startDate.year();
                    musqotmp__CampaignOptimizer__c  co1 = new musqotmp__CampaignOptimizer__c(); 
                    If(eCYear == eCurrentYear ){
                        co1.Name= marketingPlan.Name+' Q'+ q ;
                        co1.musqotmp__MarketingPlan__c = marketingPlan.id;
                    } else{
                        co1.Name= marketingPlanNE.Name+' Q'+ q ;
                        co1.musqotmp__MarketingPlan__c = marketingPlanNE.id;  
                    }
                    co1.musqotmp__AcquisitionRetention__c = 50;
                    co1.musqotmp__MoreDealsBiggerDeals__c = 50;
                    co1.musqotmp__RevenueCashflow__c = 50; 
                    co1.musqotmp__Period__c = 'Q'+ q ;
                    co1.musqotmp__FiscalYear__c = String.valueOf(eCYear);
                    COList.Add(co1);
                }
                insert COList;   
            }
        }Catch(Exception ex){
            //ExceptionHandler.logError('Apex','PostInstallDataScript','PostInstallDataScript constructor',ex);
            //System.debug('PostInstallDataScript'+ ex.getMessage());
        } 
    }
    public static Schema.Campaign getCampaign(String campaignName,Date startDate,Date endDate,String status,musqotmp__MarketingPlan__c marketingPlan, String type,String mktArea,String bu,Id recordTypeId,Id parentId){
        if(Schema.sObjectType.Campaign.fields.Name.isCreateable() &&
           Schema.sObjectType.Campaign.fields.RecordTypeId.isCreateable() &&
           Schema.sObjectType.Campaign.fields.ParentId.isCreateable() &&
           Schema.sObjectType.Campaign.fields.StartDate.isCreateable() &&
           Schema.sObjectType.Campaign.fields.EndDate.isCreateable() &&
           Schema.sObjectType.Campaign.fields.Status.isCreateable() &&
           Schema.sObjectType.Campaign.fields.IsActive.isCreateable() &&
           Schema.sObjectType.Campaign.fields.Type.isCreateable() &&
           Schema.sObjectType.Campaign.fields.musqotmp__MarketingPlan__c.isCreateable() &&
           Schema.sObjectType.Campaign.fields.musqotmp__IsFolder__c.isCreateable() &&
           Schema.sObjectType.Campaign.fields.musqotmp__MarketingArea__c.isCreateable() &&
           Schema.sObjectType.Campaign.fields.musqotmp__MarketingBusinessUnit__c.isCreateable()
          ){
            Schema.Campaign campaign = new Schema.Campaign();
            campaign.Name = campaignName;
            campaign.RecordTypeId = recordTypeId;
           	campaign.ParentId = parentId;
            campaign.StartDate = startDate;
            campaign.EndDate = endDate;
            campaign.Status = status;
            campaign.IsActive = true;
            campaign.Type = type;
            campaign.musqotmp__MarketingPlan__c = marketingPlan.Id;
            campaign.musqotmp__IsFolder__c = false;
            campaign.musqotmp__MarketingArea__c = mktArea;
            campaign.musqotmp__MarketingBusinessUnit__c = bu;
            return campaign;
    	}
        return null;
    }
    public static musqotmp__MarketingPlan__c getNewPlan(String name, String recTypeId, String description, String planType, Date sDate, Date eDate, String status){
        if(Schema.sObjectType.musqotmp__MarketingPlan__c.fields.Name.isCreateable() &&
           Schema.sObjectType.musqotmp__MarketingPlan__c.fields.RecordTypeId.isCreateable() &&
           Schema.sObjectType.musqotmp__MarketingPlan__c.fields.musqotmp__Description__c.isCreateable() &&
           Schema.sObjectType.musqotmp__MarketingPlan__c.fields.musqotmp__PlanType__c.isCreateable() &&
           Schema.sObjectType.musqotmp__MarketingPlan__c.fields.musqotmp__StartDate__c.isCreateable() &&
           Schema.sObjectType.musqotmp__MarketingPlan__c.fields.musqotmp__EndDate__c.isCreateable() &&
           Schema.sObjectType.musqotmp__MarketingPlan__c.fields.musqotmp__Status__c.isCreateable()
          ){
            musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
            marketingPlan.Name = name;
            marketingPlan.RecordTypeId = recTypeId;
            marketingPlan.musqotmp__Description__c = description;
            marketingPlan.musqotmp__PlanType__c = planType;
            marketingPlan.musqotmp__StartDate__c = sDate;
            marketingPlan.musqotmp__EndDate__c = eDate;
            marketingPlan.musqotmp__Status__c = status;
            return marketingPlan;
        }
        return null;
    }
    public static musqotmp__Gantt__c getNewGanttFilter(String name, String dateRange, String barText,String planType, String ganttView, Boolean mktActivity, Boolean journey){
    	if(Schema.sObjectType.musqotmp__Gantt__c.fields.Name.isCreateable() &&
           Schema.sObjectType.musqotmp__Gantt__c.fields.musqotmp__DateRange__c.isCreateable() &&
           Schema.sObjectType.musqotmp__Gantt__c.fields.musqotmp__BarText__c.isCreateable() &&
           Schema.sObjectType.musqotmp__Gantt__c.fields.musqotmp__PlanType__c.isCreateable() &&
           Schema.sObjectType.musqotmp__Gantt__c.fields.musqotmp__GanttView__c.isCreateable() &&
           Schema.sObjectType.musqotmp__Gantt__c.fields.musqotmp__IncludeMarketingActivities__c.isCreateable() &&
           Schema.sObjectType.musqotmp__Gantt__c.fields.musqotmp__IncludeMarketingJourney__c.isCreateable()){
                musqotmp__Gantt__c gantt = new musqotmp__Gantt__c();
                gantt.Name = name;
                gantt.musqotmp__DateRange__c = dateRange;
                gantt.musqotmp__BarText__c = barText;
                gantt.musqotmp__PlanType__c = planType; 
                gantt.musqotmp__GanttView__c = ganttView;
                gantt.musqotmp__IncludeMarketingActivities__c = mktActivity;
                gantt.musqotmp__IncludeMarketingJourney__c = journey;
                return gantt;     
           } else {
               return null;
           }   
    }
    public static musqotmp__MyWorkspace__c getWorkspace(String name,String dateRange,String barText, String planType, String showMe){
        if(Schema.sObjectType.musqotmp__MyWorkspace__c.fields.Name.isCreateable() &&
           Schema.sObjectType.musqotmp__MyWorkspace__c.fields.musqotmp__DateRange__c.isCreateable() &&
           Schema.sObjectType.musqotmp__MyWorkspace__c.fields.musqotmp__BarText__c.isCreateable() &&
           Schema.sObjectType.musqotmp__MyWorkspace__c.fields.musqotmp__PlanType__c.isCreateable() &&
           Schema.sObjectType.musqotmp__MyWorkspace__c.fields.musqotmp__ShowMe__c.isCreateable()
          ){
                musqotmp__MyWorkspace__c  workspace = new musqotmp__MyWorkspace__c(); 
                workspace.Name = name;
                workspace.musqotmp__DateRange__c = dateRange;
                workspace.musqotmp__BarText__c = barText;
                workspace.musqotmp__PlanType__c = planType; 
                workspace.musqotmp__ShowMe__c = showMe;
                return workspace;      
          } else {
              return null;
          }
    }*/
}