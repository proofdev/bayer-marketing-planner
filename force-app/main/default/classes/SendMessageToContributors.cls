public with sharing class SendMessageToContributors {
    @AuraEnabled(cacheable=true)
    public static DataModel.Result allContributors(String param, String recordId){ 
        DataModel.Result result = new DataModel.Result();
        DataModel.Track track = new DataModel.Track();
        try{
            track = (DataModel.Track)JSON.deserialize(param, DataModel.Track.class);
            if(String.isEmpty(recordId)){
                recordId = recordId;
            }
            Set<Id> CampaignContributorIds = new Set<Id>();
            if(String.isNotEmpty(recordId)){
                CampaignContributorIds.add((Id) recordId);
            }
            String query = 'SELECT Id, musqotmp__Campaign__c, musqotmp__Email__c, musqotmp__UniqueContributorCampaign__c,musqotmp__User__c,musqotmp__RoleInCampaign__c';
            query += ' FROM musqotmp__CampaignContributor__c';
            query += ' WHERE ';
            query += ' musqotmp__Campaign__c IN :CampaignContributorIds';
            List<musqotmp__CampaignContributor__c> sObjCampaignContributors = new List<musqotmp__CampaignContributor__c>();
            if(Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__Email__c.isAccessible()){
                sObjCampaignContributors = Database.query(String.escapeSingleQuotes(query));
            }
            List<String> sendTo = new List<String>();
            for(musqotmp__CampaignContributor__c sObj : sObjCampaignContributors){
                sendTo.add(sObj.musqotmp__Email__c);
            }
        //    System.debug('sendTo :'+sendTo);
            String userEmail = UserInfo.getUserEmail();
            result.contributorEmails = sendTo;
            result.recordId = recordId;
            DataModel.Toast toast = new DataModel.Toast();
            toast.ok = true;
            result.toast = toast;
        }catch(exception ex){
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            if(track.component != null){
                exceptions.component = track.component;
            }
            result = DataHandler.handleToast(exceptions);
        }
        return result;
    }
    @AuraEnabled
    public static DataModel.Result saveFile(Id idParent, List<String> strFileName, String base64Data) {
        // Decoding base64Data
        DataModel.Result result = new DataModel.Result();
        try{
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            // inserting file
            List<ContentVersion> contentVersions = new List<ContentVersion>();
            List<Id> contentIds = new List<Id>();
            if(strFileName.size() > 0){
                for(String s: strFileName){
                    ContentVersion cv = new ContentVersion();
                    cv.Title = s;
                    cv.PathOnClient = '/' + s;
                    cv.FirstPublishLocationId = idParent;
                    cv.VersionData = EncodingUtil.base64Decode(base64Data);
                    cv.IsMajorVersion = true;
                    contentVersions.add(cv);
                }
            }
            if(Schema.sObjectType.ContentVersion.fields.Title.isCreateable() &&
                Schema.sObjectType.ContentVersion.fields.PathOnClient.isCreateable() &&
                Schema.sObjectType.ContentVersion.fields.FirstPublishLocationId.isCreateable() &&
                Schema.sObjectType.ContentVersion.fields.VersionData.isCreateable() &&
                Schema.sObjectType.ContentVersion.fields.IsMajorVersion.isCreateable()
                ){
                Database.SaveResult[] sObjcontentVersion =  database.insert(contentVersions, false);
                for(Database.SaveResult sr:sObjcontentVersion) {
                    if(sr.isSuccess()){
                        contentIds.add(sr.Id);
                    }
                }   
                result.contentIds = contentIds;
            }
        }catch(exception ex){
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            result = DataHandler.handleToast(exceptions);
        }
        return result;
    }
    @AuraEnabled
    public static DataModel.Result emailMessageToContributors(Id idParent, String campaignInRole,String subject, String body, List<String> contributorEmails, List<Id> contentversionIds){
        DataModel.Result result = new DataModel.Result();
        try{
            //Set email file attachments
            // First, reserve email capacity for the current Apex transaction to ensure
            // that we won't exceed our daily email limits when sending email after
            // the current transaction is committed.
            Messaging.reserveSingleEmailCapacity(2);
            Set<Id> CampaignContributorIds = new Set<Id>();
            String isContributor ;
            if(String.isNotEmpty(idParent)){
                CampaignContributorIds.add((Id) idParent);
            }
            List<musqotmp__CampaignContributor__c> sObjCampaignContributors = new List<musqotmp__CampaignContributor__c>();
            String query = 'SELECT Id, musqotmp__Campaign__c, musqotmp__Email__c, musqotmp__UniqueContributorCampaign__c,musqotmp__User__c,musqotmp__RoleInCampaign__c';
            query += ' FROM musqotmp__CampaignContributor__c';
            query += ' WHERE ';
            query += ' musqotmp__RoleInCampaign__c = :campaignInRole';
            query += ' AND';
            query += ' musqotmp__Campaign__c IN :CampaignContributorIds';
            if(Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__Email__c.isAccessible()){
                sObjCampaignContributors = Database.query(String.escapeSingleQuotes(query));
            }
            List<String> strContributorEmail = new List<String>();
            for(musqotmp__CampaignContributor__c sObj: sObjCampaignContributors){
                strContributorEmail.add(sObj.musqotmp__Email__c);
            }
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            Set<Id> contentDocumentIds = new Set<Id>();
            List<ContentVersion> contentVersion = new List<ContentVersion>();
            if(contentVersionIds.size() > 0 ){
                contentVersion = [select Id,ContentDocumentId 
                                  From ContentVersion 
                                  Where Id IN :contentVersionIds];
            }
            for(ContentVersion cv: contentVersion){
                contentDocumentIds.add(cv.ContentDocumentId);
            }
            List<ContentDocumentLink> sObjcontentDocumentLink =  new List<ContentDocumentLink>();
            if(contentDocumentIds.size() > 0){
                sObjcontentDocumentLink = [SELECT id,ContentDocument.Title,ContentDocument.FileType,ContentDocument.FileExtension,
                                           ContentDocument.LatestPublishedVersionId,ContentDocument.LatestPublishedVersion.VersionData
                                           FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocumentIds AND LinkedEntityId =:idParent];
            }
            for (ContentDocumentLink sObj : sObjcontentDocumentLink){
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(sObj.ContentDocument.Title+'.'+sObj.ContentDocument.FileExtension);
                String blobValue = EncodingUtil.base64Encode(sObj.ContentDocument.LatestPublishedVersion.VersionData);
                efa.setBody(EncodingUtil.base64Decode(blobValue));
                fileAttachments.add(efa);
            }
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(strContributorEmail.size() >0){
                mail.setToAddresses(strContributorEmail);
            }else{
                if(campaignInRole == '---All---'){
                    mail.setToAddresses(contributorEmails);
                }else{
                    isContributor = 'There is no contributor on selected role :' +campaignInRole;
                    result.isContributor = isContributor;
                    return result;
                }
            }
            //mail.setBccAddresses(bcc);
            mail.setSubject(subject);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setFileAttachments(fileAttachments);
            mail.setHtmlBody(body);
        //    System.debug('mail :' +mail);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            //Added by Alka - start
            Id campaignId = Id.valueOf(idParent); 
            EmailMessage emailMsg = new EmailMessage(); // Created a EmailMessage and copy all details from above.
            String toAddressList = string.join(mail.getToAddresses(),',');
            emailMsg.ToAddress = toAddressList;
            emailMsg.FromAddress = UserInfo.getUserEmail();
            emailMsg.FromName = UserInfo.getName();
            emailMsg.Subject=mail.getSubject();
            emailMsg.HtmlBody=mail.getHtmlBody();
            emailMsg.MessageDate = system.now();
            emailMsg.Status = '3';
            emailMsg.RelatedToId = campaignId;
            //emailMsg.HasAttachment = true;
            if(Schema.sObjectType.EmailMessage.fields.ToAddress.isCreateable() &&
                Schema.sObjectType.EmailMessage.fields.FromAddress.isCreateable() &&
                Schema.sObjectType.EmailMessage.fields.FromName.isCreateable() &&
                Schema.sObjectType.EmailMessage.fields.Subject.isCreateable() &&
                Schema.sObjectType.EmailMessage.fields.HtmlBody.isCreateable() &&
                Schema.sObjectType.EmailMessage.fields.MessageDate.isCreateable() &&
                Schema.sObjectType.EmailMessage.fields.Status.isCreateable() &&
                Schema.sObjectType.EmailMessage.fields.RelatedToId.isCreateable()
                ){
                insert emailMsg;
            }
         //   System.debug('contentDocumentIds**'+contentDocumentIds);
            if(contentDocumentIds.size()>0){
                List<ContentDocumentLink> documentLinkList = new List<ContentDocumentLink>();
                for(Id contentDocumentId : contentDocumentIds){
                    ContentDocumentLink documentLink = new ContentDocumentLink();
                    documentLink.ContentDocumentId = contentDocumentId;
                    documentLink.LinkedEntityId = emailMsg.Id;
                    documentLink.ShareType = 'V';
                    documentLink.Visibility = 'AllUsers';
                    documentLinkList.add(documentLink);
                }
                if(Schema.sObjectType.ContentDocumentLink.fields.ContentDocumentId.isCreateable() &&
                    Schema.sObjectType.ContentDocumentLink.fields.LinkedEntityId.isCreateable() &&
                    Schema.sObjectType.ContentDocumentLink.fields.ShareType.isCreateable() &&
                    Schema.sObjectType.ContentDocumentLink.fields.Visibility.isCreateable()
                    ){
                    insert documentLinkList;
                }
            }
        //    System.debug('emailMsg***'+emailMsg);
            //Added by Alka - end
        }Catch(Exception ex){
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            result = DataHandler.handleToast(exceptions);
        }
        return result;
    }
    public static DataModel.Result DynamicObjectInfo(String param, String recordId) {
        DataModel.Result result = new DataModel.Result();
        DataModel.Track track = new DataModel.Track();
        try{
            track = (DataModel.Track)JSON.deserialize(param, DataModel.Track.class);
            String objectName = 'musqotmp__CampaignContributor__c';
            // this can accept list of strings, we describe only one object here
            List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{objectName});
            result.objectTypeLabel = describeSobjectsResult[0].getLabel();
            result.objectApiName = describeSobjectsResult[0].getName();
            result.recordId = recordId;
            DataModel.Toast toast = new DataModel.Toast();
            toast.ok = true;
            result.toast = toast;
        }Catch(Exception ex){
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            if(track.component != null){
                exceptions.component = track.component;
            }
            result = DataHandler.handleToast(exceptions);
        }
        return result;
    }
}