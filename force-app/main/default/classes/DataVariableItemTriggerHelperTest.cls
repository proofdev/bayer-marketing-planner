@isTest(SeeAllData=false)
public class DataVariableItemTriggerHelperTest {
@isTest
    static void DataVariableItemTriggerHelperTestMethod(){
        try{
            User adminUser = TestDataSetup.getUser('System Administrator');
        	insert adminUser;
            TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
            System.runAs(adminUser){
                Date TodayDate = Date.Today();
                musqotmp__DataVariable__c dataVar = TestDataSetup.getDataVariableRecord('Facebook','#642F88','Daily','Spend Amount');
                insert dataVar;            
                DataVariableItem__c datavariable = TestDataSetup.getDataVariableItemRecord(dataVar.Id,'Test112234',TodayDate);
                insert datavariable;
                TodayDate = Date.newInstance(2020, 12, 31);
                DataVariableItem__c datavariable1 = TestDataSetup.getDataVariableItemRecord(dataVar.Id,'Test112235',TodayDate);
                insert datavariable1;
                TodayDate =Date.newInstance(2020, 06, 11);
                DataVariableItem__c datavariable2 = TestDataSetup.getDataVariableItemRecord(dataVar.Id,'Test112236',TodayDate);
                insert datavariable2;
                TodayDate = Date.newInstance(2016, 01, 01);
                DataVariableItem__c datavariable3 = TestDataSetup.getDataVariableItemRecord(dataVar.Id,'Test112233',TodayDate);
                insert datavariable3;
                Test.startTest();
                    datavariable.musqotmp__Date__c = TodayDate +1;
                    update datavariable;
                	Integer weekDay = DataVariableItemTriggerHelper.getWeekFromDate(Date.newInstance(2021, 01, 01));
                	Integer weekDay1 = DataVariableItemTriggerHelper.getWeekFromDate(Date.newInstance(2021, 03, 01));
                	String quarter = DataVariableItemTriggerHelper.getQuarterFromDate(Date.newInstance(2021, 01, 01));
                	String quarter1 = DataVariableItemTriggerHelper.getQuarterFromDate(Date.newInstance(2021, 04, 01));
                	String quarter2 = DataVariableItemTriggerHelper.getQuarterFromDate(Date.newInstance(2021, 07, 01));
                	String quarter3 = DataVariableItemTriggerHelper.getQuarterFromDate(Date.newInstance(2021, 10, 01));
                Test.stopTest();
            }
        }catch(Exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read datavariable.musqotmp__Date__c');
    }
}