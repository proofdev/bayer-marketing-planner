/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)                Description
* -----------------------------------------------------------------------------------------------------------------------------------
* Alka              10/23/2019          Common class to create test data to be used in Test Classes                                                                                                              
************************************************************************************************************************************/
@isTest
@namespaceAccessible
public with sharing class TestDataSetup {
    public static Date sDate = Date.Today();
    public static Date eDate = Date.newInstance(sDate.year(), 12, 31);//year, month, day
    
    @namespaceAccessible
    public static void createCampaignManagerSetup(){
    	musqotmp__CampaignManagerSetup__c campaignmanager  = new musqotmp__CampaignManagerSetup__c();
        String orgId = UserInfo.getOrganizationId();
        campaignmanager.Name=orgId;
        campaignmanager.musqotmp__UseSalesCloud__c = true;
        campaignmanager.musqotmp__UseMarketingCloud__c = false;
        campaignmanager.musqotmp__UseBudget__c = true;
        campaignmanager.musqotmp__UseCampaignCost__c = true;
        campaignmanager.musqotmp__UseQuipEnterprize__c = False;
        campaignmanager.musqotmp__GanttProgressType__c = 1;
        campaignmanager.musqotmp__GanttProgressTypeFolder__c = 1;
        campaignmanager.musqotmp__BarColorCampaign__c = '#0E78B4,#125E62,#484848,#118ED7,#0F7980,#23A7EC,#13969F,#7F7F7F,#44BEF8,#1CAFB9';
        campaignmanager.musqotmp__BarColorMarketingActivity__c = '#642F88,#FC8527,#BD005F,#72498F,#FC9B48,#D11174,#875EA3,#FDB16C,#E32987,#A68BB9';
        campaignmanager.musqotmp__BarColorMarketingJourney__c = '#A1004D';
        //campaignmanager.musqotmp__BarColorMarketingCloudActivity__c = '#FC6D0D,#642F88,#FC8527,#BD005F,#72498F,#FC9B48,#D11174,#875EA3,#FDB16C,#E32987';
        campaignmanager.musqotmp__BarColorMarketingPlan__c = '#DC5C07,#86003C,#551E7C,#0B5E8D,#094D51,#323232,#999999';
        insert campaignmanager;    
    }
    
    @namespaceAccessible
    public static User getUser(String profileName){
        //username global uniqueness is still enforced in tests 
        //make sure we get something unique to avoid issues with parallel tests
        String uniqueness = DateTime.now()+':'+Math.random();
        Profile p = [SELECT id, Name FROM Profile WHERE Name = :profileName];
        User result = new User(
            username=UserInfo.getUserId()+'.'+uniqueness.HashCode()+'@'+UserInfo.getOrganizationId()+'.sfdcOrg',
            alias = 'testExec',
            email='apextests@example.com',
            emailencodingkey='UTF-8',
            lastname='Testing',
            languagelocalekey='en_US',
            localesidkey='en_US',
            profileid = p.Id,
            timezonesidkey='America/Los_Angeles'
        );
        return result;
    }
    @namespaceAccessible
    public static void setPermissionSet(String userId,String permissionSetName){
    	PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = :permissionSetName];
		insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = ps.Id);   
    }
        
    @namespaceAccessible
    public static musqotmp__MarketingPlan__c getMarketingPlan(String planName,Date startDate, Date endDate, String status, String planType){
        Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
        musqotmp__MarketingPlan__c  marketingPlan=new musqotmp__MarketingPlan__c();
        marketingPlan.RecordTypeId  = recordTypeId;
        marketingPlan.Name = planName; 
        marketingPlan.musqotmp__PlanType__c = planType;
        marketingPlan.musqotmp__StartDate__c = startDate;
        marketingPlan.musqotmp__EndDate__c = endDate;
        marketingPlan.musqotmp__Status__c = status;
        return marketingPlan;
    }
    @namespaceAccessible
    public static Schema.Campaign getCampaign(String campaignName,Date startDate,Date endDate,String status,musqotmp__MarketingPlan__c marketingPlan){
        Id recordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Type').getRecordTypeId();
        Schema.Campaign campaign = new Schema.Campaign();
        campaign.Name = campaignName;
        campaign.musqotmp__MarketingPlan__c = marketingPlan.Id;
        campaign.StartDate = startDate;
        campaign.EndDate = endDate;
        campaign.Status = status;
        campaign.IsActive = true;
        campaign.Type = 'Other';
        campaign.musqotmp__IsFolder__c = false;
        campaign.RecordTypeId = recordTypeId;
        return campaign;
    }
    @namespaceAccessible
    public static musqotmp__CampaignContributor__c getCampaignContributor(Id campaignId, Id userId, String role, Double workingTime){
        musqotmp__CampaignContributor__c campaignContributor = new musqotmp__CampaignContributor__c();
        campaignContributor.musqotmp__Campaign__c = campaignId;
        campaignContributor.musqotmp__User__c = userId;
        campaignContributor.musqotmp__RoleInCampaign__c  = role;
        campaignContributor.musqotmp__WorkingTime__c = workingTime;
        return campaignContributor;
    }
    @namespaceAccessible
    public static Task getTask(Schema.Campaign campaignObj, User userObj, String status, Date activityDate){
        Task tskRec = new Task();
        tskRec.WhatId = campaignObj.Id;
        tskRec.OwnerId = userObj.Id;
        tskRec.Status = status;
        tskRec.Description = 'Test Task ' + Date.Today();
        tskRec.ActivityDate = activityDate;
        tskRec.Subject = 'Test Subject ' + Date.Today();
        return tskRec;
    }
    @namespaceAccessible
    public static musqotmp__CampaignTemplate__c getCampaignTemplate(Id parentCampaignId){
        musqotmp__CampaignTemplate__c newTempalte = new musqotmp__CampaignTemplate__c();
        newTempalte.name = 'Test Template';
        newTempalte.musqotmp__Campaign__c = parentCampaignId;
        return newTempalte;
    }
    @namespaceAccessible
    public static musqotmp__MarketingJourney__c getNewJourneyInstance(String status,String type,Date startDate, Date endDate,String campaignId,String audience){
        musqotmp__MarketingJourney__c newJourney = new musqotmp__MarketingJourney__c();
        newJourney.Name = 'Test Journey';
        newJourney.musqotmp__Status__c = status;
        newJourney.musqotmp__Type__c = type;
        newJourney.musqotmp__StartDate__c = startDate;
        newJourney.musqotmp__EndDate__c = endDate;
        newJourney.musqotmp__Campaign__c = campaignId;
        newJourney.musqotmp__Audience__c = audience;
        return newJourney;
    }
    @namespaceAccessible
    public static musqotmp__MarketingActivity__c getNewMarketingActivity(String status,String type,Date startDate, Date endDate,String campaignId,String audience){
        musqotmp__MarketingActivity__c newMarketingActivity = new musqotmp__MarketingActivity__c();
        newMarketingActivity.Name = 'Test Activity';
        newMarketingActivity.musqotmp__Status__c = status;
        newMarketingActivity.musqotmp__Type__c = type;
        newMarketingActivity.musqotmp__StartDate__c = startDate;
        newMarketingActivity.musqotmp__EndDate__c = endDate;
        newMarketingActivity.musqotmp__Campaign__c = campaignId;
        newMarketingActivity.musqotmp__Audience__c = audience;
        return newMarketingActivity;
    }
    @namespaceAccessible
    public static musqotmp__DataVariable__c getDataVariableRecord(String name,String barColor,String dataType,String dataVarType){
        musqotmp__DataVariable__c dataVarObj = new musqotmp__DataVariable__c();
        dataVarObj.name = name;
        dataVarObj.musqotmp__Active__c = true;
        dataVarObj.musqotmp__BarColor__c = barColor;
        dataVarObj.musqotmp__DataType__c  = dataType;
        dataVarObj.musqotmp__DataVariableType__c = dataVarType;
        Integer len = 5;
		String str = string.valueof(Math.abs(Crypto.getRandomLong()));
		String randomNumber = str.substring(0, len);
        dataVarObj.musqotmp__ExternalId__c = randomNumber;
        dataVarObj.musqotmp__LowestFrequencyUse__c = 'Day';
        dataVarObj.musqotmp__Unit__c = '$ Currency';
        return dataVarObj;
    }
    @namespaceAccessible
    public static DataVariableItem__c getDataVariableItemRecord(Id dataVariableId,String externalId,Date dateToSet){
        DataVariableItem__c dataVarItemObj = new DataVariableItem__c();
        dataVarItemObj.musqotmp__ExternalItemId__c = externalId;
        dataVarItemObj.musqotmp__DataVariable__c = dataVariableId;
        dataVarItemObj.musqotmp__Date__c = dateToSet;
        return dataVarItemObj;
    }
    @namespaceAccessible
    public static musqotmp__GanttPerformance__c getGanttFilterRecord(String Name,Id dataVariableId){
        musqotmp__GanttPerformance__c ganttPerformance = new musqotmp__GanttPerformance__c();
        ganttPerformance.Name = name;
        ganttPerformance.musqotmp__DependentData__c  = dataVariableId;
        //ganttPerformance.musqotmp__BarColorMarketingActivity__c = barColor;
        return ganttPerformance;
    }
    @namespaceAccessible
    public static GanttPerformance__c getPerformanceGanttRecord(String Name, String dataVariableId, String dateRange){
        GanttPerformance__c ganttPerformanceObj = new GanttPerformance__c();  
        ganttPerformanceObj.Name = Name;
        ganttPerformanceObj.musqotmp__DependentData__c = dataVariableId;
        ganttPerformanceObj.musqotmp__DateRange__c = dateRange;
        ganttPerformanceObj.musqotmp__BusinessUnit__c = 'Division A';
        ganttPerformanceObj.musqotmp__IncludeJourney__c  = true;
        ganttPerformanceObj.musqotmp__Region__c   = 'Europe';
        ganttPerformanceObj.musqotmp__Country__c   = 'Sweden';
        ganttPerformanceObj.musqotmp__Audience__c   = 'B2B';
        ganttPerformanceObj.musqotmp__ActivityType__c   = 'Banner';
        return ganttPerformanceObj;
    }
    @namespaceAccessible
    public static DataVariableActivityTypeMap__c getDataVarActivityTypeMapRecord(Id dataVariableId,String campaignType){
        DataVariableActivityTypeMap__c dataVarMapObj = new DataVariableActivityTypeMap__c();
        dataVarMapObj.musqotmp__DataVariable__c = dataVariableId;
        dataVarMapObj.musqotmp__CampaignType__c = campaignType;
        return dataVarMapObj;
    }
    @namespaceAccessible
    public static KeyInsightDashboard__c getKeyInsightDashboardRecord(String name,String frequency,String userId,String proofUserId,Integer kid){
        KeyInsightDashboard__c newKidObj = new KeyInsightDashboard__c();
        newKidObj.Name = name;
        newKidObj.musqotmp__CreatedOn__c = Date.Today();
        newKidObj.musqotmp__Frequency__c = frequency;
        newKidObj.musqotmp__KIDUser__c = userId;
        newKidObj.musqotmp__ProofUserId__c = proofUserId;
        newKidObj.musqotmp__Status__c = 'Public';
        newKidObj.musqotmp__isAccessible__c = true;
        newKidObj.musqotmp__keyInsightDashboardId__c = kid;
        return newKidObj;
    }
}