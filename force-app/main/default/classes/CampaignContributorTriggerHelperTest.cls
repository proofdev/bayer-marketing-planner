/************************************************************************************************************************************
* Developer         Date                Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              07/05/2020          Created for W-000307 - test class for CampaignContributorTrigger and CampaignContributorTriggerHelper
**************************************************************************************************************************************/
@IsTest(seeAllData = false)
public class CampaignContributorTriggerHelperTest {
	@isTest
    static void workingTimeAfterInsertUpdateDeleteTest() {
        User userRecord = TestDataSetup.getUser('System Administrator');
        insert userRecord;
        User adminUser = TestDataSetup.getUser('System Administrator');
        insert adminUser;
        TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
        System.runAs(adminUser){
            musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
            insert marketingPlan;
            Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division A',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
            insert campaignRecord;
            
            Test.startTest();
            musqotmp__CampaignContributor__c campaignContributor = TestDataSetup.getCampaignContributor(campaignRecord.id,userRecord.id,'Contributor',15);
            insert campaignContributor;
            
            //Schema.Campaign campaignRecordUpdated = [select id,musqotmp__WorkinTime__c from Campaign where id =:campaignRecord.id];
            //System.assertEquals(15, campaignRecordUpdated.musqotmp__WorkinTime__c);
            
            User userRecord1 = TestDataSetup.getUser('System Administrator');
            insert userRecord1;
            musqotmp__CampaignContributor__c campaignContributor1 = TestDataSetup.getCampaignContributor(campaignRecord.id,userRecord1.id,'Contributor',20);
            insert campaignContributor1;
            System.assertEquals(20, campaignContributor1.musqotmp__WorkingTime__c);
            //campaignRecordUpdated = [select id,musqotmp__WorkinTime__c from Campaign where id =:campaignRecord.id];
            //System.assertEquals(35, campaignRecordUpdated.musqotmp__WorkinTime__c);
            
            campaignContributor1.musqotmp__WorkingTime__c = 25;
            update campaignContributor1;
            
            delete campaignContributor;
            delete campaignContributor1;
            musqotmp__CampaignContributor__c campaignContributor2 = TestDataSetup.getCampaignContributor(campaignRecord.id,userRecord.id,'Contributor',NULL);
            insert campaignContributor2;
            campaignContributor2.musqotmp__WorkingTime__c = 5;
            update campaignContributor2;
            Test.stopTest();
        }
    }
}