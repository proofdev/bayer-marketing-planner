@namespaceAccessible
public virtual class StringBuilder {
    protected List<String> buffer = new List<String>();
    
    /**
* Construct an empty StringBuilder
**/
    @namespaceAccessible
    public StringBuilder() {}
    
    /**
* Construct a StringBuilder with the given values
**/
    @namespaceAccessible
    public StringBuilder(List<String> values)
    {
        add(values);
    }
    
    /**
* Add the given values to the StringBuilder
**/
    @namespaceAccessible
    public virtual void add(List<String> values)
    {
        buffer.addAll(values);
    }
    
    /**
* Add the given value to the StringBuilder
**/
    @namespaceAccessible
    public virtual void add(String value)
    {
        buffer.add(value);
    }
    @namespaceAccessible
    public virtual override String toString()
    {
        return String.join(buffer, '');
    }
    
    /**
* Return the state of the StringBuilder
**/
    @namespaceAccessible
    public virtual String getStringValue()
    {
        return toString();
    }
    
    /**
* Subclasses the StringBuilder to produce a comma delimited contactination of strings
**/
    @namespaceAccessible
    public virtual with sharing class CommaDelimitedListBuilder extends StringBuilder
    {
        String itemPrefix = '';
        String delimiter = ',';
        @namespaceAccessible
        public CommaDelimitedListBuilder() {}
        @namespaceAccessible
        public CommaDelimitedListBuilder(List<String> values)
        {
            super(values);
        }
        @namespaceAccessible
        public void setItemPrefix(String itemPrefix)
        {
            this.itemPrefix = itemPrefix;
        }
        @namespaceAccessible
        public void setDelimiter(String delimiter)
        {
            this.delimiter = delimiter;
        }
        @namespaceAccessible
        public String getStringValue(String itemPrefix)
        {
            setItemPrefix(itemPrefix);
            return toString();
        }
        @namespaceAccessible
        public override String toString()
        {
            return itemPrefix + String.join(buffer, delimiter + itemPrefix);
        }
    }
    
    /** 
* Subclasses the StringCommaDelimitedBuilder to accept native SObjectField tokens and optional FieldSet definitions to concatinate when building queries
**/
    @namespaceAccessible
    public virtual with sharing class FieldListBuilder extends CommaDelimitedListBuilder
    {
        @namespaceAccessible
        public FieldListBuilder(List<Schema.SObjectField> values)
        {
            this(values, null);
        }
        @namespaceAccessible
        public FieldListBuilder(List<Schema.SObjectField> values, List<Schema.Fieldset> fieldSets)
        {
            // Create a distinct set of fields (or field paths) to select
            for(Schema.SObjectField value : values)
                add(String.valueOf(value)); // Alternative to value.getDescribe().getName()
            
            if(fieldSets!=null)
                for(Schema.Fieldset fieldSet : fieldSets)
                for(Schema.FieldSetMember fieldSetMember : fieldSet.getFields())
                add(fieldSetMember.getFieldPath());
        }
    }
    
    /**
* Subclasses the FieldListBuilder to auto sense and include when needed the CurrencyIsoCode field in the field list
**/
    @namespaceAccessible
    public with sharing class MultiCurrencyFieldListBuilder extends FieldListBuilder
    {
        @namespaceAccessible
        public MultiCurrencyFieldListBuilder(List<Schema.SObjectField> values)
        {
            this(values, null);
        }
        @namespaceAccessible
        public MultiCurrencyFieldListBuilder(List<Schema.SObjectField> values, List<Schema.FieldSet> fieldSets)
        {
            super(values, fieldSets);
            
            // Dynamically add CurrencyIsoCode field for mult-currency organisations						
            if(Userinfo.isMultiCurrencyOrganization())
                add('CurrencyIsoCode');
        }
    }
}