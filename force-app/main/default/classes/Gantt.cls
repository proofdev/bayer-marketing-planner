public with sharing class Gantt {
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getMarketingPlanView(String param, String recordId) {
        DataModel.Result result = DataHandler.getMarketingPlanView(param, recordId);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getMyWorkspace(String param, String recordId) {
        DataModel.Result result = DataHandler.getMyWorkspace(param, recordId);
        return result;
    }
    @AuraEnabled
    public static DataModel.Result cloneCampaignTemplate(String param, String recordId) {
        DataModel.Result result = DataHandler.cloneCampaignTemplate(param, recordId);
        return result;
    }

    @AuraEnabled
    public static DataModel.Result deepCloneCampaignTemplate(String param, String recordId) {
        DataModel.Result result = DataHandler.deepCloneCampaignTemplate(param, recordId);
        return result;
    }
    //Sowmya Nov 25 
     @AuraEnabled(cacheable=true)
    public static List<SObJectResult> getResults(String ObjectName, String fieldName, String value, String plan) {
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        if(ObjectName == 'musqotmp__MarketingPlan__c' && Schema.sObjectType.musqotmp__MarketingPlan__c.fields.Id.isAccessible()){
        if(String.isNotEmpty(value))
              for(sObject so : Database.Query('Select Id,'+String.escapeSingleQuotes(fieldName)+' FROM musqotmp__MarketingPlan__c WHERE musqotmp__EndDate__c >TODAY AND musqotmp__CampaignTemplate__c=false AND musqotmp__UnknownCampaigns__c = false AND ' +String.escapeSingleQuotes(fieldName)+' LIKE \'%' + String.escapeSingleQuotes(value) + '%\'')) {
                String fieldvalue = (String)so.get(fieldName);
                sObjectResultList.add(new SObjectResult(fieldvalue, so.Id));
            }
        }
        if(ObjectName == 'campaign' && Schema.sObjectType.Campaign.fields.Name.isAccessible()){
            string soqlLikeStr ='';
        if(String.isNotEmpty(value))
            soqlLikeStr = '%'+String.escapeSingleQuotes(value)+'%';
            plan = String.escapeSingleQuotes(plan);
            list<sObject> obj=[Select Id,Name FROM Campaign WHERE musqotmp__MarketingPlan__c = : Plan AND Name LIKE : soqlLikeStr];
            for(sObject so : obj){//Database.Query('Select Id,'+fieldName+' FROM campaign WHERE musqotmp__MarketingPlan__c IN :Plan AND ' + fieldName+' LIKE \'%' + value + '%\'')) {
                String fieldvalue = (String)so.get(fieldName);
                sObjectResultList.add(new SObjectResult(fieldvalue, so.Id));
            }
        }
        return sObjectResultList;
    }
    
    public class SObJectResult {
        @AuraEnabled
        public String recName;
        @AuraEnabled
        public Id recId;
        
        public SObJectResult(String recNameTemp, Id recIdTemp) {
            recName = recNameTemp;
            recId = recIdTemp;
        }
    }
}