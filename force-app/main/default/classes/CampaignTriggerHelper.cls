/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              06/05/2020          Created for W-000339,W-000359 (helper class for JourneyTrigger)
* Alka              07/15/2020          Update for W-000390 to created Forecast when Allocation object is not available
**************************************************************************************************************************************/
public with sharing class CampaignTriggerHelper {
	//Added for W-000339
    public static void processDataVarForecastsAfterUpdate(List<Schema.Campaign> triggerNew, Map<Id,Schema.Campaign> triggerOldMap){
        Set<Id> campaignIdSet = new Set<Id>();
        Id campaignRecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Type').getRecordTypeId();
        /* try{
       	for(Schema.Campaign campaignInst: triggerNew){
                if(campaignInst.recordTypeId == campaignRecordTypeId){
                	if(campaignInst.Type != triggerOldMap.get(campaignInst.Id).Type ||
                       campaignInst.StartDate != triggerOldMap.get(campaignInst.Id).StartDate ||
                       campaignInst.EndDate != triggerOldMap.get(campaignInst.Id).EndDate ||
                       campaignInst.musqotmp__Audience__c != triggerOldMap.get(campaignInst.Id).musqotmp__Audience__c ||
                       campaignInst.musqotmp__MarketingBusinessUnit__c != triggerOldMap.get(campaignInst.Id).musqotmp__MarketingBusinessUnit__c ||
                       campaignInst.musqotmp__MarketingArea__c   != triggerOldMap.get(campaignInst.Id).musqotmp__MarketingArea__c  ||
                       campaignInst.musqotmp__Countries__c  != triggerOldMap.get(campaignInst.Id).musqotmp__Countries__c ||
                       campaignInst.musqotmp__City__c  != triggerOldMap.get(campaignInst.Id).musqotmp__City__c ||
                       campaignInst.musqotmp__Shop__c  != triggerOldMap.get(campaignInst.Id).musqotmp__Shop__c ||
                       campaignInst.musqotmp__ProductGroup__c  != triggerOldMap.get(campaignInst.Id).musqotmp__ProductGroup__c 
                      ){
                    	campaignIdSet.add(campaignInst.Id);
                	}    
                }
            }
            String allocationSQL = 'SELECT Id';
            allocationSQL += ' FROM musqotmp__Allocation__c';
            allocationSQL += ' WHERE musqotmp__MarketingActivity__c = NULL AND';
            allocationSQL += ' musqotmp__Journey__c = NULL AND';
            allocationSQL += ' musqotmp__BeneficiaryCampaign__c in :campaignIdSet';*/
          //  System.debug('allocationSQL***'+allocationSQL);
          //Sowmya Commented for code coverage
             /*if(campaignIdSet.size () > 0 &&
              DataVariableJobForecastUtility.isObjectExist('musqotmp__Allocation__c') && 
               Schema.sObjectType.Campaign.fields.Name.isAccessible()){
                List<Id> allocationIdList = new List<Id>();
                for(Sobject allocation: Database.query(allocationSQL)){
                    allocationIdList.add(allocation.Id);
                }
                if(allocationIdList.size() > 0){
                    //DataVariableJobForecastUtility.publishAllocationForecastEvent(allocationIdList,true);
                }
            }  
        } Catch(Exception e){ 
           // ExceptionHandler.logError('Class','MarketingActivityTriggerHelper','processDataVarForecastsAfterUpdate',e);
        }*/  
    }
    //Added for W-000390
    public static void createCampaignForecast(List<Schema.Campaign> triggerNew){
        List<Id> campaignIdList = new List<Id>();
        List<Schema.Campaign> camapignList = new List<Schema.Campaign>();
        Set<String> campaignTypeSet = new Set<String>();
        Id campaignRecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Type').getRecordTypeId();
        /*try{
        	for(Integer i=0; i < triggerNew.size(); i++){
                if(triggerNew[i].recordTypeId == campaignRecordTypeId &&
                   triggerNew[i].StartDate != null &&
                   triggerNew[i].EndDate != null &&
                   (triggerNew[i].BudgetedCost != null || triggerNew[i].ActualCost != null)
                  ){
                      campaignIdList.add(triggerNew[i].Id);
                      camapignList.add(triggerNew[i]);
                      campaignTypeSet.add(triggerNew[i].Type);
                  }
            }
            //if(campaignIdList.size() > 0)
            	//DataVariableJobForecastUtility.createDataVariableCampaignForecasts(camapignList,campaignIdList, campaignTypeSet,'Insert');  
        } Catch(Exception e){
          //  ExceptionHandler.logError('Class','MarketingActivityTriggerHelper','createCampaignForecast',e);
        }*/
    }
    //Added for W-000390
    public static void updateCampaignForecast(List<Schema.Campaign> triggerNew, Map<Id,Schema.Campaign> triggerOldMap){
    	List<Id> campaignIdList = new List<Id>();
        List<Schema.Campaign> camapignList = new List<Schema.Campaign>();
        Set<String> campaignTypeSet = new Set<String>();
        Id campaignRecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Type').getRecordTypeId();
       /* try{
        	for(Integer i=0; i < triggerNew.size(); i++){
                if(triggerNew[i].recordTypeId == campaignRecordTypeId){
                	if(triggerNew[i].Type != triggerOldMap.get(triggerNew[i].Id).Type ||
                        triggerNew[i].StartDate != triggerOldMap.get(triggerNew[i].Id).StartDate ||
                       triggerNew[i].EndDate != triggerOldMap.get(triggerNew[i].Id).EndDate ||
                       triggerNew[i].musqotmp__Audience__c != triggerOldMap.get(triggerNew[i].Id).musqotmp__Audience__c ||
                       triggerNew[i].musqotmp__MarketingBusinessUnit__c != triggerOldMap.get(triggerNew[i].Id).musqotmp__MarketingBusinessUnit__c ||
                       triggerNew[i].musqotmp__MarketingArea__c   != triggerOldMap.get(triggerNew[i].Id).musqotmp__MarketingArea__c  ||
                       triggerNew[i].musqotmp__Countries__c  != triggerOldMap.get(triggerNew[i].Id).musqotmp__Countries__c ||
                       triggerNew[i].musqotmp__City__c  != triggerOldMap.get(triggerNew[i].Id).musqotmp__City__c ||
                       triggerNew[i].musqotmp__Shop__c  != triggerOldMap.get(triggerNew[i].Id).musqotmp__Shop__c ||
                       triggerNew[i].musqotmp__ProductGroup__c  != triggerOldMap.get(triggerNew[i].Id).musqotmp__ProductGroup__c ||
                       triggerNew[i].BudgetedCost != triggerOldMap.get(triggerNew[i].Id).BudgetedCost ||
                       triggerNew[i].ActualCost != triggerOldMap.get(triggerNew[i].Id).ActualCost
                      ){
                    	campaignIdList.add(triggerNew[i].Id);
                        camapignList.add(triggerNew[i]);
                        campaignTypeSet.add(triggerNew[i].Type);
                	}    
                }
            }
            if(camapignList.size() > 0){
                //DataVariableJobForecastUtility.createDataVariableCampaignForecasts(camapignList,campaignIdList,campaignTypeSet,'Update');
            }   
        } Catch(Exception e){
          //  ExceptionHandler.logError('Class','MarketingActivityTriggerHelper','updateCampaignForecast',e);
        }   */
    }
}