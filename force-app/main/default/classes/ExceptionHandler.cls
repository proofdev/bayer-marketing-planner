/************************************************************************************************************************************
* Developer      Date            Description
* ---------------------------------------------------------------------------------------------------------------------------------------                                                                                         campaigncontributorrelatedlist 
* Alka           12/04/2019      Exception logging class with method logError and Parameters:
                                 compType: component type in which exception occured like Trigger/Class etc.
                                 compName: component name which was source of exception like Class/Trigger/Controller name etc.
                                 methodName: method name in which Exception occured
                                 execptionObj: excpetion object instance
**************************************************************************************************************************************/
@namespaceAccessible
public with sharing class ExceptionHandler
{
    @namespaceAccessible
    public static void logError(String compType, String compName, String methodName, Exception execptionObj){
        if(Schema.sObjectType.MusqotLog__c.fields.ComponentType__c.isCreateable() &&
        Schema.sObjectType.MusqotLog__c.fields.ComponentName__c.isCreateable() &&
        Schema.sObjectType.MusqotLog__c.fields.LineNumber__c.isCreateable() &&
        Schema.sObjectType.MusqotLog__c.fields.MethodName__c.isCreateable() &&
        Schema.sObjectType.MusqotLog__c.fields.ErrorType__c.isCreateable() &&
        Schema.sObjectType.MusqotLog__c.fields.ErrorDescription__c.isCreateable()){
            MusqotLog__c log = new MusqotLog__c();
            log.ComponentType__c = compType;
            log.ComponentName__c = compName;
            log.MethodName__c = methodName;
            log.ErrorType__c  = execptionObj.getTypeName();
            log.ErrorDescription__c = execptionObj.getMessage();
            log.LineNumber__c = execptionObj.getLineNumber();
            Database.insert(log);	
        }	
    }
}