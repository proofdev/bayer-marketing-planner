public class SObjectService implements ISObjectService {
    /**
     * Alternative to the Records property, provided to support mocking of Domain classes
     **/
    public List<SObject> getRecords(DataModel.Track track)
    {
        QueryFactory queryFactory = new QueryFactory();
       	String query = queryFactory.toSOQL(track);
    	return Database.query(query);
    }
}