global with sharing class DataModel { 
    global class Result {
        @AuraEnabled
        global String objectTypeLabel {get; set;}
        @AuraEnabled
        global Integer totalRecord {get; set;}
        @AuraEnabled
        global Integer recordStart {get; set;}
        @AuraEnabled
        global Integer recordEnd {get; set;}
        @AuraEnabled
        global Integer pageNumber {get; set;}
        @AuraEnabled
        global Boolean multiCurrencyOrganization {get; set;}
        @AuraEnabled
        global String defaultCurrency {get; set;}
        @AuraEnabled
        global Toast toast {get; set;}
        @AuraEnabled
        global SObject[] sObjects {get; set;}
        @AuraEnabled
        global datatable datatable {get; set;}
        @AuraEnabled
        global SObject sObj {get; set;}
        @AuraEnabled
        global Field[] fields {get; set;}
        @AuraEnabled
        global String objectApiName {get; set;}
        @AuraEnabled
        global String recordId {get; set;}
        @AuraEnabled
        global SObject[] sObjectList {get; set;}
        @AuraEnabled
        global Map<String, List<Field>> fieldMap {get; set;}
        @AuraEnabled
        global SObject customSetting {get; set;}
        @AuraEnabled
        global Map<String, String> stringMap {get; set;}
        @AuraEnabled
        global List<String> files {get; set;}
        @AuraEnabled
        global String isContributor {get; set;}
        @AuraEnabled
        global List<Id> contentIds {get; set;}
        @AuraEnabled
        global List<String> contributorEmails {get; set;}
        @AuraEnabled
        global Map<String, List<SObject>> sObjectUnMap {get; set;}        
        @AuraEnabled
        global Map<String, Object> resultMap {get; set;}
        @AuraEnabled
        global List<Object> resultList {get; set;}
        @AuraEnabled
        global Map<String, Field> customFieldMap {get; set;}
        @AuraEnabled
        global Set<Id> sObjIds {get; set;}
        @AuraEnabled 
        global Database.SaveResult[] dbSaveResult {get; set;}
        //Anji
        @AuraEnabled
        public Map<String, Schema.SObjectField> objectFieldSetMap {get; set;}
        
        @AuraEnabled 
        public JWTAuthHandler proofAuthMap {get; set;}
        @AuraEnabled 
        public String proofIdToken {get; set;}
        @AuraEnabled 
        public String proofToken {get; set;}
         @AuraEnabled
        global SObject[] sObjectListMarketingActivity {get; set;}
        
    }
	@namespaceAccessible
    public class Track {
        @AuraEnabled
        public String objectApiName {get; set;}
        @AuraEnabled
        public String fieldApiName {get; set;}
        @AuraEnabled
        public String fieldSetApiName {get; set;}
        @AuraEnabled
        public Integer amountOfRecords {get; set;}
        @AuraEnabled
        public List<WhereCaluse> whereCaluses {get; set;}
        @AuraEnabled
        public WhereCaluse[] whereCaluseSubs {get; set;}
        @AuraEnabled
        public String operator {get; set;}
        @AuraEnabled
        public String sortBy {get; set;}
        @AuraEnabled
        public String sortDirection {get; set;}
        @AuraEnabled
        public Integer totalRecord {get; set;}
        @AuraEnabled
        public Integer recordStart {get; set;}
        @AuraEnabled
        public Integer recordEnd {get; set;}
        @AuraEnabled
        public Integer pageNumber {get; set;}
        @AuraEnabled
        public Map<String, Field> fieldMap {get; set;}
        @AuraEnabled
        public List<Action> rowLevelactions {get; set;}
        @AuraEnabled
        public Toast toast {get; set;}
        @AuraEnabled
        public String component {get; set;}
        @AuraEnabled
        public String recordId {get; set;}
        @AuraEnabled
        public String recordTypeId {get; set;}
        @AuraEnabled
        public String folderRecordTypeId {get; set;}
        @AuraEnabled
        public Boolean linkableNameField {get; set;}
        @AuraEnabled
        public String relatedListType {get; set;}
        @AuraEnabled
        public Boolean sortable {get; set;}
        //Added By Subhasis
        @AuraEnabled
        public String AccessToken {get; set;}
        @AuraEnabled
        public String ContentType {get; set;}
        @AuraEnabled
        public String Body {get; set;}
        @AuraEnabled
        public String URL {get; set;}
        @AuraEnabled
        public String ContentLength {get; set;}
        @AuraEnabled
        public String Authorization {get; set;}
        @AuraEnabled
        public String MCAuthorization {get; set;}
        @AuraEnabled
        public SObject SObj {get; set;}
         //Anji
        @AuraEnabled
        public String campaignName {get; set;}
        @AuraEnabled
        public Date startDate{get; set;}
        @AuraEnabled
        public Date endDate{get; set;}
        @AuraEnabled
        public String currentText {get; set;}
        @AuraEnabled
        public String fieldName {get; set;}

    }
	@namespaceAccessible
    public class datatable {
        @AuraEnabled
        public Object[] data {get; set;}
        @AuraEnabled
        public Object[] columns {get; set;}
    }
    global class Field {
        @AuraEnabled
        global String name {get; set;}
        @AuraEnabled
        global String label {get; set;}
        @AuraEnabled
        global String type {get; set;}
        @AuraEnabled
        global Boolean visible {get; set;}
        @AuraEnabled
        global List<Action> headerLevelactions {get; set;}
    }
	@namespaceAccessible
    public class WhereCaluse {
        @AuraEnabled
        public String field {get; set;}
        @AuraEnabled
        public String type {get; set;}
        @AuraEnabled
        public String value {get; set;}
        @AuraEnabled
        public String[] values {get; set;}
        @AuraEnabled
        public String operator {get; set;}
        @AuraEnabled
        public String clause {get; set;}
    }
    global class Toast {
        @AuraEnabled
        global Boolean ok {get; set;}
        @AuraEnabled
        global Integer status {get; set;}
        @AuraEnabled
        global String statusText {get; set;}
        @AuraEnabled
        global String message {get; set;}
        @AuraEnabled
        global Body body {get; set;}
        @AuraEnabled
        global String component {get; set;}
        @AuraEnabled
        global Boolean showToast {get; set;}
        @AuraEnabled
        global Boolean sendToast {get; set;}
    }
	@namespaceAccessible
    public class Body {
        @AuraEnabled
        public String message {get; set;}
        @AuraEnabled
        public Integer statusCode {get; set;}
        @AuraEnabled
        public String errorCode {get; set;}
        @AuraEnabled
        public String title {get; set;}
        @AuraEnabled
        public String variant {get; set;}
        @AuraEnabled
        public String mode {get; set;}
    }
	@namespaceAccessible
    public class Action {
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String checked {get; set;}
        @AuraEnabled
        public String disabled {get; set;}
        @AuraEnabled
        public String iconName {get; set;}
    }
	@namespaceAccessible
    public static Map<Integer, String> StatusCode = new Map<Integer, String>{
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        500 => 'Internal Server Error'
    };
    global class Exceptions {
        @AuraEnabled
        global Exception ex {get; set;}
        @AuraEnabled
        global String component {get; set;}
    }
    /*
    @namespaceAccessible
    public class ResponseContent{
        @AuraEnabled
        public String AccessToken {get; set;}
        @AuraEnabled
        public String ContentType {get; set;}
        @AuraEnabled
        public String Body {get; set;}
        @AuraEnabled
        public String URL {get; set;}
        @AuraEnabled
        public String ContentLength {get; set;}
        @AuraEnabled
        public String Authorization {get; set;}
    }
    */
    global class ResultAPI {
        global String objectTypeLabel {get; set;}
        global Integer totalRecord {get; set;}
        global Integer recordStart {get; set;}
        global Integer recordEnd {get; set;}
        global Integer pageNumber {get; set;}
        global Boolean multiCurrencyOrganization {get; set;}
        global String defaultCurrency {get; set;}
        global Toast toast {get; set;}
        global SObject[] sObjects {get; set;}
        global datatable datatable {get; set;}
        global SObject sObj {get; set;}
        global Field[] fields {get; set;}
        global String objectApiName {get; set;}
        global String recordId {get; set;}
        global SObject[] sObjectList {get; set;}
        global Map<String, List<Field>> fieldMap {get; set;}
        global SObject customSetting {get; set;}
        global Map<String, String> stringMap {get; set;}
        global List<String> files {get; set;}
        global String isContributor {get; set;}
        global List<Id> contentIds {get; set;}
        global List<String> contributorEmails {get; set;}
        global Map<String, List<SObject>> sObjectUnMap {get; set;}        
        global Map<String, Object> resultMap {get; set;}
        global List<Object> resultList {get; set;}
        global Map<String, Field> customFieldMap {get; set;}
        global Set<Id> sObjIds {get; set;}
    }
    global class proofAuthMap{
        @AuraEnabled public Integer orgId {get;set;} // in json: https://testapp.get-proof.com/orgId
        @AuraEnabled public String nickname {get;set;} 
        @AuraEnabled public String name {get;set;} 
        @AuraEnabled public String picture {get;set;} 
        @AuraEnabled public String updated_at {get;set;} 
        @AuraEnabled public String email {get;set;} 
        @AuraEnabled public String iss {get;set;} 
        @AuraEnabled public String sub {get;set;} 
        @AuraEnabled public String aud {get;set;} 
        @AuraEnabled public Integer iat {get;set;} 
        @AuraEnabled public Long exp {get;set;} 
    }
}