/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)              Description
* -----------------------------------------------------------------------------------------------------------------------------------
* Alka              10/23/2019           Test class for Trigger TaskTrigger and class TaskTriggerHelper to 
											add new Campaign Contributor                                                                                                               
***********************************************************************************************************************************/
@IsTest(seeAllData = false)
private class TaskTriggerHelperTest{
    
    @isTest
    static void testCampaignContributerCreation() {
        try{
            User userRecord = TestDataSetup.getUser('System Administrator');
            insert userRecord;
        	User userRecord1 = TestDataSetup.getUser('System Administrator');
            insert userRecord1;
            User adminUser = TestDataSetup.getUser('System Administrator');
            insert adminUser;
            TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
            System.runAs(adminUser){
            	musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
                insert marketingPlan;
                Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division A',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
                insert campaignRecord;
                Task taskRecord = TestDataSetup.getTask(campaignRecord,userRecord,'Not Started',Date.Today());
                Database.SaveResult sr = Database.insert(taskRecord,false);
                musqotmp__CampaignContributor__c newContributor = [select id,musqotmp__RoleInCampaign__c,musqotmp__Campaign__c,musqotmp__User__c from musqotmp__CampaignContributor__c where musqotmp__Campaign__c = :campaignRecord.Id limit 1];
                System.assertEquals(newContributor.musqotmp__User__c, userRecord.Id);
            	taskRecord.OwnerId = userRecord1.Id;
            	update taskRecord;
                musqotmp__CampaignContributor__c newContributorAfterUpdate = [select id,musqotmp__RoleInCampaign__c,musqotmp__Campaign__c,musqotmp__User__c from musqotmp__CampaignContributor__c where musqotmp__Campaign__c = :campaignRecord.Id limit 1];
                //System.assertEquals(newContributorAfterUpdate.musqotmp__User__c, userRecord1.Id);
            }  
        }Catch(Exception e){
          //  System.debug('Exceptioon occured in test class TaskTriggerHelperTest'+e.getMessage());
        } 
    }
}