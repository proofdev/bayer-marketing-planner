/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Rasbehari         07/27/2020          Created for W-000396
**************************************************************************************************************************************/
public class DataVariableItemTriggerHelper {
    
    public static void beforeInsert(List<DataVariableItem__c> TriggerNew){
        updateYearQuarterWeekBeforeInsert(TriggerNew);
    }
    public static void beforeUpdate(List<DataVariableItem__c> TriggerNew,Map<Id,DataVariableItem__c> triggerOldMap){
        updateYearQuarterWeekBeforeUpdate(TriggerNew,triggerOldMap);
    }
	//Added for W-000396
    public static void updateYearQuarterWeekBeforeInsert(List<DataVariableItem__c> TriggerNew){
        String Month;
        String Year;
        String Week;
        String quarter;
        Integer currentWeek;
        for(DataVariableItem__c dataVarItem: TriggerNew){
            //Set Year from musqotmp__Date__c
            if(Schema.sObjectType.DataVariableItem__c.fields.musqotmp__Year__c.isUpdateable()){
            	Year = dataVarItem.musqotmp__Year__c = String.valueOf(dataVarItem.musqotmp__Date__c.Year());   
            }
            //Set Month from musqotmp__Date__c
            if(Schema.sObjectType.DataVariableItem__c.fields.musqotmp__Month__c.isUpdateable()){
                Month = String.valueOf(dataVarItem.musqotmp__Date__c.month());
            	dataVarItem.musqotmp__Month__c = Year +'-'+ Month;
            }
            //Set Week from musqotmp__Date__c
            if(Schema.sObjectType.DataVariableItem__c.fields.musqotmp__Week__c.isUpdateable()){
            	currentWeek = getWeekFromDate(dataVarItem.musqotmp__Date__c);
            	dataVarItem.musqotmp__Week__c = Year +'-'+ String.valueOf(currentWeek);
            }
            //Set Quarter from musqotmp__Date__c
            if(Schema.sObjectType.DataVariableItem__c.fields.musqotmp__Quarter__c.isUpdateable()){
            	quarter = getQuarterFromDate(dataVarItem.musqotmp__Date__c);
            	dataVarItem.musqotmp__Quarter__c = Year +'-'+quarter;
            }
        }
    }
    //Added for W-000396
    public static void updateYearQuarterWeekBeforeUpdate(List<DataVariableItem__c> triggerNew, Map<Id,DataVariableItem__c> triggerOldMap){
        String Month;
        String Year;
        String Week;
        String quarter;
        Integer currentWeek;
        for(DataVariableItem__c dataVarItem: TriggerNew){
            if(dataVarItem.musqotmp__Date__c != triggerOldMap.get(dataVarItem.Id).musqotmp__Date__c ){
                //Set Year from musqotmp__Date__c
                if(Schema.sObjectType.DataVariableItem__c.fields.musqotmp__Year__c.isUpdateable()){
                	Year = dataVarItem.musqotmp__Year__c = String.valueOf(dataVarItem.musqotmp__Date__c.Year());
                }
                //Set Month from musqotmp__Date__c
                if(Schema.sObjectType.DataVariableItem__c.fields.musqotmp__Month__c.isUpdateable()){
                	Month = String.valueOf(dataVarItem.musqotmp__Date__c.month());
                	dataVarItem.musqotmp__Month__c = Year +'-'+ Month;
                }
                //Set Week from musqotmp__Date__c
                if(Schema.sObjectType.DataVariableItem__c.fields.musqotmp__Week__c.isUpdateable()){
                	currentWeek = getWeekFromDate(dataVarItem.musqotmp__Date__c);
                	dataVarItem.musqotmp__Week__c=Year +'-'+ String.valueOf(currentWeek);
                }
                //Set Quarter from musqotmp__Date__c
                if(Schema.sObjectType.DataVariableItem__c.fields.musqotmp__Quarter__c.isUpdateable()){
                    quarter = getQuarterFromDate(dataVarItem.musqotmp__Date__c);
                    dataVarItem.musqotmp__Quarter__c = Year +'-'+quarter;
                }
            }
        }
    }
    //Added for W-000396
    public static Integer getWeekFromDate(Date triggerDate){
        Integer numberOfWeek;
        Date todaydateinstance = date.newinstance(triggerDate.year(), triggerDate.month(),triggerDate.day());
        Integer currentyear = triggerDate.year();
        Date yearstartdate = date.newinstance(currentyear, 01, 01);
        Date year2ndweek = yearstartdate.adddays(7).tostartofweek();
        if(triggerDate<year2ndweek){
            return  numberOfWeek = 1;
        }
        integer numberDaysDue = year2ndweek.daysBetween(todaydateinstance);
        if(math.mod(numberDaysDue,7) == 0){
            return numberOfWeek = math.MOD(Integer.valueof(math.FLOOR( ( numberDaysDue )/7)),52)+1;   
        } else {
            return numberOfWeek = math.MOD(Integer.valueof(math.FLOOR( ( numberDaysDue )/7)),52)+2;
        }
    }
    //Added for W-000396
    public static String getQuarterFromDate(Date triggerDate){
        Integer MM=Integer.valueOf(triggerDate.month());
        String quarter;
        switch on MM {
   			when 1,2, 3 {
                quarter='Q1';
   					}
   			when 4,5, 6 {
                quarter= 'Q2';
   			}
   			when 7,8,9 {
                quarter= 'Q3';
   			}
   			when 10,11,12 {
                quarter= 'Q4';
   			}
		}
        return quarter;
    }
}