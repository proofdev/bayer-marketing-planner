/************************************************************************************************************************************
* Developer         Date                Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              05/05/2020          Created for W-000307 - (Update Campaign.WorkingTime__c as sum of all child musqotmp__CampaignContributor__c.WorkingTime__c)
**************************************************************************************************************************************/
public with sharing class CampaignContributorTriggerHelper {
    //Added for W-000307
    public static void rollUpWorkingTimeAfterInsert(Set<Id> campaignIds,List<musqotmp__CampaignContributor__c> newTrigger){
        try{ 
            Map<id,Schema.Campaign> campaignMap = new Map<id,Schema.Campaign>(); 
            if (Schema.sObjectType.Campaign.fields.musqotmp__WorkinTime__c.isAccessible()){ 
                campaignMap = new Map<id,Schema.Campaign>([select id,musqotmp__WorkinTime__c from Campaign where id IN :campaignIds]);
                List<Schema.Campaign> campaignsToUpdate = new List<Schema.Campaign>();
                //Double workingTime = 0;
                for(musqotmp__CampaignContributor__c cc: newTrigger){
                    if(campaignMap.get(cc.musqotmp__Campaign__c).musqotmp__WorkinTime__c != null){
                        campaignMap.get(cc.musqotmp__Campaign__c).musqotmp__WorkinTime__c += cc.musqotmp__WorkingTime__c;
                    } else {
                        campaignMap.get(cc.musqotmp__Campaign__c).musqotmp__WorkinTime__c = cc.musqotmp__WorkingTime__c;
                    }
                }
                campaignsToUpdate = campaignMap.Values();
                if(Schema.sObjectType.Campaign.fields.musqotmp__WorkinTime__c.isUpdateable()){
                    Database.update(campaignsToUpdate);
                } 
            }   
        }catch(Exception e){
            ExceptionHandler.logError('Class','CampaignContributorTriggerHelper','rollUpWorkingTimeAfterInsert',e);
        }
    }
    //Added for W-000307
    public static void rollUpWorkingTimeAfterUpdate(Set<Id> campaignIds,List<musqotmp__CampaignContributor__c> newTrigger,Map<Id, musqotmp__CampaignContributor__c> oldMap){
        try{
            if(Schema.sObjectType.Campaign.fields.musqotmp__WorkinTime__c.isAccessible()){
               // System.debug('rollUpWorkingTimeAfterUpdate');
                Map<id,Schema.Campaign> campaignMap = new Map<id,Schema.Campaign>([select id, musqotmp__WorkinTime__c from Campaign where id IN :campaignIds]);
                List<Schema.Campaign> campaignsToUpdate = new List<Schema.Campaign>();
                for(musqotmp__CampaignContributor__c cc: newTrigger){
                    Double workingTime = cc.musqotmp__WorkingTime__c - oldMap.get(cc.Id).musqotmp__WorkingTime__c ;
                  //  System.debug('workingTime'+workingTime);
                    if(campaignMap.get(cc.musqotmp__Campaign__c).musqotmp__WorkinTime__c != null){
                            campaignMap.get(cc.musqotmp__Campaign__c).musqotmp__WorkinTime__c += workingTime;
                    } else {
                        campaignMap.get(cc.musqotmp__Campaign__c).musqotmp__WorkinTime__c = workingTime;
                    }
                }
                campaignsToUpdate = campaignMap.Values();
                if(Schema.sObjectType.Campaign.fields.musqotmp__WorkinTime__c.isUpdateable()){
                    Database.update(campaignsToUpdate);
                }
            }
        } catch(Exception e){
            ExceptionHandler.logError('Class','CampaignContributorTriggerHelper','rollUpWorkingTimeAfterUpdate',e);
        }
    }
    //Added for W-000307
    public static void rollUpWorkingTimeAftereDelete(Set<Id> campaignIds,List<musqotmp__CampaignContributor__c> oldTrigger){
        try{
            if(Schema.sObjectType.Campaign.fields.musqotmp__WorkinTime__c.isAccessible()){
                Map<id,Schema.Campaign> campaignMap = new Map<id,Schema.Campaign>([select id, musqotmp__WorkinTime__c  from Campaign where id IN :campaignIds]);
                List<Schema.Campaign> campaignsToUpdate = new List<Schema.Campaign>();
                for(musqotmp__CampaignContributor__c cc: oldTrigger){
                    if(campaignMap.get(cc.musqotmp__Campaign__c).musqotmp__WorkinTime__c != null){
                        campaignMap.get(cc.musqotmp__Campaign__c).musqotmp__WorkinTime__c -= cc.musqotmp__WorkingTime__c;
                    }
                }
                campaignsToUpdate = campaignMap.Values();
                if(Schema.sObjectType.Campaign.fields.musqotmp__WorkinTime__c.isUpdateable()){
                    Database.update(campaignsToUpdate);
                }
            }
        } catch(Exception e){
            ExceptionHandler.logError('Class','CampaignContributorTriggerHelper','rollUpWorkingTimeAftereDelete',e);
        }
    }
}