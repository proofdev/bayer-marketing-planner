//Comment
@isTest(seeAllData = false)
public class PostInstallScriptControllerTest {
    @isTest
    static void testScript(){
        User adminUser = TestDataSetup.getUser('System Administrator');
        insert adminUser;
        TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
        System.runAs(adminUser){
            PostInstallDataScript obj = new PostInstallDataScript();
        	PostInstallScriptController postinstall = new PostInstallScriptController();
            Test.testInstall(postinstall,  null);
            /*List<musqotmp__MarketingPlan__c> mpList = [SELECT id from musqotmp__MarketingPlan__c limit 5];
            System.AssertEquals(mpList.size() > 0,true);*/
            System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
        }
    }
    /*
    @isTest
    static void testPostInstallDataScript(){
        User adminUser = TestDataSetup.getUser('System Administrator');
        insert adminUser;
        TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
        System.runAs(adminUser){
            PostInstallDataScript obj = new PostInstallDataScript();
            //List<Schema.Campaign> camapignList = [SELECT id from Campaign limit 5];
            //System.AssertEquals(camapignList.size() > 0,true);
            List<musqotmp__MarketingPlan__c> mpList = [SELECT id from musqotmp__MarketingPlan__c limit 5];
            System.AssertEquals(mpList.size() > 0,true);
        }
    }*/
}