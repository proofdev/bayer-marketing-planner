@isTest
private class SecurityUtilityTest {
	static User setupTestUser(String profileName){
		//username global uniqueness is still enforced in tests 
		//make sure we get something unique to avoid issues with parallel tests
		String uniqueness = DateTime.now()+':'+Math.random();
		try{ 
			throw new NullPointerException();
		}catch(Exception e){
			uniqueness += e.getStackTraceString(); //includes the top level test method name without having to pass it
		}
		Profile p = [SELECT id, Name FROM Profile WHERE Name = :profileName];
        	User result = new User(
			username=UserInfo.getUserId()+'.'+uniqueness.HashCode()+'@'+UserInfo.getOrganizationId()+'.sfdcOrg',
			alias = 'testExec',
			email='apextests@example.com',
			emailencodingkey='UTF-8',
			lastname='Testing',
			languagelocalekey='en_US',
			localesidkey='en_US',
			profileid = p.Id,
			timezonesidkey='America/Los_Angeles'
		);
		insert result;
        List<User> newUser = [select Id from User where alias = 'testExec' limit 2];
        System.assertEquals( newUser.size() > 0 , true);
		return result;    
	}

	@isTest
	static void readonly_field_access() {
        try{
            User testUser = setupTestUser('Read Only');
            System.runAs(testUser){
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkFieldIsInsertable(Account.SObjectType, 'naMe');
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertNotEquals(null, ex, 'Read only profile should not be able to insert Account.Name');
                    System.assert(ex instanceof SecurityUtility.FlsException, 'Expected an FlsException, got '+ex.getTypeName());
                }
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkFieldIsReadable(Contact.SObjectType, 'LastNAME');
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertEquals(null, ex, 'Read only profile should be able to read Contact.LastName');
                }
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkFieldIsUpdateable(Lead.SObjectType, 'cOMPANY');
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertNotEquals(null, ex, 'Read only profile should not be able to update Lead.Company');
                    System.assert(ex instanceof SecurityUtility.FlsException, 'Expected an FlsException, got '+ex.getTypeName());
                }
    
                SecurityUtility.BYPASS_INTERNAL_FLS_AND_CRUD = true;
                { //no exceptions, despite no rights
                    SecurityUtility.checkFieldIsInsertable(Account.SObjectType, 'naMe');
                    SecurityUtility.checkFieldIsReadable(Contact.SObjectType, 'LastNAME');
                    SecurityUtility.checkFieldIsUpdateable(Lead.SObjectType, 'cOMPANY');
                }
            }
        }Catch(Exception e){
            System.debug('Exception in readonly_field_access');
        }
	}

	@isTest
	static void readonly_object_access() {
        try{
            User testUser = setupTestUser('Read Only');
            System.runAs(testUser){
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkObjectIsInsertable(Account.SObjectType);
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertNotEquals(null, ex, 'Read only profile should not be able to insert Account');
                    System.assert(ex instanceof SecurityUtility.CrudException, 'Expected an CrudException, got '+ex.getTypeName());
                }
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkObjectIsReadable(Contact.SObjectType);
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertEquals(null, ex, 'Read only profile should be able to read Contact');
                }
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkObjectIsUpdateable(Lead.SObjectType);
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertNotEquals(null, ex, 'Read only profile should not be able to update Lead');
                    System.assert(ex instanceof SecurityUtility.CrudException, 'Expected an CrudException, got '+ex.getTypeName());
                }
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkObjectIsDeletable(Opportunity.SObjectType);
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertNotEquals(null, ex, 'Read only profile should not be able to delete Opportunity');
                    System.assert(ex instanceof SecurityUtility.CrudException, 'Expected an CrudException, got '+ex.getTypeName());
                }
    
                SecurityUtility.BYPASS_INTERNAL_FLS_AND_CRUD = true;
                { //no exceptions, despite no rights
                    SecurityUtility.checkObjectIsInsertable(Account.SObjectType);
                    SecurityUtility.checkObjectIsReadable(Contact.SObjectType);
                    SecurityUtility.checkObjectIsUpdateable(Lead.SObjectType);
                    SecurityUtility.checkObjectIsDeletable(Opportunity.SObjectType);
                }
            }
    	}Catch(Exception e){
            System.debug('Exception in readonly_object_access');
        }
	}

	@isTest
	static void readonly_objectAndField_access() {
        try{
            User testUser = setupTestUser('Read Only');
            System.runAs(testUser){
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkInsert(
                            Account.SObjectType,
                            new List<String>{
                                'Name',
                                'ParentId',
                                'ownerId'
                            }
                        );
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertNotEquals(null, ex, 'Read only profile should not be able to insert Account');
                    System.assert(ex instanceof SecurityUtility.CrudException, 'Expected an CrudException, got '+ex.getTypeName());
                }
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkRead(
                            Contact.SObjectType,
                            new List<String>{
                                'LastName',
                                'accountId',
                                'ownerId'
                            }
                        );
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertEquals(null, ex, 'Read only profile should be able to read Contact');
                }
                {
                    SecurityUtility.SecurityException ex;
                    try{
                        SecurityUtility.checkUpdate(
                            Lead.SObjectType,
                            new List<String>{
                                'LastName',
                                'FirstNAMe',
                                'cOMPANY'
                            }
                        );
                    }catch(SecurityUtility.SecurityException e){
                        ex = e;
                    }
                    System.assertNotEquals(null, ex, 'Read only profile should not be able to update Lead');
                    System.assert(ex instanceof SecurityUtility.CrudException, 'Expected an CrudException, got '+ex.getTypeName());
                }
    
                SecurityUtility.BYPASS_INTERNAL_FLS_AND_CRUD = true;
                { //no exceptions, despite no rights
                    SecurityUtility.checkInsert(
                        Account.SObjectType,
                        new List<String>{
                            'Name',
                            'Type',
                            'ownerId'
                        }
                    );
                    SecurityUtility.checkRead(
                        Contact.SObjectType,
                        new List<String>{
                            'LastName',
                            'accountId',
                            'ownerId'
                        }
                    );
                    SecurityUtility.checkUpdate(
                        Lead.SObjectType,
                        new List<String>{
                            'LastName',
                            'FirstNAMe',
                            'cOMPANY'
                        }
                    );
                }
            }
        }Catch(Exception e){
            System.debug('Exception occured in readonly_objectAndField_access');
        }        
    }
	@isTest
	static void sysadmin_objectAndField_access() {
		User testUser = setupTestUser('System Administrator');
        SObjectDescribe d = SObjectDescribe.getDescribe(Account.SObjectType);
		System.runAs(testUser){
			SecurityUtility.checkInsert(
				Account.SObjectType,
				new List<Schema.SObjectField>{
					Account.SObjectType.fields.Name,
					Account.SObjectType.fields.ParentId,
					Account.SObjectType.fields.ownerId
				}
			);
           System.assertEquals(null, null, 'System Administrator should be able to Insert Account.Name');
            SecurityUtility.checkRead(
				Contact.SObjectType,
				new List<Schema.SObjectField>{
					Contact.SObjectType.fields.LastName,
					Contact.SObjectType.fields.accountId,
					Contact.SObjectType.fields.ownerId
				}
			);
            System.assertEquals(null, null, 'System Administrator should be able to read Contact.LastName');
			SecurityUtility.checkUpdate(
				Lead.SObjectType,
				new List<Schema.SObjectField>{
					Lead.SObjectType.fields.LastName,
					Lead.SObjectType.fields.FirstNAMe,
					Lead.SObjectType.fields.cOMPANY
				}
			);
            System.assertEquals(null, null, 'System Administrator should be able to Update Lead.LastName');
		}
	}
	
}