@isTest
public class PlanMemberControllerTest {
    @isTest
    static void getPlanMembers(){
        User adminUser1 = TestDataSetup.getUser('System Administrator');
        insert adminUser1;
        //try{
        Date sDate = Date.newInstance(2019, 01, 01);
        Date eDate = Date.newInstance(2025, 01, 01);
        Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
        musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
        marketingPlan.Name = 'Marketing Plan';
        marketingPlan.musqotmp__StartDate__c = sDate;
        marketingPlan.musqotmp__EndDate__c = eDate;
        marketingPlan.musqotmp__Description__c = '';
        marketingPlan.musqotmp__CampaignTemplate__c = false;
        marketingPlan.musqotmp__Status__c = 'In Progress';
        marketingPlan.musqotmp__MarketingArea__c = 'Europe';
        marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
        marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
        marketingPlan.RecordTypeId = recordTypeId;
        insert marketingPlan;
        List<musqotmp__MarketingPlanMember__c> planMembers = new List<musqotmp__MarketingPlanMember__c>();
        musqotmp__MarketingPlanMember__c marketingPlanMember = new musqotmp__MarketingPlanMember__c();
        marketingPlanMember.musqotmp__MarketingPlan__c = marketingPlan.Id;
        marketingPlanMember.musqotmp__User__c = adminUser1.id;//UserInfo.getUserId();
        //marketingPlanMember.musqotmp__UniquePlanMember__c = marketingPlan.Id+''+adminUser1.Id; 
        planMembers.add(marketingPlanMember);
        //insert planMembers;
        Test.startTest();
        PlanMemberController.getPlanMembers(marketingPlan.Id);
        PlanMemberController.CreateRecords(planMembers);
        Test.stopTest();
        //}catch(Exception ex){}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
}