/* Date          :26-Sep-2019
** Author        :Alka Kumari 
** Description   :Helper class for trigger TaskTrigger to create new Campaign member if not already created for W-000096
*/
public with sharing class TaskTriggerHelper{
    
    public static void createCampaignContributer(List<Task> campaignTaskList,List<String> newContributerList){
        List<musqotmp__CampaignContributor__c> newCampaignContributerList = new List<musqotmp__CampaignContributor__c>();
        List<String> existingUniqCampaignContributerIdList = new List<String>();
        try{
            if(Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__UniqueContributorCampaign__c.isAccessible()){
                for(musqotmp__CampaignContributor__c contributer: [select musqotmp__UniqueContributorCampaign__c 
                                                            from musqotmp__CampaignContributor__c
                                                            where musqotmp__UniqueContributorCampaign__c In :newContributerList]){
                                                            
                    existingUniqCampaignContributerIdList.add(String.valueOf(contributer.musqotmp__UniqueContributorCampaign__c));
                }
                for(Task tsk: campaignTaskList){
                    String campId = String.valueOf(tsk.whatId).substring(0, 15);
                    String userId = String.valueOf(tsk.OwnerId).substring(0, 15);
                    if(!existingUniqCampaignContributerIdList.contains(campId+userId))
                        newCampaignContributerList.add(getNewContributer(tsk));
                }
                if(newCampaignContributerList.size() > 0 && 
                    Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__Campaign__c.isCreateable() &&
                    Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__User__c.isCreateable() &&
                    Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__RoleInCampaign__c.isCreateable()
                    ){
                    Database.SaveResult[] srList = Database.insert(newCampaignContributerList, false);
                }   
            }
        } Catch(Exception e){
            ExceptionHandler.logError('Apex', 'TaskTriggerHelper', 'createCampaignContributer', e);
        } 
    }
    
    public static void updateCampaignContributer(List<Task> newTrigger, Map<Id,Task> oldMap, List<String> newContributerList){
        List<musqotmp__CampaignContributor__c> newCampaignContributerList = new List<musqotmp__CampaignContributor__c>();
        List<String> existingUniqCampaignContributerIdList = new List<String>();
        try{
            if(Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__UniqueContributorCampaign__c.isAccessible()){
                for(musqotmp__CampaignContributor__c contributer: [select musqotmp__UniqueContributorCampaign__c 
                                                            from musqotmp__CampaignContributor__c
                                                            where musqotmp__UniqueContributorCampaign__c In :newContributerList]){
                                                            
                    existingUniqCampaignContributerIdList.add(String.valueOf(contributer.musqotmp__UniqueContributorCampaign__c));
                }
                for(Task tsk: newTrigger){
                    if(tsk.OwnerId != oldMap.get(tsk.Id).OwnerId){
                        String campId = String.valueOf(tsk.whatId).substring(0, 15);
                        String userId = String.valueOf(tsk.OwnerId).substring(0, 15);
                        if(!existingUniqCampaignContributerIdList.contains(campId+userId))
                            newCampaignContributerList.add(getNewContributer(tsk));
                    }
                    if(newCampaignContributerList.size() > 0 && 
                        Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__Campaign__c.isCreateable() &&
                        Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__User__c.isCreateable() &&
                        Schema.sObjectType.musqotmp__CampaignContributor__c.fields.musqotmp__RoleInCampaign__c.isCreateable()
                        ){
                        Database.SaveResult[] srList = Database.insert(newCampaignContributerList, false);
                    }
                }   
            }
        } Catch(Exception e){
            ExceptionHandler.logError('Apex', 'TaskTriggerHelper', 'updateCampaignContributer', e);
        }
    }
    
    public static musqotmp__CampaignContributor__c getNewContributer(Task tsk){
      musqotmp__CampaignContributor__c campContriObj = new musqotmp__CampaignContributor__c();
      campContriObj.musqotmp__Campaign__c = tsk.whatId;
      campContriObj.musqotmp__User__c = tsk.OwnerId;
      campContriObj.musqotmp__RoleInCampaign__c = 'Contributor';
      return campContriObj;
    } 
}