@isTest
public class ExceptionHandlerTest {

    @isTest
    static void logError(){
        try{
            MusqotLog__c log = new MusqotLog__c();
            update log;
        }
        catch(Exception e){
            ExceptionHandler.logError('compType', 'compName', 'methodName', e);
            System.assertEquals(e != null, true);
        }
    }
}