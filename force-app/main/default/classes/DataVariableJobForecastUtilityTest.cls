/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              06/08/2020          Created for W-000339 (test class for DataVariableJobForecastUtility,JourneyTriggerHelper,
											MarketingActivityTriggerHelper and CampaignTriggerHelper classes)
**************************************************************************************************************************************/
@IsTest(seeAllData = false)
public class DataVariableJobForecastUtilityTest {
    @isTest
    static void testDataVariableJobForecast() {
    	Boolean allocationObjectCheck = DataVariableJobForecastUtility.isObjectExist('musqotmp__Allocation__c');
        Boolean JourneyObjectCheck = DataVariableJobForecastUtility.isObjectExist('musqotmp__MarketingJourney__c');
        System.AssertEquals(JourneyObjectCheck,true,'Journey object availability check should be true');
        List<Id> allocationIdList = new List<Id>();
        List<Id> camapignIdList = new List<Id>();
        //DataVariableJobForecastUtility.publishAllocationForecastEvent(allocationIdList, true);
        //DataVariableJobForecastUtility.publishAllocationForecastEvent(allocationIdList, false);
        //DataVariableJobForecastUtility.publishCampaignForecastEvent(camapignIdList, true);
        //DataVariableJobForecastUtility.publishCampaignForecastEvent(camapignIdList, false);
        musqotmp__DataVariable__c dataVar = TestDataSetup.getDataVariableRecord('Sales','#3F2386','Daily','Standard');
        insert dataVar;
        DataVariableActivityTypeMap__c dataVarMap = TestDataSetup.getDataVarActivityTypeMapRecord(dataVar.Id,'Webinar');
        insert dataVarMap;
        musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
        insert marketingPlan;
        Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division A',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
        campaignRecord.BudgetedCost = 10000;
        campaignRecord.ActualCost = 15000;
        campaignRecord.Type = 'Webinar';
        campaignRecord.musqotmp__ProductGroup__c = 'n/a';
        campaignRecord.musqotmp__MarketingBusinessUnit__c = 'Division A;Division B';
        campaignRecord.musqotmp__MarketingArea__c = 'Europe';
        campaignRecord.musqotmp__Countries__c = 'Sweden';
        campaignRecord.musqotmp__Audience__c = 'B2B;B2C';
        insert campaignRecord;
        List<Schema.Campaign> campaignList = new List<Schema.Campaign>();
        campaignList.add(campaignRecord);
        CampaignTriggerHelper.createCampaignForecast(campaignList);
        
        Map<Id,Schema.Campaign> campaignMap = new Map<Id,Schema.Campaign>();
        campaignMap.put(campaignRecord.Id,campaignRecord);
        List<Schema.Campaign> campaignNew = new List<Schema.Campaign>();
        campaignRecord.BudgetedCost = 20000;
        campaignNew.add(campaignRecord);
        CampaignTriggerHelper.updateCampaignForecast(campaignNew,campaignMap);
    }
    @isTest
    static void testJourneyTriggerHelper() {
        User adminUser = TestDataSetup.getUser('System Administrator');
        insert adminUser;
        TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
        System.runAs(adminUser){
        	musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
            insert marketingPlan;
            Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division A',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
            insert campaignRecord;
            musqotmp__MarketingJourney__c journeyRecord = TestDataSetup.getNewJourneyInstance('Planned','News',Date.Today(),Date.Today()+1,campaignRecord.Id,'B2B');
            insert journeyRecord;
            musqotmp__MarketingJourney__c journeyCreated = [SELECT Id from musqotmp__MarketingJourney__c where musqotmp__Campaign__c  =: campaignRecord.Id];
            System.AssertEquals(journeyCreated != null ,true);
            journeyRecord.musqotmp__EndDate__c = Date.Today()+1;
            update journeyRecord;
            campaignRecord.EndDate = Date.Today();
            update campaignRecord;
        }
    }
    @isTest
    static void testMarketingActivityTriggerHelper() {
        User adminUser = TestDataSetup.getUser('System Administrator');
        insert adminUser;
        TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
        System.runAs(adminUser){
        	musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
            insert marketingPlan;
            Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division B',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
            insert campaignRecord;
            musqotmp__MarketingActivity__c marketActivityRecord = TestDataSetup.getNewMarketingActivity('Planned','Facebook',Date.Today(),Date.Today()+1,campaignRecord.Id,'B2B');
            insert marketActivityRecord;
            marketActivityRecord.musqotmp__EndDate__c = Date.Today()+2;
            update marketActivityRecord;
            musqotmp__MarketingActivity__c updatedMActivityRecord = [SELECT Id,musqotmp__EndDate__c from musqotmp__MarketingActivity__c where Id =:marketActivityRecord.Id limit 1];
            system.assertEquals(updatedMActivityRecord.musqotmp__EndDate__c, Date.Today()+2);
        }
    }
    @isTest
    static void testCampaignTriggerHelper() {
        User adminUser = TestDataSetup.getUser('System Administrator');
        insert adminUser;
        TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
        System.runAs(adminUser){
            musqotmp__DataVariable__c dataVar = TestDataSetup.getDataVariableRecord('Sales','#3F2386','Daily','Standard');
            insert dataVar;
            DataVariableActivityTypeMap__c dataVarMap = TestDataSetup.getDataVarActivityTypeMapRecord(dataVar.Id,'Webinar');
        	musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
            insert marketingPlan;
            Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division A',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
            campaignRecord.BudgetedCost = 10000;
            campaignRecord.ActualCost = 15000;
            campaignRecord.Type = 'Webinar';
            insert campaignRecord;
            campaignRecord.EndDate = Date.Today()+1;
            update campaignRecord;
            campaignRecord.musqotmp__Audience__c = 'B2B';
            campaignRecord.musqotmp__MarketingBusinessUnit__c = 'Division A';
            update campaignRecord;
            campaignRecord.musqotmp__MarketingArea__c = 'Europe';
            campaignRecord.musqotmp__Countries__c = '';
            campaignRecord.musqotmp__City__c = '';
            campaignRecord.musqotmp__Shop__c = 'n/a';
            campaignRecord.musqotmp__ProductGroup__c = 'n/a';
            update campaignRecord;
            campaignRecord.BudgetedCost = 1000;
            update campaignRecord;
            campaignRecord.ActualCost = 10000;
            update campaignRecord;
            Schema.Campaign campaignRecordUpdated = [SELECT Id, EndDate from Campaign where Id = :campaignRecord.Id];
            System.assertEquals(campaignRecordUpdated.EndDate, Date.Today()+1);
            List<Schema.Campaign> campaignList = new List<Schema.Campaign>();
            campaignList.add(campaignRecord);
            Map<Id, Schema.Campaign> campMap = new Map<Id,Schema.Campaign>();
            campaignRecord.ActualCost = 17000;
            campMap.put(campaignRecord.Id,campaignRecord);
            CampaignTriggerHelper.processDataVarForecastsAfterUpdate(campaignList,campMap);
        }
    }
}