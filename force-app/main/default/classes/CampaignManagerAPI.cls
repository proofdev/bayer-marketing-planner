@namespaceAccessible
public with sharing class CampaignManagerAPI {
    
    @namespaceAccessible
    public static DataModel.Result handleToast(DataModel.Exceptions exceptions) {
        DataModel.Result result = DataHandler.handleToast(exceptions);
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result sendToast(String param) {
        DataModel.Result result = DataHandler.sendToast(param);
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result datatable(String param) {
        DataModel.Result result = DataHandler.datatable(param);
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result getDynamicObjectInfo(String param, String recordId) {
        DataModel.Result result = DataHandler.getDynamicObjectInfo(param, recordId);
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result getOrganizationId(String param) {
        DataModel.Result result = DataHandler.getOrganizationId(param);
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result getRecords(String param) {
        DataModel.Result result = DataHandler.getRecords(param);
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result readCampaignManagerSetup() {
        DataModel.ResultAPI api = new  DataModel.ResultAPI();
        DataModel.Result result = DataHandler.readCampaignManagerSetup();
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result readMarketingCloudCredential() {
        DataModel.ResultAPI api = new  DataModel.ResultAPI();
        DataModel.Result result = DataHandler.readMarketingCloudCredential();
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result DynamicObjectInfo(String param, String recordId) {
        DataModel.Result result = SendMessageToContributors.DynamicObjectInfo(param, recordId);
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result postCalloutResponseContents(String param) {
        DataModel.Result result = DataHandler.postCalloutResponseContents(param);
        return result;
    }
    @namespaceAccessible
    public static DataModel.Result getCalloutResponseContents(String param) {
        DataModel.Result result = DataHandler.getCalloutResponseContents(param); 
        return result;
    }
    @namespaceAccessible
    public static String toSOQL(DataModel.Track track){
        QueryFactory qf = new QueryFactory();
        String queryStr = qf.toSOQL(track);
        return queryStr;
    } 
    @namespaceAccessible
    public static List<DataModel.Field> getFieldSetFields(Schema.SObjectType token,String fieldSetName){
        SObjectDescribe objDesc = SObjectDescribe.getDescribe(token);
        List<DataModel.Field> fields = objDesc.getFieldSetFields(fieldSetName);
        return fields;
    }
    @namespaceAccessible
    public static List<SObject> buildChild(List<SObject> campaigns, String campaignId){
       return  DataHandler.buildChild(campaigns, campaignId);
    }
    @namespaceAccessible
    public static DataModel.ResultAPI readRecords(String param) {
        DataModel.ResultAPI result = new DataModel.ResultAPI();
        DataModel.Track track = new DataModel.Track();
        DataModel.Toast toast = new DataModel.Toast();

        try {
            track = (DataModel.Track)JSON.deserialize(param, DataModel.Track.class);
            ISObjectService iSObjectService = new SObjectService();
            result.sObjects = iSObjectService.getRecords(track);
            toast.ok = true;
            result.toast = toast;
        } catch (Exception ex) {
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            toast.ok=false;
            toast.component = track.component;
            toast.message=ex.getStackTraceString();
            result.toast = toast;
        }
        return result;
    }
}