@namespaceAccessible
public interface ISObjectService {
    /**
     * Alternative to the Records property, provided to support mocking of Service classes
     **/	
	List<SObject> getRecords(DataModel.Track track);
}