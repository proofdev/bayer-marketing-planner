@isTest
public class CampaignTest {
    @isTest
    static void testReadCampaignManagerSetup(){
        DataModel.Result result = Campaign.readCampaignManagerSetup();
        SObject objInfo = (Sobject)CampaignManagerSetup__c.getInstance();
        System.assertEquals(objInfo , result.sObj);
    }
    @isTest
    static void testreadMarketingCloudCredential(){
        DataModel.Result result = Campaign.readMarketingCloudCredential();
        SObject objInfo = (Sobject)MarketingCloudCredential__c.getInstance();
        System.assertEquals(objInfo , result.sObj);
    }
    @isTest
    static void testGetDynamicObjectInfo(){
        DataModel.Result result = Campaign.getDynamicObjectInfo('Campaign','');
        System.assertEquals(null, result.contentIds,'Content Ids should be null for this scenario');
    }
    @isTest
    static void testDynamicObjectInfo(){
        DataModel.Result result = Campaign.DynamicObjectInfo('Campaign','');
        System.assertEquals(null, result.contentIds,'Content Ids should be null for this scenario');
    }
    @isTest
    static void testDatatable(){
        DataModel.Result result = Campaign.datatable('Campaign');
        System.assertEquals(null, result.contentIds,'Content Ids should be null for this scenario');
    }
    @isTest
    static void testgetRecords(){
        DataModel.Track track = new DataModel.Track();
        track.recordId = '';
        String param = JSON.serialize(track);
        DataModel.Result result = Campaign.getRecords(param);
        System.assertEquals(null, result.contentIds,'Content Ids should be null for this scenario');
    }
    @isTest
    static void testSendToast(){
        DataModel.Track track = new DataModel.Track();
        track.component = 'sendToast';
        System.assertEquals('sendToast' , track.component);
        String param = JSON.serialize(track);
        DataModel.Result result = Campaign.sendToast(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void testPostCalloutResponseContents(){
        DataModel.Track track = new DataModel.Track();
        track.URL = 'sendToast';
        track.Body = 'sendToast';
        String param = JSON.serialize(track);
        DataModel.Result result = Campaign.postCalloutResponseContents(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void testGetCalloutResponseContents(){          
        DataModel.Track track = new DataModel.Track();
        track.URL = 'https://wwww.musqot.com';
        track.Authorization = 'Bearer ndjhqwihdiuqdnq';
        track.ContentType = 'application/json';
        String param = JSON.serialize(track);
        DataModel.Result result = Campaign.getCalloutResponseContents(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
}