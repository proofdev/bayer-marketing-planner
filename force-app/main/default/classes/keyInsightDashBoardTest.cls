@isTest
public class keyInsightDashBoardTest {
	@isTest
    static void getRecordsTest(){
        try{
            DataModel.Field field = new DataModel.Field();
            field.name = 'Description';
            field.label = 'Description';
            field.type = 'textarea';
            field.visible = true;
            List<DataModel.whereCaluse> whereCaluses = new List<DataModel.whereCaluse>();
            DataModel.whereCaluse whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereCaluses.add(whereClause);
            List<DataModel.whereCaluse> whereCaluseSubs = new List<DataModel.whereCaluse>();
            whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereClause.operator = 'OR';
            whereCaluseSubs.add(whereClause);
            whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereCaluseSubs.add(whereClause);
            DataModel.Track track = new DataModel.Track();
            track.component = 'DataHandlerTest-getRecords';
            track.objectApiName = 'Campaign';
            //track.fieldSetApiName = 'Musqotmpm__ItemsToApproveFinanceDetail';
            track.fieldMap = new Map<String, DataModel.Field>
            {
                'Id' => new DataModel.Field(),
                    'OwnerId' => new DataModel.Field(),
                    'CreatedById' => new DataModel.Field(),
                    'LastModifiedById' => new DataModel.Field()
                    };
                        track.whereCaluses = whereCaluses;
            track.operator = 'AND';
            track.whereCaluseSubs = whereCaluseSubs;
            track.sortBy = 'Name';
            track.sortDirection = 'ASC';
            track.amountOfRecords = 40000;
            String param = JSON.serialize(track);
            System.assertEquals('Campaign' , track.objectApiName);
            Test.startTest();
            keyInsightDashBoard.getRecords(param);
            //DataHandler.getLookupResults(param);
            Test.stopTest();
        }catch(exception ex){
            
        }
          System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void getCalloutResponseContentListTest() {
        DataModel.Track track = new DataModel.Track();
        track.URL = 'https://wwww.musqot.com';
        track.Authorization = 'Bearer ndjhqwihdiuqdnq';
        track.ContentType = 'application/json';
        String param = JSON.serialize(track);
        DataModel.Result result = keyInsightDashBoard.getCalloutResponseContentList(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void getCalloutResponseContentMapTest() {
        DataModel.Track track = new DataModel.Track();
        track.URL = 'https://wwww.musqot.com';
        track.Authorization = 'Bearer ndjhqwihdiuqdnq';
        track.ContentType = 'application/json';
        String param = JSON.serialize(track);
        DataModel.Result result = keyInsightDashBoard.getCalloutResponseContentMap(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    //public static List<KeyInsightDashboard__c> saveKidRecords(List<KeyInsightDashboard__c> kidRecList){
    @isTest
    static void saveKidRecordsTest() {
        //method signature 
        //getKeyInsightDashboardRecord(String name,String frequency,Id userId,String proofUserId,Double kid)
        KeyInsightDashboard__c kidRec = TestDataSetup.getKeyInsightDashboardRecord('test','Daily',Userinfo.getUserId(),'000',000);
        insert kidRec;
        List<KeyInsightDashboard__c> kidRecList = new List<KeyInsightDashboard__c>();
        kidRecList.add(kidRec);
        List<KeyInsightDashboard__c> recList = keyInsightDashBoard.saveKidRecords(kidRecList);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void putCalloutResponseContentsTest() {
        Test.setMock(HttpCalloutMock.class, new MockHttpPUTResponseTest());
        DataModel.Track track = new DataModel.Track();
        track.URL = 'https://wwww.musqot.com';
        track.Authorization = 'Bearer ndjhqwihdiuqdnq';
        track.ContentType = 'application/json';
        String param = JSON.serialize(track);
        keyInsightDashBoard.putCalloutResponseContents(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void postCalloutResponseContentsTest() {
        Test.setMock(HttpCalloutMock.class, new MockHttpPOSTResponseTest());
        DataModel.Track track = new DataModel.Track();
        track.URL = 'https://wwww.musqot.com';
        track.Authorization = 'Bearer ndjhqwihdiuqdnq';
        track.ContentType = 'application/json';
        String param = JSON.serialize(track);
        keyInsightDashBoard.postCalloutResponseContents(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void parseDataFromJwtTokenTest() {
        String token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjJzRGNPUkhXMzM2Vm9pcHFNTWJmZyJ9.eyJodHRwczovL3Rlc3RhcHAuZ2V0LXByb29mLmNvbS9vcmdJZCI6MTg4OSwiZW1haWwiOiJha2F5NDdAd2Vib2FwcHMuY29tIiwiaXNzIjoiaHR0cHM6Ly9wcm9vZi10ZXN0LnVzLmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHwxNjM3IiwiYXVkIjoiNnJUYWR3TE80NTRNQWRIdkFodFdncFI1M21lMnFzUE0iLCJpYXQiOjE2MTQzMTc0MjEsImV4cCI6MTYxNDM1MzQyMX0.aFdz2e6LO8LrxknlW0rxzNYxmHdifFuIWPaDvA2fBX5kujbL5-j6xEA0Q3D07YsY6Q6brcKk_7BZl3mprvnUg289CsTI96yFAsWxd5qdc2LuLDDFxGM1LhoBRrcVmEXLmstecfJgN7hMZ-SFBWP3HPb5duJkrXmIsQvhtTOTF4Tr42SnuyeyL9YD-ZeeCksbug5PK-gxqLMZIDtvDGKhIzynxsIQ-acQc06poFTul99qCjvruCraiSg4u8jooiZWa3kJShL__6Y7MtiqUAHl0pj_XmgRIaTb2KhFr7mNQO7bXjPAjN6S8c5HWmmv0t6atG2V8MD9NoDZVlAVzv6Flg';
        /*JSON.stringify({
        recordId: this.sfUserId,
      });*/
        String userId = UserInfo.getUserId();
        User currentUser = [select id, musqotmp__ProofIdToken__c from User where Id = :userId];
        currentUser.musqotmp__ProofIdToken__c = token;
        update currentUser;
        DataModel.Track track = new DataModel.Track();
        track.recordId = userId;
        String param = JSON.serialize(track);
        DataModel.Result result = keyInsightDashBoard.parseDataFromJwtToken(param);
        token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjJzRGNPUkhXMzM2Vm9pcHFNTWJmZyJ9.eyJodHRwczovL3Rlc3RhcHAuZ2V0LXByb29m_LmNvbS9vcmdJZCI6MTg4OSwiZW1haWwiOiJha2F5NDdAd2Vib2FwcHMuY29tIiwiaXNzIjoiaHR0cHM6Ly9wcm9vZi10ZXN0LnVzLmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHwxNjM3IiwiYXVkIjoiNnJUYWR3TE80NTRNQWRIdkFodFdncFI1M21lMnFzUE0iLCJpYXQiOjE2MTQzMTc0MjEsImV4cCI6MTYxNDM1MzQyMX0.aFdz2e6LO8LrxknlW0rxzNYxmHdifFuIWPaDvA2fBX5kujbL5-j6xEA0Q3D07YsY6Q6brcKk_7BZl3mprvnUg289CsTI96yFAsWxd5qdc2LuLDDFxGM1LhoBRrcVmEXLmstecfJgN7hMZ-SFBWP3HPb5duJkrXmIsQvhtTOTF4Tr42SnuyeyL9YD-ZeeCksbug5PK-gxqLMZIDtvDGKhIzynxsIQ-acQc06poFTul99qCjvruCraiSg4u8jooiZWa3kJShL__6Y7MtiqUAHl0pj_XmgRIaTb2KhFr7mNQO7bXjPAjN6S8c5HWmmv0t6atG2V8MD9NoDZVlAVzv6Flg';
        //token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjJzRGNPUkhXMzM2Vm9pcHFNTWJmZyJ9.eyJodHRwczovL3Rlc3RhcHAuZ2V0LXByb29m_LmNvbS9vcmdJZCI6MTg4OSwiZW1haWwiOiJha2F5NDdAd2Vib2FwcHMuY29tIiwiaXNzIjoiaHR0cHM6Ly9wcm9vZi10ZXN0LnVzLmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHwxNjM3IiwiYXVkIjoiNnJUYWR3TE80NTRNQWRIdkFodFdncFI1M21lMnFzUE0iLCJpYXQiOjE2MTQzMTc0MjEsImV4cCI6MTYxNDM1MzQyMX0.aFdz2e6LO8LrxknlW0rxzNYxmHdifFuIWPaDvA2fBX5kujbL5-j6xEA0Q3D07YsY6Q6brcKk_7BZl3mprvnUg289CsTI96yFAsWxd5qdc2LuLDDFxGM1LhoBRrcVmEXLmstecfJgN7hMZ-SFBWP3HPb5duJkrXmIsQvhtTOTF4Tr42SnuyeyL9YD-ZeeCksbug5PK-gxqLMZIDtvDGKhIzynxsIQ-acQc06poFTul99qCjvruCraiSg4u8jooiZWa3kJShL__6Y7MtiqUAHl0pj_XmgRIaTb2KhFr7mNQO7bXjPAjN6S8c5HWmmv0t6atG2V8MD9NoDZVlAVzv6Flg';
        currentUser.musqotmp__ProofIdToken__c = token;
        update currentUser;
        result = keyInsightDashBoard.parseDataFromJwtToken(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void readKIDSetupSettingsTest() {
        DataModel.Result result = keyInsightDashBoard.readKIDSetupSettings('Param');
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void datatableTest() {
        List<DataModel.Action> headerLevelactions = new List<DataModel.Action>();
        List<DataModel.Action> rowLevelactions = new List<DataModel.Action>();
        DataModel.Action action = new DataModel.Action();
        action.label = 'Wilson';
        action.name = 'Bob Wilson';
        action.iconName = 'Avatar';
        headerLevelactions.add(action);
        rowLevelactions.add(action);
        DataModel.Field field = new DataModel.Field();
        field.name = 'Musqotmpm__Description__c';
        field.label = 'Description';
        field.type = 'textarea';
        field.visible = true;
        field.headerLevelactions = headerLevelactions;
        DataModel.Track track = new DataModel.Track();
        track.component = 'dataTables';
        track.objectApiName = 'Campaign';
        track.fieldApiName = 'Name';
        track.fieldMap = new Map<String, DataModel.Field>{
            'Id' => field,
                'OwnerId' => new DataModel.Field(),
                'CreatedById' => new DataModel.Field(),
                'LastModifiedById' => new DataModel.Field()
                };
                    track.rowLevelactions = rowLevelactions;
        track.relatedListType = 'finance';
        String param = JSON.serialize(track);
        System.assertEquals('Campaign' , track.objectApiName);
        Test.startTest();
        keyInsightDashBoard.datatable(param);
        Test.stopTest();
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void exceptions(){
        Id execCampRecTypeIdId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Type').getRecordTypeId();
        exception ex;
        DataModel.Exceptions exceptions = new DataModel.Exceptions();
        exceptions.component = 'exceptions';
        exceptions.ex = ex;
        String param = JSON.serialize(exceptions);
        System.assertEquals('exceptions' , exceptions.component);
        Id tpltPlanRecordTypeId = Schema.SObjectType.musqotmp__MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('TemplatePlan').getRecordTypeId();
        musqotmp__MarketingPlan__c  templatePlan = TestDataSetup.getMarketingPlan('Template Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
        templatePlan.recordTypeId = tpltPlanRecordTypeId;
        insert templatePlan;
		Schema.Campaign templateCampaign = TestDataSetup.getCampaign('Division A Template',Date.Today(),Date.Today()+1,'In Progress',templatePlan);
        templateCampaign.recordTypeId= execCampRecTypeIdId;
        insert templateCampaign;
        Test.startTest();
        //keyInsightDashBoard.parseDataFromJwtToken(param);
        Id mktPlanRecordTypeId = Schema.SObjectType.musqotmp__MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
        musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
        marketingPlan.recordTypeId = mktPlanRecordTypeId;
        insert marketingPlan;
        Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division C',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
        //campaignRecord.recordTypeId= execCampRecTypeIdId;
       // insert campaignRecord;
                
        musqotmp__CampaignTemplate__c ct = TestDataSetup.getCampaignTemplate(campaignRecord.Id);
        DataHandler.deepCloneCampaignTemplate(param,campaignRecord.Id);
        DataModel.Result result = keyInsightDashBoard.readKIDSetupSettings(param);
        
        
        
		//DataHandler.cloneCampaignTemplate(param,templateCampaign.Id);
        //DataHandler.parseDataFromJwtToken(param);
        //DataModel.Result res = Organization.getOrganizationId(param);
        Test.stopTest();
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    /*@isTest static void testCallout() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseTest());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        MyCalloutService tw = new MyCalloutService();
        tw.methodGet();

    }*/
    @isTest
    static void testDataModel(){
        Id execCampRecTypeIdId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Type').getRecordTypeId();
        Id mktPlanRecordTypeId = Schema.SObjectType.musqotmp__MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
        musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
        marketingPlan.recordTypeId = mktPlanRecordTypeId;
        insert marketingPlan;
        Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division A',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
        campaignRecord.recordTypeId= execCampRecTypeIdId;
        insert campaignRecord;
        Test.startTest();
        DataModel.ResultAPI resAPI = new DataModel.ResultAPI();
        resAPI.objectTypeLabel = 'TestLabel';
        resAPI.totalRecord = 10;
        resAPI.recordStart = 1;
        resAPI.recordEnd = 10;
        resAPI.pageNumber = 3;
        resAPI.multiCurrencyOrganization = false;
        resAPI.defaultCurrency = 'SEK';
        resAPI.Toast = null;
        resAPI.sObjects = null;
        resAPI.datatable = null;
        resAPI.sObj = null;
        resAPI.fields = null;
        resAPI.objectApiName = 'Campaign';
        resAPI.recordId = campaignRecord.Id;
        resAPI.sObjectList = null;
        resAPI.fieldMap = null;
        resAPI.customSetting = null;
       	resAPI.stringMap = null;
        resAPI.files = null;
        resAPI.isContributor = 'Yes';
        resAPI.contentIds = null;
        resAPI.contributorEmails = null;
        resAPI.sObjectUnMap = null;       
        resAPI.resultMap = null;
        resAPI.resultList = null;
        resAPI.customFieldMap = null;
        resAPI.sObjIds = null;
        DataModel.proofAuthMap authMap = new DataModel.proofAuthMap();
        authMap.orgId = 333; // in json: https://testapp.get-proof.com/orgId
        authMap.nickname = 'TestNickName';
        authMap.name = 'TestName';
        authMap.picture = 'Yes';
        authMap.updated_at = null;
        authMap.email = 'test@test.com';
        authMap.iss  = 'https://proof-test.auth0.com/'; 
        authMap.sub = 'auth0|910';
        authMap.aud  = 'oxafaRMhPDb70jWNxjVe2zIPwt0AftNc';
        authMap.iat = 1614764672;
        authMap.exp = 1614800672;
        
        DataModel.Result resObj = new DataModel.Result();
        resObj.objectTypeLabel = 'Campaign';
        resObj.totalRecord = 1000;
        resObj.recordStart = 1;
        resObj.recordEnd = 1000;
        resObj.pageNumber = 5;
        resObj.multiCurrencyOrganization = false;
        resObj.defaultCurrency = 'SEK';
        resObj.toast = null;
        resObj.sObjects = null;
        resObj.datatable = null;
        resObj.sObj = null;
        resObj.fields = null;
        resObj.objectApiName = 'Campaign';
        resObj.recordId = campaignRecord.Id;
        resObj.sObjectList = null;
        resObj.fieldMap  = null;
        resObj.customSetting = null;
        resObj.stringMap = null; 
        resObj.files = null;
        resObj.isContributor = 'NO';
        resObj.contentIds = null;
        resObj.contributorEmails  = null;
        resObj.sObjectUnMap = null;
        resObj.resultMap = null;
        resObj.resultList = null;
        resObj.customFieldMap = null;
        resObj.sObjIds  = null;
        resObj.dbSaveResult = null;
        resObj.objectFieldSetMap = null;
        Test.stopTest();
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
}