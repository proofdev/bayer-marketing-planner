public with sharing class keyInsightDashBoard {
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getRecords(String param) {
        DataModel.Result result = DataHandler.getRecords(param);
        return result;
    }
    @AuraEnabled
    public static DataModel.Result getCalloutResponseContentList(String param) {
        DataModel.Result result = DataHandler.getCalloutResponseContentList(param); 
        return result;
    }
    @AuraEnabled
    public static DataModel.Result getCalloutResponseContentMap(String param) {
        DataModel.Result result = DataHandler.getCalloutResponseContents(param); 
        return result;
    }
    @AuraEnabled
    public static List<KeyInsightDashboard__c> saveKidRecords(List<KeyInsightDashboard__c> kidRecList){
        deleteKidRecords(kidRecList);
        List<String> unikIdList = new List<String>();
        Map<String,KeyInsightDashboard__c> mapFromProof = new Map<String,KeyInsightDashboard__c>();
        //System.debug('kidRecList[0]'+kidRecList[0]);
        //System.debug('kidRecList from Proof'+kidRecList.size());
        List<KeyInsightDashboard__c> existingList = new List<KeyInsightDashboard__c>();
        List<KeyInsightDashboard__c> newList = new List<KeyInsightDashboard__c>();
        List<KeyInsightDashboard__c> updateList = new List<KeyInsightDashboard__c>();
        List<KeyInsightDashboard__c> kidList = new List<KeyInsightDashboard__c>();
        //map<id,KeyInsightDashboard__c> kidMap = new map<id,KeyInsightDashboard__c>();
        if(kidRecList.size() > 0){
            String userId = UserInfo.getUserId();
            for(KeyInsightDashboard__c kid: kidRecList){
                unikIdList.add(kid.musqotmp__keyInsightDashboardId__c+''+userId);
                KeyInsightDashboard__c newKidObj = new KeyInsightDashboard__c();
                newKidObj.Name = kid.Name;
                newKidObj.musqotmp__CreatedOn__c = kid.musqotmp__CreatedOn__c;
                newKidObj.musqotmp__Frequency__c = kid.musqotmp__Frequency__c;
                newKidObj.musqotmp__KIDUser__c = userId;
                newKidObj.musqotmp__ProofUserId__c = kid.musqotmp__ProofUserId__c;
                newKidObj.musqotmp__Status__c = kid.musqotmp__Status__c;
                newKidObj.musqotmp__isAccessible__c = kid.musqotmp__isAccessible__c;
                newKidObj.musqotmp__keyInsightDashboardId__c = kid.musqotmp__keyInsightDashboardId__c;
                mapFromProof.put(kid.musqotmp__keyInsightDashboardId__c+''+userId,newKidObj);
            }
            existingList = [SELECT Id,Name,musqotmp__KIDUniqId__c, musqotmp__keyInsightDashboardId__c,musqotmp__KIDUser__c 
                            from KeyInsightDashboard__c 
                            where musqotmp__KIDUniqId__c IN :unikIdList];
        }
        if(existingList.size() == 0){
            if(Schema.sObjectType.KeyInsightDashboard__c.fields.Name.isCreateable()){
                Database.insert(kidRecList);
            }
            return kidRecList;
        } else {
            Map<String,KeyInsightDashboard__c> proofRecMap = new Map<String,KeyInsightDashboard__c>();
            Map<String,KeyInsightDashboard__c> existingRecMap = new Map<String,KeyInsightDashboard__c>();
            /*for(KeyInsightDashboard__c kidObj: kidRecList){
                //System.debug('kidRec'+kidObj);
                proofRecMap.put(kidObj.musqotmp__keyInsightDashboardId__c+''+kidObj.musqotmp__KIDUser__c,kidObj);
            }
            //System.debug('proofRecMap'+proofRecMap);*/
            for(KeyInsightDashboard__c kidObj: existingList){
                //System.debug('kidRecExisting'+kidObj);
                existingRecMap.put(kidObj.musqotmp__KIDUniqId__c,kidObj);
            }
            //System.debug('existingRecMap'+existingRecMap);
            for(String uId: unikIdList){
                if(existingRecMap.get(uId) != NULL){
                    KeyInsightDashboard__c kidToUPdate = mapFromProof.get(uId);
                    kidToUPdate.Id = existingRecMap.get(uId).Id;
                    updateList.add(kidToUPdate);
                    //System.debug('updateListValue:'+kidToUPdate);
                } else {
                    newList.add(mapFromProof.get(uId));
                    //System.debug('newListValue:'+mapFromProof.get(uId));
                }
            }
            if(newList.size() > 0){
                if(Schema.sObjectType.KeyInsightDashboard__c.fields.Name.isCreateable() &&
                    Schema.sObjectType.KeyInsightDashboard__c.fields.musqotmp__keyInsightDashboardId__c.isCreateable() &&
                   Schema.sObjectType.KeyInsightDashboard__c.fields.musqotmp__KIDUser__c.isCreateable()){
                    Database.insert(newList);
                }
                //insert newList;
                kidList.addAll(newList);
            }
            if(updateList.size() > 0){
                if(Schema.sObjectType.KeyInsightDashboard__c.fields.Name.isUpdateable() &&
                   Schema.sObjectType.KeyInsightDashboard__c.fields.musqotmp__keyInsightDashboardId__c.isUpdateable() &&
                   Schema.sObjectType.KeyInsightDashboard__c.fields.musqotmp__KIDUser__c.isUpdateable()){
                	update updateList;   
                }
                kidList.addAll(updateList);
            }
            
            /*map<id,KeyInsightDashboard__c> kidmap = new map<id,KeyInsightDashboard__c>();
            kidmap.putall(updateList);
            if(kidmap.size()>0){
            update kidmap.values();
            }*/
                        
        }
        return kidList;
    }
    @AuraEnabled
    public static void deleteKidRecords(List<KeyInsightDashboard__c> kidRecList){ 
        List<KeyInsightDashboard__c> KIDListToBeDeleted = new List<KeyInsightDashboard__c>();
        List<KeyInsightDashboard__c> KIDList4Upsert = new List<KeyInsightDashboard__c>();
        Map<String,KeyInsightDashboard__c> userKIDIdToKIDObjectMap = new Map<String,KeyInsightDashboard__c>();
        List<KeyInsightDashboard__c>  existingList = new List<KeyInsightDashboard__c>();
        //try{
            //System.debug('kidUserId'+ kidRecList[0].musqotmp__KIDUser__c);
            //System.debug('kidRecList'+ kidRecList);
            if(kidRecList.size() > 0){
                existingList = [SELECT Id,Name, musqotmp__keyInsightDashboardId__c,musqotmp__KIDUser__c from KeyInsightDashboard__c where musqotmp__KIDUser__c = :kidRecList[0].musqotmp__KIDUser__c];
                //System.debug('existingList'+ existingList.size());
                if(existingList.size() > 0){
                    for(KeyInsightDashboard__c kid :kidRecList){
                        userKIDIdToKIDObjectMap.put(kid.musqotmp__KIDUser__c+''+kid.musqotmp__keyInsightDashboardId__c,kid);
                    }
                    for(KeyInsightDashboard__c kid : existingList){
                        if((userKIDIdToKIDObjectMap.get(kid.musqotmp__KIDUser__c+''+kid.musqotmp__keyInsightDashboardId__c)) == NULL){
                            KIDListToBeDeleted.add(kid);
                        } else {
                           KIDList4Upsert.add(kid); 
                        }
                    }
                    if(KIDListToBeDeleted.size() > 0){
                        //system.debug('KIDListToBeDeleted'+KIDListToBeDeleted);
                        if(KeyInsightDashboard__c.sObjectType.getDescribe().isDeletable()){
                        	delete KIDListToBeDeleted; 
                        	Database.emptyRecycleBin(KIDListToBeDeleted);   
                        }
                    }	
                    /*if(KIDList4Upsert.size() > 0){
                        //System.debug('KIDList4Upsert size'+KIDList4Upsert.size());
                        //upsert KIDList4Upsert;
                    }*/      
                }    
            }
        //}catch(Exception e){
          //  System.debug('--->'+e);
        //}
        //return KIDList4Upsert; 
    }
    @AuraEnabled
    public static DataModel.Result putCalloutResponseContents(String param){
        DataModel.Result result = DataHandler.putCalloutResponseContents(param);
        return result;
    }
    @AuraEnabled
    public static DataModel.Result postCalloutResponseContents(String param){
        DataModel.Result result = DataHandler.postCalloutResponseContents(param);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result parseDataFromJwtToken(String param){
        DataModel.Result result = DataHandler.parseDataFromJwtToken(param);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result readKIDSetupSettings(String param){
        DataModel.Result result = DataHandler.readKIDSetupSettings(param);
        return result;    
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result datatable(String param) {
        DataModel.Result result = DataHandler.datatable(param);
        return result;
    }
}