@isTest
private class DataHandlerTest {
    @isTest
    static void handleToast(){
        NoAccessException ex = new NoAccessException();
        try{
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            DataHandler.handleToast(exceptions);
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
    @isTest
    static void sendToast(){
        NoAccessException ex = new NoAccessException();
        try{
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            DataModel.Body body = new DataModel.Body();
            body.message = 'funds avilable';
            DataModel.Toast toast = new DataModel.Toast();
            toast.body = body;
            toast.component = 'sendToast';
            DataModel.Track track = new DataModel.Track();
            track.toast = toast;
            track.component = 'sendToast';
            System.assertEquals('sendToast' , track.component);
            String param = JSON.serialize(track);
            Test.startTest();
            Campaign.sendToast(param);
            Test.stopTest();
            //jsonGenData.writeObjectField('fieldMap', (Object) JSON.deserializeUntyped(jsonGenDataFieldMap.getAsString()));
            
            //jsonGenData.writeStartObject();
            //jsonGenData.writeObjectField('component', 'sendToast');
            //jsonGenData.writeEndObject();
            //DataHandler.sendToast(jsonGenData.getAsString());
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
    @isTest
    static void datatable(){
        NoAccessException ex = new NoAccessException();
        try{
            Date sDate = Date.newInstance(2019, 01, 01);
            Date eDate = Date.newInstance(2025, 01, 01);
            Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
            musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
            marketingPlan.Name = 'Marketing Plan';
            marketingPlan.musqotmp__StartDate__c = sDate;
            marketingPlan.musqotmp__EndDate__c = eDate;
            marketingPlan.musqotmp__Description__c = '';
            marketingPlan.musqotmp__CampaignTemplate__c = false;
            marketingPlan.musqotmp__Status__c = 'In Progress';
            marketingPlan.musqotmp__MarketingArea__c = 'Europe';
            marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
            marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
            marketingPlan.RecordTypeId = recordTypeId;
            insert marketingPlan;
            musqotmp__MarketingPlanMember__c marketingPlanMember = new musqotmp__MarketingPlanMember__c();
            marketingPlanMember.musqotmp__MarketingPlan__c = marketingPlan.Id;
            marketingPlanMember.musqotmp__User__c = UserInfo.getUserId();
            insert marketingPlanMember;
            Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('Campaign').getDescribe().getSObjectType();
            SObject newSobjectCampaign = objectDef.newSobject();
            newSobjectCampaign.put('Name', 'Campaign');
            newSobjectCampaign.put('musqotmp__MarketingPlan__c', marketingPlan.Id);
            newSobjectCampaign.put('StartDate', sDate);
            newSobjectCampaign.put('EndDate', eDate);
            insert newSobjectCampaign;
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'datatable');
            jsonGenData.writeObjectField('recordId', marketingPlan.Id);
            jsonGenData.writeObjectField('objectApiName', 'Campaign');
            JSONGenerator jsonGenDataIdField = JSON.createGenerator(false);
            jsonGenDataIdField.writeStartObject();
            jsonGenDataIdField.writeObjectField('name', 'Id');
            jsonGenDataIdField.writeEndObject();
            JSONGenerator jsonGenDataNameField = JSON.createGenerator(false);
            jsonGenDataNameField.writeStartObject();
            jsonGenDataNameField.writeObjectField('name', 'Name');
            jsonGenDataNameField.writeEndObject();
            JSONGenerator jsonGenDataMarketingPlanIdField = JSON.createGenerator(false);
            jsonGenDataMarketingPlanIdField.writeStartObject();
            jsonGenDataMarketingPlanIdField.writeObjectField('name', 'musqotmp__MarketingPlan__c');
            jsonGenDataMarketingPlanIdField.writeEndObject();
            JSONGenerator jsonGenDataFieldMap = JSON.createGenerator(false);
            jsonGenDataFieldMap.writeStartObject();
            jsonGenDataFieldMap.writeObjectField('Id', (Object) JSON.deserializeUntyped(jsonGenDataIdField.getAsString()));
            jsonGenDataFieldMap.writeEndObject();
            jsonGenDataFieldMap.writeStartObject();
            jsonGenDataFieldMap.writeObjectField('Name', (Object) JSON.deserializeUntyped(jsonGenDataNameField.getAsString()));
            jsonGenDataFieldMap.writeEndObject();
            jsonGenDataFieldMap.writeStartObject();
            jsonGenDataFieldMap.writeObjectField('musqotmp__MarketingPlan__c', (Object) JSON.deserializeUntyped(jsonGenDataMarketingPlanIdField.getAsString()));
            jsonGenDataFieldMap.writeEndObject();
            jsonGenData.writeObjectField('fieldMap', (Object) JSON.deserializeUntyped(jsonGenDataFieldMap.getAsString()));
            jsonGenData.writeObjectField('fieldSetApiName', 'musqotmp__CampaignFinanceList');
            jsonGenData.writeObjectField('relatedListType', 'finance');
            jsonGenData.writeObjectField('linkableNameField', true);
            jsonGenData.writeObjectField('pageNumber', 1);
            jsonGenData.writeObjectField('amountOfRecords', 2);
            jsonGenData.writeEndObject();
            Campaign.datatable(jsonGenData.getAsString());
            Campaign.datatable('');
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
    @isTest
    static void getDynamicObjectInfo(){
        NoAccessException ex = new NoAccessException();
        try{
            Date sDate = Date.newInstance(2019, 01, 01);
            Date eDate = Date.newInstance(2025, 01, 01);
            Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
            musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
            marketingPlan.Name = 'Marketing Plan';
            marketingPlan.musqotmp__StartDate__c = sDate;
            marketingPlan.musqotmp__EndDate__c = eDate;
            marketingPlan.musqotmp__Description__c = '';
            marketingPlan.musqotmp__CampaignTemplate__c = false;
            marketingPlan.musqotmp__Status__c = 'In Progress';
            marketingPlan.musqotmp__MarketingArea__c = 'Europe';
            marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
            marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
            marketingPlan.RecordTypeId = recordTypeId;
            insert marketingPlan;
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'getDynamicObjectInfo');
            jsonGenData.writeEndObject();
            DataHandler.getDynamicObjectInfo(jsonGenData.getAsString(), marketingPlan.Id);
            DataHandler.getDynamicObjectInfo(jsonGenData.getAsString(), '');
            Campaign.DynamicObjectInfo(jsonGenData.getAsString(), marketingPlan.Id);
            Campaign.DynamicObjectInfo(jsonGenData.getAsString(), '');
            Campaign.getDynamicObjectInfo(jsonGenData.getAsString(), marketingPlan.Id);
            Campaign.getDynamicObjectInfo(jsonGenData.getAsString(), '');
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
   /* @isTest
    static void getMarketingPlanView(){
        NoAccessException ex = new NoAccessException();
        try{
             
            musqotmp__Gantt__c gantt = new musqotmp__Gantt__c();
            gantt.Name = 'Gantt';
            gantt.musqotmp__PlanType__c = 'Marketing Plan';
            gantt.musqotmp__DateRange__c = 'THIS_QUARTER,NEXT_QUARTER';
            gantt.musqotmp__BarText__c = 'Name';
            gantt.musqotmp__Country__c = 'Austria,Canada';
            gantt.musqotmp__IncludeMarketingActivities__c = true;
            gantt.musqotmp__IncludeMarketingJourney__c = true;
            gantt.musqotmp__GanttView__c = 'Standard';
            //gantt.musqotmp__Global__c = true;
            gantt.musqotmp__IncludeMarketingCloudActivities__c = true;
            insert gantt;
            musqotmp__CampaignManagerSetup__c campaignManagerSetup = musqotmp__CampaignManagerSetup__c.getInstance();
            campaignManagerSetup.musqotmp__BarColorMarketingPlan__c  = '#330099';
            campaignManagerSetup.musqotmp__BarColorCampaign__c = '#444555';
            campaignManagerSetup.musqotmp__BarColorMarketingActivity__c = '#606060';
            insert campaignManagerSetup;
            
            Date sDate = Date.newInstance(2019, 01, 01);
            Date eDate = Date.newInstance(2025, 01, 01);
            Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
            musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
            marketingPlan.Name = 'Marketing Plan';
            marketingPlan.musqotmp__StartDate__c = sDate;
            marketingPlan.musqotmp__EndDate__c = eDate;
            marketingPlan.musqotmp__Description__c = '';
            marketingPlan.musqotmp__Country__c = 'Austria';
             marketingPlan.musqotmp__Country_Code__c = 'AT';
            marketingPlan.musqotmp__Therapeutic_Area__c = 'Cardiovascular';
             marketingPlan.musqotmp__Possible_Tactical_Campaign_Types__c = 'Commercial';
             marketingPlan.musqotmp__Portfolio_Commercial__c = 'Cardiovascular Portfolio';
             marketingPlan.musqotmp__Product_Commercial__c = 'Fineranone';
             marketingPlan.musqotmp__Product_Code_Commercial__c = 'FINE';
            marketingPlan.musqotmp__CampaignTemplate__c = false;
            marketingPlan.musqotmp__Status__c = 'In Progress';
            marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
            marketingPlan.RecordTypeId = recordTypeId;
            insert marketingPlan;
            ID recordTypeIdUn = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('UnknownPlan').getRecordTypeId();
            musqotmp__MarketingPlan__c marketingPlan1 = new musqotmp__MarketingPlan__c();
            marketingPlan1.Name = 'Marketing Plan';
            marketingPlan1.musqotmp__StartDate__c = sDate;
            marketingPlan1.musqotmp__EndDate__c = eDate;
            marketingPlan1.musqotmp__Description__c = '';
            marketingPlan1.musqotmp__Country__c = 'Canada';
            marketingPlan1.musqotmp__CampaignTemplate__c = false;
            marketingPlan1.musqotmp__Status__c = 'In Progress';
            marketingPlan1.musqotmp__PlanType__c = 'Unknown Plan';
            marketingPlan1.RecordTypeId = recordTypeIdUn;
            insert marketingPlan1;
            musqotmp__MarketingPlanMember__c marketingPlanMember = new musqotmp__MarketingPlanMember__c();
            marketingPlanMember.musqotmp__MarketingPlan__c = marketingPlan.Id;
            marketingPlanMember.musqotmp__User__c = UserInfo.getUserId();
            musqotmp__MarketingPlanMember__c marketingPlanMember1 = new musqotmp__MarketingPlanMember__c();
            marketingPlanMember1.musqotmp__MarketingPlan__c = marketingPlan1.Id;
            marketingPlanMember1.musqotmp__User__c = UserInfo.getUserId();
            insert marketingPlanMember;
            Id recordTypeIdCam = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Customer_Evolution_Campaign').getRecordTypeId();
            Schema.Campaign campaign = new Schema.Campaign();
            campaign.Name = 'Gantt Cam';
            campaign.musqotmp__MarketingPlan__c = marketingPlan.Id;
            campaign.StartDate = sDate;
            campaign.EndDate = eDate;
            campaign.Status = 'In Progress';
            campaign.IsActive = true;
            campaign.Type = 'Other';
            campaign.musqotmp__IsFolder__c = false;
            campaign.RecordTypeId = recordTypeIdCam;
            campaign.musqotmp__Country__c = marketingPlan.musqotmp__Country__c;
            insert campaign;
            Id recordTypeIdCam1 = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Tactical_Campaign').getRecordTypeId();
            Schema.Campaign campaign1 = new Schema.Campaign();
            campaign1.Name = 'Gantt Campaign';
            campaign1.musqotmp__MarketingPlan__c = marketingPlan.Id;
            campaign1.StartDate = sDate;
            campaign1.ParentId = campaign.Id;
            campaign1.EndDate = eDate;
            campaign1.Status = 'In Progress';
            campaign1.IsActive = true;
            campaign1.Type = 'Other';
            campaign1.musqotmp__IsFolder__c = false;
            campaign1.RecordTypeId = recordTypeIdCam1;
            campaign1.musqotmp__Country__c = marketingPlan.musqotmp__Country__c;
            insert campaign1;
            musqotmp__MarketingJourney__c marketingjourney=new musqotmp__MarketingJourney__c();
            marketingjourney.Name='name';
            marketingjourney.musqotmp__StartDate__c=sDate;
            marketingjourney.musqotmp__EndDate__c=eDate;
            marketingjourney.musqotmp__JourneyStatus__c='In Progress';
            marketingjourney.musqotmp__Campaign__c = campaign.Id;
            insert marketingjourney;
               Id recordTypeIdMA = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Approved_Email').getRecordTypeId();
            musqotmp__MarketingActivity__c marketingactivities=new musqotmp__MarketingActivity__c();
            marketingactivities.Name='Name';
            marketingactivities.musqotmp__StartDate__c=sDate;
            marketingactivities.musqotmp__EndDate__c= eDate;
            marketingactivities.musqotmp__MarketingJourney__c =marketingjourney.Id;
            marketingactivities.musqotmp__Status__c='Planned';
            marketingactivities.musqotmp__Type__c='Banner';
            marketingactivities.musqotmp__Campaign__c=campaign.Id;
             marketingactivities.RecordTypeId = recordTypeIdMA;
            insert marketingactivities;
            //   System.debug('marketingactivities '+marketingactivities);
            
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'marketingPlanView-getMarketingPlanView');
            
            jsonGenData.writeEndObject();
            //Gantt.getMarketingPlanView(jsonGenData.getAsString(), gantt.Id);
            DataHandler.getMarketingPlanView(jsonGenData.getAsString(), gantt.Id);
            DataHandler.getMarketingPlanView('', '');
            DataHandler.getMarketingPlanView(jsonGenData.getAsString(), campaignManagerSetup.Id);
            DataHandler.getMarketingPlanView(jsonGenData.getAsString(), marketingPlanMember.Id);
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }*/
    
    @isTest
    static void getOrganizationId(){
        NoAccessException ex = new NoAccessException();
        try{
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'getOrganizationId');
            jsonGenData.writeEndObject();
            DataHandler.getOrganizationId(jsonGenData.getAsString());
            DataHandler.getOrganizationId('');
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static DataModel.Result getOrg(){
        NoAccessException ex = new NoAccessException();
        DataModel.Result result = new DataModel.Result();
        try{
            result =  DataHandler.readCampaignManagerSetup();
            result =  DataHandler.readMarketingCloudCredential();
            result =  DataHandler.readKIDSetupSettings('');
            result =  DataHandler.postCalloutResponseContents('');
            result =  DataHandler.getCalloutResponseContents('');
            result =  DataHandler.putCalloutResponseContents('');
            result =  DataHandler.getLookupResults('');
            result =  DataHandler.getCalloutResponseContentList('');
            result =  DataHandler.parseDataFromJwtToken('');
        }catch(Exception e){
        }
        //System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
        return result;
    }
    
    @isTest
    static void getRecords(){
        NoAccessException ex = new NoAccessException();
        try{
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'getRecords');
            jsonGenData.writeEndObject();
            Campaign.getRecords(jsonGenData.getAsString());
            
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
    
     @isTest
    static void getMarketingPlannerView(){
        NoAccessException ex = new NoAccessException();
        try{
             musqotmp__Gantt__c gantt = new musqotmp__Gantt__c();
            gantt.Name = 'Gantt';
            gantt.musqotmp__PlanType__c = 'Marketing Plan';
            gantt.musqotmp__DateRange__c = 'THIS_QUARTER,NEXT_QUARTER';
            gantt.musqotmp__BarText__c = 'Name';
            gantt.musqotmp__Country__c = 'Austria';
            gantt.musqotmp__IncludeMarketingActivities__c = true;
            gantt.musqotmp__IncludeMarketingJourney__c = true;
            gantt.musqotmp__GanttView__c = 'Compact';
            //gantt.musqotmp__Global__c = true;
            //gantt.musqotmp__IncludeMarketingCloudActivities__c = true;
            insert gantt;
            
            musqotmp__CampaignManagerSetup__c campaignManagerSetup = musqotmp__CampaignManagerSetup__c.getInstance();
            campaignManagerSetup.musqotmp__BarColorMarketingPlan__c  = '#330099';
            campaignManagerSetup.musqotmp__BarColorCampaign__c = '#444555';
            campaignManagerSetup.musqotmp__BarColorMarketingActivity__c = '#606060';
            insert campaignManagerSetup;
            
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'marketingPlanView');
          
            jsonGenData.writeEndObject();
            //Gantt.getMarketingPlanView(jsonGenData.getAsString(), gantt.Id);
            DataHandler.getMarketingPlanView(jsonGenData.getAsString(), gantt.Id);
          
           // DataHandler.getMarketingPlanView(jsonGenData.getAsString(), campaignManagerSetup.Id);
            //DataHandler.getMarketingPlanView(jsonGenData.getAsString(), marketingPlanMember.Id);            
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void getMyWorkspace(){
        NoAccessException ex = new NoAccessException();
        try{
            musqotmp__MyWorkspace__c myWorkspace = new musqotmp__MyWorkspace__c();
            myWorkspace.Name = 'My Workspace';
            myWorkspace.musqotmp__PlanType__c = 'Marketing Plan';
            myWorkspace.musqotmp__ShowMe__c = 'All Campaigns';
            myWorkspace.musqotmp__DateRange__c = 'THIS_QUARTER,NEXT_QUARTER';
            myWorkspace.musqotmp__BarText__c = 'Name;Type';
            insert myWorkspace;
            musqotmp__CampaignManagerSetup__c campaignManagerSetup = musqotmp__CampaignManagerSetup__c.getInstance();
            campaignManagerSetup.musqotmp__BarColorMarketingPlan__c  = '#330099';
            campaignManagerSetup.musqotmp__BarColorCampaign__c = '#444555';
            campaignManagerSetup.musqotmp__BarColorMarketingActivity__c = '#606060';
            insert campaignManagerSetup;
            JSONGenerator jsonGenData = JSON.createGenerator(false);
            jsonGenData.writeStartObject();
            jsonGenData.writeObjectField('component', 'getMyWorkspace');
            jsonGenData.writeEndObject();
            DataHandler.getMyWorkspace(jsonGenData.getAsString(), myWorkspace.Id);
            DataHandler.getMyWorkspace(jsonGenData.getAsString(), campaignManagerSetup.Id);
            DataHandler.getMyWorkspace(jsonGenData.getAsString(), '');
            
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
    @isTest
    static void deepCloneCampaignTemplate(){
        //NoAccessException ex = new NoAccessException();
        //try{
        Date sDate = Date.newInstance(2019, 01, 01);
        Date eDate = Date.newInstance(2025, 01, 01);
        Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
        musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
        marketingPlan.Name = 'Marketing Plan';
        marketingPlan.musqotmp__StartDate__c = sDate;
        marketingPlan.musqotmp__EndDate__c = eDate;
        marketingPlan.musqotmp__Description__c = '';
        marketingPlan.musqotmp__Country__c = 'Austria';
        marketingPlan.musqotmp__CampaignTemplate__c = false;
        marketingPlan.musqotmp__Status__c = 'In Progress';
        marketingPlan.musqotmp__MarketingArea__c = 'Europe';
        marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
        marketingPlan.musqotmp__PlanType__c = 'Marketing Plan';
        marketingPlan.musqotmp__CampaignTemplate__c = true;
        marketingPlan.RecordTypeId = recordTypeId;
        insert marketingPlan;
        Id campaignRecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Type').getRecordTypeId();
        Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('Campaign').getDescribe().getSObjectType();
        SObject newSobjectCampaign = objectDef.newSobject();
        newSobjectCampaign.put('Name', 'Campaign');
        newSobjectCampaign.put('musqotmp__MarketingPlan__c', marketingPlan.Id);
        newSobjectCampaign.put('StartDate', sDate);
        newSobjectCampaign.put('EndDate', eDate);
        newSobjectCampaign.put('musqotmp__Country__c', 'Austria');
        newSobjectCampaign.put('RecordTypeId', campaignRecordTypeId);
        insert newSobjectCampaign;
        SObject newSobjectCampaign1 = objectDef.newSobject();
        newSobjectCampaign1.put('Name', 'Campaign1');
        newSobjectCampaign1.put('musqotmp__MarketingPlan__c', marketingPlan.Id);
        newSobjectCampaign1.put('ParentId', newSobjectCampaign.Id);
        newSobjectCampaign1.put('musqotmp__Country__c', 'Austria');
        newSobjectCampaign1.put('StartDate', sDate);
        newSobjectCampaign1.put('EndDate', eDate);
        //newSobjectCampaign1.put('RecordTypeId', campaignRecordTypeId);
        //insert newSobjectCampaign1;
        musqotmp__MarketingActivity__c marketingactivities=new musqotmp__MarketingActivity__c();
        marketingactivities.Name='Name';
        marketingactivities.musqotmp__StartDate__c=sDate;
        marketingactivities.musqotmp__EndDate__c= eDate;
        marketingactivities.musqotmp__Status__c='Planned';
        marketingactivities.musqotmp__Type__c='Banner';
        marketingactivities.musqotmp__Campaign__c=newSobjectCampaign.Id;
        insert marketingactivities;
        //   System.debug('marketingactivities '+marketingactivities);
        musqotmp__MarketingJourney__c marketingjourney=new musqotmp__MarketingJourney__c();
        marketingjourney.Name='name';
        marketingjourney.musqotmp__StartDate__c=sDate;
        marketingjourney.musqotmp__EndDate__c=eDate;
        marketingjourney.musqotmp__JourneyStatus__c='In Progress';
        marketingjourney.musqotmp__Campaign__c=newSobjectCampaign.Id;
        insert marketingjourney;
        
        musqotmp__CampaignTemplate__c newTempalte1 = TestDataSetup.getCampaignTemplate(newSobjectCampaign.Id);
        musqotmp__CampaignTemplate__c newTempalte = new musqotmp__CampaignTemplate__c();
        newTempalte.name = 'Test Template';
        newTempalte.musqotmp__Campaign__c = newSobjectCampaign.Id;
        newTempalte.musqotmp__Retention__c = true;
        insert newTempalte;
        
        JSONGenerator jsonGenData = JSON.createGenerator(false);
        jsonGenData.writeStartObject();
        jsonGenData.writeObjectField('component', 'cloneCampaignTemplate');
        jsonGenData.writeEndObject();
        
        DataModel.track track = new DataModel.track();
        track.campaignName = 'test';
        track.startDate = sDate;
        track.endDate = eDate;
        track.recordId = newSobjectCampaign.Id;
        String param = JSON.serialize(track);
        // System.debug('Json Data '+JSON.serializePretty(jsonGenData));
        Gantt.deepCloneCampaignTemplate(param, newSobjectCampaign.Id);
        Gantt.cloneCampaignTemplate(param, newTempalte.Id);        
        //}catch(exception e){
        //}
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void dataTables(){
        List<DataModel.Action> headerLevelactions = new List<DataModel.Action>();
        List<DataModel.Action> rowLevelactions = new List<DataModel.Action>();
        DataModel.Action action = new DataModel.Action();
        action.label = 'Wilson';
        action.name = 'Bob Wilson';
        action.iconName = 'Avatar';
        headerLevelactions.add(action);
        rowLevelactions.add(action);
        DataModel.Field field = new DataModel.Field();
        field.name = 'Musqotmpm__Description__c';
        field.label = 'Description';
        field.type = 'textarea';
        field.visible = true;
        field.headerLevelactions = headerLevelactions;
        DataModel.Track track = new DataModel.Track();
        track.component = 'dataTables';
        track.objectApiName = 'Campaign';
        track.fieldApiName = 'Name';
        track.fieldMap = new Map<String, DataModel.Field>{
            'Id' => field,
                'OwnerId' => new DataModel.Field(),
                'CreatedById' => new DataModel.Field(),
                'LastModifiedById' => new DataModel.Field()
                };
                    track.rowLevelactions = rowLevelactions;
        track.relatedListType = 'finance';
        String param = JSON.serialize(track);
        System.assertEquals('Campaign' , track.objectApiName);
        Test.startTest();
        DataHandler.datatable(param);
        Test.stopTest();
    }
    @isTest
    static void queryFactorTest(){
        try{
            DataModel.Field field = new DataModel.Field();
            field.name = 'Description';
            field.label = 'Description';
            field.type = 'textarea';
            field.visible = true;
            List<DataModel.whereCaluse> whereCaluses = new List<DataModel.whereCaluse>();
            DataModel.whereCaluse whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereCaluses.add(whereClause);
            List<DataModel.whereCaluse> whereCaluseSubs = new List<DataModel.whereCaluse>();
            whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereClause.operator = 'OR';
            whereCaluseSubs.add(whereClause);
            whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereCaluseSubs.add(whereClause);
            DataModel.Track track = new DataModel.Track();
            track.component = 'DataHandlerTest-getRecords';
            track.objectApiName = 'Campaign';
            //track.fieldSetApiName = 'Musqotmpm__ItemsToApproveFinanceDetail';
            track.fieldMap = new Map<String, DataModel.Field>
            {
                'Id' => new DataModel.Field(),
                    'OwnerId' => new DataModel.Field(),
                    'CreatedById' => new DataModel.Field(),
                    'LastModifiedById' => new DataModel.Field()
                    };
                        track.whereCaluses = whereCaluses;
            track.operator = 'AND';
            track.whereCaluseSubs = whereCaluseSubs;
            track.sortBy = 'Name';
            track.sortDirection = 'ASC';
            track.amountOfRecords = 40000;
            String param = JSON.serialize(track);
            System.assertEquals('Campaign' , track.objectApiName);
            Test.startTest();
            DataHandler.getRecords(param);
            DataHandler.getLookupResults(param);
            Test.stopTest();
        }catch(exception ex){
            
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    
    @isTest
    static void exceptions(){
        exception ex;
        DataModel.Exceptions exceptions = new DataModel.Exceptions();
        exceptions.component = 'exceptions';
        exceptions.ex = ex;
        String param = JSON.serialize(exceptions);
        System.assertEquals('exceptions' , exceptions.component);
        Test.startTest();
        DataHandler.sendToast(param);
        DataHandler.getMarketingPlanView(param, param);
        //DataHandler.getMarketingPlanView(param, param);
        DataHandler.getOrganizationId(param);
        DataHandler.datatable(param);
        DataHandler.getOrganizationId(param);
        DataHandler.getLookupResults(param);
        //DataHandler.readCampaignManagerSetup();
        DataModel.Result res = Organization.getOrganizationId(param);
        Test.stopTest();
    }
    /*@isTest
static void readCampaignManagerSetupTest(){
DataModel.Result result = DataHandler.readCampaignManagerSetup();
SObject objInfo = (Sobject)CampaignManagerSetup__c.getInstance(); 
//('result'+result.sObj);
System.assertEquals(objInfo , result.sObj);
}
@isTest
static void readMarketingCloudCredentialTest(){
DataModel.Result result = DataHandler.readMarketingCloudCredential();
SObject objInfo = (Sobject)MarketingCloudCredential__c.getInstance();
System.assertEquals(objInfo , result.sObj);
}*/
    @isTest
    static void testDataModelResultAPI(){
        DataModel.ResultAPI api = new  DataModel.ResultAPI();
        api = CampaignManagerAPI.readRecords(null); 
        System.assertEquals(null, null, 'api should be null');
    }
    
}