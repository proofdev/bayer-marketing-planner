public class JWTAuthHandler{

	@AuraEnabled public Integer orgId {get;set;} // in json: https://testapp.get-proof.com/orgId
	/*@AuraEnabled public String nickname {get;set;} 
	@AuraEnabled public String name {get;set;} 
	@AuraEnabled public String picture {get;set;} 
	@AuraEnabled public String updated_at {get;set;} */
	@AuraEnabled public String email {get;set;} 
	@AuraEnabled public String iss {get;set;} 
	@AuraEnabled public String sub {get;set;} 
	@AuraEnabled public String aud {get;set;} 
	@AuraEnabled public Integer iat {get;set;} 
	@AuraEnabled public Long exp {get;set;} 

	public JWTAuthHandler(JSONParser parser) {
        musqotmp__KIDSetup__c kisSetupSettings = musqotmp__KIDSetup__c.getInstance();
        If(parser != NULL){
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text.contains('/orgId')) {
                            orgId = parser.getIntegerValue();
                            //System.debug('orgId'+orgId);
                        /*} else if (text == 'nickname') {
                            nickname = parser.getText();
                        } else if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'picture') {
                            picture = parser.getText();
                        } else if (text == 'updated_at') {
                            updated_at = parser.getText();*/
                        } else if (text == 'email') {
                            email = parser.getText();
                        } else if (text == 'iss') {
                            iss = parser.getText();
                        } else if (text == 'sub') {
                            sub = parser.getText();
                        } else if (text == 'aud') {
                            aud = parser.getText();
                        } else if (text == 'iat') {
                            iat = parser.getIntegerValue();
                        } else if (text == 'exp') {
                            exp = parser.getLongValue();
                        } else {
                            //System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
	}
	
	
	public static JWTAuthHandler parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new JWTAuthHandler(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
}