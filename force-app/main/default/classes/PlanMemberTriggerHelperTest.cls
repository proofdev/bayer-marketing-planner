/************************************************************************************************************************************
* Developer			Date				Description
* ---------------------------------------------------------------------------------------------------------------------------------------	                                                               												campaigncontributorrelatedlist 
* Abhishek          06/01/2021			Test class for PlanMemberTrigger & PlanMemberTriggerHelper
**************************************************************************************************************************************/
@isTest(SeeAllData = false)
public class PlanMemberTriggerHelperTest {
    @isTest
    static void testPlanMemberTrigger(){
        try{
        	Date todaysdate = Date.today();
            Date sDateCurrentYear = DATE.newInstance(todaysdate.year(), 1, 1);
            Date eDateCurrentYear = DATE.newInstance(todaysdate.year(), 12, 31);
            User u = TestDataSetup.getUser('System Administrator');
            User u1 = TestDataSetup.getUser('Standard User');
        	insert u1;
        	TestDataSetup.setPermissionSet(u1.id, 'CampaignManagerSalesforceUser');
            System.runAs(u) {

               	DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance(); 
               	insert disableTrigger;
                
                musqotmp__MarketingPlan__c marketingPlan = musqotmp.TestDataSetup.getMarketingPlan('Marketing Plan',sDateCurrentYear,eDateCurrentYear,'In Progress','Global');
                marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
                marketingPlan.musqotmp__CampaignTemplate__c = false;
                marketingPlan.musqotmp__MarketingArea__c = 'Europe';
                Id recordTypeId =Schema.SObjectType.musqotmp__MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
                marketingPlan.RecordTypeId = recordTypeId;
                insert marketingPlan;
                
                musqotmp__MarketingPlanMember__c planMember = new musqotmp__MarketingPlanMember__c();
                planMember.musqotmp__MarketingPlan__c = marketingPlan.id;
                planMember.musqotmp__PlanAccess__c  = 'Owner';
                planMember.musqotmp__User__c = marketingPlan.OwnerId;
                planMember.musqotmp__UniquePlanMember__c = marketingPlan.id+''+marketingPlan.OwnerId;
                insert planMember;
                
                musqotmp__MarketingPlanMember__c planMember2 = new musqotmp__MarketingPlanMember__c();
                planMember2.musqotmp__MarketingPlan__c = marketingPlan.id;
                planMember2.musqotmp__PlanAccess__c  = 'Read Only';
                planMember2.musqotmp__User__c = u1.Id;
                planMember2.musqotmp__UniquePlanMember__c = marketingPlan.id+''+marketingPlan.OwnerId;
                insert planMember2;
                
                Test.startTest();
                planMember.musqotmp__PlanAccess__c = 'Read Only';
                update planMember;
                planMember.musqotmp__PlanAccess__c = 'Read Write';
                update planMember;
                delete planMember;
                planMember2.musqotmp__PlanAccess__c = 'Read Write';
                update planMember2;
                delete planMember2;
                Test.stopTest();
                System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    		}
        }catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanMemberTriggerHelperTest','testPlanMemberTrigger',ex);
        	}
		}
	}