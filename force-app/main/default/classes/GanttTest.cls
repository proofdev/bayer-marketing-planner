@isTest
public class GanttTest {
     public static Date sDate = Date.Today();
     public static Date eDate = Date.newInstance(2030, 04, 04);
	@isTest
	static void getMarketingPlanView(){
        DataModel.Result result = Gantt.getMarketingPlanView('param','recordId');
        System.assertEquals(null, result.contentIds,'Content Ids should be null if not passed as param');
    }
    @isTest
    static void getMyWorkspace(){
        DataModel.Result result = Gantt.getMyWorkspace('param', 'recordId');
        System.assertEquals(null, result.contentIds,'Content Ids should be null if not passed as param');
    }
    @isTest
    static void createplan(){
        Id recordTypeId = Schema.SObjectType.MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
        musqotmp__MarketingPlan__c marketingPlan = new musqotmp__MarketingPlan__c();
        marketingPlan.Name = 'Marketing Plan';
        marketingPlan.musqotmp__StartDate__c = sDate;
        marketingPlan.musqotmp__EndDate__c = eDate;
        marketingPlan.musqotmp__CampaignTemplate__c = false;
        marketingPlan.musqotmp__Status__c = 'Completed';
        marketingPlan.musqotmp__MarketingArea__c = 'Europe';
        marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
        marketingPlan.musqotmp__PlanType__c = 'Global';
        marketingPlan.recordTypeId = recordTypeId;
        insert marketingPlan;
        
        Schema.Campaign campaing = TestDataSetup.getCampaign('campaign 2020', sDate, eDate, 'Created', marketingPlan);
        insert campaing;  
        List<Gantt.SObJectResult> result = Gantt.getResults('campaign', marketingPlan.Name, 'Marketing', marketingPlan.Id); 
        system.assertEquals(0, result.size(),'For given scenario result is expected to be 0');
    }   
}