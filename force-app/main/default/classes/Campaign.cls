public with sharing class Campaign {
    @AuraEnabled(cacheable=true)
    public static DataModel.Result sendToast(String param) {
        DataModel.Result result = DataHandler.sendToast(param);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result datatable(String param) {
        DataModel.Result result = DataHandler.datatable(param);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getDynamicObjectInfo(String param, String recordId) {
        DataModel.Result result = DataHandler.getDynamicObjectInfo(param, recordId);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getRecords(String param) {
        DataModel.Result result = DataHandler.getRecords(param);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result DynamicObjectInfo(String param, String recordId) {
        DataModel.Result result = SendMessageToContributors.DynamicObjectInfo(param, recordId);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result readCampaignManagerSetup() {
        DataModel.Result result = DataHandler.readCampaignManagerSetup();
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result readMarketingCloudCredential() {
        DataModel.Result result = DataHandler.readMarketingCloudCredential();
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result postCalloutResponseContents(String param) {
      //  System.debug('postCalloutResponseContents '+ param);
        DataModel.Result result = DataHandler.postCalloutResponseContents(param);
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getCalloutResponseContents(String param) {
        DataModel.Result result = DataHandler.getCalloutResponseContents(param); 
        return result;
    }
    //Anji
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getLookupResults(String param) {
        DataModel.Result result = DataHandler.getLookupResults(param);
        return result;
    } 
        @AuraEnabled(cacheable=true)
    public static List<RecordsData> fetchRecords(String objectName, String filterField, String searchString, String value) {
        try {
            List<RecordsData> recordsDataList = new List<RecordsData>();
            String query = 'SELECT Id, ' + filterField + ' FROM '+objectName;
            if(String.isNotBlank(value)) {
                query += ' WHERE Id = \''+ value + '\' LIMIT 49999';
            } else {
                query += ' WHERE '+filterField+
                		' LIKE ' + '\'' + String.escapeSingleQuotes(searchString.trim()) + '%\' LIMIT 49999';
            }
	        for(SObject s : Database.query(query)) {
	            recordsDataList.add( new RecordsData((String)s.get(filterField), (String)s.get('id')) );
	        }
            return recordsDataList;
	    } catch (Exception err) {
	    	if ( String.isNotBlank( err.getMessage() ) && err.getMessage().contains( 'error:' ) ) {
                throw new AuraHandledException(err.getMessage().split('error:')[1].split(':')[0] + '.');
            } else {
                throw new AuraHandledException(err.getMessage());
            }
	    }
    }
 
    public class RecordsData {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        public RecordsData(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}