/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              06/05/2020          Created for W-000339, W-000359
* Alka              07/15/2020          Update for W-000390 to created Forecast when Allocation object is not available
**************************************************************************************************************************************/
@namespaceAccessible
public with sharing class DataVariableJobForecastUtility {
    //added for W-000359
    /*
    @namespaceAccessible
    public static void publishAllocationForecastEvent(List<Id> allocationIdList, Boolean isUpdateEvent){
        List<musqotmp__DataVariableEvent__e> dataVariableEventList = new List<musqotmp__DataVariableEvent__e>();
        for(String allocationId: allocationIdList){
           musqotmp__DataVariableEvent__e dataVariableEvent = new musqotmp__DataVariableEvent__e(
           musqotmp__Allocation__c = allocationId, 
           musqotmp__isUpdateEvent__c = isUpdateEvent);
           dataVariableEventList.add(dataVariableEvent);
        }
        List<Database.SaveResult> results = EventBus.publish(dataVariableEventList);
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                //System.debug('Successfully published event.');
            }     
        }
    }*/

    //Added for W-000390
    //Commented after review - start
    /*
    public static void createDataVariableCampaignForecasts(List<Schema.Campaign> campaignListForForecast, List<Id> campaignIdList,Set<String> campaignTypeSet,String eventType){
        List<Id> campaignIdListToDeleteForecast = new List<Id>();
        Map<String,Id> campTypeDataVarMap = new Map<String,Id>();
        List<musqotmp__DataVariableItem__c> dataVariableItemsToCreate = new List<musqotmp__DataVariableItem__c>();
        List<musqotmp__DataVariableActivityTypeMap__c> dataVariableMappingList = new List<musqotmp__DataVariableActivityTypeMap__c>();
        try{
            if(eventType == 'Update'){
                campaignIdListToDeleteForecast = campaignIdList;
            }
            if(campaignIdList.size() > 0){
                String mappingSQL = 'SELECT'; 
                mappingSQL += ' musqotmp__CampaignType__c, musqotmp__DataVariable__c';
                mappingSQL += ' FROM musqotmp__DataVariableActivityTypeMap__c';
                mappingSQL += ' WHERE musqotmp__CampaignType__c in :campaignTypeSet';
                //System.debug('mappingSQL'+mappingSQL);
                dataVariableMappingList = Database.query(mappingSQL);
				//System.Debug('dataVariableMappingList***'+dataVariableMappingList);                
                for(musqotmp__DataVariableActivityTypeMap__c mappingInst :dataVariableMappingList){
                    if(mappingInst.get('musqotmp__CampaignType__c') != null){
                        campTypeDataVarMap.put((String)mappingInst.get('musqotmp__CampaignType__c'),(String)mappingInst.get('musqotmp__DataVariable__c'));    
                    }	
                }
                //system.debug('campTypeDataVarMap'+campTypeDataVarMap);
                List<DataVariableItemWrapper> dataVarItemObjList = new List<DataVariableItemWrapper>();
                for(Schema.Campaign campignInst : campaignListForForecast){
                    List<String> audienceList = new List<String>();
                    List<String> divisionList = new List<String>();
                    List<String> regionList = new List<String>();
                    List<String> countryList = new List<String>();
                    List<String> cityList = new List<String>();
                    List<String> shopList = new List<String>();
                    List<String> productGroupList = new List<String>();
                    DataVariableItemWrapper dataVarItemObj = new DataVariableItemWrapper();
                    dataVarItemObj.campaignId = campignInst.Id;
                    if(campignInst.BudgetedCost != null && campignInst.ActualCost != null){
                        if(campignInst.BudgetedCost < campignInst.ActualCost){
                        	dataVarItemObj.amount = campignInst.ActualCost;    
                        } else {
                            dataVarItemObj.amount = campignInst.BudgetedCost;
                        }
                    } else {
                        if(campignInst.BudgetedCost != null){
                        	dataVarItemObj.amount = campignInst.BudgetedCost;    
                        }
                        if(campignInst.ActualCost != null){
                        	dataVarItemObj.amount = campignInst.ActualCost;    
                        }
                    }
                    dataVarItemObj.dataVariableId = campTypeDataVarMap.get(campignInst.Type);
                    dataVarItemObj.startDate = campignInst.StartDate;
                    dataVarItemObj.endDate = campignInst.EndDate ;
                    if(campignInst.musqotmp__Audience__c != null){
                        audienceList.addAll(campignInst.musqotmp__Audience__c.split(';'));
                        dataVarItemObj.audienceList = audienceList;
                    }
                    if(campignInst.musqotmp__MarketingBusinessUnit__c != null){
                        divisionList.addAll(campignInst.musqotmp__MarketingBusinessUnit__c.split(';'));
                        dataVarItemObj.divisionList = divisionList;    
                    }
                    if(campignInst.musqotmp__MarketingArea__c != null){
                        regionList.addAll(campignInst.musqotmp__MarketingArea__c.split(';'));
                        dataVarItemObj.regionList = regionList;    
                    }	
                    if(campignInst.musqotmp__Countries__c != null){
                        countryList.addAll(campignInst.musqotmp__Countries__c.split(';'));
                        dataVarItemObj.countryList = countryList;   
                    }
                    if(campignInst.musqotmp__City__c != null){
                        cityList.addAll(campignInst.musqotmp__City__c.split(';'));
                        dataVarItemObj.cityList = cityList;   
                    }
                    if(campignInst.musqotmp__Shop__c != null){
                        shopList.addAll(campignInst.musqotmp__Shop__c.split(';'));
                        dataVarItemObj.shopList = shopList;    
                    }
                    if(campignInst.musqotmp__ProductGroup__c != null){
                        productGroupList.addAll(campignInst.musqotmp__ProductGroup__c.split(';'));
                        dataVarItemObj.productGroupList = productGroupList;    
                    }
                    dataVarItemObjList.add(dataVarItemObj);
                    //System.debug('audienceList'+dataVarItemObj.audienceList);
                    //dataVariableItemsToCreate.addAll(getDataVariableItemListForForecast(dataVarItemObjList)); 
                    //System.debug('dataVariableItemsToCreate***'+dataVariableItemsToCreate);
                }  
                if(campaignIdListToDeleteForecast.size() > 0){
                    List<musqotmp__DataVariableItem__c> listOfCustomObject = [SELECT id from musqotmp__DataVariableItem__c where musqotmp__Campaign__c in :campaignIdListToDeleteForecast];
                    if(musqotmp__DataVariableItem__c.sObjectType.getDescribe().isDeletable()){
                        Database.delete(listOfCustomObject);
                    } 
                }
                if(dataVarItemObjList.size() > 0)
                	getDataVariableItemListForForecast(dataVarItemObjList);
            }   
        } Catch(Exception e){
            ExceptionHandler.logError('Class','DataVariableJobTriggerHelper','createDataVariableCampaignForecasts',e);
        }    
    }*/
    //Commented after review - end
    //Added for W-000339
    //Commented after review - start
    /*
    @namespaceAccessible
    public static void getDataVariableItemListForForecast(List<DataVariableItemWrapper> dataVarItemWrapperList){
        List<musqotmp__DataVariableItem__c> dataVariableItemListFinal = new List<musqotmp__DataVariableItem__c>();
        for(DataVariableItemWrapper dataVarItemWrapper: dataVarItemWrapperList){
            Date startDate = dataVarItemWrapper.startDate;
            Integer duration = startDate.daysBetween(dataVarItemWrapper.endDate);
            List<musqotmp__DataVariableItem__c> dataVariableItemList = new List<musqotmp__DataVariableItem__c>();
            if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isCreateable()){
                for(Integer i = 0; i <= duration; i++){
                    musqotmp__DataVariableItem__c dataVarItem = new musqotmp__DataVariableItem__c();
                    dataVarItem.musqotmp__Date__c = startDate;
                    dataVariableItemList.add(dataVarItem);
                    startDate = startDate.addDays(1);
                    //System.debug('dataVariableItemList'+dataVariableItemList);
                }   
            }
            if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isCreateable()){
                if(dataVarItemWrapper.audienceList.size() > 0 && dataVariableItemList.size() > 0){
                    List<musqotmp__DataVariableItem__c> dataVarItemListWithAudience = new List<musqotmp__DataVariableItem__c>();
                    for(musqotmp__DataVariableItem__c dataVarItem : dataVariableItemList){
                        for(String audience : dataVarItemWrapper.audienceList){
                            musqotmp__DataVariableItem__c dataVarItemWithAudience = new musqotmp__DataVariableItem__c();
                            dataVarItemWithAudience.musqotmp__Date__c = dataVarItem.musqotmp__Date__c;
                            dataVarItemWithAudience.musqotmp__Audience__c = audience;
                            //System.debug('dataVarItemWithAudience'+dataVarItemWithAudience);
                            dataVarItemListWithAudience.add(dataVarItemWithAudience);
                            //System.debug('dataVarItemListWithAudience'+dataVarItemListWithAudience);
                        }
                    }
                    dataVariableItemList.clear();
                    dataVariableItemList.addAll(dataVarItemListWithAudience);
                }   
            }
            if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__BusinessUnit__c.isCreateable()){
                if(dataVarItemWrapper.divisionList.size() > 0 && dataVariableItemList.size() > 0){
                    List<musqotmp__DataVariableItem__c> dataVarItemListWithDivision = new List<musqotmp__DataVariableItem__c>();
                    for(musqotmp__DataVariableItem__c dataVarItem : dataVariableItemList){
                        for(String division : dataVarItemWrapper.divisionList){
                            musqotmp__DataVariableItem__c dataVarItemWithDivision = new musqotmp__DataVariableItem__c();
                            dataVarItemWithDivision.musqotmp__Date__c = dataVarItem.musqotmp__Date__c;
                            dataVarItemWithDivision.musqotmp__Audience__c = dataVarItem.musqotmp__Audience__c;
                            dataVarItemWithDivision.musqotmp__BusinessUnit__c = division;
                            dataVarItemListWithDivision.add(dataVarItemWithDivision);
                        }
                    }
                    //System.debug('dataVarItemListWithDivision'+dataVarItemListWithDivision);
                    dataVariableItemList.clear();
                    dataVariableItemList.addAll(dataVarItemListWithDivision);
                    //System.debug('dataVariableItemList'+dataVariableItemList);
                }       
            }
            if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__BusinessUnit__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Region__c.isCreateable()){
                if(dataVarItemWrapper.regionList.size() > 0 && dataVariableItemList.size() > 0){
                    List<musqotmp__DataVariableItem__c> dataVarItemListWithRegion = new List<musqotmp__DataVariableItem__c>();
                    for(musqotmp__DataVariableItem__c dataVarItem : dataVariableItemList){
                        for(String region : dataVarItemWrapper.regionList){
                            musqotmp__DataVariableItem__c dataVarItemWithRegion = new musqotmp__DataVariableItem__c();
                            dataVarItemWithRegion.musqotmp__Date__c = dataVarItem.musqotmp__Date__c;
                            dataVarItemWithRegion.musqotmp__Audience__c = dataVarItem.musqotmp__Audience__c;
                            dataVarItemWithRegion.musqotmp__BusinessUnit__c = dataVarItem.musqotmp__BusinessUnit__c;
                            dataVarItemWithRegion.musqotmp__Region__c = region;
                            dataVarItemListWithRegion.add(dataVarItemWithRegion);
                        }
                    }
                    //System.debug('dataVarItemListWithRegion'+dataVarItemListWithRegion);
                    dataVariableItemList.clear();
                    dataVariableItemList.addAll(dataVarItemListWithRegion);
                    //System.debug('dataVariableItemList'+dataVariableItemList);
                }
            }
            
            if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__BusinessUnit__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Region__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Country__c.isCreateable()){
               if(dataVarItemWrapper.countryList.size() > 0 && dataVariableItemList.size() > 0){
                    List<musqotmp__DataVariableItem__c> dataVarItemListWithCountry = new List<musqotmp__DataVariableItem__c>();
                    for(musqotmp__DataVariableItem__c dataVarItem : dataVariableItemList){
                        for(String country : dataVarItemWrapper.countryList){
                            musqotmp__DataVariableItem__c dataVarItemWithCoutry = new musqotmp__DataVariableItem__c();
                            dataVarItemWithCoutry.musqotmp__Date__c = dataVarItem.musqotmp__Date__c;
                            dataVarItemWithCoutry.musqotmp__Audience__c = dataVarItem.musqotmp__Audience__c;
                            dataVarItemWithCoutry.musqotmp__BusinessUnit__c = dataVarItem.musqotmp__BusinessUnit__c;
                            dataVarItemWithCoutry.musqotmp__Region__c = dataVarItem.musqotmp__Region__c;
                            dataVarItemWithCoutry.musqotmp__Country__c = country;
                            dataVarItemListWithCountry.add(dataVarItemWithCoutry);
                        }
                    }
                    //System.debug('dataVarItemListWithCountry'+dataVarItemListWithCountry);
                    dataVariableItemList.clear();
                    dataVariableItemList.addAll(dataVarItemListWithCountry);
                    //System.debug('dataVariableItemList'+dataVariableItemList);
                }    
            }
            if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__BusinessUnit__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Region__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Country__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__City__c.isCreateable()){
                if(dataVarItemWrapper.cityList.size() > 0 && dataVariableItemList.size() > 0){
                    List<musqotmp__DataVariableItem__c> dataVarItemListWithCity = new List<musqotmp__DataVariableItem__c>();
                    for(musqotmp__DataVariableItem__c dataVarItem : dataVariableItemList){
                        for(String city : dataVarItemWrapper.cityList){
                            musqotmp__DataVariableItem__c dataVarItemWithCity = new musqotmp__DataVariableItem__c();
                            //dataVarItemWithCity.musqotmp__Allocation__c = dataVarItem.musqotmp__Allocation__c;
                            //dataVarItemWithCity.musqotmp__DataVariable__c = dataVarItem.musqotmp__DataVariable__c;
                            dataVarItemWithCity.musqotmp__Date__c = dataVarItem.musqotmp__Date__c;
                            dataVarItemWithCity.musqotmp__Audience__c = dataVarItem.musqotmp__Audience__c;
                            dataVarItemWithCity.musqotmp__BusinessUnit__c = dataVarItem.musqotmp__BusinessUnit__c;
                            dataVarItemWithCity.musqotmp__Region__c = dataVarItem.musqotmp__Region__c;
                            dataVarItemWithCity.musqotmp__Country__c = dataVarItem.musqotmp__Country__c;
                            dataVarItemWithCity.musqotmp__City__c = city;
                            dataVarItemListWithCity.add(dataVarItemWithCity);
                        }
                    }
                    //System.debug('dataVarItemListWithCity'+dataVarItemListWithCity);
                    dataVariableItemList.clear();
                    dataVariableItemList.addAll(dataVarItemListWithCity);
                    //System.debug('dataVariableItemList'+dataVariableItemList);
                }      
            }
            if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__BusinessUnit__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Region__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Country__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__City__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Shop__c.isCreateable()){
                if(dataVarItemWrapper.shopList.size() > 0 && dataVariableItemList.size() > 0){
                    List<musqotmp__DataVariableItem__c> dataVarItemListWithShop = new List<musqotmp__DataVariableItem__c>();
                    for(musqotmp__DataVariableItem__c dataVarItem : dataVariableItemList){
                        for(String shop : dataVarItemWrapper.shopList){
                            musqotmp__DataVariableItem__c dataVarItemWithShop = new musqotmp__DataVariableItem__c();
                            dataVarItemWithShop.musqotmp__Date__c = dataVarItem.musqotmp__Date__c;
                            dataVarItemWithShop.musqotmp__Audience__c = dataVarItem.musqotmp__Audience__c;
                            dataVarItemWithShop.musqotmp__BusinessUnit__c = dataVarItem.musqotmp__BusinessUnit__c;
                            dataVarItemWithShop.musqotmp__Region__c = dataVarItem.musqotmp__Region__c;
                            dataVarItemWithShop.musqotmp__Country__c = dataVarItem.musqotmp__Country__c;
                            dataVarItemWithShop.musqotmp__City__c = dataVarItem.musqotmp__City__c;
                            dataVarItemWithShop.musqotmp__Shop__c = shop;
                            dataVarItemListWithShop.add(dataVarItemWithShop);
                        }
                    }
                    //System.debug('dataVarItemListWithShop'+dataVarItemListWithShop);
                    dataVariableItemList.clear();
                    dataVariableItemList.addAll(dataVarItemListWithShop);
                    //System.debug('dataVariableItemList'+dataVariableItemList);
                }       
            }
            if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__BusinessUnit__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Region__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Country__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__City__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Shop__c.isCreateable() &&
               Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__ProductGroup__c.isCreateable()){
                if(dataVarItemWrapper.productGroupList.size() > 0 && dataVariableItemList.size() > 0){
                    List<musqotmp__DataVariableItem__c> dataVarItemListWithProdGroup = new List<musqotmp__DataVariableItem__c>();
                    for(musqotmp__DataVariableItem__c dataVarItem : dataVariableItemList){
                        for(String productGroup : dataVarItemWrapper.productGroupList){
                            musqotmp__DataVariableItem__c dataVarItemWithProdGroup = new musqotmp__DataVariableItem__c();
                            dataVarItemWithProdGroup.musqotmp__Date__c = dataVarItem.musqotmp__Date__c;
                            dataVarItemWithProdGroup.musqotmp__Audience__c = dataVarItem.musqotmp__Audience__c;
                            dataVarItemWithProdGroup.musqotmp__BusinessUnit__c = dataVarItem.musqotmp__BusinessUnit__c;
                            dataVarItemWithProdGroup.musqotmp__Region__c = dataVarItem.musqotmp__Region__c;
                            dataVarItemWithProdGroup.musqotmp__Country__c = dataVarItem.musqotmp__Country__c;
                            dataVarItemWithProdGroup.musqotmp__City__c = dataVarItem.musqotmp__City__c;
                            dataVarItemWithProdGroup.musqotmp__Shop__c = dataVarItem.musqotmp__Shop__c;
                            dataVarItemWithProdGroup.musqotmp__ProductGroup__c = productGroup;
                            dataVarItemListWithProdGroup.add(dataVarItemWithProdGroup);
                        }
                    }
                    //System.debug('dataVarItemListWithProdGroup'+dataVarItemListWithProdGroup);
                    dataVariableItemList.clear();
                    dataVariableItemList.addAll(dataVarItemListWithProdGroup);
                    //System.debug('dataVariableItemList'+dataVariableItemList);
                }      
            }
            if(dataVariableItemList.size() > 0){
                List<musqotmp__DataVariableItem__c> dataVarItemListWithExternalId = new List<musqotmp__DataVariableItem__c>();
                String extId;
                if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isAccessible() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isAccessible() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__BusinessUnit__c.isAccessible() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Region__c.isAccessible() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Country__c.isAccessible() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__City__c.isAccessible() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Shop__c.isAccessible() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__ProductGroup__c.isAccessible() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__ExternalItemId__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Forecast__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Campaign__c.isCreateable() &&
                   Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__DataVariable__c.isCreateable()){
                    for(musqotmp__DataVariableItem__c dataVarItem : dataVariableItemList){
                        if(dataVarItemWrapper.allocationId != null){
                            extId  = dataVarItemWrapper.allocationId;   
                        } else {
                            extId  = dataVarItemWrapper.campaignId;
                        }
                        extId += dataVarItem.musqotmp__Date__c;
                        if(dataVarItem.musqotmp__Audience__c != null)
                            extId += dataVarItem.musqotmp__Audience__c;
                        if(dataVarItem.musqotmp__BusinessUnit__c != null)
                            extId += dataVarItem.musqotmp__BusinessUnit__c;
                        if(dataVarItem.musqotmp__Region__c != null)
                            extId += dataVarItem.musqotmp__Region__c;
                        if(dataVarItem.musqotmp__Country__c != null)
                            extId += dataVarItem.musqotmp__Country__c;
                        if(dataVarItem.musqotmp__City__c != null)
                            extId += dataVarItem.musqotmp__City__c;
                        if(dataVarItem.musqotmp__Shop__c != null)
                            extId += dataVarItem.musqotmp__Shop__c;
                        if(dataVarItem.musqotmp__ProductGroup__c != null)
                            extId += dataVarItem.musqotmp__ProductGroup__c;
                        //System.debug('extId'+extId);
                        dataVarItem.musqotmp__ExternalItemId__c = extId;
                        dataVarItem.musqotmp__Forecast__c = true;
                        if(dataVarItemWrapper.allocationId != null){
                            dataVarItem.put('musqotmp__Allocation__c',dataVarItemWrapper.allocationId);
                        } else {
                            dataVarItem.musqotmp__Campaign__c = dataVarItemWrapper.campaignId;
                        }
                        dataVarItem.musqotmp__DataVariable__c = dataVarItemWrapper.dataVariableId;
                    }      
                }
                //System.debug('dataVarItem with Ext Id'+dataVariableItemList);
                dataVariableItemList = splitAmount(dataVariableItemList,dataVarItemWrapper.amount);
            }
        	//System.debug('dataVariableItemList'+dataVariableItemList);
        	dataVariableItemListFinal.addAll(dataVariableItemList);
        }
        if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.Name.isCreateable()){
            if(dataVariableItemListFinal.size() > 0)
                Database.insert(dataVariableItemListFinal);    
        }
        //return dataVariableItemListFinal;
    }
	//Commented after review - end
    //Added for W-000339
    //Commented after review - start
    /*
    @namespaceAccessible
    public static List<musqotmp__DataVariableItem__c> splitAmount(List<musqotmp__DataVariableItem__c> dataVariableItemList, Decimal amount){
        Integer listSize = dataVariableItemList.size();
        Decimal splitValue = 1.00;
        Decimal extraAmount = 0;
        musqotmp__DataVariableItem__c forecastRec;
        List<musqotmp__DataVariableItem__c> finalForecastList = new List<musqotmp__DataVariableItem__c>();
        //System.debug('listSize***'+listSize);
        //System.debug('amount***'+amount);
        if(listSize > 0){
            splitValue = Math.floor(amount/listSize);
            extraAmount = amount - (splitValue * listSize);
        }
        if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__ValueCurrency__c.isCreateable()){
        	for(Integer i = 0; i < listSize ; i ++){
                forecastRec = dataVariableItemList[i];
                forecastRec.musqotmp__ValueCurrency__c = (Decimal)splitValue;
                if(i == (listSize-1))
                    forecastRec.musqotmp__ValueCurrency__c += extraAmount;
                finalForecastList.add(forecastRec);
            }   
        }
        return finalForecastList;
    }*/
     //Commented after review - end
    //Added for W-000339
    /*
    @namespaceAccessible
    public class DataVariableItemWrapper{
        String campaignId {get;set;} 
        String allocationId {get;set;}        
        String dataVariableId {get;set;}
        Date startDate {get;set;}
        Date endDate {get;set;}
        Double amount{get;set;}
        List<String> audienceList = new List<String>();
        List<String> divisionList = new List<String>();
        List<String> regionList = new List<String>();
        List<String> countryList = new List<String>();
        List<String> cityList = new List<String>();
        List<String> shopList = new List<String>();
        List<String> productGroupList = new List<String>();
    }*/
    //Added for W-000339
    @namespaceAccessible
    public static boolean isObjectExist(String ObjectNameParam){
     for (Schema.SObjectType sObjectType : Schema.getGlobalDescribe().values() ) {
        String sobjName = String.valueOf(sObjectType);
        if (sobjName.contains(ObjectNameParam) ) {
            return true;
        }
     }
     return false;
   }
}