@isTest(seeAllData = false)
public class PerformanceGanttTest {
    @isTest
    static void testScript(){
        User adminUser = TestDataSetup.getUser('System Administrator');
        insert adminUser;
        TestDataSetup.setPermissionSet(adminUser.Id,'CampaignManagerAdminSalesforceUser');
        System.runAs(adminUser){
            TestDataSetup.createCampaignManagerSetup();
            musqotmp__DataVariable__c dataVarFB = TestDataSetup.getDataVariableRecord('Facebook','#642F88','Daily','Spend Amount');
        	insert dataVarFB;
            musqotmp__DataVariable__c independentDV = TestDataSetup.getDataVariableRecord('You Tube','#642F88','Daily','Spend Amount');
        	insert independentDV;
            musqotmp__GanttPerformance__c ganttFilter = TestDataSetup.getGanttFilterRecord('Test Filter'+Date.Today(),dataVarFB.Id);
            insert ganttFilter;
            ganttFilter.musqotmp__DependentData__c = dataVarFB.id;
            ganttFilter.musqotmp__IndependentData__c = independentDV.Id;
            update ganttFilter;
            GanttPerformance__c ganttPerformance =  TestDataSetup.getPerformanceGanttRecord('Name', dataVarFB.Id, 'THIS_QUARTER');
        	insert ganttPerformance;
            PerformanceGantt.getPerformanceGantt(ganttFilter.Id);  
            PerformanceGantt.getPerformanceGantt(ganttPerformance.Id);
        	DataModel.Track track = new DataModel.Track();
        	track.recordId = ganttPerformance.Id;
        	String param = JSON.serialize(track);
        	DataModel.Result result = PerformanceGantt.getRecords(param);
        	DataModel.Result getDataSet = PerformanceGantt.getDataSet(param);
            System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
        }
    }
}