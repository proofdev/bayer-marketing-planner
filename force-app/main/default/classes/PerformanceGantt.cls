public with sharing class PerformanceGantt {
    //Get Data for gantt in performance gantt 
    //input performanceGantt Id
    //output list of Plan, Canpaigns;marketingActivity and Journey Records
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getPerformanceGantt(String recordId){
        DataModel.Result result = new DataModel.Result();
        //DataModel.Track track = new DataModel.Track();
        try{
            //track = (DataModel.Track)JSON.deserialize(param, DataModel.Track.class);
            String userId = UserInfo.getUserId(); 
            Map<String, List<DataModel.Field>> infoFieldMap = new Map<String, List<DataModel.Field>>();
            SObjectDescribe d = SObjectDescribe.getDescribe('Campaign');
            musqotmp__CampaignManagerSetup__c campaignManagerSetup = musqotmp__CampaignManagerSetup__c.getInstance();
            Map<String, String> stringMap = new Map<String, String>();
            infoFieldMap.put('Campaign', d.getFieldSetFields('musqotmp__CampaignGanttInfo'));
            musqotmp__GanttPerformance__c ganttPerformance = new musqotmp__GanttPerformance__c();
            if(Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__ActivityType__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Audience__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__BusinessUnit__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__BarText__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__City__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Country__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__DateRange__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__DependentChartType__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__DependentData__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__DisplayGroupedBy__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__IncludeForecast__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__IncludeJourney__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__IndependentData__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__IndexedIndependentData__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__ProductGroup__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Region__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Scatter__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Shop__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__StackedField__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.Name.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.Id.isAccessible()){
             ganttPerformance = [SELECT Id, musqotmp__ActivityType__c, musqotmp__Audience__c, musqotmp__BarText__c, 
                                                              musqotmp__BusinessUnit__c, musqotmp__City__c, musqotmp__Country__c, musqotmp__DateRange__c, 
                                                              musqotmp__DependentChartType__c, musqotmp__DependentData__c, musqotmp__DisplayGroupedBy__c, 
                                                              musqotmp__IncludeForecast__c, musqotmp__IncludeJourney__c, musqotmp__IndependentData__c,
                                                              musqotmp__IndexedIndependentData__c, musqotmp__ProductGroup__c, musqotmp__Region__c, 
                                                              musqotmp__Scatter__c, musqotmp__Shop__c, musqotmp__StackedField__c, Name, musqotmp__SeasonalVariable__r.musqotmp__DataVariableType__c, musqotmp__SeasonalVariable__r.Id, musqotmp__SeasonalVariable__r.musqotmp__BarColor__c FROM musqotmp__GanttPerformance__c 
                                                              WHERE Id = :recordId LIMIT 1];
          
            }
            
            d = SObjectDescribe.getDescribe('musqotmp__MarketingActivity__c');
            if(campaignManagerSetup.musqotmp__BarColorMarketingActivity__c != null){
                List<String> barColors = campaignManagerSetup.musqotmp__BarColorMarketingActivity__c.split(',');
                Integer i = 0;
                Schema.DescribeFieldResult objectFieldResult = d.getField('musqotmp__Type__c').getDescribe();
                List<Schema.PicklistEntry> picklistValues = objectFieldResult.getPickListValues(); 
                for (Schema.PicklistEntry a : picklistValues) {
                    stringMap.put('musqotmp__MarketingActivity__c'+'~~'+'musqotmp__Type__c'+'~~'+a.getValue(), barColors[i]);
                    if(barColors.size() - 1 == i){
                        i = 0;
                    }else{
                        i++;
                    }
                }
            }
         //   System.debug('barColors'+stringMap);
            infoFieldMap.put('musqotmp__MarketingActivity__c', d.getFieldSetFields('musqotmp__MarketingActivityGanttInfo'));
            d = SObjectDescribe.getDescribe('musqotmp__MarketingJourney__c');
            infoFieldMap.put('musqotmp__MarketingJourney__c', d.getFieldSetFields('musqotmp__MarketingJourneyGanttInfo'));            
            StringBuilder.CommaDelimitedListBuilder sb = new StringBuilder.CommaDelimitedListBuilder();
            String dateRange = ganttPerformance.musqotmp__DateRange__c;
            String query = 'SELECT ';
            query += 'Id, musqotmp__Campaign__c, musqotmp__Audience__c, musqotmp__Description__c, musqotmp__EndDate__c, musqotmp__MarketingArea__c, musqotmp__MarketingBusinessUnit__c, musqotmp__StartDate__c, musqotmp__StatusIcon__c, musqotmp__Status__c, musqotmp__Type__c, Name, CreatedDate, CreatedById, CreatedBy.Name ';
            query += ' FROM musqotmp__MarketingActivity__c';
            query += ' WHERE ';
            List<String> dateFields = new List<String>{'musqotmp__StartDate__c', 'musqotmp__EndDate__c'};
                if(ganttPerformance.musqotmp__ActivityType__c != null){
                    List<String> activityTypes = new List<String>();
                    activityTypes.addAll(ganttPerformance.musqotmp__ActivityType__c.split(';'));
                    query += ' musqotmp__Type__c IN :activityTypes';
                    query += ' AND ';
                }
            if(ganttPerformance.musqotmp__Region__c != null){
                List<String> regions = ganttPerformance.musqotmp__Region__c.split(';');
               // System.debug('regions'+regions);
                query += ' musqotmp__MarketingArea__c IN :regions';
                query += ' AND ';
            }
          //  System.debug('musqotmp__BusinessUnit__c' + ganttPerformance.musqotmp__BusinessUnit__c);
            if(ganttPerformance.musqotmp__BusinessUnit__c != null){
                List<String> businessUnits = ganttPerformance.musqotmp__BusinessUnit__c.split(';');
                query += ' musqotmp__MarketingBusinessUnit__c IN :businessUnits';
                query += ' AND ';
            }
            if(ganttPerformance.musqotmp__Audience__c != null){
                List<String> audiences = ganttPerformance.musqotmp__Audience__c.split(';');
                query += ' musqotmp__Audience__c IN :audiences';
                query += ' AND ';
            }
            List<String> dateRanges = dateRange.split(',');
            query += ' (';
            for(String df : dateFields){
                query += '(';
                for(String r : dateRanges){
                    query += '(' + df +' = ' + r +')';
                    if(!(r == dateRanges.get(dateRanges.size() - 1))) {
                        query += ' OR ';
                    }
                }
                query += ')';
                if(!(df == dateFields.get(dateFields.size() - 1))) {
                    query += ' OR ';
                }
            }
            query += ')';
            query += ' ORDER BY CreatedDate, Name, Id';
            query += ' LIMIT 40000';
          //  System.debug(query);
          List<SObject> sObjMarketingActivities = new List<SObject>();
          if(Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__Campaign__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__Audience__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__Description__c.isAccessible() && 
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__EndDate__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__MarketingArea__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__MarketingBusinessUnit__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__StartDate__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__Status__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__StatusIcon__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__Type__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__Status__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.musqotmp__Status__c.isAccessible() &&
          Schema.sObjectType.musqotmp__MarketingActivity__c.fields.Id.isAccessible()){
            sObjMarketingActivities = Database.query(query);
          }
          //  System.debug('sObjMarketingActivities'+sObjMarketingActivities);
            List<SObject> sObjMarketingJourneys = new List<SObject>();
            if(ganttPerformance.musqotmp__IncludeJourney__c == true){
                query = 'SELECT ';
                query += ' musqotmp__Audience__c, musqotmp__Campaign__c, musqotmp__Description__c, musqotmp__EndDate__c, musqotmp__JourneyDescription__c, musqotmp__JourneyDuration__c, musqotmp__JourneyID__c, musqotmp__JourneyName__c, musqotmp__JourneyOwner__c, musqotmp__JourneyStatus__c, musqotmp__JourneyTemplateID__c, musqotmp__JourneyTemplateName__c, musqotmp__JourneyVersion__c, musqotmp__StartDate__c, musqotmp__StatusIcon__c, musqotmp__Status__c, CreatedById ,CreatedBy.Name, Name, CreatedDate, Id';
                query += ' FROM musqotmp__MarketingJourney__c';
                query += ' WHERE ';
                if(ganttPerformance.musqotmp__Audience__c != null){
                    List<String> audiences = ganttPerformance.musqotmp__Audience__c.split(';');
                    query += ' musqotmp__Audience__c IN :audiences';
                    query += ' AND ';
                }
                if(ganttPerformance.musqotmp__ActivityType__c != null){
                    List<String> activityTypes = new List<String>();
                    activityTypes.addAll(ganttPerformance.musqotmp__ActivityType__c.split(';'));
                    query += ' musqotmp__Type__c IN :activityTypes';
                    query += ' AND ';
                }
                if(ganttPerformance.musqotmp__Region__c != null){
                    List<String> regions = ganttPerformance.musqotmp__Region__c.split(';');
                  //  System.debug('regions'+regions);
                    query += ' musqotmp__MarketingRegion__c IN :regions';
                    query += ' AND ';
                }                             
                query += ' (';
                for(String df : dateFields){
                    query += '(';
                    for(String r : dateRanges){
                        query += '(' + df +' = ' + r +')';
                        if(!(r == dateRanges.get(dateRanges.size() - 1))) {
                            query += ' OR ';
                        }
                    }
                    query += ')';
                    if(!(df == dateFields.get(dateFields.size() - 1))) {
                        query += ' OR ';
                    }
                }
                query += ')';
                query += ' ORDER BY musqotmp__StartDate__c DESC, Name, Id';
                query += ' LIMIT 40000';
             //   System.debug(query);
             if(Schema.sObjectType.musqotmp__MarketingJourney__c.fields.musqotmp__Audience__c.isAccessible() &&
             Schema.sObjectType.musqotmp__MarketingJourney__c.fields.musqotmp__Campaign__c.isAccessible() &&
             Schema.sObjectType.musqotmp__MarketingJourney__c.fields.musqotmp__JourneyDescription__c.isAccessible()){
                sObjMarketingJourneys = Database.query(query);
             }
            }
            List<DataModel.Field> fields = new List<DataModel.Field>();
            for(String fieldName : ganttPerformance.musqotmp__BarText__c.split(';')){
                Schema.SObjectField sObjField = d.getField(fieldName);
                DataModel.Field field = new DataModel.Field();
                /*field.name = sObjField.getDescribe().getName();
                 field.label = sObjField.getDescribe().getLabel();
                 field.type = String.valueOf(sObjField.getDescribe().getType()).toLowerCase();*/
                fields.add(field);
            }
           // System.debug('fields'+fields);
            Map<String, List<SObject>> sObjectUnMap = new Map<String, List<SObject>>();
            sObjectUnMap.put('musqotmp__MarketingActivity__c', sObjMarketingActivities);
          //  System.debug('sObjectUnMap'+sObjectUnMap);
            sObjectUnMap.put('musqotmp__MarketingJourney__c', sObjMarketingJourneys);
          //  System.debug('sObjectUnMap'+sObjectUnMap);
            result.customSetting = campaignManagerSetup;
            result.stringMap = stringMap;
            result.sObj = ganttPerformance;
            result.fieldMap = infoFieldMap;
            result.fields = fields;
            result.sObjectUnMap = sObjectUnMap;
            //result.sObjIds = marketingPlanIds;
            DataModel.Toast toast = new DataModel.Toast();
            //toast.ok = true;
            result.toast = toast;
        }Catch(Exception ex){
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            result = DataHandler.handleToast(exceptions);    
        }
        return result;
    }
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getRecords(String param){
        return CampaignManagerAPI.getRecords( param);
    }
    
    //Get Data For Chart
    //Input performanceGantt Id
    //Output List of DataItems records
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getDataSet(String param){
        DataModel.Result result = new DataModel.Result();
        DataModel.Track track = new DataModel.Track();
        try{
            track = (DataModel.Track)JSON.deserialize(param, DataModel.Track.class);
            String recordId = track.recordId;
            musqotmp__GanttPerformance__c ganttPerformance = new musqotmp__GanttPerformance__c();
            if(Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__ActivityType__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Audience__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__BusinessUnit__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__BarText__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__City__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Country__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__DateRange__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__DependentChartType__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__DependentData__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__DisplayGroupedBy__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__IncludeForecast__c.isAccessible() && 
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__IncludeJourney__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__IndependentData__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__IndexedIndependentData__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__ProductGroup__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Region__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Scatter__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__Shop__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.musqotmp__StackedField__c.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.Name.isAccessible() &&
            Schema.sObjectType.musqotmp__GanttPerformance__c.fields.Id.isAccessible()){
             ganttPerformance = [SELECT Id, musqotmp__ActivityType__c, musqotmp__Audience__c, musqotmp__BarText__c, 
                                                              musqotmp__BusinessUnit__c, musqotmp__City__c, musqotmp__Country__c, musqotmp__DateRange__c, 
                                                              musqotmp__DependentChartType__c, musqotmp__DependentData__c, musqotmp__DisplayGroupedBy__c, 
                                                              musqotmp__IncludeForecast__c, musqotmp__IncludeJourney__c, musqotmp__IndependentData__c,
                                                              musqotmp__IndexedIndependentData__c, musqotmp__ProductGroup__c, musqotmp__Region__c, 
                                                              musqotmp__Scatter__c, musqotmp__Shop__c, musqotmp__StackedField__c, Name FROM musqotmp__GanttPerformance__c 
                                                              WHERE Id = :recordId LIMIT 1];
            }
            String dateRange = ganttPerformance.musqotmp__DateRange__c;
            String query = 'SELECT Id,musqotmp__DataVariable__r.musqotmp__Unit__c, musqotmp__DataVariable__r.Name,musqotmp__DataVariable__r.musqotmp__ExternalId__c, musqotmp__DataVariable__r.musqotmp__DataVariableType__c, musqotmp__DataVariable__r.musqotmp__BarColor__c, musqotmp__Audience__c, musqotmp__BusinessUnit__c, musqotmp__City__c, musqotmp__Country__c, musqotmp__Date__c, musqotmp__ExternalItemId__c, musqotmp__Forecast__c, musqotmp__Month__c, musqotmp__Quarter__c, musqotmp__Region__c, musqotmp__Shop__c, musqotmp__ValueCurrency__c, musqotmp__Value__c, musqotmp__Week__c, musqotmp__Year__c, Name FROM musqotmp__DataVariableItem__c';
            query += ' WHERE ';
            List<String> dateFields = new List<String>{'musqotmp__Date__c'};
                if(ganttPerformance.musqotmp__Region__c  != null){
                    List<String> regions = ganttPerformance.musqotmp__Region__c.split(';');
                    query += ' musqotmp__Region__c IN :regions';
                    query += ' AND ';
                }
            if(ganttPerformance.musqotmp__Country__c != null){
                List<String> countries = ganttPerformance.musqotmp__Country__c.split(';');
                query += ' musqotmp__Country__c IN :countries';
                query += ' AND ';
            }
            if(ganttPerformance.musqotmp__BusinessUnit__c != null){
                List<String> businessUnits = ganttPerformance.musqotmp__BusinessUnit__c.split(';');
                query += ' musqotmp__BusinessUnit__c IN :businessUnits';
                query += ' AND ';
            }
            List<String> dataSetTypes = new List<String>();
            if(ganttPerformance.musqotmp__IndependentData__c != null){
                dataSetTypes.addAll(ganttPerformance.musqotmp__IndependentData__c.split(';'));
            }
            if(ganttPerformance.musqotmp__DependentData__c  != null){
                dataSetTypes.add(ganttPerformance.musqotmp__DependentData__c );
            }
            result.files = dataSetTypes;
            
            query += ' musqotmp__DataVariable__r.id IN :dataSetTypes';
            query += ' AND ';
            
            List<String> dateRanges = dateRange.split(',');
            String dateQuery = ' (';
            for(String df : dateFields){
                dateQuery += '(';
                for(String r : dateRanges){
                    dateQuery += '(' + df +' = ' + r +')';
                    if(!(r == dateRanges.get(dateRanges.size() - 1))) {
                        dateQuery += ' OR ';
                    }
                }
                dateQuery += ')';
                if(!(df == dateFields.get(dateFields.size() - 1))) {
                    dateQuery += ' OR ';
                }
            }
            dateQuery += ')';
         //   System.debug('dateQuery '+ dateQuery);
            query+= dateQuery;
            
            query += ' ORDER BY musqotmp__Date__c';
            query += ' LIMIT 40000';
         //   System.debug('query'+ query);
         List<SObject> dataSets = new List<SObject>();
         if(Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Audience__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__BusinessUnit__c.isAccessible() && 
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__City__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Country__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Date__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__ExternalItemId__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Forecast__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Month__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Quarter__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Region__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Shop__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__ValueCurrency__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Value__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Week__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.musqotmp__Year__c.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.Name.isAccessible() &&
          Schema.sObjectType.musqotmp__DataVariableItem__c.fields.Id.isAccessible() ){
             dataSets = Database.query(query);
         }
            result.sObjects = dataSets;
            result.sObj = ganttPerformance;
            
            
        }Catch(Exception ex){
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            result = DataHandler.handleToast(exceptions);  
        }
        return result;
    }
}