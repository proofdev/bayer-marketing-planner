/************************************************************************************************************************************
* Developer Date    Description
* ---------------------------------------------------------------------------------------------------------------------------------------                                                                 campaigncontributorrelatedlist 
* Abhishek          28/12/2020  Helper class for PlanTrigger.
**************************************************************************************************************************************/
public class PlanTriggerHelper {
    //Create PlanMember for new Plan created.
    public static void afterInsert(List<musqotmp__MarketingPlan__c> newTrigger){
        List<musqotmp__MarketingPlanMember__c> planMemberList = new List<musqotmp__MarketingPlanMember__c>();
        try{
            	if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.isCreateable())
                {
            		for(musqotmp__MarketingPlan__c plan : newTrigger)
                    {
                
                        musqotmp__MarketingPlanMember__c planMember = new musqotmp__MarketingPlanMember__c();
                        planMember.musqotmp__MarketingPlan__c = plan.id;
                        planMember.musqotmp__PlanAccess__c  = 'Owner';
                        planMember.musqotmp__User__c = plan.OwnerId;
                        planMember.musqotmp__UniquePlanMember__c = plan.id+''+plan.OwnerId;
                        planMemberList.add(planMember);
                	}
                    if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__MarketingPlan__c.isCreateable() &&
                      Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__PlanAccess__c.isCreateable() &&
                      Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__User__c.isCreateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__UniquePlanMember__c.isCreateable()){
                    		Database.insert(planMemberList);
                       }
            }
    }catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanTriggerHelper','afterInsert',ex);
        }
    }
    //Update Old Owner access as Read/Write and Add/Update new Owner as Member with Role as: Owner
    //No need to handle share record of the owner, only previous owner sharing should be taken care
    public static void afterUpdate(List<musqotmp__MarketingPlan__c> newTrigger, Map<Id,musqotmp__MarketingPlan__c> oldMap){
        List<musqotmp__MarketingPlan__c> ownerChangePlanList = new List<musqotmp__MarketingPlan__c>();
        Map<String,musqotmp__MarketingPlanMember__c> uniqueIdToPlanMemberMap = new Map<String,musqotmp__MarketingPlanMember__c>();
        List<musqotmp__MarketingPlanMember__c> planMemberListToUpsert = new List<musqotmp__MarketingPlanMember__c>();
        List<musqotmp__MarketingPlanMember__c> objMemberList = new List<musqotmp__MarketingPlanMember__c>();
        try{
            for(musqotmp__MarketingPlan__c plan : newTrigger){
                if(plan.OwnerId != oldMap.get(plan.Id).OwnerId){
                    ownerChangePlanList.add(plan);
                    //System.debug('ownerChangePlanList'+ownerChangePlanList);
                }
            }
            if(ownerChangePlanList.size() > 0){
                if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.isAccessible())
                {
                    objMemberList = [select musqotmp__MarketingPlan__c,musqotmp__User__c , musqotmp__UniquePlanMember__c,musqotmp__Role__c 
                                                                 from musqotmp__MarketingPlanMember__c
                                                                 Where musqotmp__MarketingPlan__c IN :ownerChangePlanList];
                }
                //System.debug('ObjMemberList****' + objMemberList);
                for(musqotmp__MarketingPlanMember__c objPlanMember : objMemberList){
                    uniqueIdToPlanMemberMap.put(objPlanMember.musqotmp__UniquePlanMember__c,objPlanMember);
                }
                //System.debug('uniqueIdToPlanMemberMap***' + uniqueIdToPlanMemberMap);
                 if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.isUpdateable())
                 	{
                        for(musqotmp__MarketingPlan__c plan : ownerChangePlanList)
                        {
                            String uniqueMarketingPlanMember = plan.Id +''+ plan.OwnerId;
                            //System.debug('uniqueMarketingPlanMember for new owner'+uniqueMarketingPlanMember);
                            musqotmp__MarketingPlanMember__c planMember;
                           
                            if(uniqueIdToPlanMemberMap.get(uniqueMarketingPlanMember) != null){
                                planMember = uniqueIdToPlanMemberMap.get(uniqueMarketingPlanMember);
                                planMember.musqotmp__PlanAccess__c  = 'Owner';
                                planMemberListToUpsert.add(planMember);
                            }else{
                                planMember = new musqotmp__MarketingPlanMember__c();
                                planMember.musqotmp__MarketingPlan__c = plan.Id;
                                planMember.musqotmp__User__c = plan.OwnerId;
                                planMember.musqotmp__PlanAccess__c  = 'Owner';
                                planMember.musqotmp__UniquePlanMember__c  = uniqueMarketingPlanMember; 
                                planMemberListToUpsert.add(planMember);
                            }
                            //System.debug('planMemberListToUpsert owner'+planMemberListToUpsert);
                            String uniquePlanMemberOldOwner = plan.Id +''+ oldMap.get(plan.Id).OwnerId;
                            //System.debug('uniquePlanMemberOldOwner'+uniquePlanMemberOldOwner);
                            if(uniqueIdToPlanMemberMap.get(uniquePlanMemberOldOwner) != null){
                               /* planMember = uniqueIdToPlanMemberMap.get(uniquePlanMemberOldOwner);
                                planMember.musqotmp__PlanAccess__c  = 'Read Write';
                                //System.debug('Plan Member to read/write');
                                if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__MarketingPlan__c.isCreateable() &&
                      				Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__PlanAccess__c.isCreateable() &&
                      				Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__User__c.isCreateable() &&
                                    Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__UniquePlanMember__c.isCreateable() &&
                                  	Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__MarketingPlan__c.isUpdateable() &&
                      				Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__PlanAccess__c.isUpdateable() &&
                      				Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__User__c.isUpdateable() &&
                                    Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__UniquePlanMember__c.isUpdateable()){
                                     PlanMemberListToUpsert.add(planMember);  
                                   }*/
                            }
                    	}
                    //System.debug('planMemberListToUpsert*'+ planMemberListToUpsert);
                    if(planMemberListToUpsert.size() > 0 /*&& 
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__User__c.isCreateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__MarketingPlan__c.isCreateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__PlanAccess__c.isCreateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__UniquePlanMember__c.isCreateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__User__c.isUpdateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__MarketingPlan__c.isUpdateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__PlanAccess__c.isUpdateable() &&
                       Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__UniquePlanMember__c.isUpdateable()*/){
                        Database.upsert(planMemberListToUpsert);
                    }
                }
            }
        }catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanTriggerHelper','afterUpdate',ex);
        }
    }   
}