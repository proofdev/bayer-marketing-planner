public with sharing class PlanMemberController {
   @AuraEnabled(cacheable=true)
    public static DataModel.Result getPlanMembers(string recordId){
        DataModel.Result result = new DataModel.Result();
        DataModel.Track track = new DataModel.Track();
        try{
            Map<Id, String> IdMap = new Map<Id, String>();
            Set<Id> parentIds = new Set<Id>();
            List<ObjectPermissions> sObjPermissions = [SELECT Id, ParentId, SobjectType, PermissionsRead FROM ObjectPermissions 
                                                       where SobjectType = 'musqotmp__MarketingPlan__c' LIMIT 40000];
            for(ObjectPermissions obj: sObjPermissions) {
                if(obj.PermissionsRead == true){
                    parentIds.add(obj.ParentId);
                }            
            }
            Set<Id> assigneeIds = new Set<Id>();
            List<PermissionSetAssignment> sObjPermissionSetAssign = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment 
                                                                     WHERE PermissionSetId IN :parentIds];
            for(PermissionSetAssignment sObj: sObjPermissionSetAssign){
                assigneeIds.add(sObj.AssigneeId);
            }
            List<musqotmp__MarketingPlanMember__c> sObjPlanMembers = [Select Id, musqotmp__UesrEmail__c, musqotmp__UserName__c, 
                                                                      musqotmp__User__c FROM musqotmp__MarketingPlanMember__c 
                                                                      WHERE musqotmp__MarketingPlan__c = :recordId LIMIT 40000];
          //  System.debug('sObjPlanMembers :'+sObjPlanMembers);
            Set<Id> planUserIds = new Set<Id>();
            for(musqotmp__MarketingPlanMember__c planMember : sObjPlanMembers){
                planUserIds.add(planMember.musqotmp__User__c);
            }
         //   System.debug('planUserIds :'+planUserIds);
            Set<Id> sObjIds = new Set<Id>();
            List<SObject> sObjUsers = [Select Id, Name, Email FROM User Where Id IN :assigneeIds AND Id NOT IN :planUserIds LIMIT 10];
            for(SObject sObj: sObjUsers){
                sObjIds.add((Id) sObj.get('Id'));
            }
         //   System.debug('sObjIds :'+sObjIds);
            result.sObjectList = sObjUsers;
            result.sObjIds = sObjIds;
            DataModel.Toast toast = new DataModel.Toast();
            toast.ok = true;
            result.toast = toast;
          //  System.debug('sObjUsers :'+sObjUsers);        
        }catch(Exception ex){
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            if(track.component != null){
                exceptions.component = track.component;
            }
            result = DataHandler.handleToast(exceptions);
        }
        return result;
    }
    @AuraEnabled
    public Static List<musqotmp__MarketingPlanMember__c> CreateRecords(List<musqotmp__MarketingPlanMember__c> sObjectList){
      //  System.debug('enter ');
        DataModel.Result result = new DataModel.Result();
        DataModel.Track track = new DataModel.Track();
        List<musqotmp__MarketingPlanMember__c> sObjList = new List<musqotmp__MarketingPlanMember__c>();
        try{
            sObjList.addAll(sObjectList);
            if(Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__MarketingPlan__c.isCreateable() &&
            Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__User__c.isCreateable() &&
            Schema.sObjectType.musqotmp__MarketingPlanMember__c.fields.musqotmp__PlanAccess__c.isCreateable()){
              insert sObjList;
          }
          //  System.debug('sObjectList Planmem :'+sObjectList);
        }catch(Exception ex){
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            if(track.component != null){
                exceptions.component = track.component;
            }
            result = DataHandler.handleToast(exceptions);
        }
        return sObjList;
    }
}