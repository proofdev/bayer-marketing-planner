public class KIDController {
    public PageReference updateCode(){
        String codeParameter = System.currentPagereference().getParameters().get('code');
        //System.debug('codeParameter'+codeParameter);
        if(codeParameter != null){
            String headerdata = ApexPages.currentPage().getHeaders().get('Host');
            String baseURL = headerdata.split('--')[0];
            String URL = 'https://' + baseURL + '.lightning.force.com/lightning/n/musqotmp__KeyInsightDashboards?c__code=';
        	//return new PageReference('https://business-speed-12662-dev-ed.lightning.force.com/lightning/n/musqotmp__KeyInsightDashboards?c__code='+codeParameter); 
        	return new PageReference(URL + codeParameter);
        } else {
            return null;
        }	     
    }
}