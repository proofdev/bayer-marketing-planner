/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              06/04/2020          Created for W-000339,W-000359 (helper class for JourneyTrigger)
**************************************************************************************************************************************/
public with sharing class JourneyTriggerHelper {
    //Added for W-000339
    public static void processDataVarForecastsAfterUpdate(List<MarketingJourney__c> triggerNew, Map<Id,MarketingJourney__c> triggerOldMap){
        Set<Id> journeyIdSetForDataVarForecast = new Set<Id>();
        try{
        	for(MarketingJourney__c journey: triggerNew){
                if(journey.musqotmp__Type__c  != triggerOldMap.get(journey.Id).musqotmp__Type__c ||
                   journey.musqotmp__StartDate__c != triggerOldMap.get(journey.Id).musqotmp__StartDate__c ||
                   journey.musqotmp__EndDate__c != triggerOldMap.get(journey.Id).musqotmp__EndDate__c ||
                   journey.musqotmp__Audience__c != triggerOldMap.get(journey.Id).musqotmp__Audience__c ||
                   journey.musqotmp__BusinessUnit__c != triggerOldMap.get(journey.Id).musqotmp__BusinessUnit__c ||
                   journey.musqotmp__MarketingRegion__c  != triggerOldMap.get(journey.Id).musqotmp__MarketingRegion__c ||
                   journey.musqotmp__Countries__c  != triggerOldMap.get(journey.Id).musqotmp__Countries__c ||
                   journey.musqotmp__City__c  != triggerOldMap.get(journey.Id).musqotmp__City__c ||
                   journey.musqotmp__Shop__c  != triggerOldMap.get(journey.Id).musqotmp__Shop__c ||
                   journey.musqotmp__ProductGroup__c  != triggerOldMap.get(journey.Id).musqotmp__ProductGroup__c
                  ){
                    journeyIdSetForDataVarForecast.add(journey.Id);
                }
            }
            String allocationSQL = 'SELECT Id';
            allocationSQL += ' FROM musqotmp__Allocation__c';
            allocationSQL += ' WHERE musqotmp__MarketingActivity__c = NULL AND';
            allocationSQL += ' musqotmp__Journey__c in :journeyIdSetForDataVarForecast';
            /*if(journeyIdSetForDataVarForecast.size () > 0 && 
                DataVariableJobForecastUtility.isObjectExist('musqotmp__Allocation__c')){
                List<Id> allocationIdList = new List<Id>();
                 if(Schema.describeSObjects(new String[]{'musqotmp__Allocation__c'})[0].isAccessible()){
                    for(Sobject allocation: Database.query(String.escapeSingleQuotes(allocationSQL))){
                    allocationIdList.add(allocation.Id);
                    }
                    if(allocationIdList.size() > 0){
                        //DataVariableJobForecastUtility.publishAllocationForecastEvent(allocationIdList,true);
                    }
                }
            }  */  
        } Catch(Exception e){
            ExceptionHandler.logError('Class','JourneyTriggerHelper','processDataVarForecastsAfterUpdate',e);
        }
    }
}