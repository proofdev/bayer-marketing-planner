public class QueryFactory {
	/**
	 * Convert the values provided to this instance into a full SOQL string for use with Database.query
	 * Check to see if subqueries queries need to be added after the field list.
	 **/
	public String toSOQL(DataModel.Track track) {
	//	System.debug('1>>>>>> '+track);
		StringBuilder.CommaDelimitedListBuilder sb = new StringBuilder.CommaDelimitedListBuilder();
		SObjectDescribe d = SObjectDescribe.getDescribe(track.objectApiName);
		List<String> values = new List<String>();
		values.addAll(track.fieldMap.keySet());
	//	System.debug('12>>>>>> '+values);
		sb.add(values);
		String query = 'SELECT ';
		query += sb.getStringValue();
		query += ' FROM ' + track.objectApiName + '';
		if(track.whereCaluses != null && track.whereCaluses.size() > 0) {
			query += ' WHERE ';
            //System.debug('whereCaluses'+track.whereCaluses);
			for(DataModel.whereCaluse w :track.whereCaluses) {
                String value = w.value;
                //List<String> valuesList = new List<String>();
				Schema.DisplayType fieldType;
				if(!w.field.contains('.')) {
					fieldType = d.getField(w.field).getDescribe().getType();
				}
				query += ' ' + w.field + '';
				query += ' ' + w.clause + '';
				if(w.clause == 'LIKE') {
					query += ' ' + '\'%' + value + '%\'' + '';
				} else if(w.clause == 'IN' || w.clause == 'NOT IN' || w.clause == 'INCLUDES') {
                    String[] valuesArr = w.values;
                    for(Integer i; i<= valuesArr.size(); i++){
                        valuesArr[i] = valuesArr[i];
                    }
					query += ' (\'' + String.join(valuesArr, '\',\'') + '\')';
				} else if(fieldType == Schema.DisplayType.DATE || fieldType == Schema.DisplayType.DATETIME) {
					query += ' ' + '' + value + '' + '';
				} else if(fieldType == Schema.DisplayType.BOOLEAN) {
					query += ' ' + '' + Boolean.valueOf(value) + '' + '';
				} else if(fieldType == Schema.DisplayType.INTEGER || fieldType == Schema.DisplayType.DOUBLE) {
					query += ' ' + value + '';
				} else {
					if(value == 'true') {
						query += ' ' + '' + Boolean.valueOf(value) + '' + '';
					} else {
						if(w.field == 'YearQuarter__c'){
                            query += ' ' + '' + value + '' + '';
                        }else{
                            query += ' ' + '\'' + value + '\'' + '';
                        }
					}
				}

				if(!String.isEmpty(w.operator)) {
					query += ' ' + w.operator + '';

				}
			}
			if(track.whereCaluseSubs != null && track.whereCaluseSubs.size() > 0) {
				query += ' ' + track.operator + '';
				query += ' (';
				Integer i = 0;
				for(DataModel.whereCaluse w :track.whereCaluseSubs) {
                    String subClauseValue = w.value;
					Schema.DisplayType fieldType;
					if(!w.field.contains('.')) {
						fieldType = d.getField(w.field).getDescribe().getType();
					}
					query += ' ' + w.field + '';
					query += ' ' + w.clause + '';
					if(w.clause == 'LIKE') {
						query += ' ' + '\'%' + subClauseValue + '%\'' + '';
					} else if(w.clause == 'IN' || w.clause == 'NOT IN') {
                        String[] subClausevaluesArr = w.values;
                        for(Integer j; j<= subClausevaluesArr.size(); j++){
                            subClausevaluesArr[j] = subClausevaluesArr[j];
                        }
						query += ' (\'' + String.join(subClausevaluesArr, '\',\'') + '\')';
					} else if(fieldType == Schema.DisplayType.DATE || fieldType == Schema.DisplayType.DATETIME) {
						query += ' ' + '' + subClauseValue + '' + '';
					} else if(fieldType == Schema.DisplayType.BOOLEAN) {
						query += ' ' + '' + Boolean.valueOf(subClauseValue) + '' + '';
					} else if(fieldType == Schema.DisplayType.INTEGER || fieldType == Schema.DisplayType.DOUBLE) {
						query += ' ' + subClauseValue + '';
					} else {
						if(subClauseValue == 'true') {
							query += ' ' + '' + Boolean.valueOf(subClauseValue) + '' + '';
						} else {
							query += ' ' + '\'' + subClauseValue + '\'' + '';
						}
					}

					if(!String.isEmpty(w.operator) && !(i ++== track.whereCaluseSubs.size() -1)) {
						query += ' ' + w.operator + '';

					}
				}
				query += ')';
			}
		//	System.debug('94>>>>>'+ query);
		}
		if(track.sortBy != null) {
			query += ' ORDER BY ' + track.sortBy + '';
			if(track.sortDirection != null) {
				query += ' ' + track.sortDirection + '';
			}
		}
		if(track.amountOfRecords != null) {
			query += ' LIMIT ' + track.amountOfRecords + '';
		}
		Integer offset = 0;
		Integer recordEnd = 0;
		if(track.pageNumber != null && track.amountOfRecords != null) {
			recordEnd = track.amountOfRecords * track.pageNumber;
			offset = (track.pageNumber - 1) * track.amountOfRecords;
			query += ' OFFSET ' + offset + '';
		}
		return query;
		//List<Sobject> recordList = Database.query(query);
        //return recordList;
	}
}