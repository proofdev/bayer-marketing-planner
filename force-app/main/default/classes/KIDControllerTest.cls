@isTest
public class KIDControllerTest {
	@isTest
    static void updateCodeTest() {
        KIDController obj = new KIDController();
        ApexPages.currentPage().getHeaders().put('Host',URL.getSalesforceBaseUrl().toExternalForm());
        PageReference pf = obj.updateCode();
        ApexPages.currentPage().getParameters().put('code','pCHbEgE_zluDr9io');
        pf = obj.updateCode();
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
}