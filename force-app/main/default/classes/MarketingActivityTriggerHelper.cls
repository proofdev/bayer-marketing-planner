/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              06/05/2020          Created for W-000339,W-000359 (helper class for JourneyTrigger)
**************************************************************************************************************************************/
public with sharing class MarketingActivityTriggerHelper {
    //Added for W-000339
    public static void processDataVarForecastsAfterUpdate(List<MarketingActivity__c> triggerNew, Map<Id,MarketingActivity__c> triggerOldMap){
        Set<Id> marketActivityIdSet = new Set<Id>();
      /*  try{
        	for(MarketingActivity__c marketActivity: triggerNew){
                if(marketActivity.musqotmp__Type__c != triggerOldMap.get(marketActivity.Id).musqotmp__Type__c ||
                   marketActivity.musqotmp__StartDate__c != triggerOldMap.get(marketActivity.Id).musqotmp__StartDate__c ||
                   marketActivity.musqotmp__EndDate__c != triggerOldMap.get(marketActivity.Id).musqotmp__EndDate__c ||
                   marketActivity.musqotmp__Audience__c != triggerOldMap.get(marketActivity.Id).musqotmp__Audience__c ||
                   marketActivity.musqotmp__MarketingBusinessUnit__c != triggerOldMap.get(marketActivity.Id).musqotmp__MarketingBusinessUnit__c ||
                   marketActivity.musqotmp__MarketingArea__c   != triggerOldMap.get(marketActivity.Id).musqotmp__MarketingArea__c  ||
                   marketActivity.musqotmp__Countries__c  != triggerOldMap.get(marketActivity.Id).musqotmp__Countries__c ||
                   marketActivity.musqotmp__City__c  != triggerOldMap.get(marketActivity.Id).musqotmp__City__c ||
                   marketActivity.musqotmp__Shop__c  != triggerOldMap.get(marketActivity.Id).musqotmp__Shop__c ||
                   marketActivity.musqotmp__ProductGroup__c  != triggerOldMap.get(marketActivity.Id).musqotmp__ProductGroup__c
                  ){
                    marketActivityIdSet.add(marketActivity.Id);
                }
            }
            String allocationSQL = 'SELECT Id';
            allocationSQL += ' FROM musqotmp__Allocation__c';
            allocationSQL += ' WHERE musqotmp__MarketingActivity__c in :marketActivityIdSet';
            
            if(marketActivityIdSet.size () > 0 && 
               DataVariableJobForecastUtility.isObjectExist('musqotmp__Allocation__c')){
                List<Id> allocationIdList = new List<Id>();
                if(Schema.describeSObjects(new String[]{'musqotmp__Allocation__c'})[0].isAccessible()){
                    for(Sobject allocation: Database.query(String.escapeSingleQuotes(allocationSQL))){
                    allocationIdList.add(allocation.Id);
                    }
                    if(allocationIdList.size() > 0){
                        //DataVariableJobForecastUtility.publishAllocationForecastEvent(allocationIdList,true);
                    }
                }
            }  
        } Catch(Exception e){
           // ExceptionHandler.logError('Class','MarketingActivityTriggerHelper','processDataVarForecastsAfterUpdate',e);
        }*/
    }
}