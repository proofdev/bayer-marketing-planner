@isTest
private with sharing class CampaignManagerAPITest {
    @isTest
    static void handleToast(){
        NoAccessException ex = new NoAccessException();
        try{
            DataModel.Exceptions exceptions = new DataModel.Exceptions();
            exceptions.ex = ex;
            CampaignManagerAPI.handleToast(exceptions);
        }catch(Exception e){
        }
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
	@isTest
	static void testReadCampaignManagerSetup() {
		DataModel.Result result = CampaignManagerAPI.readCampaignManagerSetup();
        SObject objInfo = (Sobject)CampaignManagerSetup__c.getInstance();
        System.assertEquals(objInfo , result.sObj);
    }
    @isTest
	static void testReadMarketingCloudCredential() {
		DataModel.Result result = CampaignManagerAPI.readMarketingCloudCredential();
        SObject objInfo = (Sobject)MarketingCloudCredential__c.getInstance();
        System.assertEquals(objInfo , result.sObj);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
	}
	@isTest
	static void testGetDynamicObjectInfo() {
		DataModel.Result result = CampaignManagerAPI.getDynamicObjectInfo('Campaign', '');
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
	}
	@isTest
	static void testDynamicObjectInfo() {
		DataModel.Result result = CampaignManagerAPI.DynamicObjectInfo('Campaign', '');
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
	}
	@isTest
	static void testDatatable() {
		DataModel.Result result = CampaignManagerAPI.datatable('Campaign');
        System.assertEquals(null, result.contentIds,'Content Ids should be null for this scenario');
	}
    @isTest
    static void testgetRecords(){
        DataModel.Track track = new DataModel.Track();
        track.recordId = '';
        String param = JSON.serialize(track);
        DataModel.Result result = CampaignManagerAPI.getRecords(param);
        System.assertEquals(null, result.contentIds,'Content Ids should be null for this scenario');
    }
    @isTest
    static void testSendToast(){
        DataModel.Track track = new DataModel.Track();
        track.component = 'sendToast';
        System.assertEquals('sendToast' , track.component);
        String param = JSON.serialize(track);
        DataModel.Result result = CampaignManagerAPI.sendToast(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
	static void testGetOrgId() {
		DataModel.Result result = CampaignManagerAPI.getOrganizationId('');
        System.assertEquals(null, result.contentIds, 'Read only profile should be able to read Contact.LastName');
	}
    @isTest
    static void testPostCalloutResponseContents(){
        DataModel.Track track = new DataModel.Track();
        track.URL = 'sendToast';
        track.Body = 'sendToast';
        String param = JSON.serialize(track);
        DataModel.Result result = CampaignManagerAPI.postCalloutResponseContents(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void testGetCalloutResponseContents(){          
        DataModel.Track track = new DataModel.Track();
        track.URL = 'https://wwww.musqot.com';
        track.Authorization = 'Bearer ndjhqwihdiuqdnq';
        track.ContentType = 'application/json';
        String param = JSON.serialize(track);
        DataModel.Result result = CampaignManagerAPI.getCalloutResponseContents(param);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
	static void testToSOQL() {
        DataModel.Field field = new DataModel.Field();
            field.name = 'Description';
            field.label = 'Description';
            field.type = 'textarea';
            field.visible = true;
            List<DataModel.whereCaluse> whereCaluses = new List<DataModel.whereCaluse>();
            DataModel.whereCaluse whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereCaluses.add(whereClause);
            List<DataModel.whereCaluse> whereCaluseSubs = new List<DataModel.whereCaluse>();
            whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereClause.operator = 'OR';
            whereCaluseSubs.add(whereClause);
            whereClause = new DataModel.whereCaluse();
            whereClause.field = 'Id';
            whereClause.value = '';
            whereClause.clause = '!=';
            whereCaluseSubs.add(whereClause);
            DataModel.Track track = new DataModel.Track();
            track.component = 'DataHandlerTest-getRecords';
            track.objectApiName = 'Campaign';
            //track.fieldSetApiName = 'Musqotmpm__ItemsToApproveFinanceDetail';
            track.fieldMap = new Map<String, DataModel.Field>
            {
                'Id' => new DataModel.Field(),
                    'OwnerId' => new DataModel.Field(),
                    'CreatedById' => new DataModel.Field(),
                    'LastModifiedById' => new DataModel.Field()
                    };
                        track.whereCaluses = whereCaluses;
            track.operator = 'AND';
            track.whereCaluseSubs = whereCaluseSubs;
            track.sortBy = 'Name';
            track.sortDirection = 'ASC';
            track.amountOfRecords = 40000;
		String result = CampaignManagerAPI.toSOQL(track);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
	}
    @isTest
    static void getFieldSetFieldsTest(){
        //List<DataModel.Field> getFieldSetFields(Schema.SObjectType token,String fieldSetName
        Schema.SObjectType mktPlan = musqotmp__MarketingPlan__c.sObjectType ;
        List<DataModel.Field> fields = CampaignManagerAPI.getFieldSetFields(mktPlan,'musqotmp__MarketingPlanGanttInfo');
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
    @isTest
    static void buildChildTest(){
        musqotmp__MarketingPlan__c  marketingPlan = TestDataSetup.getMarketingPlan('Marketing Plan',Date.Today(),Date.Today()+1,'In Progress','Global');
        insert marketingPlan;
        Schema.Campaign campaignRecord = TestDataSetup.getCampaign('Division A',Date.Today(),Date.Today()+1,'In Progress',marketingPlan);
        insert campaignRecord;
        List<SObject> camList = new List<SObject>();
        camList.add((SObject)campaignRecord);
        List<SObject> objLst = CampaignManagerAPI.buildChild(camList,campaignRecord.Id);
        System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
    }
}