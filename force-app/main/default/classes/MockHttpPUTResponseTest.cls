@isTest
global class MockHttpPUTResponseTest implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req){
        System.assertEquals('PUT', req.getMethod());
      	
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"example":"test"}');
        res.setStatusCode(401);
        return res;
    }
}