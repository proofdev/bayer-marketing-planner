public with sharing class Organization {
    @AuraEnabled(cacheable=true)
    public static DataModel.Result getOrganizationId(String param) {
        DataModel.Result result = DataHandler.getOrganizationId(param);
        return result;
    }
}