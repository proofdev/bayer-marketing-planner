/************************************************************************************************************************************
* Developer			Date				Description
* ---------------------------------------------------------------------------------------------------------------------------------------	                                                               												campaigncontributorrelatedlist 
* Abhishek          06/01/2021			Test class for PlanTrigger & PlanTriggerHelper
**************************************************************************************************************************************/
@isTest(SeeAllData = false)
public class PlanTriggerHelperTest {
	@isTest
    static void testPlanTrigger(){ 
        try{
            Date todaysdate = Date.today();
            Date sDateCurrentYear = DATE.newInstance(todaysdate.year(), 1, 1);
            Date eDateCurrentYear = DATE.newInstance(todaysdate.year(), 12, 31);
            User u = TestDataSetup.getUser('System Administrator');
            User u1 = TestDataSetup.getUser('Standard User');
        	insert u1;
        	TestDataSetup.setPermissionSet(u1.id, 'CampaignManagerSalesforceUser');
            System.runAs(u) {

               	DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance(); 
               	insert disableTrigger;
                musqotmp__MarketingPlan__c marketingPlan = musqotmp.TestDataSetup.getMarketingPlan('Marketing Plan',sDateCurrentYear,eDateCurrentYear,'In Progress','Global');
                marketingPlan.musqotmp__MarketingBusinessUnit__c = 'Division A';
                marketingPlan.musqotmp__CampaignTemplate__c = false;
                marketingPlan.musqotmp__MarketingArea__c = 'Europe';
                Id recordTypeId =Schema.SObjectType.musqotmp__MarketingPlan__c.getRecordTypeInfosByDeveloperName().get('MarketingPlan').getRecordTypeId();
                marketingPlan.RecordTypeId = recordTypeId;
                insert marketingPlan;
                
                Test.startTest();
                marketingPlan.OwnerId = u1.Id;
                update marketingPlan;
                Test.stopTest();
                System.assertEquals(null, null, 'Read only profile should be able to read Contact.LastName');
            }
            
        }Catch(Exception ex){
            ExceptionHandler.logError('Apex','PlanTriggerHelperTest','testPlanTrigger',ex);
        }
    }
}