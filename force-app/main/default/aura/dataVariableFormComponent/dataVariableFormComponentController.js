({
    closeUtilityBar : function(component, event, helper) {
        var value = event.getParam('v');
        var utilityAPI = component.find("utilitybar");
         utilityAPI.minimizeUtility();
     },
    doInit : function(component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "xyz__TestObject__c"
        });
        createRecordEvent.fire();
    }
})