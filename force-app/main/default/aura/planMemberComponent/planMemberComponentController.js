({
    doInit: function (component, event, helper) {
        var pageRef = component.get("v.pageReference");
        var state = pageRef.state; // state holds any query params
        var base64Context = state.inContextOfRef;
		/*
			*For some reason, the string starts with "1.", if somebody knows why,
			*this solution could be better generalized.
		*/
        if (base64Context.startsWith("1\.")) {
            base64Context = base64Context.substring(2);
        }
        var addressableContext = JSON.parse(window.atob(base64Context));
        component.set("v.recordId", addressableContext.attributes.recordId);
        component.set("v.showModal", true);
    },
    closeModal: function (component, event, helper) {
        var value = event.getParam('v');
        $A.get("e.force:closeQuickAction").fire();
        /* window.setTimeout(
             $A.getCallback(function(){
              $A.get("e.force:closeQuickAction").fire(); 
             }),0
         );*/
    },
})