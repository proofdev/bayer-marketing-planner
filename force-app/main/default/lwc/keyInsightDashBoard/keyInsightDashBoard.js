import { LightningElement, wire, track, api } from "lwc";
import { updateRecord, getRecord } from "lightning/uiRecordApi";
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";
import { loadStyle, loadScript } from "lightning/platformResourceLoader";
import { CurrentPageReference, NavigationMixin } from "lightning/navigation";
import { refreshApex, getSObjectValue } from "@salesforce/apex";
import userId from "@salesforce/user/Id";

import highchart from "@salesforce/resourceUrl/highchart_8";
import getRecords from "@salesforce/apex/keyInsightDashBoard.getRecords";
import parseDataFromJwtToken from "@salesforce/apex/keyInsightDashBoard.parseDataFromJwtToken";
import KEYINSIGHTDASHBOARD_OBJECT from "@salesforce/schema/KeyInsightDashboard__c";
import KEYINSIGHTDASHBOARD_ID_FIELD from "@salesforce/schema/KeyInsightDashboard__c.Id";
import getCalloutResponseContentList from "@salesforce/apex/keyInsightDashBoard.getCalloutResponseContentList";
import getCalloutResponseContentMap from "@salesforce/apex/keyInsightDashBoard.getCalloutResponseContentMap";
import putCalloutResponseContents from "@salesforce/apex/keyInsightDashBoard.putCalloutResponseContents";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import readKIDSetupSettings from "@salesforce/apex/keyInsightDashBoard.readKIDSetupSettings";
import alternate_drive from "@salesforce/resourceUrl/alternate_drive";
import primary_drive from "@salesforce/resourceUrl/primary_drive";
import MOMENT_JS from "@salesforce/resourceUrl/moment";

import chevronDown from "@salesforce/resourceUrl/chevronDown";
import chevronUp from "@salesforce/resourceUrl/chevronUp";
import cardIcon from "@salesforce/resourceUrl/menu2";
export default class KeyInsightDashBoard extends LightningElement {
  @api recordId;
  @api keyInsightDashboardId;
  @track paramRouteToValueGetRecords;
  @track paramMovingValueItemGetRecords;
  @track paramRegressionGetRecords;
  @track wiredRegressionData;
  @track regressionMovingValueList = [];
  @track sectionCssClass = "slds-modal slds-fade-in-open slds-modal_small";
  @track altHsh = {};
  @track altLabel;
  @track showInputSection = false;
  @track altScenarioHsh = {};
  @track keyInsightData;
  @track routeToValueMap = {};
  @track insightPanel = [];
  @track driverType;
  @track commentValue;
  @track commentBoxValue;
  @track refreshArray = [];
  @track refeshButtonActive = false;
  @track updatedCauses = [];
  @track resultMap;
  @track wiredGetRecord;
  @track domainURL;
  @track kid;
  @track sfUserId = userId;
  @track proofUserId;
  @track proofOrgId;
  @track proofToken;
  @track proofIdToken;
  //@track proofURL = "https://testapp.get-proof.com";
  @track proofURL;
  @track paramParseDataFromJwtToken;
  @track calloutResponseContentMapData;
  @track KIDSettings;
  @track myValue = "";
  @track futureData = [];
  @track targetForecastOutput = "";
  @track isResult = false;
  @track isMaxLimit = false;
  @track forwardLookingDate = "";
  @track calForwardLookingDateRange = 0.0;
  @track calForwardLookingDateOutput = false;
  @track kidMonthlyFLDR = "";
  @track frequencyMonthly = false;
  @track frequencyDaily = false;
  @track frequencyWeekly = false;
  @track frequencyQuarterly = false;
  @track quarterOptions = [];
  @track futureTimePeriodList = [];
  @track forwardLookingDateRangeResult = "";
  @track forwardLookingDateRangeDate = "";
  @track getProjectedSpend = "";
  @track tagetTotaloutcomeStartDate = "";
  @track tagetTotaloutcome = "";
  @track historicalTimePeriodQuarterly = [];
  @track historicalTimePeriodOptions = [];
  @track getTagetTotaloutcome = "";
  @track getTagetTotaloutcomeValue = "";

  @track targetOutcomeResult = "";
  @track targetTotalOutcome = "";
  @track aggregateTargetOutcome = 0.0;

  activeSections = ["A", "B", "C", "D"];
  currentPageReference = null;
  urlStateParameters = null;
  @track primary = primary_drive;
  @track alternate = alternate_drive;
  @track chevronDown = chevronDown;
  @track chevronUp = chevronUp;
  @track cardIcon = cardIcon;
  urlId = null;
  urlLanguage = null;
  urlType = null;
  @track isLoading = false;
  @track showInsights = true;
  @track showRouteToValue = true;
  @track showAlternative = true;
  @track proofrequency;
  @track getTargetTotalOutcome = "";
  @track alternateScenarioProjectedTotalOutComeFromProof=false;
  @track alternateScenarioTargetForecastFromProof=false;
  @track alternateScenarioForwardLookingDateRangeFromProof=false;

  @track maxDate;
  @track minDate;
  @track maxFLDR;
  @track minFLDR;

  @track maxDateTargetTotal;
  @track minDateTargetTotal;
  handleInsightsAccordion() {
    this.showInsights = this.showInsights ? false : true;
  }
  handleRouteToValueAccordion() {
    this.showRouteToValue = this.showRouteToValue ? false : true;
    if (!this.showRouteToValue) {
      this.template.querySelector(".routeToValue").classList.add("display-none");
    } else {
      this.template.querySelector(".routeToValue").classList.remove("display-none");
    }
  }
  handleAlternativeAccordion() {
    this.showAlternative = this.showAlternative ? false : true;
  }

  get moment() {
    return moment;
  }

  renderedCallback() {
    try {
      Promise.all([loadScript(this, MOMENT_JS)])
        .then(() => {
          if (moment) {
            this.moment;
            //this.handleRefresh();
          }
        })
        .catch((error) => {
          //	this.template.querySelector("c-toast").showToast(error);
        });
    } catch (error) {
      //this.template.querySelector("c-toast").showToast(error);
    }
  }

  @wire(parseDataFromJwtToken, { param: "$paramParseDataFromJwtToken" })
  wiredParseDataFromJwtToken(result) {
    if (result.data) {
      let proofAuthMap = result.data.proofAuthMap;
      this.proofToken = result.data.proofToken;
      this.proofIdToken = result.data.proofIdToken;
      this.proofOrgId = result.data.proofAuthMap.orgId;
      this.proofUserId = result.data.proofAuthMap.sub.split("|")[1];
      this.domainURL = this.proofURL + "/api/organisations/" + this.proofOrgId + "/users/" + this.proofUserId + "/key_insight_dashboard/" + this.kid;
      this.getResponseContents();
      this.getResponseInsight();
    }
  }
  @wire(getRecord, { recordId: "$recordId", layoutTypes: ["Full"], modes: ["Edit"] })
  wiredGetRecordData({ data, error }) {
    if (data) {
      this.kid = data.fields.musqotmp__keyInsightDashboardId__c.value;
      this.getKIDSetupSettings();
    } else if (error) {
      this.template.querySelector("c-toast").showToast(error);
    }
  }

  @wire(CurrentPageReference)
  getStateParameters(currentPageReference) {
    if (currentPageReference) {
      this.urlStateParameters = currentPageReference.state;
      this.setParametersBasedOnUrl();
    }
  }

  setParametersBasedOnUrl() {
    this.urlId = this.urlStateParameters.id || null;
    this.urlLanguage = this.urlStateParameters.lang || null;
    this.urlType = this.urlStateParameters.type || "10";
  }

  getKIDSetupSettings() {
    let paramdata = JSON.stringify({ Param: "TestParam" });
    readKIDSetupSettings({ paramdata }).then((res) => {
      this.KIDSettings = res;
      this.proofURL = this.KIDSettings.sObj.musqotmp__proofURL__c;
      this.paramParseDataFromJwtToken = JSON.stringify({
        recordId: this.sfUserId,
      });
    });
  }
  disconnectedCallback() {
    this.altScenarioHsh = {};
    this.regressionMovingValueList = [];
    this.routeToValueMap = {};
  }
  getResponseInsight() {
    let param = JSON.stringify({
      URL: this.domainURL + "?resource_type=KEY_INSIGHT_DASHBOARD",
      Authorization: this.proofIdToken,
    });
    getCalloutResponseContentMap({ param: param }).then((res) => {
      this.refreshArray = res;
      this.insightPanel = [];
      if (res && res.resultMap && res.resultMap.insights) {
        let drive = {};
        res.resultMap.insights.forEach((ele) => {
          if (ele.driverType == "PRIMARY") {
            drive["causeId"] = ele.causeId;
            drive["causeName"] = ele.causeName;
            drive["multiplier"] = ele.multiplier;
            drive["driverType"] = ele.driverType;
            drive["comment"] = ele.comment;
            drive["driveimage"] = this.primary;
            this.insightPanel.push(drive);
            drive = {};
          } else if (ele.driverType == "ALTERNATE") {
            drive["causeId"] = ele.causeId;
            drive["causeName"] = ele.causeName;
            drive["multiplier"] = ele.multiplier;
            drive["driverType"] = ele.driverType;
            drive["comment"] = ele.comment;
            drive["driveimage"] = this.alternate;
            this.insightPanel.push(drive);
            drive = {};
          }
        });
      } else {
        //console.log("No Data");
      }
    });
  }
  getResponseContents() {
    let url = "";
    let kid = new URL(window.location.href).searchParams.get("c__keyInsightDashboardId");
    let param = JSON.stringify({
      URL: this.domainURL + "/route_to_value?resource_type=KEY_INSIGHT_DASHBOARD",
      Authorization: this.proofIdToken,
    });
    getCalloutResponseContentMap({ param: param }).then((res) => {
      this.calloutResponseContentMapData = res;
      this.altScenarioHsh = {};
      this.regressionMovingValueList = [];
      this.resultMap = res.resultMap;
      if (res.resultMap) {
        this.proofrequency = res.resultMap.frequency;
        if (res.resultMap.routeToValues) {
          this.isLoading = true;
          let causes = [];
          if (res.resultMap.routeToValues[1].causes) {
            causes = res.resultMap.routeToValues[1].causes.slice(0, 5);
          }
          causes.forEach((ele) => {
            let regressionMap = {};
            regressionMap["id"] = ele.id;
            regressionMap["name"] = ele.name;
            regressionMap["unit"] = ele.unit;
            regressionMap["delta"] = ele.delta;
            regressionMap["lastHistoricalValue"] = ele.lastHistoricalValue;
            regressionMap["lastHistoricalTimePeriod"] = ele.lastHistoricalTimePeriod;
            regressionMap["altLabel"] = ele.delta;
            this.altScenarioHsh[ele.id] = regressionMap;
            this.regressionMovingValueList.push(regressionMap);
          });
          this.alterNativeScenarioData(res.resultMap);
        } else {
          const event = new ShowToastEvent({
            //title: 'Get Help',
            title: res.resultMap.reasonPhrase,
            message: res.resultMap.description,
            variant: "error",
          });
          this.dispatchEvent(event);
        }
      }
    });
  }

  alterNativeScenarioData(alternativeData) {
    this.routeToValueMap = {};
    let routeDate = [];
    let projectedData = [];
    let alternativeDataMonthly = [];
    let alternativeFutureDataMonthly = [];
    this.futureData = alternativeData;
    let projectedTime = this.futureData.futureTimePeriodList;
    //let projectedData = this.futureData.routeToValues[1].meanRegressionValues;
   
    this.alternateScenarioProjectedTotalOutComeFromProof = true;
    this.alternateScenarioTargetForecastFromProof = true;
    this.alternateScenarioForwardLookingDateRangeFromProof = true;
    if (alternativeData.frequency === "MONTHLY") {
      this.frequencyMonthly = true;
      alternativeData.historicalTimePeriodList.slice(Math.max(alternativeData.historicalTimePeriodList.length - 12, 0)).forEach((element) => {
        projectedData.push(null);
      });
      alternativeDataMonthly = alternativeData.historicalRegressionValues.slice(Math.max(alternativeData.historicalRegressionValues.length - 12, 0));
      routeDate = alternativeData.historicalTimePeriodList.slice(Math.max(alternativeData.historicalTimePeriodList.length - 12, 0));
      routeDate = routeDate.concat(alternativeData.futureTimePeriodList.slice(0, 12));
      var routeDateFormat = [];
      for (var i = 0; i < routeDate.length; i++) {
        var s = routeDate[i].slice(3);
        routeDateFormat.push(s);
      }
      alternativeFutureDataMonthly = alternativeData.routeToValues[1].meanRegressionValues.slice(0, 12);
      this.routeToValueMap["routeDate"] = routeDateFormat;
      this.routeToValueMap["historicalData"] = alternativeDataMonthly.map(Number);
      this.routeToValueMap["projectedData"] = projectedData.concat(alternativeFutureDataMonthly.map(Number));
      this.initilizeHightChart();
       // Set max and min dates
      this.minFLDR = this.moment(projectedTime[0]).format("YYYY-MM");
      this.maxFLDR = this.moment(projectedTime[projectedTime.length - 1]).format("YYYY-MM");
     
      this.maxDate = this.moment(projectedTime[projectedTime.length - 1]).format("YYYY-MM");
      this.minDate = this.moment(projectedTime[0]).format("YYYY-MM");
    //End
      //Min and max date for target total outcome
      let maxDate=alternativeData.futureTimePeriodList[alternativeData.futureTimePeriodList.length -1];
      let minDate=alternativeData.historicalTimePeriodList[0];
      this.maxDateTargetTotal=moment(maxDate).format("YYYY-MM");
      this.minDateTargetTotal=moment(minDate).format("YYYY-MM");
     
      this.kidMonthlyFLDR = alternativeData.historicalTimePeriodList[alternativeData.historicalTimePeriodList.length - 1];
      let formatMonth = this.moment(this.kidMonthlyFLDR).format("YYYY-MMMM");
     // let weeklyMonthYear = this.kidMonthlyFLDR.slice(3);
      let yearname = formatMonth.split("-")[0];
      let monthname = formatMonth.split("-")[1];
      this.forwardLookingDate = monthname + "," + " " + yearname;
      //this.forwardLookingDate = this.kidMonthlyFLDR.slice(3);
     
      if(alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome != undefined){
        let mothly = alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome["forwardLookingDate"];
        this.forwardLookingDateRangeResult = moment(mothly, "DD-MMM-YYYY").format("YYYY-MM");
        if (this.forwardLookingDateRangeResult != "" && this.forwardLookingDateRangeResult != undefined) {
          this.KIDCalculationFLDR(this.forwardLookingDateRangeResult);
        }
       }
     
      if(alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome != undefined){
        let tagetTotaloutcomeStartDate = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["startDate"];
        this.getTagetTotaloutcomeValue = moment(tagetTotaloutcomeStartDate, "DD-MMM-YYYY").format("YYYY-MM");
        this.tagetTotaloutcome = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["targetTotalOutcome"];
        if (this.tagetTotaloutcome != "" && this.getTagetTotaloutcomeValue != "") {
          this.CalculateTargetTotalOutcome(this.tagetTotaloutcome, this.getTagetTotaloutcomeValue);
        }
        }
    
    } else if (alternativeData.frequency === "QUARTERLY") {
      this.frequencyQuarterly = true;
      this.futureTimePeriodList = alternativeData.futureTimePeriodList;
      for (var i = 1; i < this.futureTimePeriodList.length; i++) {
        this.quarterOptions.push({ label: alternativeData.futureTimePeriodList[i - 1], value: alternativeData.futureTimePeriodList[i - 1] });
      }

      this.historicalTimePeriodQuarterly = alternativeData.historicalTimePeriodList.concat(alternativeData.futureTimePeriodList);
      for (var i = 1; i < this.historicalTimePeriodQuarterly.length; i++) {
        this.historicalTimePeriodOptions.push({ label: this.historicalTimePeriodQuarterly[i - 1], value: this.historicalTimePeriodQuarterly[i - 1] });
      }
      alternativeData.historicalTimePeriodList.slice(Math.max(alternativeData.historicalTimePeriodList.length - 8, 0)).forEach((element) => {
        projectedData.push(null);
      });
      alternativeDataMonthly = alternativeData.historicalRegressionValues.slice(Math.max(alternativeData.historicalRegressionValues.length - 8, 0));
      routeDate = alternativeData.historicalTimePeriodList.slice(Math.max(alternativeData.historicalTimePeriodList.length - 8, 0));
      routeDate = routeDate.concat(alternativeData.futureTimePeriodList.slice(0, 4));
      alternativeFutureDataMonthly = alternativeData.routeToValues[1].meanRegressionValues.slice(0, 4);
      this.routeToValueMap["routeDate"] = routeDate;
      this.routeToValueMap["historicalData"] = alternativeDataMonthly.map(Number);
      this.routeToValueMap["projectedData"] = projectedData.concat(alternativeFutureDataMonthly.map(Number));
      this.forwardLookingDate = alternativeData.historicalTimePeriodList[alternativeData.historicalTimePeriodList.length - 1];
      this.initilizeHightChart();
      if(alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome != undefined){
        this.forwardLookingDateRangeResult = alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome["forwardLookingDate"];
        if (this.forwardLookingDateRangeResult != "" && this.forwardLookingDateRangeResult != undefined) {
          this.KIDCalculationFLDR(this.forwardLookingDateRangeResult);
        }
      }
      let tagetTotaloutcome;
      if(alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome != undefined){
        this.tagetTotaloutcomeStartDate = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["startDate"];
        tagetTotaloutcome = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["targetTotalOutcome"];
        if (tagetTotaloutcome != "" && tagetTotaloutcome != undefined) {
          this.tagetTotaloutcome = tagetTotaloutcome;
        }
        if (this.tagetTotaloutcome != "" && this.tagetTotaloutcome != undefined) {
          this.CalculateTargetTotalOutcome(this.tagetTotaloutcome, this.tagetTotaloutcomeStartDate);
        }
        }
    } else if (alternativeData.frequency === "WEEKLY") {
      this.frequencyWeekly = true;
      alternativeData.historicalTimePeriodList.slice(Math.max(alternativeData.historicalTimePeriodList.length - 12, 0)).forEach((element) => {
        projectedData.push(null);
      });
      alternativeDataMonthly = alternativeData.historicalRegressionValues.slice(Math.max(alternativeData.historicalRegressionValues.length - 12, 0));
      routeDate = alternativeData.historicalTimePeriodList.slice(Math.max(alternativeData.historicalTimePeriodList.length - 12, 0));
      routeDate = routeDate.concat(alternativeData.futureTimePeriodList.slice(0, 12));
      alternativeFutureDataMonthly = alternativeData.routeToValues[1].meanRegressionValues.slice(0, 12);
      this.routeToValueMap["routeDate"] = routeDate;
      this.routeToValueMap["historicalData"] = alternativeDataMonthly.map(Number);
      this.routeToValueMap["projectedData"] = projectedData.concat(alternativeFutureDataMonthly.map(Number));
      let weeklyNoYear = alternativeData.historicalTimePeriodList[alternativeData.historicalTimePeriodList.length - 1];
      let weeklydata = weeklyNoYear.split("-")[0];
      let yearis = weeklyNoYear.split("-")[1];
      let weekNo = weeklydata.substr(2, 2);
      this.forwardLookingDate = "Week " + weekNo + "," + " " + yearis;
      this.initilizeHightChart();
      // Set min and max value for weekly
      let maxDate = projectedTime[projectedTime.length - 1];
      let maxYear = maxDate.substring(maxDate.length - 4);
      let maxWeekNumber;
      if (maxDate.length === 9) {
        maxWeekNumber = maxDate.substring(2, 4);
      } else if (maxDate.length === 8) {
        maxWeekNumber = "0" + maxDate[2];
      }
      this.maxFLDR= maxYear + "-" + "W" + maxWeekNumber;
      let minDate = projectedTime[0];
      let minYear = minDate.substring(minDate.length - 4);
      let minWeekNumber;
      if (minDate.length === 9) {
        minWeekNumber = minDate.substring(2, 4);
      } else if (minDate.length === 8) {
        minWeekNumber = "0" + minDate[2];
      }
      this.minFLDR= minYear + "-" + "W" + minWeekNumber;
      // End
      if(alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome != undefined){
        let weekly = alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome["forwardLookingDate"];
        let year = weekly.substring(weekly.length - 4);
        let weekNumber;
        if (weekly.length === 9) {
          weekNumber = weekly.substring(2, 4);
        } else if (weekly.length === 8) {
          weekNumber = "0" + weekly[2];
        }
        this.forwardLookingDateRangeResult = year + "-" + "W" + weekNumber;
        if (this.forwardLookingDateRangeResult != "" && this.forwardLookingDateRangeResult != undefined) {
          this.KIDCalculationFLDR(this.forwardLookingDateRangeResult);
        }
      }
     
      let tagetTotaloutcome;
      let tagetTotaloutcomeStartDate;
      if(alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome != undefined){
         tagetTotaloutcome = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["targetTotalOutcome"];
         tagetTotaloutcomeStartDate = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["startDate"];
         if (tagetTotaloutcome != "" && tagetTotaloutcome != undefined) {
          this.tagetTotaloutcome = tagetTotaloutcome;
        }
        this.tagetTotaloutcomeStartDate = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["startDate"];
        this.getTagetTotaloutcomeValue = tagetTotaloutcomeStartDate;
        if (this.tagetTotaloutcome != "" && this.getTagetTotaloutcomeValue != "") {
          this.CalculateTargetTotalOutcome(this.tagetTotaloutcome, this.getTagetTotaloutcomeValue);
        }
      }
     
     
    } else if (alternativeData.frequency === "DAILY") {
      this.frequencyDaily = true;
      alternativeData.historicalTimePeriodList.slice(Math.max(alternativeData.historicalTimePeriodList.length - 14, 0)).forEach((element) => {
        projectedData.push(null);
      });
      alternativeDataMonthly = alternativeData.historicalRegressionValues.slice(Math.max(alternativeData.historicalRegressionValues.length - 14, 0));
      routeDate = alternativeData.historicalTimePeriodList.slice(Math.max(alternativeData.historicalTimePeriodList.length - 14, 0));
      routeDate = routeDate.concat(alternativeData.futureTimePeriodList.slice(0, 30));
      alternativeFutureDataMonthly = alternativeData.routeToValues[1].meanRegressionValues.slice(0, 30);
      this.routeToValueMap["routeDate"] = routeDate;
      this.routeToValueMap["historicalData"] = alternativeDataMonthly.map(Number);
      this.routeToValueMap["projectedData"] = projectedData.concat(alternativeFutureDataMonthly.map(Number));
      let forwardLookingDate = alternativeData.historicalTimePeriodList[alternativeData.historicalTimePeriodList.length - 1];
      this.forwardLookingDate = moment(forwardLookingDate, "DD-MMM-YYYY").format("DD-MM-YYYY");
     
      this.initilizeHightChartDaily();

      this.minFLDR = this.moment(projectedTime[0]).format("YYYY-MM-DD");
      this.maxFLDR = this.moment(projectedTime[projectedTime.length - 1]).format("YYYY-MM-DD");

      //Min and max date for target total outcome
      let maxDate=alternativeData.futureTimePeriodList[alternativeData.futureTimePeriodList.length -1];
      let minDate=alternativeData.historicalTimePeriodList[0];
      this.maxDateTargetTotal=moment(maxDate).format("YYYY-MM-DD");
      this.minDateTargetTotal=moment(minDate).format("YYYY-MM-DD");
      console.log("Historical Min date == "+this.minDateTargetTotal);
      console.log("Future Time period list max date == "+this.maxDateTargetTotal);
      if(alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome != undefined)  {
        let tagetTotaloutcome = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["targetTotalOutcome"];
        if (tagetTotaloutcome != "" && tagetTotaloutcome != undefined) {
          this.tagetTotaloutcome = tagetTotaloutcome;
        }
        let tagetTotaloutcomeStartDate = alternativeData.routeToValues[1].alternateScenarioTimeToTotalOutCome["startDate"];
        this.getTagetTotaloutcomeValue = moment(tagetTotaloutcomeStartDate, "DD-MMM-YYYY").format("YYYY-MM-DD");
        if (this.tagetTotaloutcome != "" && this.getTagetTotaloutcomeValue != "") {
          this.CalculateTargetTotalOutcome(this.tagetTotaloutcome, this.getTagetTotaloutcomeValue);
        }
      }
      let daily;
      if(alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome != undefined){
       daily =alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome["forwardLookingDate"];
       this.forwardLookingDateRangeResult = moment(daily, "DD-MMM-YYYY").format("YYYY-MM-DD");
      if(this.forwardLookingDateRangeResult != "" && this.forwardLookingDateRangeResult != undefined) {
        this.KIDCalculationFLDR(this.forwardLookingDateRangeResult);
      }
      }
    }
    let getProjectedSpend;
    if(alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome != undefined){
       getProjectedSpend = alternativeData.routeToValues[1].alternateScenarioProjectedTotalOutCome["projectedSpend"];
    }
    if (getProjectedSpend != "" && getProjectedSpend != undefined) {
      this.getProjectedSpend = getProjectedSpend;
    } else {
      this.getProjectedSpend = "";
    }
    if(alternativeData.routeToValues[1].routeToValueTimeToForecast != undefined){
      this.myValue = alternativeData.routeToValues[1].routeToValueTimeToForecast["targetForecast"];
      if (this.myValue != "" && this.myValue != undefined) {
        this.KIDCalculation(this.myValue);
      }
    }
   
  }
  handleOnChange(event) {
    let id = event.target.name;
    let value = event.target.value;
    this.altScenarioHsh[id].delta = parseFloat(value);
    this.altScenarioHsh[id]["altLabel"] = parseFloat(value);
    this.refeshButtonActive = true;
  }
  handleSubmitChangedCauses() {
    this.refeshButtonActive = false;
    let causeDetails = [];
    let timePeriod;
    for (let key in this.altScenarioHsh) {
      if (!timePeriod) {
        timePeriod = this.altScenarioHsh[key].lastHistoricalTimePeriod;
      }
      let hsh = {};
      hsh.id = this.altScenarioHsh[key].id;
      hsh.delta = this.altScenarioHsh[key].delta;
      causeDetails.push(hsh);
    }
    let bodydata = JSON.stringify({
      causeDetails: causeDetails,
      timePeriod: timePeriod,
      alternateScenarioType: "SCENARIO_ONE",
    });
   
    let param = JSON.stringify({
      URL: this.domainURL + "/alternate_scenario?resource_type=KEY_INSIGHT_DASHBOARD",
      Body: bodydata,
      Authorization: this.proofIdToken,
    });
    putCalloutResponseContents({ param: param }).then((res) => {
      let event;
      if (res.resultMap) {
        if (res.resultMap.code == 200) {
          event = new ShowToastEvent({
            //title: 'Get Help',
            message: res.resultMap.description,
            variant: "success",
          });
        } else {
          event = new ShowToastEvent({
            //title: 'Get Help',
            message: res.resultMap.description,
            variant: "error",
          });
        }
        this.dispatchEvent(event);
      }
      this.getResponseContents();
    });
  }
  initilizeHightChart() {
    const ctx = this.template.querySelector(".container");
    let lastHistoricalValue = this.routeToValueMap.historicalData.pop();
    this.routeToValueMap.projectedData[this.routeToValueMap.historicalData.length] = lastHistoricalValue;
    Promise.all([loadScript(this, highchart + "/highchart.js")])
      .then(() => {
        Highcharts.setOptions({
          lang: {
            thousandsSep: ",",
          },
        }),
          Highcharts.chart(ctx, {
            chart: {
              //	type: "spline",
              //	inverted: true,
            },
            title: {
              text: "Total Sales" + "($M)",
            },

            xAxis: {
              categories: this.routeToValueMap.routeDate,

              labels: {
                text: "Historical Message",
                style: {
                  left: "100px",
                  top: "100px",
                },
              },
              plotLines: [
                {
                  color: "#000000",
                  // width: 1.5,
                  value: this.routeToValueMap.historicalData.length,
                  dashStyle: "dash",
                  color: "#0035b9",
                  label: {
                    rotation: 0,
                    text: "",
                    x: -10,
                    y: -10,
                  },
                },
              ],
            },
            tooltip: {
              useHTML: true,
              valueDecimals: 3,
              formatter: function () {
                return this.x + "<br>" + Highcharts.numberFormat(this.y, 2);
              },
            },

            series: [
              {
                type: "scatter",
                name: "Historical",
                data: this.routeToValueMap.historicalData,
                enableMouseTracking: true,
                color: "#0035b9",
                marker: {
                  radius: 3,
                  color: "#0035b9",
                },
              },
              {
                type: "line",
                name: "Projected",
                data: this.routeToValueMap.projectedData,
                enableMouseTracking: true,
                color: "#0035b9",
              },
            ],
          });
      })
      .catch((error) => {
        this.template.querySelector("c-toast").showToast(error);
      });
  }

  initilizeHightChartDaily() {
    const ctx = this.template.querySelector(".container");
    let lastHistoricalValue = this.routeToValueMap.historicalData.pop();
    this.routeToValueMap.projectedData[this.routeToValueMap.historicalData.length] = lastHistoricalValue;
    Promise.all([loadScript(this, highchart + "/highchart.js")])
      .then(() => {
        Highcharts.setOptions({
          lang: {
            thousandsSep: ",",
          },
        }),
          Highcharts.chart(ctx, {
            chart: {
              //	type: "spline",
              //	inverted: true,
            },
            title: {
              text: "Total Sales" + "($M)",
            },

            xAxis: {
              categories: this.routeToValueMap.routeDate,
              tickInterval: 2,

              labels: {
                text: "Historical Message",
                style: {
                  left: "100px",
                  top: "100px",
                },
              },
              plotLines: [
                {
                  color: "#000000",
                  // width: 1.5,
                  value: this.routeToValueMap.historicalData.length,
                  dashStyle: "dash",
                  color: "#0035b9",
                  label: {
                    rotation: 0,
                    text: "",
                    x: -10,
                    y: -10,
                  },
                },
              ],
            },
            tooltip: {
              useHTML: true,
              valueDecimals: 3,
              formatter: function () {
                return this.x + "<br>" + Highcharts.numberFormat(this.y, 2);
              },
            },

            series: [
              {
                type: "scatter",
                name: "Historical",
                data: this.routeToValueMap.historicalData,
                enableMouseTracking: true,
                color: "#0035b9",
                marker: {
                  radius: 3,
                  color: "#0035b9",
                },
              },
              {
                type: "line",
                name: "Projected",
                data: this.routeToValueMap.projectedData,
                enableMouseTracking: true,
                color: "#0035b9",
              },
            ],
          });
      })
      .catch((error) => {
        this.template.querySelector("c-toast").showToast(error);
      });
  }

  handleNew(event) {
    const modal = this.template.querySelector("c-record-form-modal");
    modal.show();
    this.driverType = event.currentTarget.getAttribute("data-action");
    this.insightPanel.forEach((ele) => {
      if (ele.driverType == this.driverType) {
        this.commentBoxValue = ele.comment;
      }
    });
  }
  handleCancelRecordFormModal() {
    const modal = this.template.querySelector("c-record-form-modal");
    modal.hide();
  }
  handleSubmit() {
    this.commentValue = this.template.querySelector("[data-field='comment']").value;
    this.putRecorsData();
    this.handleCancelRecordFormModal();
    //	this.getResponseInsight();
    refreshApex(this.refreshArray);
  }
  putRecorsData() {
    let bodydata = JSON.stringify({
      driverInsightsUpdateRequest: {
        driverInsights: this.commentValue,
        driverType: this.driverType,
      },
    });
    let param = JSON.stringify({
      URL: this.domainURL + "?resource_type=KEY_INSIGHT_DASHBOARD",
      Body: bodydata,
      Authorization: this.proofIdToken,
    });
    putCalloutResponseContents({ param: param }).then((res) => {
      if (res.objectTypeLabel == 200) {
        const event = new ShowToastEvent({
          //	title: 'Get Help',
          message: "Record Insert Succesfully",
          variant: "Success",
        });
        this.dispatchEvent(event);
        this.getResponseInsight();
      } else {
        const event = new ShowToastEvent({
          //title: 'Get Help',
          message: "Not Inserted",
          variant: "error",
        });
        this.dispatchEvent(event);
      }
    });
  }

  handleKidCalculationChange(event) {
    this.myValue = event.target.value;
    this.alternateScenarioTargetForecastFromProof = false;
    if(this.myValue != "" && this.myValue != undefined){
      this.KIDCalculation(this.myValue);
    }
   // this.putRecorsDataTargetForecast(this.myValue);
  }

  KIDCalculation(myValue) {
    this.myValue = myValue;
    var futureTimeDataMapResult = {};
    let projectedData = [];
    let projectedTime = [];
    let plotLineValue = this.futureData.historicalRegressionValues[this.futureData.historicalRegressionValues.length - 1];
    projectedTime = this.futureData.futureTimePeriodList;
    projectedData = this.futureData.routeToValues[1].meanRegressionValues;

    this.targetForecastOutput = "";
    let frequency = "";

    futureTimeDataMapResult = projectedTime.map(function (x, i) {
      return [x, projectedData[i]];
    });

    if (this.futureData.frequency == "MONTHLY") {
      frequency = "Months";
    } else if (this.futureData.frequency == "QUARTERLY") {
      frequency = "Quarters";
    } else if (this.futureData.frequency == "WEEKLY") {
      frequency = "Weeks";
    } else if (this.futureData.frequency == "DAILY") {
      frequency = "Days";
    }
    for (var i = 0; i <= projectedData.length - 1; i++) {
      if (projectedData[i] > this.myValue) {
        this.isMaxLimit = true;
      } else {
        this.targetForecastOutput = "Max Limit Reached";
      }
    }
   
    if (this.myValue <= plotLineValue) {
      this.targetForecastOutput = "0 " +" "+ frequency;
      this.isResult = true;
    } else if (this.myValue >= plotLineValue && this.myValue <= projectedData[0]) {
      this.targetForecastOutput = "1 " +" "+ frequency;
      this.isResult = true;
    } else if (this.myValue > projectedData[0]) {
      var j = 0;
      for (var i = 0; i <= projectedData.length - 1; i++) {
        j = i;
        j++;
        if (this.myValue > projectedData[i] && this.myValue < projectedData[j]) {
          this.targetForecastOutput = j + 1 +" "+ frequency;
          this.isResult = true;
          break;
        }
      }
    } else {
      this.targetForecastOutput = "Max Limit Reached";
    }
    if(!this.alternateScenarioTargetForecastFromProof){
      this.putRecorsDataTargetForecast(this.myValue);
    }
   
  }

  
  onchangeProjectedSpend(event){
    let projectedSpend = event.target.value;
    this.getProjectedSpend = projectedSpend;
    this.alternateScenarioForwardLookingDateRangeFromProof = false;
    if(this.forwardLookingDateRange != "" ) {
      this.putRecorsDataForwardLookingDateRange(this.forwardLookingDateRangeDate, projectedSpend);
    }
  }

  handleKidFLDRChange(event) {
    this.alternateScenarioForwardLookingDateRangeFromProof = false;
    this.forwardLookingDateRange = event.target.value;
    this.calForwardLookingDateOutput = false;
   // this.forwardLookingDateRangeResult = this.forwardLookingDateRange;
   if(this.futureData.frequency == "Monthly"){
    this.forwardLookingDateRangeResult =this.moment(this.forwardLookingDateRange).format("YYYY-MMM");
   }else{
    this.forwardLookingDateRangeResult = this.forwardLookingDateRange;
   }
 
    this.getProjectedSpend = this.getProjectedSpend;
    if (this.forwardLookingDateRange != "" && this.forwardLookingDateRange != undefined) {
      this.KIDCalculationFLDR(this.forwardLookingDateRange);
    //  this.putRecorsDataForwardLookingDateRange(this.forwardLookingDateRangeDate, this.getProjectedSpend);
    }
  }

  // KID Calculation for Forward looking date range
  KIDCalculationFLDR(forwardLookingDateRange) {
    this.forwardLookingDateRange = forwardLookingDateRange;
    let projectedData = [];
    let projectedTime = [];
    let historialProjectedTime = {};
    projectedTime = this.futureData.futureTimePeriodList;
    projectedData = this.futureData.routeToValues[1].meanRegressionValues;

    let startDate;
    let endDate;
    for (let i = 0; i < projectedTime.length; i++) {
      let tm = projectedTime[i];
      let data = projectedData[i];
      historialProjectedTime[tm] = data;
    }
    if (this.futureData.frequency == "MONTHLY") {
      this.calForwardLookingDateRange = 0;
        //startDate = this.moment(this.forwardLookingDate).format("YYYY-MM-DD");
        endDate = this.moment(this.forwardLookingDateRange).format("YYYY-MM-DD");
        projectedTime.forEach((key) => {
        let date = key;
        let value = historialProjectedTime[key];
        let projectTime = key;
        let projTime = this.moment(projectTime).format("YYYY-MM-DD");
        if (projTime <= endDate) {
          this.calForwardLookingDateRange += historialProjectedTime[key];
          if (projTime === endDate) { 
            this.forwardLookingDateRangeDate = moment(endDate, "YYYY-MM-DD").format("DD-MMM-YYYY");
            return false;
          
          }
        }
      });
      this.calForwardLookingDateRange = this.calForwardLookingDateRange.toFixed(2);
      /*startDate = this.moment(this.forwardLookingDate).format("YYYY-MM-DD");
      endDate = this.moment(this.forwardLookingDateRange).format("YYYY-MM-DD");
      this.calForwardLookingDateRange = 0;
      // if(startDate < endDate){
      for (var i = 0; i <= projectedTime.length - 1; i++) {
        let projTime = this.moment(projectedTime[i]).format("YYYY-MM-DD");
        if (this.calForwardLookingDateOutput) {
          break;
        }
        if (Date(projTime) < Date(endDate) || Date(projTime) === Date(endDate)) {
          for (var j = i; j <= projectedData.length - 1; j++) {
            this.calForwardLookingDateRange += projectedData[j];
            if (projTime === endDate) {
              this.calForwardLookingDateOutput = true;
              this.forwardLookingDateRangeDate = moment(endDate, "YYYY-MM-DD").format("DD-MMM-YYYY");
            }
            break;

        }
      }*/
      //	}
     
    } else if (this.futureData.frequency == "QUARTERLY") {
      /*this.calForwardLookingDateRange = 0;
      let FYstart = this.forwardLookingDate.substring(this.forwardLookingDate.length - 4);
      let QEnd = this.forwardLookingDateRange.substring(1, 2);
      let FYEnd = this.forwardLookingDateRange.substring(this.forwardLookingDate.length - 4);
      projectedTime.forEach((key) => {
      let date = key;
      let value = historialProjectedTime[key];
      let projectTime = key;
      let QProjTime = projectTime.substring(1, 2);
      let FYProjTime = projectTime.substring(projectTime.length - 4);
      //let projTime = this.moment(projectTime).format("YYYY-MM-DD");
      if (QProjTime === QEnd || FYProjTime <= FYEnd) {
        this.calForwardLookingDateRange += historialProjectedTime[key];
        this.forwardLookingDateRangeDate = this.forwardLookingDateRange;
        if (QProjTime === QEnd && FYProjTime === FYEnd) {
          return false;
        }
      }
    });*/
  // Old one
      this.calForwardLookingDateRange = 0.0;
      let Qstart = this.forwardLookingDate.substring(2, 2);
      let FYstart = this.forwardLookingDate.substring(this.forwardLookingDate.length - 4);
      let QEnd = this.forwardLookingDateRange.substring(1, 2);
      let FYEnd = this.forwardLookingDateRange.substring(this.forwardLookingDate.length - 4);

      if (FYstart <= FYEnd) {
        for (var i = 0; i <= projectedTime.length - 1; i++) {
          if (this.calForwardLookingDateOutput) {
            break;
          }
          let projTime = projectedTime[i];
          let QProjTime = projTime.substring(1, 2);
          let FYProjTime = projTime.substring(projTime.length - 4);
          if (QProjTime === QEnd || FYProjTime <= FYEnd) {
            for (var j = i; j <= projectedData.length - 1; j++) {
              this.calForwardLookingDateRange += projectedData[j];
              if (QProjTime === QEnd && FYProjTime === FYEnd) {
                this.calForwardLookingDateOutput = true;
                this.forwardLookingDateRangeDate = this.forwardLookingDateRange;
              }
              break;
            }
          }
        }
      }
      this.calForwardLookingDateRange = this.calForwardLookingDateRange.toFixed(2);
     
    } else if (this.futureData.frequency == "WEEKLY") {
      
      this.calForwardLookingDateRange = 0;
      let FYstart = this.forwardLookingDate.substring(this.forwardLookingDate.length - 4);

      let WkEnd = this.forwardLookingDateRange.substring(this.forwardLookingDateRange.length - 2);
      let WEnd;
      if (WkEnd[0] == "0") {
        WEnd = WkEnd[1];
      } else {
        WEnd = WkEnd;
      }
      let FYEnd = this.forwardLookingDateRange.substring(0, 4);
      let weeklyResult;
      //	if(FYstart <= FYEnd){
      for (var i = 0; i <= projectedTime.length - 1; i++) {
        let projTime = projectedTime[i];

        if (this.calForwardLookingDateOutput) {
          break;
        }
        let WProjTime;
        if (projTime.length === 9) {
          if (projTime[2] == "0") {
            WProjTime = projTime.substring(3, 4);
          } else {
            WProjTime = projTime.substring(2, 4);
          }
        } else if (projTime.length === 8) {
          WProjTime = projTime[2];
        }

        let FYProjTime = projTime.substring(projTime.length - 4);
        if (WProjTime === WEnd || FYProjTime <= FYEnd) {
          for (var j = i; j <= projectedData.length - 1; j++) {
            this.calForwardLookingDateRange += projectedData[j];
            if (WProjTime === WEnd && FYProjTime === FYEnd) {
              this.calForwardLookingDateOutput = true;
              weeklyResult = projTime;
              this.forwardLookingDateRangeDate = weeklyResult;
            }
            break;
          }
        }
      }
      //  }
      this.calForwardLookingDateRange = this.calForwardLookingDateRange.toFixed(2);
    } else if (this.futureData.frequency == "DAILY") {
      this.calForwardLookingDateRange = 0;
      //startDate = this.moment(this.forwardLookingDate).format("YYYY-MM-DD");
      endDate = this.moment(this.forwardLookingDateRange).format("YYYY-MM-DD");
      projectedTime.forEach((key) => {
      let date = key;
      let value = historialProjectedTime[key];
      let projectTime = key;
      let projTime = this.moment(projectTime).format("YYYY-MM-DD");
      if (projTime <= endDate) {
        this.calForwardLookingDateRange += historialProjectedTime[key];
        if (projTime === endDate) { 
          this.forwardLookingDateRangeDate = moment(endDate, "YYYY-MM-DD").format("DD-MMM-YYYY");
          return false;
        
        }
      }
    });
    this.calForwardLookingDateRange = this.calForwardLookingDateRange.toFixed(2);
      /*this.calForwardLookingDateRange = 0;
      startDate = this.forwardLookingDate;
      endDate = this.moment(this.forwardLookingDateRange).format("YYYY-MM-DD");
      for (var i = 0; i <= projectedTime.length - 1; i++) {
       let projTime = this.moment(projectedTime[i]).format("YYYY-MM-DD");
        if (this.calForwardLookingDateOutput) {
          break;
        }
        if (Date(projTime) < Date(endDate) || Date(projTime) === Date(endDate)) {
          for (var j = i; j <= projectedData.length - 1; j++) {
            this.calForwardLookingDateRange += projectedData[j];
            if (projTime === endDate) {
              this.calForwardLookingDateOutput = true;
              this.forwardLookingDateRangeDate = moment(endDate, "YYYY-MM-DD").format("DD-MMM-YYYY");
            }
            break;
          }
        }
      }*/
     // this.calForwardLookingDateRange = this.calForwardLookingDateRange.toFixed(2);
    }
    if(!this.alternateScenarioForwardLookingDateRangeFromProof){
      this.putRecorsDataForwardLookingDateRange(this.forwardLookingDateRangeDate, this.getProjectedSpend);
    }
  }
  handleTargetTotalOutcome(event) {
    this.alternateScenarioProjectedTotalOutComeFromProof=false;
    this.tagetTotaloutcome = event.target.value;
    this.aggregateTargetOutcome = 0.0;
    if (this.tagetTotaloutcomeStartDate != "" && this.tagetTotaloutcomeStartDate != undefined) {
      this.CalculateTargetTotalOutcome(this.tagetTotaloutcome, this.tagetTotaloutcomeStartDate);
      //this.putRecordsDataTargetTotalOutcome(this.tagetTotaloutcome, this.tagetTotaloutcomeStartDate);
    }
  }

  handleKidTargetTotalOutcomeStartDate(event) {
    this.alternateScenarioProjectedTotalOutComeFromProof = false;
    this.tagetTotaloutcomeStartDate = event.target.value;

    this.aggregateTargetOutcome = 0.0;
    this.getTagetTotaloutcomeValue = this.tagetTotaloutcomeStartDate;
    // Check if weekly then convert into proof frienly date formate
    if (this.futureData.frequency == "WEEKLY") {
      let tyr = this.tagetTotaloutcomeStartDate.split("-")[0];
      let twk = this.tagetTotaloutcomeStartDate.split("-")[1];
      this.tagetTotaloutcomeStartDate = twk + "-" + tyr;
    }
    if (this.tagetTotaloutcome != "" && this.tagetTotaloutcome != undefined) {
      this.CalculateTargetTotalOutcome(this.tagetTotaloutcome, this.tagetTotaloutcomeStartDate);
    }

    if (this.tagetTotaloutcome != "" && this.tagetTotaloutcome != undefined) {
      //this.putRecordsDataTargetTotalOutcome(this.tagetTotaloutcome, this.tagetTotaloutcomeStartDate);
    }
  }

  CalculateTargetTotalOutcome(targetTotalOutcome, targetTotalOutcomeStartDate) {
    this.targetTotalOutcome = targetTotalOutcome;
    //let startDate = this.moment(targetTotalOutcomeStartDate).format("DD-MM-YYYY");
    let historicalProjectedTime = this.futureData.historicalTimePeriodList.concat(this.futureData.futureTimePeriodList);
    let historicalProjectedData = this.futureData.historicalRegressionValues.concat(this.futureData.routeToValues[1].meanRegressionValues);
    //If there is mismatch between hitorical regression and historical time periot
    // then we are removing the date or value from the front of the list to make the balance
    // between the two list
    let historicalProjectedTimeLength = historicalProjectedTime.length;
    let historicalProjectedDataLength = historicalProjectedData.length;
    if (historicalProjectedTimeLength > historicalProjectedDataLength) {
      historicalProjectedTime.splice(0, historicalProjectedTimeLength - historicalProjectedDataLength);
    } else if (historicalProjectedTimeLength < historicalProjectedDataLength) {
      historicalProjectedData.splice(0, historicalProjectedDataLength - historicalProjectedTimeLength);
    }
    

    let count = 0;
    let frequency;
    if (this.futureData.frequency == "MONTHLY") {
      frequency = "Months";
    } else if (this.futureData.frequency == "QUARTERLY") {
      frequency = "Quarters";
    } else if (this.futureData.frequency == "WEEKLY") {
      frequency = "Weeks";
    } else if (this.futureData.frequency == "DAILY") {
      frequency = "Days";
    }
    if (this.futureData.frequency == "MONTHLY") {
      // this.maxDate = this.moment(historicalProjectedTime[historicalProjectedTime.length - 1]).format("YYYY-MM");
      // this.minDate = this.moment(historicalProjectedTime[0]).format("YYYY-MM");
      console.log("max date **** "+this.maxDate);
      console.log("Min date ***** "+ this.minDate);
      let startDate = this.moment(targetTotalOutcomeStartDate).format("DD-MM-YYYY");
      for (var i = 0; i <= historicalProjectedTime.length - 1; i++) {
        let historicalProjecteddate = this.moment(historicalProjectedTime[i]).format("DD-MM-YYYY");
        if (startDate === historicalProjecteddate) {
          for (var j = i; j <= historicalProjectedData.length - 1; j++) {
            if (this.targetTotalOutcome <= this.aggregateTargetOutcome) {
              if (this.targetTotalOutcome <= historicalProjectedData[j]) {
                this.targetOutcomeResult = 0;
                break;
              }
            } else {
              if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
                this.aggregateTargetOutcome += historicalProjectedData[j];
                if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
                  count++;
                  this.targetOutcomeResult = count + " " + frequency;
                } else {
                  break;
                }
              }
            }
          }
          if(this.targetTotalOutcome < this.aggregateTargetOutcome){
            this.targetOutcomeResult = count + ' '+ frequency;
          }
          if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
            this.targetOutcomeResult = "Max limit reached";
          }
        }
      }
      this.tagetTotaloutcomeStartDate = this.moment(targetTotalOutcomeStartDate).format("DD-MMM-YYYY");
    } else if (this.futureData.frequency == "QUARTERLY") {
      let QStartDate = targetTotalOutcomeStartDate.substring(1, 2);
      let FYStartDate = targetTotalOutcomeStartDate.substring(this.forwardLookingDate.length - 4);
      for (var i = 0; i <= historicalProjectedTime.length - 1; i++) {
        // let historicalProjecteddate = this.moment(historicalProjectedTime[i]).format("DD-MM-YYYY");
        let QhistoricalProjected = historicalProjectedTime[i].substring(1, 2);
        let FYhistoricalProjected = historicalProjectedTime[i].substring(this.forwardLookingDate.length - 4);
        if (QStartDate === QhistoricalProjected && FYStartDate === FYhistoricalProjected) {
          for (var j = i; j <= historicalProjectedData.length - 1; j++) {
            if (this.targetTotalOutcome <= this.aggregateTargetOutcome) {
              if (this.targetTotalOutcome <= historicalProjectedData[j]) {
                this.targetOutcomeResult = 0;
                break;
              }
            } else {
              if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
                this.aggregateTargetOutcome += historicalProjectedData[j];
                if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
                  count++;
                  this.targetOutcomeResult = count + " " + frequency;
                } else {
                  break;
                }
              }
            }
          }
          if(this.targetTotalOutcome < this.aggregateTargetOutcome){
            this.targetOutcomeResult = count + ' '+ frequency;
          }
          if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
            this.targetOutcomeResult = "Max limit reached";
            break;
          }
        }
      }
      this.tagetTotaloutcomeStartDate = targetTotalOutcomeStartDate;
     
    } else if (this.futureData.frequency == "WEEKLY") {
      //Set Max Min to Calander Start
      let timeDataMap = {};
      let maxDtWk = historicalProjectedTime[historicalProjectedTime.length - 1].split("-")[0];
      let maxDtYr = historicalProjectedTime[historicalProjectedTime.length - 1].split("-")[1];
      let minDtWk = historicalProjectedTime[0].split("-")[0];
      let minDtYr = historicalProjectedTime[0].split("-")[1];
      maxDtWk = maxDtWk.replace(/[^0-9\.]/g, "");
      if (maxDtWk.length == 1) {
        maxDtWk = "W0" + maxDtWk;
      } else {
        maxDtWk = "W" + maxDtWk;
      }
      minDtWk = minDtWk.replace(/[^0-9\.]/g, "");
      if (minDtWk.length == 1) {
        minDtWk = "W0" + minDtWk;
      } else {
        minDtWk = "W" + minDtWk;
      }
      this.maxDate = maxDtYr + "-" + maxDtWk;
      this.minDate = minDtYr + "-" + minDtWk;
      //End
      for (let i = 0; i < historicalProjectedTime.length; i++) {
        let tm = historicalProjectedTime[i];
        let data = historicalProjectedData[i];
        timeDataMap[tm] = data;
      }
      let aggregateOutcome = 0;
      let numberOfWeek = 0;
      count = 0;
      // convert to UI friendly week format
      let ttow = targetTotalOutcomeStartDate.split("-")[0];
      let ttoy = targetTotalOutcomeStartDate.split("-")[1];
      let wkNo = ttow.replace(/[^0-9\.]/g, "");
      this.getTagetTotaloutcomeValue = ttoy+'-W'+wkNo;
      let startDate = ttoy + ttow;
      startDate = parseInt(startDate.replace(/[^0-9\.]/g, ""));

      historicalProjectedTime.forEach((key) => {
        let date = key;
        let value = timeDataMap[key];
        let tm = key;
        let yr = tm.split("-")[1];
        let wk = tm.split("-")[0];
        wk = wk.replace(/[^0-9\.]/g, "");
        if (wk.length == 1) {
          wk = "0" + wk;
        }
        let yrwk = yr + wk;
        let hpt = parseInt(yrwk);
        if (hpt >= startDate && parseInt(targetTotalOutcome) > aggregateOutcome) {
          aggregateOutcome += timeDataMap[key];
          if (parseInt(targetTotalOutcome) < Math.ceil(aggregateOutcome)) {
            return false;
          }
          count++;
          //console.log(aggregateOutcome);
        }
      });
      //console.log("Test");
      if (count == 0 || count == 1) this.targetOutcomeResult = count + " Week";
      else this.targetOutcomeResult = count + " " + frequency;
      if (targetTotalOutcome >= aggregateOutcome) {
        this.targetOutcomeResult = "Max limit reached";
      }

      //this.putRecordsDataTargetTotalOutcome(this.targetTotalOutcome,targetTotalOutcomeStartDate);
    } else if (this.futureData.frequency == "DAILY") {
      //Set Max and Min date to the calander 
     // this.maxDate = this.moment(historicalProjectedTime[historicalProjectedTime.length - 1]).format("YYYY-MM-DD");
    //  this.minDate = this.moment(historicalProjectedTime[0]).format("YYYY-MM-DD");
      let startDate = this.moment(targetTotalOutcomeStartDate).format("DD-MM-YYYY");
      for (var i = 0; i <= historicalProjectedTime.length - 1; i++) {
        let date = this.moment(historicalProjectedTime[i]).format("DD-MM-YYYY");
        if (startDate === date) {
          for (var j = i; j <= historicalProjectedData.length - 1; j++) {
            if (this.targetTotalOutcome <= this.aggregateTargetOutcome) {
              if (this.targetTotalOutcome <= historicalProjectedData[j]) {
                this.targetOutcomeResult = 0;
                break;
              }
            } else {
              if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
                this.aggregateTargetOutcome += historicalProjectedData[j];
                if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
                  count++;
                  this.targetOutcomeResult = count + frequency;
                } else {
                  break;
                }
              }
            }
          }
          if(this.targetTotalOutcome < this.aggregateTargetOutcome){
            this.targetOutcomeResult = count + ' '+ frequency;
          }
          if (this.targetTotalOutcome >= this.aggregateTargetOutcome) {
            this.targetOutcomeResult = "Max limit reached";
          }
        }
      }
      this.tagetTotaloutcomeStartDate = this.moment(targetTotalOutcomeStartDate).format("DD-MMM-YYYY");
      //this.tagetTotaloutcomeStartDate = targetTotalOutcomeStartDate;
    }
    if(!this.alternateScenarioProjectedTotalOutComeFromProof){
      this.putRecordsDataTargetTotalOutcome(this.targetTotalOutcome, this.tagetTotaloutcomeStartDate);
    }
  }
  putRecorsDataTargetForecast(InputValue) {
    let bodydata = JSON.stringify({
      routeToValueTimeToForecast: {
        targetForecast: InputValue,
        alternateScenarioType: "SCENARIO_ONE",
      },
    });
    let param = JSON.stringify({
     
      URL: this.domainURL + "/configurations?resource_type=KEY_INSIGHT_DASHBOARD",
      Body: bodydata,
      Authorization: this.proofIdToken,
    });
    
    putCalloutResponseContents({ param: param }).then((res) => {
      
      if (res.objectTypeLabel == 200) {
        const event = new ShowToastEvent({
          
          message: "Record Insert Succesfully",
          variant: "Success",
        });
        this.dispatchEvent(event);
        
      } else {
        const event = new ShowToastEvent({
         
          message: "Not Inserted",
          variant: "error",
        });
        this.dispatchEvent(event);
      }
    });
  }

  putRecorsDataForwardLookingDateRange(forwardLookingDateRangeResult, projectedSpend) {
    
    let bodydata;
    if (projectedSpend === "") {
      bodydata = JSON.stringify({
        alternateScenarioProjectedTotalOutCome: {
          forwardLookingDate: forwardLookingDateRangeResult,
          alternateScenarioType: "SCENARIO_ONE",
        },
      });
    } else if (projectedSpend != "" && forwardLookingDateRangeResult != "") {
      bodydata = JSON.stringify({
        alternateScenarioProjectedTotalOutCome: {
          forwardLookingDate: forwardLookingDateRangeResult,
          projectedSpend: projectedSpend,
          alternateScenarioType: "SCENARIO_ONE",
        },
      });
    }

    let param = JSON.stringify({
      //	URL: this.domainURL + "?resource_type=KEY_INSIGHT_DASHBOARD",
      URL: this.domainURL + "/configurations?resource_type=KEY_INSIGHT_DASHBOARD",
      Body: bodydata,
      Authorization: this.proofIdToken,
    });
    //console.log("putRecorsDataTargetForecast === "+JSON.stringify(param));
    putCalloutResponseContents({ param: param }).then((res) => {
      //console.log("Status code is ===" + JSON.stringify(res));
      if (res.objectTypeLabel == 200) {
        const event = new ShowToastEvent({
          //	title: 'Get Help',
          message: "Record Insert Succesfully",
          variant: "Success",
        });
        this.dispatchEvent(event);
        //refreshApex(this.refreshArray);
        //	this.getResponseContents();
      } else {
        const event = new ShowToastEvent({
          //title: 'Get Help',
          message: "Not Inserted",
          variant: "error",
        });
        this.dispatchEvent(event);
      }
    });
  }
  putRecordsDataTargetTotalOutcome(targetTotalOutCome, selectedDate) {
    let bodydata;
    bodydata = JSON.stringify({
      alternateScenarioTimeToTotalOutCome: {
        startDate: selectedDate,
        targetTotalOutcome: targetTotalOutCome,
        alternateScenarioType: "SCENARIO_ONE",
      },
    });

    let param = JSON.stringify({
      URL: this.domainURL + "/configurations?resource_type=KEY_INSIGHT_DASHBOARD",
      Body: bodydata,
      Authorization: this.proofIdToken,
    });
    putCalloutResponseContents({ param: param }).then((res) => {
      if (res.objectTypeLabel == 200) {
        const event = new ShowToastEvent({
          message: "Record Insert Succesfully",
          variant: "Success",
        });
        this.dispatchEvent(event);
      } else {
        const event = new ShowToastEvent({
          message: "Not Inserted",
          variant: "error",
        });
        this.dispatchEvent(event);
      }
    });
  }
}