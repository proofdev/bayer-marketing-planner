import { LightningElement, api, track, wire } from 'lwc';
import getDynamicObjectInfo from '@salesforce/apex/Campaign.getDynamicObjectInfo';
import datatable from '@salesforce/apex/Campaign.datatable';

import CAMPAIGN_OBJECT from '@salesforce/schema/Campaign';
import CAMPAIGN_ID_FIELD from '@salesforce/schema/Campaign.Id';
import CAMPAIGN_HIERARCHYBUDGETEDCOSTDIFFCOLOR_FIELD from '@salesforce/schema/Campaign.HierarchyBudgetedCostDiffColor__c';
import CAMPAIGN_HIERARCHYEXPECTEDREVENUEDIFFCOLOR_FIELD from '@salesforce/schema/Campaign.HierarchyExpectedRevenueDiffColor__c';
import CAMPAIGN_ACTUALCOST_FIELD from '@salesforce/schema/Campaign.ActualCost';
import CAMPAIGN_BUDGETEDCOST_FIELD from '@salesforce/schema/Campaign.BudgetedCost';
import CAMPAIGN_AMOUNTWONOPPORTUNITIES_FIELD from '@salesforce/schema/Campaign.AmountWonOpportunities';
import CAMPAIGN_EXPECTEDREVENUE_FIELD from '@salesforce/schema/Campaign.ExpectedRevenue';
import CAMPAIGN_BUDGETEDCOSTDIFF_FIELD from '@salesforce/schema/Campaign.BudgetedCostDiff__c';
import CAMPAIGN_EXPECTEDREVENUEDIFF_FIELD from '@salesforce/schema/Campaign.ExpectedRevenueDiff__c';

import msg_Finance_overview from '@salesforce/label/c.msg_Finance_overview';

export default class CampaignFinanceList extends LightningElement {
    label={msg_Finance_overview
    };
    @api recordId;
    @api objectApiName;
    @track data = [];
    @track columns = [];
    @track rowOffset = 0;
    @track tableLoadingState = true;
    @track showRowNumberColumn = false;
    @track sortBy = 'Name';
    @track sortDirection = 'desc';
    @track sortedBy = 'Name';
    @track sortedDirection = 'desc';
    @track selectedRows = [];
    @track hideCheckboxColumn = true;
    @track objectTypeLabel = this.label.msg_Finance_overview;
    @track paramDataTable;

    @wire(getDynamicObjectInfo, {
        param: JSON.stringify({ component: 'campaignFinanceList-getDynamicObjectInfo'}),
        recordId: '$recordId'
    })

    wiredGetDynamicObjectInfo(result) {
        if (result.data) {
            if (result.data.toast.ok) {
                this.paramDataTable = this.buildParamDataTable();
                this.template.querySelector('c-toast').showToast(result.data.toast);
            }else{
                this.template.querySelector('c-toast').showToast(result.data.toast);
            }
        } else if (result.error) {
            this.template.querySelector('c-toast').showToast(result.error);
        }
    }

    buildParamDataTable(){
        const whereCaluses = [];
        let fieldSetApiName = 'musqotmp__CampaignFinanceList';
        if(this.objectApiName === 'Campaign'){
            whereCaluses.push({
                field: 'Id',
                value: this.recordId,
                clause: '=',
                operator: 'OR'
            });
            whereCaluses.push({
                field: 'ParentId',
                value: this.recordId,
                clause: '='
            });
        }else{
            whereCaluses.push({
                field: 'ParentId',
                value: '',
                clause: '=',
                operator: 'AND'
            });
            whereCaluses.push({
                field: 'musqotmp__MarketingPlan__c',
                value: this.recordId,
                clause: '='
            });
        }
        return JSON.stringify({
            component: 'campaignFinanceList-datatable',
            relatedListType: 'finance',
            recordId: this.recordId,
            sortable: true,
            objectApiName: CAMPAIGN_OBJECT.objectApiName,
            fieldMap: {
                [CAMPAIGN_ID_FIELD.fieldApiName]: {},
                [CAMPAIGN_HIERARCHYBUDGETEDCOSTDIFFCOLOR_FIELD.fieldApiName]: { visible: false},
                [CAMPAIGN_HIERARCHYEXPECTEDREVENUEDIFFCOLOR_FIELD.fieldApiName]: { visible: false},
                [CAMPAIGN_ACTUALCOST_FIELD.fieldApiName]: { visible: false},
                [CAMPAIGN_BUDGETEDCOST_FIELD.fieldApiName]: { visible: false},
                [CAMPAIGN_BUDGETEDCOSTDIFF_FIELD.fieldApiName]: { visible: false},
                [CAMPAIGN_AMOUNTWONOPPORTUNITIES_FIELD.fieldApiName]: { visible: false},
                [CAMPAIGN_EXPECTEDREVENUE_FIELD.fieldApiName]: { visible: false},
                [CAMPAIGN_EXPECTEDREVENUEDIFF_FIELD.fieldApiName]: { visible: false}
            },
            fieldSetApiName: fieldSetApiName,
            linkableNameField: true,
            whereCaluses: whereCaluses,
            sortBy: this.sortBy,
            sortDirection: this.sortDirection
        });
    }

    @wire(datatable, {
        param: '$paramDataTable'
    })

    wiredDataTable(result) {
        this.wiredData = result;
        if (result.data) {
            if (result.data.toast.ok) {
                this.sortedBy = this.sortBy;
                this.sortedDirection = this.sortDirection;
                this.columns = result.data.datatable.columns;
                this.data = this.buildDataTableData(result.data.datatable.data);
                this.template.querySelector('c-toast').showToast(result.data.toast);
            }else{
                this.data = undefined;
                this.columns = undefined;
                this.template.querySelector('c-toast').showToast(result.data.toast);
            }
        } else if (result.error) {
            this.data = undefined;
            this.columns = undefined;
            this.template.querySelector('c-toast').showToast(result.error);
        }
        this.tableLoadingState = false;
    }

    buildDataTableData(data){
        const datas = [];
        let summaryData = {}
        let hierarchyActualCost = 0;
        let hierarchyBudgetedCost = 0;
        let hierarchyExpectedRevenue = 0;
        let hierarchyAmountWonOpportunities = 0;
        data.forEach(dt => {
            datas.push(dt);
            hierarchyActualCost += dt.HierarchyActualCost != null ? dt.HierarchyActualCost : 0;
            hierarchyBudgetedCost += dt.HierarchyBudgetedCost != null ? dt.HierarchyBudgetedCost : 0;
            hierarchyAmountWonOpportunities += dt.HierarchyAmountWonOpportunities !=null ? dt.HierarchyAmountWonOpportunities : 0;
            hierarchyExpectedRevenue += dt.HierarchyExpectedRevenue !=null ? dt.HierarchyExpectedRevenue: 0;
        });
        summaryData['Id'] = '';
        summaryData['Campaign.Name'] = '';
        summaryData['Name'] = '';
        summaryData['HierarchyActualCost'] = hierarchyActualCost;
        summaryData['HierarchyBudgetedCost'] = hierarchyBudgetedCost;
        //summaryData['musqotmp__HierarchyBudgetedCostDiff__c'] = hierarchyBudgetedCost > 0 ? (hierarchyActualCost / hierarchyBudgetedCost).toFixed(1) + '%' : '0.0%';
        summaryData['musqotmp__HierarchyBudgetedCostDiff__c'] = (hierarchyBudgetedCost > 0 && (hierarchyActualCost / hierarchyBudgetedCost).toFixed(1) > 100) ? (hierarchyActualCost / hierarchyBudgetedCost * 100).toFixed(1) + '%' : '0.0%';

        summaryData['HierarchyAmountWonOpportunities'] = hierarchyAmountWonOpportunities;
        summaryData['HierarchyExpectedRevenue'] = hierarchyExpectedRevenue;
        summaryData['musqotmp__HierarchyExpectedRevenueDiff__c'] = (hierarchyAmountWonOpportunities > 0 && (hierarchyExpectedRevenue / hierarchyAmountWonOpportunities).toFixed(1)>100) ? (hierarchyExpectedRevenue / hierarchyAmountWonOpportunities * 100).toFixed(1) + '%' : '0.0%';
        datas.push(summaryData);
        return datas;
    }

    handleColumnSort(event) {
        this.sortBy = event.detail.sortBy;
        this.sortDirection = event.detail.sortDirection;
        this.paramDataTable = this.buildParamDataTable();
    }
}