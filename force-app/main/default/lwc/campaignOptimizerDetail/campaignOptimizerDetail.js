import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD from '@salesforce/schema/CampaignOptimizer__c.AcquisitionRetention__c';
import CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD from '@salesforce/schema/CampaignOptimizer__c.MoreDealsBiggerDeals__c';
import CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD from '@salesforce/schema/CampaignOptimizer__c.RevenueCashflow__c';

import frm_Acquisition_Retention from '@salesforce/label/c.frm_Acquisition_Retention';
import frm_More_Bigger_Deals from '@salesforce/label/c.frm_More_Bigger_Deals';
import frm_Revenue_cashflow from '@salesforce/label/c.frm_Revenue_cashflow';

const fields = [CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD, CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD, CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD];

export default class CampaignOptimizerDetail extends LightningElement {
    label = {frm_Acquisition_Retention,
        frm_More_Bigger_Deals,
        frm_Revenue_cashflow
    };
    @api recordId;
    @api objectApiName;
    @track layoutSections = [];
    @track activeAccordionSections;

    @wire(getRecord, { recordId: '$recordId', fields })
    wiredCampaignOptimizer;

    get acquisitionRetention() {
        return getFieldValue(this.wiredCampaignOptimizer.data, CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD);
    }

    get moreDealsBiggerDeals() {
        return getFieldValue(this.wiredCampaignOptimizer.data, CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD);
    }

    get revenueCashflow() {
        return getFieldValue(this.wiredCampaignOptimizer.data, CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD);
    }

    onLoad(event){
        this.layoutSections = [event.detail.layout.sections[0]];
        this.activeAccordionSections = [this.layoutSections[0].id];
    }
}