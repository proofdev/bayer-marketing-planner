import { LightningElement, api } from 'lwc';

export default class Datatable extends LightningElement {
    @api records;
    @api rowoffset;
    @api columns;
    @api tableloadingstate;
    @api sortedby;
    @api sorteddirection;
    @api hidecheckboxcolumn;
    @api selectedrows;
    @api showrownumbercolumn;

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const actionLabel = event.detail.action.label;
        const row = event.detail.row;
        switch (actionName) {
            case 'edit':
            case 'detail':
            case 'delete':
                this.showRowDetails(row, event, actionName, actionLabel);
                break;
            default:
        }
    }

    showRowDetails(row, event, actionName, actionLabel) {
        // 1. Prevent default behavior of anchor tag click which is to navigate to the href url
        event.preventDefault();
        // 2. Read about event best practices at http://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.events_best_practices
        const selectEvent = new CustomEvent('select', {
            detail: {
                id: row.Id,
                actionName: actionName,
                actionLabel: actionLabel
            }
        });
        // 3. Fire the custom event
        this.dispatchEvent(selectEvent);
    }
    getSelectedRows(event) {
        const ids = [];
        event.detail.selectedRows.forEach(selectedRow => {
            ids.push(selectedRow.Id);
        });
        // 1. Prevent default behavior of anchor tag click which is to navigate to the href url
        event.preventDefault();
        // 2. Read about event best practices at http://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.events_best_practices
        const selectEvent = new CustomEvent('rowselect', {
            detail: {
                ids: ids
            }
        });
        // 3. Fire the custom event
        this.dispatchEvent(selectEvent);
    }

    updateColumnSorting(event) {
        const fieldName = event.detail.fieldName;
        const sortDirection = event.detail.sortDirection;
        // 1. Prevent default behavior of anchor tag click which is to navigate to the href url
        event.preventDefault();
        // 2. Read about event best practices at http://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.events_best_practices
        const sortEvent = new CustomEvent('columnsort', {
            detail: {
                sortBy: fieldName,
                sortDirection: sortDirection
            }
        });
        // 3. Fire the custom event
        this.dispatchEvent(sortEvent);
    }
}