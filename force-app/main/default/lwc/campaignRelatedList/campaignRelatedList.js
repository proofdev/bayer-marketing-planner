/* eslint-disable no-empty */
import { LightningElement, track, wire, api } from 'lwc';
import datatable from '@salesforce/apex/Campaign.datatable';
import getDynamicObjectInfo from '@salesforce/apex/Campaign.getDynamicObjectInfo';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import LOCALE from '@salesforce/i18n/locale';

import getRecords from '@salesforce/apex/Campaign.getRecords';
import { getRecord, deleteRecord, updateRecord, createRecord } from 'lightning/uiRecordApi';
import { getToast } from 'c/lds';
import { refreshApex, getSObjectValue } from '@salesforce/apex';

import MARKETINGPLAN_OBJECT from '@salesforce/schema/MarketingPlan__c';
import MARKETINGPLAN_SD_FIELD from '@salesforce/schema/MarketingPlan__c.StartDate__c';
import MARKETINGPLAN_ED_FIELD from '@salesforce/schema/MarketingPlan__c.EndDate__c';
import MARKETINGPLAN_ID_FIELD from '@salesforce/schema/MarketingPlan__c.Id';
import MARKETINGPLAN_NAME_FIELD from '@salesforce/schema/MarketingPlan__c.Name';
import MARKETINGPLAN_MARKETINGAREA_FIELD from '@salesforce/schema/MarketingPlan__c.MarketingArea__c';
import MARKETINGPLAN_BUSINESS_UNIT_FIELD from '@salesforce/schema/MarketingPlan__c.MarketingBusinessUnit__c';

import CAMPAIGN_OBJECT from '@salesforce/schema/Campaign';
import CAMPAIGN_ID_FIELD from '@salesforce/schema/Campaign.Id';
import CAMPAIGN_STATUS_FIELD from '@salesforce/schema/Campaign.Status';
import RECORD_TYPE_FIELD from '@salesforce/schema/Campaign.RecordTypeId';
import IS_FOLDER_FIELD from '@salesforce/schema/Campaign.IsFolder__c';
import CAMPAIGN_SD_FIELD from '@salesforce/schema/Campaign.StartDate';
import CAMPAIGN_ED_FIELD from '@salesforce/schema/Campaign.EndDate';
import CAMPAIGN_MARKETINGAREA_FIELD from '@salesforce/schema/Campaign.MarketingArea__c';
import CAMPAIGN_BUSINESS_UNIT_FIELD from '@salesforce/schema/Campaign.MarketingBusinessUnit__c';
import CAMPAIGN_AUDIENCE_FIELD from '@salesforce/schema/Campaign.Audience__c';
import CAMPAIGN_MARKETINGPLAN_ID_FIELD from '@salesforce/schema/Campaign.MarketingPlan__c';
import CAMPAIGN_MARKETINGPLAN_NAME_FIELD from '@salesforce/schema/Campaign.MarketingPlan__r.Name';

import MARKETINGPLAN_STATUS_FIELD from '@salesforce/schema/MarketingPlan__c.Status__c';
import MARKETINGPLAN_UNKNOWNPLAN_FIELD from '@salesforce/schema/MarketingPlan__c.UnknownCampaigns__c'; //Sowmya changes 17/10/2019 for W-000123

import hl_Search from '@salesforce/label/c.hl_Search';
import msg_Delete from '@salesforce/label/c.msg_Delete';
import modalHeader from '@salesforce/label/c.modalHeader';
import btn_save from '@salesforce/label/c.btn_save';
import btn_New from '@salesforce/label/c.btn_new';
import btn_cancel from '@salesforce/label/c.btn_cancel';
import btn_Delete from '@salesforce/label/c.btn_Delete';
import msg_Record_Delete from '@salesforce/label/c.msg_Record_Delete';
import msg_Record_Saved from '@salesforce/label/c.msg_Record_Saved';
import hl_Campaign from '@salesforce/label/c.hl_Campaign';
import btn_Next from '@salesforce/label/c.btn_Next';
import msg_Folder_Description from '@salesforce/label/c.msg_Folder_Description';
import msg_Required_Fields from '@salesforce/label/c.msg_Required_Fields';
import hl_Select_a_record_type from '@salesforce/label/c.hl_Select_a_record_type';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;

export default class CampaignRelatedList extends LightningElement {
	label = {
		msg_Delete,
		hl_Search,
		modalHeader,
		btn_save,
		btn_New,
		btn_cancel,
		btn_Delete,
		msg_Record_Delete,
		msg_Record_Saved,
		hl_Campaign,
		btn_Next,
		msg_Folder_Description,
		msg_Required_Fields,
		hl_Select_a_record_type
	};
	@api recordId;
	@api objectApiName;
	@api objectFileds = [];
	@track searchKey = '';
	@track data = [];
	@track columns = [];
	@track rowOffset = 0;
	@track tableLoadingState = true;
	@track showRowNumberColumn = false;
	@track sortBy = 'LastModifiedDate';
	@track sortDirection = 'desc';
	@track sortedBy = 'LastModifiedDate';
	@track sortedDirection = 'desc';
	@track selectedRows = [];
	@track hideCheckboxColumn = true;
	@track objectTypeLabel = 'Campaigns';
	@track listFields = [];
	@track paramDataTable;
	@track sectionCssClass = 'slds-modal slds-fade-in-open slds-modal_small';
	@track recordFormId;
	@track content = this.label.msg_Delete;
	@track header = this.label.modalHeader;
	@track layoutSections = [];
	@track activeAccordionSections = [];
	@track showNew = true;
	@track paramGetRecords;
	@track typeLayoutSections = [];
	@track showUnknownRelatedList = false; //Sowmya changes 17/10/2019 for W-000123
	@track isSelectRecordType = false;
	@track selectedRecordTypeId;
	@track options = [];
	@track layoutInfo = false;
	@track recordTypeId;
	@track reloadForm = true;
	@track folderDescription = this.label.msg_Folder_Description;
	@track relatedListObject;
	@track recordTypeIdParent;
	@track recordTypeIdParentName;
	//custom lookup
	@track objInfo = [];
	@api fieldLabel;
	@api selectRecordName;
	@track fieldValue;
	@track fields;
	@api selectRecordId;
	@track hasDisabled = false;
	@track clearIconFlag;
	@track fieldName = 'Name';
	@api currentText;
	@api lastView;
	@track showIcon;
	@track startDate;
	@track endDate;
	@track action;
	@track campaignStartDate;
	@track campaignEndDate;
	@track marketingArea = '';
	@track marketingBusinessUnit = '';
	@track audience = '';
	@track sectioncssclass = 'slds-modal slds-fade-in-open slds-modal_small';
	@track newCampaigns=this.label.btn_New+' '+this.label.hl_Campaign;
	@track Country;
	@track CountryV = false;
	@track selectedRecordTypeName;
	/** Wired Apex result so it may be programmatically refreshed. */
	wiredData;
	wiredGetRecord;

	@wire(getObjectInfo, { objectApiName: MARKETINGPLAN_OBJECT })
	wiredPlanInfo({ data, error }) {
		if (data) {
			this.objInfo = data;
			if (this.objectApiName == 'musqotmp__MarketingPlan__c') {
				this.objectFileds = [MARKETINGPLAN_NAME_FIELD, MARKETINGPLAN_SD_FIELD, MARKETINGPLAN_ED_FIELD, MARKETINGPLAN_MARKETINGAREA_FIELD, MARKETINGPLAN_BUSINESS_UNIT_FIELD];
			} else if (this.objectApiName == 'Campaign') {
				this.objectFileds = [CAMPAIGN_SD_FIELD, CAMPAIGN_ED_FIELD, CAMPAIGN_MARKETINGAREA_FIELD, CAMPAIGN_BUSINESS_UNIT_FIELD, CAMPAIGN_AUDIENCE_FIELD, CAMPAIGN_MARKETINGPLAN_ID_FIELD, CAMPAIGN_MARKETINGPLAN_NAME_FIELD];
			}
			//console.log('this.objectFileds :' + JSON.stringify(this.objectFileds));
		} else if (error) {
			this.template.querySelector('c-toast').showToast(error);
		}
	}

	@wire(getObjectInfo, { objectApiName: CAMPAIGN_OBJECT })
	wiredObjectInfo({ data, error }) {
		if (data) {
			this.sObjcampaign = data;
			//	window.console.log('recordId :' + this.recordId);
			let optionsValues = [];
			// map of record type Info
			const rtInfos = data.recordTypeInfos;
			// getting map values
			let rtValues = Object.values(rtInfos);
			//rtValues.splice(0,1);
			for (let i = 0; i < rtValues.length; i++) {
			  if(rtValues[i].available === true){
				if (rtValues[i].name !== 'Master') {
					/*if (rtValues[i].name === 'Folder Campaign') {
						this.recordTypeId = rtValues[i].recordTypeId;
						this.selectedRecordTypeId = rtValues[i].recordTypeId;
						optionsValues.push({
							label: rtValues[i].name,
							value: rtValues[i].recordTypeId,
							selected: true,
							description: this.folderDescription
						});
						//	window.console.log('recordTypeId :' + this.recordTypeId);
					} else if (rtValues[i].name === "Execution Campaign") {//Customer Evolution Campaign (CEC), Tactical Campaign (TC)
						this.selectedRecordTypeId = rtValues[i].recordTypeId;
						optionsValues.push({
							label: rtValues[i].name,
							value: rtValues[i].recordTypeId
						});
					}*/
                   if(this.objectApiName === 'musqotmp__MarketingPlan__c'){
                      if(rtValues[i].name === 'Customer Evolution Campaign (CEC)'){
						this.selectedRecordTypeName = 'Customer Evolution Campaign (CEC)';	   
						this.selectedRecordTypeId = rtValues[i].recordTypeId;
						optionsValues.push({
							label: rtValues[i].name,
							value: rtValues[i].recordTypeId
						});
					  }
				   } else if(this.objectApiName === 'Campaign'){
					   if(rtValues[i].name === 'Tactical Campaign (TC)'){
						this.selectedRecordTypeName = 'Tactical Campaign (TC)';	   
					this.selectedRecordTypeId = rtValues[i].recordTypeId;
					optionsValues.push({
						label: rtValues[i].name,
						value: rtValues[i].recordTypeId
					});
				   }
				  }
				}
			  }
			}
			this.options = optionsValues;
		} else if (error) {
			this.template.querySelector('c-toast').showToast(error);
		}
	}

	handleChange(event) {
		this.selectedRecordTypeId = '';
		this.selectedRecordTypeId = event.detail.value;
		//	this.layoutInfo = true;
		const selectedValue = event.target.value;
		this.selectedRecordTypeId = selectedValue;
		refreshApex(this.wiredGetRecord);
	}
	closeModal() {
		this.isSelectRecordType = false;
	}
	buildParamGetRecords() {
		const whereCaluses = [];
		whereCaluses.push({
			field: 'Id',
			value: this.recordId,
			clause: '='
		});
		if (this.objectApiName === 'Campaign') {
			return JSON.stringify({
				component: 'campaignRelatedList-getRecords',
				recordId: this.recordId,
				objectApiName: this.objectApiName,
				fieldMap: {
					[CAMPAIGN_STATUS_FIELD.fieldApiName]: {}
				},
				whereCaluses: whereCaluses
			});
		}
		return JSON.stringify({
			component: 'campaignRelatedList-getRecords',
			recordId: this.recordId,
			objectApiName: this.objectApiName,
			fieldMap: {
				[MARKETINGPLAN_STATUS_FIELD.fieldApiName]: {},
				[MARKETINGPLAN_UNKNOWNPLAN_FIELD.fieldApiName]: {} //Sowmya changes 17/10/2019 for W-000123
			},
			whereCaluses: whereCaluses
		});
	}

	@wire(getRecords, { param: '$paramGetRecords' })
	wiredGetRecords(result) {
		if (result.data) {
			if (result.data.toast.ok) {
				result.data.sObjects.forEach((sObject) => {
					if (this.objectApiName === 'Campaign') {
						if (
							getSObjectValue(sObject, CAMPAIGN_STATUS_FIELD) === 'Completed' ||
							getSObjectValue(sObject, CAMPAIGN_STATUS_FIELD) === 'Aborted' ||
							getSObjectValue(sObject, CAMPAIGN_STATUS_FIELD) === 'On Hold'
						) {
							this.showNew = false;
						}
					} else {
						if (
							getSObjectValue(sObject, MARKETINGPLAN_STATUS_FIELD) === 'Completed' ||
							getSObjectValue(sObject, MARKETINGPLAN_STATUS_FIELD) === 'Aborted' ||
							getSObjectValue(sObject, MARKETINGPLAN_STATUS_FIELD) === 'On Hold'
						) {
							this.showNew = false;
						}
					}
					//Sowmya changes 17/10/2019 for W-000123
					if (this.objectApiName === 'musqotmp__MarketingPlan__c') {
						if (getSObjectValue(sObject, MARKETINGPLAN_UNKNOWNPLAN_FIELD) === true) {
							this.showUnknownRelatedList = true;
							this.showNew = false;
						}
					}
					//W-000123 end
				});
				this.template.querySelector('c-toast').showToast(result.data.toast);
				this.paramDataTable = this.buildParamDataTable();
			}
		} else if (result.error) {
			this.template.querySelector('c-toast').showToast(result.error);
		}
	}
	buildParamDataTable() {
		const whereCaluses = [];
		const whereCaluseSubs = [];
		const operator = 'AND';
		let fieldSetApiName = 'musqotmp__CampaignRelatedList';
		if (this.objectApiName === 'Campaign') {
			fieldSetApiName = 'musqotmp__SubCampaignRelatedList';
			whereCaluses.push({
				field: 'ParentId',
				value: this.recordId,
				clause: '='
			});
		} else {
			if (this.showUnknownRelatedList === false) {
				//Sowmya changes 17/10/2019 for W-000123
				whereCaluses.push({
					field: 'ParentId',
					value: '',
					clause: '=',
					operator: 'AND'
				});
				whereCaluses.push({
					field: 'musqotmp__MarketingPlan__c',
					value: this.recordId,
					clause: '='
				});
				//Sowmya changes 17/10/2019 for W-000123
			} else {
				whereCaluses.push({
					field: 'ParentId',
					value: '',
					clause: '=',
					operator: 'AND'
				});
				whereCaluses.push({
					field: 'musqotmp__MarketingPlan__c',
					value: '',
					clause: '='
				});
			}
			//End W-000123
		}

		if (this.searchKey !== '') {
			this.listFields.forEach((listField) => {
				let name;
				if (listField.type === 'string') {
					name = listField.name;
				} else if (listField.type === 'combobox') {
					name = listField.name;
				} else if (listField.type === 'picklist') {
					name = listField.name;
				} else if (listField.type === 'reference') {
					name = listField.name;
					if (name === 'OwnerId') {
						name = 'Owner.Name';
					} else if (name === 'CreatedById') {
						name = 'CreatedBy.Name';
					} else if (name === 'LastModifiedById') {
						name = 'LastModifiedBy.Name';
					} else if (name.includes('__c')) {
						name = name.replace('__c', '__r.Name');
					}
				}

				if (name) {
					whereCaluseSubs.push({
						field: name,
						value: this.searchKey,
						clause: 'LIKE',
						operator: 'OR'
					});
				}
			});
		}

		const rowLevelactions = [{ label: 'Delete', name: 'delete', iconName: 'utility:delete' }];
		return JSON.stringify({
			component: 'campaignRelatedList-datatable',
			relatedListType: 'finance',
			objectApiName: CAMPAIGN_OBJECT.objectApiName,
			fieldMap: {
				[CAMPAIGN_ID_FIELD.fieldApiName]: {}
			},
			fieldSetApiName: fieldSetApiName,
			linkableNameField: true,
			whereCaluses: whereCaluses,
			operator: operator,
			whereCaluseSubs: whereCaluseSubs,
			sortBy: this.sortBy,
			sortDirection: this.sortDirection,
			rowLevelactions: rowLevelactions
		});
	}

	@wire(datatable, {
		param: '$paramDataTable'
	})
	wiredDataTable(result) {
		this.wiredData = result;
		if (result.data) {
			if (result.data.toast.ok) {
				this.sortedBy = this.sortBy;
				this.sortedDirection = this.sortDirection;
				//this.columns = result.data.datatable.columns;
				this.columns = [{ "label": "Name", "sortable": true, "fieldName": "Campaign.Name", "type": "url", "typeAttributes": { "label": { "fieldName": "Name" }, "target": "_self" } }, { "label": "Type", "sortable": true, "fieldName": "Type", "type": "picklist" }, { "label": "Start Date", "sortable": true, "fieldName": "StartDate", "type": "text", }, { "label": "End Date", "sortable": true, "fieldName": "EndDate", "type": "text" }, { "label": "Status", "sortable": true, "fieldName": "Status", "type": "picklist" }, { "type": "action", "typeAttributes": { "rowActions": [{ "label": "Delete", "name": "delete", "iconName": "utility:delete" }] } }];
				this.data = result.data.datatable.data;
				this.listFields = result.data.fields;
				this.objectTypeLabel = result.data.objectTypeLabel;
				this.template.querySelector('c-toast').showToast(result.data.toast);
			} else {
				this.data = undefined;
				this.columns = undefined;
				this.template.querySelector('c-toast').showToast(result.data.toast);
			}
		} else if (result.error) {
			this.data = undefined;
			this.columns = undefined;
			this.template.querySelector('c-toast').showToast(result.error);
		}
		this.tableLoadingState = false;
	}
	handleColumnSort(event) {
		this.sortBy = event.detail.sortBy;
		this.sortDirection = event.detail.sortDirection;
		this.paramDataTable = this.buildParamDataTable();
	}

	handleSearchKeyChange(event) {
		// Debouncing this method: Do not update the reactive property as long as this function is being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchKey = event.target.value;
		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.searchKey = searchKey;
			this.paramDataTable = this.buildParamDataTable();
		}, DELAY);
	}
	handleNew() {
		this.hasDisabled = false;
		/*if (this.recordTypeIdParent != undefined || this.recordTypeIdParentName === 'Folder Campaign') {
			this.isSelectRecordType = true;
			this.handleReset();
		} else {*/
			//this.hasDisabled = false;//sowmya
			this.header = this.label.btn_New+' ' + this.objectTypeLabel;
			const modal = this.template.querySelector('c-record-form-modal');
			modal.show();
			this.handleReset();
		//}
	}
	handleNext() {
		this.recordFormId = null;
		this.activeAccordionSections = [];
		this.typeLayoutSections.forEach((sectionHeaderName) => {
			this.activeAccordionSections.push(sectionHeaderName.heading);
		})
		this.isSelectRecordType = false;
		this.header = this.label.btn_New+' ' + this.objectTypeLabel;
		const modal = this.template.querySelector('c-record-form-modal');
		modal.show();
	}

	handleSelect(event) {
		this.recordFormId = event.detail.id;
		if (event.detail.actionName === 'edit') {
			const modal = this.template.querySelector('c-record-form-modal');
			modal.show();
		} else {
			const modal = this.template.querySelector('c-modal');
			modal.show();
		}
		this.header = event.detail.actionLabel + ' ' + this.objectTypeLabel;
	}

	handleCancelRecordFormModal() {
		this.recordFormId = undefined;
		this.layoutInfo = false;
		//	this.recordTypeIdParent = undefined;
		//	this.recordTypeIdParentName = undefined;
		const modal = this.template.querySelector('c-record-form-modal');
		modal.hide();
	}

	handleCancelModal() {
		this.recordFormId = null;
		const modal = this.template.querySelector('c-modal');
		modal.hide();
	}

	handleDelete() {
		const recordId = this.recordFormId;
		deleteRecord(recordId)
			.then(() => {
				const toast = getToast('success', 'pester', this.label.msg_Record_Delete);
				this.template.querySelector('c-toast').showToast(toast);
				this.handleRefresh();
				this.handleCancelModal();
			})
			.catch((error) => {
				this.template.querySelector('c-toast').showToast(error);
				this.handleCancelModal();
			});
	}
	onLoad(event) {
		this.layoutSections = [];
		this.activeAccordionSections = [];
		for (let i = 0; i <= 1; i++) {
			this.layoutSections.push(event.detail.layout.sections[i]);
			this.activeAccordionSections.push(event.detail.layout.sections[i].heading);
		}
		//If The changed any value in plan, that'll reflect in campaign without reloading the page
		if (this.wiredGetRecord) {
			refreshApex(this.wiredGetRecord);
			this.buildTypeLayoutSections(this.layoutSections, this.wiredGetRecord);
		}

	}
	@wire(getRecord, { recordId: '$recordId', layoutTypes: ['Full'], modes: ['Edit'] })
	wiredGetRecordInfo({ data, error }) {
		if (data) {
			this.wiredGetRecord = data;
			if (this.objectApiName === 'Campaign') {
				this.recordTypeIdParent = data.recordTypeInfo.recordTypeId;
				this.recordTypeIdParentName = data.recordTypeInfo.name;
				if (data.recordTypeInfo.name === 'Execution Campaign') {
					this.selectedRecordTypeId = data.recordTypeInfo.recordTypeId;
				}
				if (data.recordTypeInfo.name === 'Execution Campaign') {
					this.selectedRecordTypeId = data.recordTypeInfo.recordTypeId;
				}
				else if (data.recordTypeInfo.name === 'Folder Campaign') {
					this.selectedRecordTypeId = data.recordTypeInfo.recordTypeId;
				//	console.log("Folder Id =====" + this.selectedRecordTypeId);
				}
			}
			this.paramGetRecords = this.buildParamGetRecords();
			this.buildTypeLayoutSections(this.layoutSections, this.wiredGetRecord);
		} else if (error) {
			this.template.querySelector('c-toast').showToast(error);
		//	console.log('error: ', error);
		}
	}
	buildTypeLayoutSections(layoutSections, record) {
		this.typeLayoutSections = [];
		this.parentValueMap = {};
		layoutSections.forEach((layoutSection) => {
			let typeLayoutSection = {};
			typeLayoutSection['id'] = layoutSection.id;
			typeLayoutSection['heading'] = layoutSection.heading;
			let layoutRows = [];
			layoutSection.layoutRows.forEach((layoutRow) => {
				let typeLayoutRows = {};
				let layoutItems = [];
				layoutRow.layoutItems.forEach((layoutItem) => {
					let typeLayoutItems = {};
					let layoutComponents = [];
					layoutItem.layoutComponents.forEach((layoutComponent) => {
						let typeLayoutComponent = {};
						typeLayoutComponent['apiName'] = layoutComponent.apiName;
						if (this.objectApiName === 'Campaign') {
							if (layoutComponent.apiName === 'ParentId') {
								this.parentValueMap[layoutComponent.apiName] = this.recordId;
								typeLayoutComponent['value'] = this.recordId;
							} else if (layoutComponent.apiName === 'StartDate') {
								let rightNow = new Date();
								// Adjust for the user's time zone
								rightNow.setMinutes(
									new Date().getMinutes() - new Date().getTimezoneOffset()
								);
								// Return the date in "YYYY-MM-DD" format
								let yyyyMmDd = rightNow.toISOString().slice(0, 10);
								let yyyyMmDdTime = new Date(yyyyMmDd).getTime();

								const startDate = record.fields.StartDate.value;

								let startDateTime = new Date(startDate).getTime();
								let dateToSet;
								if (yyyyMmDdTime < startDateTime) {
									dateToSet = startDate;
								} else {
									dateToSet = yyyyMmDd;
								}
								typeLayoutComponent['value'] = dateToSet;
								typeLayoutComponent['label'] = layoutComponent.label;
								typeLayoutComponent['setdateFormat'] = true;
								this.parentValueMap[layoutComponent.apiName] = dateToSet;
								this.campaignStartDate = dateToSet;
							} else if (layoutComponent.apiName === 'EndDate') {
								let endDate = record.fields.EndDate.value;
								let endDateTime = new Date(endDate).getTime();
								let campenddate = new Date(this.campaignStartDate);
								campenddate.setDate(campenddate.getDate() + 30);
								let yyyyMmDdEnd = campenddate.toISOString().slice(0, 10);
								let campenddateTime = new Date(yyyyMmDdEnd).getTime();
								let endDateToSet;
								if (campenddateTime < endDateTime) {
									endDateToSet = yyyyMmDdEnd;
								} else {
									endDateToSet = endDate;
								}
								typeLayoutComponent['value'] = endDateToSet;
								typeLayoutComponent['label'] = layoutComponent.label;
								typeLayoutComponent['setdateFormat'] = true;
								this.campaignEndDate = endDateToSet;
								this.parentValueMap[layoutComponent.apiName] = endDateToSet;
							
							} else if (layoutComponent.apiName === 'musqotmp__MarketingPlan__c') {
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__MarketingPlan__c.value;
								typeLayoutComponent['hasPlan'] = true;
								if (layoutComponent.label === 'Marketing Plan') {
									this.fieldLabel = layoutComponent.label;
								}
								typeLayoutComponent['value'] = record.fields.musqotmp__MarketingPlan__c.value;
								this.selectRecordId = record.fields.musqotmp__MarketingPlan__c.value;
								this.fieldValue = record.fields.musqotmp__MarketingPlan__r.value.fields.Name.value;
							} else if (record !== null && layoutComponent.apiName !== null && layoutComponent.apiName !== 'Name' && record.fields[layoutComponent.apiName]) {
								this.parentValueMap[layoutComponent.apiName] = record.fields[layoutComponent.apiName].value;
								typeLayoutComponent['value'] = record.fields[layoutComponent.apiName].value;
							}
						} else if (this.objectApiName === 'musqotmp__MarketingPlan__c') {
							this.fieldValue = record.fields.Name.value;
							if (layoutComponent.apiName === 'musqotmp__MarketingPlan__c') {
								this.parentValueMap['musqotmp__MarketingPlan__c'] = this.recordId;
								typeLayoutComponent['value'] = this.recordId;
								typeLayoutComponent['hasPlan'] = true;
								if (layoutComponent.label === 'Marketing Plan') {
									this.fieldLabel = layoutComponent.label;
								}
								this.selectRecordId = this.recordId;
							} else if (layoutComponent.apiName === 'StartDate') {
								// Get the current date/time in UTC
								let rightNow = new Date();
								// Adjust for the user's time zone
								rightNow.setMinutes(
									new Date().getMinutes() - new Date().getTimezoneOffset()
								);
								// Return the date in "YYYY-MM-DD" format
								let yyyyMmDd = rightNow.toISOString().slice(0, 10);
								let yyyyMmDdTime = new Date(yyyyMmDd).getTime();

								const startDate = record.fields.musqotmp__StartDate__c.value;
								let startDateTime = new Date(startDate).getTime();
								let dateToSet;
								if (yyyyMmDdTime < startDateTime) {
									dateToSet = startDate;
								} else {
									dateToSet = yyyyMmDd;
								}
								typeLayoutComponent['value'] = dateToSet;
								typeLayoutComponent['label'] = layoutComponent.label;
								typeLayoutComponent['setdateFormat'] = true;
								this.campaignStartDate = dateToSet;
								this.parentValueMap[layoutComponent.apiName] = dateToSet;
							} else if (layoutComponent.apiName === 'EndDate') {
								let endDate = record.fields.musqotmp__EndDate__c.value;
								let endDateTime = new Date(endDate).getTime();
								let campenddate = new Date(this.campaignStartDate);
								campenddate.setDate(campenddate.getDate() + 30);
								let yyyyMmDdEnd = campenddate.toISOString().slice(0, 10);
								let campenddateTime = new Date(yyyyMmDdEnd).getTime(); 
								let endDateToSet;
								if (campenddateTime < endDateTime) {
									endDateToSet = yyyyMmDdEnd;
								} else {
									endDateToSet = endDate;
								}
								typeLayoutComponent['value'] = endDateToSet;
								typeLayoutComponent['label'] = layoutComponent.label;
								typeLayoutComponent['setdateFormat'] = true;
								this.campaignEndDate = endDateToSet;
								this.parentValueMap[layoutComponent.apiName] = endDateToSet;
                                //Bayer Start 
							} else if (layoutComponent.apiName === 'musqotmp__Country__c'){  
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Country__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Country__c.value;
								
							} else if (layoutComponent.apiName === 'musqotmp__Country_Code__c'){
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Country_Code__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Country_Code__c.value;
						
							} else if (layoutComponent.apiName === 'musqotmp__Therapeutic_Area__c'){ 
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Therapeutic_Area__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Therapeutic_Area__c.value;
								
							} else if (layoutComponent.apiName === 'musqotmp__Portfolio_Commercial__c'){
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Portfolio_Commercial__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Portfolio_Commercial__c.value;
							
							} else if (layoutComponent.apiName === 'musqotmp__Portfolio_Medical__c'){
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Portfolio_Medical__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Portfolio_Medical__c.value;
								
							}else if (layoutComponent.apiName === 'musqotmp__Product_Commercial__c'){ 
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Product_Commercial__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Product_Commercial__c.value;
							
							} else if (layoutComponent.apiName === 'musqotmp__Product_Medical__c'){  
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Product_Medical__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Product_Medical__c.value;
							
							} else if (layoutComponent.apiName === 'musqotmp__Product_Code_Commercial__c'){  
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Product_Code_Commercial__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Product_Code_Commercial__c.value;
								
							} else if (layoutComponent.apiName === 'musqotmp__Product_Code_Medical__c'){  
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Product_Code_Medical__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Product_Code_Medical__c.value;
							
							} else if (layoutComponent.apiName === 'musqotmp__Possible_Tactical_Campaign_Types__c'){    
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Possible_Tactical_Campaign_Types__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Possible_Tactical_Campaign_Types__c.value;  
							} else if (layoutComponent.apiName === 'musqotmp__CEC_ID_text__c' && this.selectedRecordTypeName === 'Tactical Campaign (TC)'){    
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__CEC_ID_text__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__CEC_ID_text__c.value;    
							} else if (layoutComponent.apiName === 'musqotmp__TC_ID_text__c' && this.selectedRecordTypeName === 'Tactical Campaign (TC)'){    
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__TC_ID_text__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__TC_ID_text__c.value;
							/*} else if (layoutComponent.apiName === 'musqotmp__Tactical_Campaign_Type__c' && this.selectedRecordTypeName === 'Tactical Campaign (TC)'){    
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Tactical_Campaign_Type__c.value;
								typeLayoutComponent['value'] = record.fields.musqotmp__Tactical_Campaign_Type__c.value;   */   
								//Bayer End
							} else if (record !== null && layoutComponent.apiName !== null && layoutComponent.apiName !== 'Name' && record.fields[layoutComponent.apiName]) {
								//this.parentValueMap[layoutComponent.apiName] = record.fields[layoutComponent.apiName].value;
								typeLayoutComponent['value'] = record.fields[layoutComponent.apiName].value;
							}
						}
						if(typeLayoutComponent.apiName==='Type'){
							typeLayoutComponent.value=undefined;
						}
						layoutComponents.push(typeLayoutComponent);
					});
					typeLayoutItems['layoutComponents'] = layoutComponents;
					typeLayoutItems['required'] = layoutItem.required;
					layoutItems.push(typeLayoutItems);
				});
				typeLayoutRows['layoutItems'] = layoutItems;
				layoutRows.push(typeLayoutRows);
			});
			typeLayoutSection['layoutRows'] = layoutRows;
			this.typeLayoutSections.push(typeLayoutSection);
		});
	}
	handleClose() {
		this.isSelectRecordType = true;
		this.handleCancelRecordFormModal();
		this.handleRefresh();
	}
	//custom lookup 
	get getDisabled() {
		if (this.hasDisabled) {
			this.clearIconFlag = false;
		}
		return this.hasDisabled;
	}
	get getshowIcons() {
		if (this.fieldValue === '' || this.fieldValue === undefined) {
			this.showIcon = false;
			this.clearIconFlag = false;
		} else {
			this.showIcon = true;
			this.clearIconFlag = true;
		}
		return this.showIcon, this.clearIconFlag;
	}
	handleClickValue(event) {
		this.onClickValue = event.detail.onClickValue;
		let objectApiName = event.detail.objectApiName
		const whereCaluses = [];
		whereCaluses.push({
			field: 'LastViewedDate',
			clause: '!=',
			value: null
		})
		let sortedDirection = 'DESC';
		let amountOfRecords = 400000;
		let sortedBy = 'Name';
		let param = JSON.stringify({
			component: 'Customlookup',
			objectApiName: objectApiName,
			whereCaluses: whereCaluses,
			sortBy: sortedBy,
			sortDirection: sortedDirection,
			amountOfRecords: amountOfRecords
		})
		this.template.querySelector('c-customlookup').getResultData(param);
	}
	handleClick(event) {
		this.hasDisabled = false;
		if (event.detail.lastView) {
			this.lastView = event.detail.lastView;
			let objectApiName = event.detail.objectApiName;
			if (this.lastView) {
				const whereCaluses = [];
				whereCaluses.push({
					field: 'LastViewedDate',
					clause: '!=',
					value: null
				})
				let sortedDirection = 'DESC';
				let amountOfRecords = 400000;
				let sortedBy = 'Name';
				let param = JSON.stringify({
					component: 'Customlookup',
					objectApiName: objectApiName,
					whereCaluses: whereCaluses,
					sortBy: sortedBy,
					sortDirection: sortedDirection,
					amountOfRecords: amountOfRecords
				})
				this.template.querySelector('c-customlookup').getResultData(param);
			}
		}
	}
	handelSearch(event) {
		if (event.detail.currentText) {
			this.currentText = event.detail.currentText;
			let objectApiName = event.detail.objectApiName;
			if (this.currentText != null && this.currentText != '') {
				const whereCaluses = [];
				whereCaluses.push({
					field: this.fieldName,
					value: this.currentText,
					clause: 'LIKE'
				})
				let sortedDirection = 'DESC';
				let amountOfRecords = 400000;
				let sortedBy = 'Name';
				let param = JSON.stringify({
					component: 'Customlookup',
					objectApiName: objectApiName,
					whereCaluses: whereCaluses,
					sortBy: sortedBy,
					sortDirection: sortedDirection,
					amountOfRecords: amountOfRecords
				})
				this.template.querySelector('c-customlookup').getResultData(param);
			}
		}
	}
	//sumbitting the record
	handelSelect(event) {
		if (event.detail.recordId) {
			this.selectRecordId = event.detail.recordId;
		}
	}
	handleSubmit(event) {
		event.preventDefault(); // stop the form from submitting
		this.fields = event.detail.fields;
		if (event.currentTarget.getAttribute("data-action")) {
			this.action = event.currentTarget.getAttribute("data-action");
		}
		if (this.action === 'new') {
			this.hasDisabled = true;
			this.createRecord();
		}
		this.recordFormId = null;
		this.recordTypeIdParent = undefined;
		this.recordTypeIdParentName = undefined;
	}
	handleDateChange(event) {
		this.hasDisabled = false;
	}
	handleInputChange(event) {
		this.hasDisabled = false;
	}
	handleReset() {
		const inputFields = this.template.querySelectorAll(
			'lightning-input-field'
		);
		if (inputFields) {
			inputFields.forEach(field => {
				field.reset();
			});
		}
		if (this.wiredGetRecord != null) {
			if (this.objectApiName === 'Campaign') {
				this.template.querySelector("[data-field='ParentId']").value = this.recordId;
			}
			this.template.querySelector("[data-field='StartDate']").value = this.parentValueMap['StartDate'];
			this.template.querySelector("[data-field='EndDate']").value = this.parentValueMap['EndDate'];

			this.template.querySelector("[data-field='musqotmp__Country__c']").value = this.parentValueMap['musqotmp__Country__c'];
			this.template.querySelector("[data-field='musqotmp__Country_Code__c']").value = this.parentValueMap['musqotmp__Country_Code__c'];
			this.template.querySelector("[data-field='musqotmp__Therapeutic_Area__c']").value = this.parentValueMap['musqotmp__Therapeutic_Area__c'];
			this.template.querySelector("[data-field='musqotmp__Portfolio_Commercial__c']").value = this.parentValueMap['musqotmp__Portfolio_Commercial__c'];
			this.template.querySelector("[data-field='musqotmp__Portfolio_Medical__c']").value = this.parentValueMap['musqotmp__Portfolio_Medical__c'];
			this.template.querySelector("[data-field='musqotmp__Product_Commercial__c']").value = this.parentValueMap['musqotmp__Product_Commercial__c'];
			this.template.querySelector("[data-field='musqotmp__Product_Medical__c']").value = this.parentValueMap['musqotmp__Product_Medical__c'];
			this.template.querySelector("[data-field='musqotmp__Product_Code_Commercial__c']").value = this.parentValueMap['musqotmp__Product_Code_Commercial__c'];
			this.template.querySelector("[data-field='musqotmp__Product_Code_Medical__c']").value = this.parentValueMap['musqotmp__Product_Code_Medical__c'];
			this.template.querySelector("[data-field='musqotmp__Possible_Tactical_Campaign_Types__c']").value = this.parentValueMap['musqotmp__Possible_Tactical_Campaign_Types__c'];
			//this.template.querySelector("[data-field='musqotmp__Tactical_Campaign_Type__c']").value = this.parentValueMap['musqotmp__Tactical_Campaign_Type__c'];
            if(this.selectedRecordTypeName === 'Tactical Campaign (TC)'){
				this.template.querySelector("[data-field='musqotmp__CEC_ID_text__c']").value = this.parentValueMap['musqotmp__CEC_ID_text__c'];
				this.template.querySelector("[data-field='musqotmp__TC_ID_text__c']").value = this.parentValueMap['musqotmp__TC_ID_text__c'];	
			}
			
			
			/*this.template.querySelector("[data-field='musqotmp__Countries__c']").value = this.parentValueMap['musqotmp__Countries__c'];
			this.template.querySelector("[data-field='musqotmp__MarketingBusinessUnit__c']").value = this.parentValueMap['musqotmp__MarketingBusinessUnit__c'];
			this.template.querySelector("[data-field='musqotmp__MarketingArea__c']").value = this.parentValueMap['musqotmp__MarketingArea__c'];
			this.template.querySelector("[data-field='musqotmp__ProductGroup__c']").value = this.parentValueMap['musqotmp__ProductGroup__c'];
			this.template.querySelector("[data-field='musqotmp__Shop__c']").value = this.parentValueMap['musqotmp__Shop__c'];
			this.template.querySelector("[data-field='musqotmp__Audience__c']").value = this.parentValueMap['musqotmp__Audience__c'];
			this.template.querySelector("[data-field='musqotmp__City__c']").value = this.parentValueMap['musqotmp__City__c'];*/
		}
	}

	createRecord() {
		const fields = this.fields;
		fields[CAMPAIGN_MARKETINGPLAN_ID_FIELD.fieldApiName] = this.selectRecordId;
		fields[RECORD_TYPE_FIELD.fieldApiName] = this.selectedRecordTypeId;
		let startDt = new Date(this.template.querySelector("[data-field='StartDate']").value);
		let endDt = new Date(this.template.querySelector("[data-field='EndDate']").value);
		fields[CAMPAIGN_SD_FIELD.fieldApiName] = startDt;//this.template.querySelector("[data-field='StartDate']").value;
		fields[CAMPAIGN_ED_FIELD.fieldApiName] = endDt;//this.template.querySelector("[data-field='EndDate']").value;
		if (this.recordTypeId === this.selectedRecordTypeId) {
			fields[IS_FOLDER_FIELD.fieldApiName] = true;
		}
		let validateMessage = this.label.msg_Required_Fields;
		let validateField = '';
		let valid = true;
		if (!fields.Name) {
			validateField = ' ' + this.sObjcampaign.fields.Name.label + ',';
			valid = false;
		} if (!fields.StartDate) {
			validateField = ' ' + this.sObjcampaign.fields.StartDate.label + ',';
			valid = false;
		} if (!fields.EndDate) {
			validateField = ' ' + this.sObjcampaign.fields.EndDate.label + ',';
			valid = false;
		} if (!fields.musqotmp__MarketingPlan__c) {
			validateField = ' ' + this.sObjcampaign.fields.musqotmp__MarketingPlan__c.label;
			valid = false;
		}
		if (valid) {
			const recordInput = { apiName: CAMPAIGN_OBJECT.objectApiName, fields };
			createRecord(recordInput)
				.then((result) => {
					this.result = result;
					this.handleCancelRecordFormModal();
					this.handleRefresh();
					const toast = getToast('success', 'pester', this.label.msg_Record_Saved);
					this.template.querySelector('c-toast').showToast(toast);
				})
				.catch((error) => {
					let customErrorField = [];
					let ErrorMessage = '';
					customErrorField = error.body.output.errors;
					let standardErrorField = error.body.output.fieldErrors;
					if (standardErrorField) {
						if (standardErrorField.StartDate) {
							let customvalidation = error.body.output.fieldErrors.StartDate[0].message;
							const evt = new ShowToastEvent({
								title: 'Error',
								message: customvalidation,
								variant: 'error',
								mode: 'sticky'
							});
							this.dispatchEvent(evt);
						}
						if (standardErrorField.EndDate) {
							let customvalidation = error.body.output.fieldErrors.EndDate[0].message;
							const evt = new ShowToastEvent({
								title: 'Error',
								message: customvalidation,
								variant: 'error',
								mode: 'sticky'
							});
							this.dispatchEvent(evt);
						}
					}
					if (customErrorField) {
						for (let i = 0; i < customErrorField.length; i++) {
							const evt = new ShowToastEvent({
								title: 'Error',
								message: customErrorField[i].message,
								variant: 'error',
								mode: 'sticky'
							});
							this.dispatchEvent(evt);
						}
					}
				});
		} else {
			this.hasDisabled = false;
			validateMessage += validateField;
			const toast = getToast('error', 'sticky', validateMessage);
			this.template.querySelector('c-toast').showToast(toast);
		}
	}
	handleSelectionChange(event) {
		this.errors = [];
		this.planRecord = event.detail.recordId;
	}
	handleRefresh() {
		this.selectedRows = [];
		if (this.wiredData.data) {
			refreshApex(this.wiredData);
		}
	}
}