import { LightningElement, wire, track, api } from "lwc";
import { updateRecord, getRecord } from "lightning/uiRecordApi";
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";

import { loadScript, loadStyle } from "lightning/platformResourceLoader";
import { CurrentPageReference, NavigationMixin } from "lightning/navigation";
import { refreshApex, getSObjectValue } from "@salesforce/apex";
import MOMENT_JS from "@salesforce/resourceUrl/moment";
import chartjs from "@salesforce/resourceUrl/chartjs_2";
import Gantt from "@salesforce/resourceUrl/gantt_6";
import ChartjsPluginAnnotation from "@salesforce/resourceUrl/ChartjsPluginAnnotation";

import getPerformanceGantt from "@salesforce/apex/PerformanceGantt.getPerformanceGantt";
import getDataSet from "@salesforce/apex/PerformanceGantt.getDataSet";
import getRecords from "@salesforce/apex/PerformanceGantt.getRecords";

import modalHeader from "@salesforce/label/c.modalHeader";
import statusHeader from "@salesforce/label/c.gantt_StatusHeader";
import campaignHeader from "@salesforce/label/c.gantt_columnCampaignHeader";
import cancel from "@salesforce/label/c.btn_cancel";
import open from "@salesforce/label/c.btn_open";
import btnOpenInMC from "@salesforce/label/c.btn_open_in_mc";
import msgCorrelation from "@salesforce/label/c.msg_correlation";

import CAMPAIGN_OBJECT from "@salesforce/schema/Campaign";
import MARKETING_ACTIVITY_OBJECT from "@salesforce/schema/MarketingActivity__c";
import STACKED_FIELD from "@salesforce/schema/GanttPerformance__c.StackedField__c";

import DATA_VARIABLE_ITEM_OBJECT from "@salesforce/schema/DataVariableItem__c";
import DATA_VARIABLE_FIELD from "@salesforce/schema/DataVariableItem__c.DataVariable__c";
import DATA_VARIABLE_DATE_FIELD from "@salesforce/schema/DataVariableItem__c.Date__c";

import LOCALE from "@salesforce/i18n/locale";

import datatable from "@salesforce/apex/Campaign.datatable";


export default class PerformanceGantt extends NavigationMixin(LightningElement) {
	label = {
		modalHeader,
		statusHeader,
		campaignHeader,
		cancel,
		open,
		btnOpenInMC,
		msgCorrelation,
	};
	@track paramGetRecordDynamic;
	@track taskId;
	@track objectApiName;
	@track fields = [];
	@track header;

	@api recordId;
	@track performanceGanttResult;
	@track dataSetResult = [];
	@track halfCellWidth = 15;
	@track sObject;
	@track startDate;
	@track endDate;
	@track fieldMap;
	@track barTextFields;
	@track wiredData;
	@track campaignManagerSetup;
	@track sObjects;
	@track sObjectList;
	@track barColorMap;
	@track standardIcons = ["Adwords", "Banner", "Event", "Email", "Mobile", "Outdoor", "Postal", "Print", "Radio", "Social", "Sponsor", "Telemarketing", "Tv", "Web", "Other"];
	@track dataSet;
	@track chartLabels = [];
	@track chartWidthStyle = "width:35000";
	@track chartWidth = "35000";
	@track legentStyle = "width:230px;";
	@track yaxisStyle = "margin-left: 160px;";
	@track isChartLoaded = false;
	@track onMousePoint;
	@track chartStyle;
	@track showGantt = false;
	@track showChart = true;
	@track rectangleSet = false;
	@track yAxisHeight = "190";
	@track chartHeight = "190";
	@track dependentVariable = "";
	@track isScatter = false;
	@track independentDataList = [];
	@track dependentData;
	@track dependentDataList = [];
	@track showScatterButton = false;
	@track toDate;
	@track fromDate;
	@track displayGroupBy;
	@track performanceGanttId;

	@track chartType;
	@track isStacked = false;
	@track isFilled = false;
	proccedDataSet;
	tempScratter;
	paramGetRecords;
	seasonableDataItem;
	//Sowmya user story W-000346
	@track sectioncssclass = "slds-modal slds-fade-in-open slds-modal_medium";
	@track fieldsCampaign = [];
	@track isMarketingActivity = false;
	@track isMarketingJourney = false;
	@track taskIdCampaign;
	@track otherobjectapiname;
	@track data = [];
	@track columns = [];
	@track tableLoadingState = true;
	@track hasCampaignTab = false;
	@track hasOpenButton = false;
	@track paramDataTable;
	@track hideCheckboxColumn = true;
	@track financeAllocation = false;
	@track barTextMarketingActivity;
	@track isReload = false;
	@track dynamicRecord;
	@track campaignId;
	@track campaignObj;
	dataSetWiredResult;

	@wire(getRecord, { recordId: "$recordId", layoutTypes: ["Full"], modes: ["View"] })
	wiredGetRecord(result) {
		if (result.data) {
			/*	getPerformanceGantt({recordId: '$recordId'}).then((e) => {
				this.wiredGetPerformanceGantt(e)
			})}*/

			this.performanceGanttId = result.data.id;
			this.handleRefresh();
		}
	}
	@wire(getRecord, { recordId: "$taskId", layoutTypes: ["Full"], modes: ["View"] })
	wiredGetDynamicRecord(result) {
		if (result.data) {
			this.dynamicRecord = result.data;
			if (result.data.fields.musqotmp__Campaign__c) this.campaignId = result.data.fields.musqotmp__Campaign__c.value;
		}
	}
	@wire(getRecord, { recordId: "$campaignId", layoutTypes: ["Full"], modes: ["View"] })
	wiredGetCampaignRecord(result) {
		if (result.data) {
			this.campaignObj = result.data;
		}
	}

	connectedCallBack() {
		eval("$A.get('e.force:refreshView').fire();");
	}
	disconnectedCallback() {
		eval("$A.get('e.force:refreshView').fire();");
	}
	renderedCallback() {
		//eval("$A.get('e.force:refreshView').fire();");
		if (this.isScatter) {
			this.buildScratter();
		}
	}
	getRandomColor() {
		let letters = "0123456789ABCDEF";
		let color = "#";
		for (let i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
	//Start initialize section
	get moment() {
		if (moment) return moment;
		else this.moment;
	}
	initializePgantt() {
		this.momentjsInitialized = true;
		loadScript(this, MOMENT_JS)
			.then(() => {
				this.moment;
				this.ganttPromise();
			})
			.catch((error) => {
				this.momentjsInitialized = false;
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	chartPromise() {
		Promise.all([loadScript(this, chartjs + "/chart.js")])
			.then(() => {
				if (Chart && this.dataSet) {
					Promise.all([loadScript(this, ChartjsPluginAnnotation)]).then(() => {
						this.createChart();
					});
				}
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
					});
	}
	ganttPromise() {
		Promise.all([loadScript(this, Gantt + "/codebase/dhtmlxgantt.js"), loadStyle(this, Gantt + "/codebase/dhtmlxgantt.css")])
			.then(() => {
				this.initializeGantt();
				this.populateGantt();
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	//END intialize section
	buildParamGetRecords() {
		const whereCaluses = [];
		whereCaluses.push({
			field: "musqotmp__Campaign__c",
			value: this.recordId,
			clause: "=",
		});
	}

	//start Wire sections
	//Wire Gantt Data
	@wire(getRecords, { param: "$paramGetRecords" })
	wiredGetRecords(result) {
		if (result.data) {
			this.seasonableDataItem = result.data.sObjects;
			this.initializePgantt();
		}
	}
	@wire(getObjectInfo, { objectApiName: CAMPAIGN_OBJECT })
	campaignInfo;
	@wire(getObjectInfo, { objectApiName: MARKETING_ACTIVITY_OBJECT })
	marketingActivityInfo;
	@wire(getPicklistValues, { recordTypeId: "012000000000000AAA", fieldApiName: STACKED_FIELD })
	stackedFieldInfo(result) {
	//	console.log(result);
	}
	@wire(getPerformanceGantt, { recordId: "$performanceGanttId" })
	wiredGetPerformanceGantt(result) {
		this.wiredData = result;
		if (result.data && result.data.sObjectUnMap) {
			this.performanceGanttResult = result.data.sObjectUnMap;
			this.campaignManagerSetup = result.data.customSetting;
			this.barColorMap = result.data.stringMap;
			this.sObject = result.data.sObj;
			this.sObjects = result.data.sObjects;
			this.sObjectList = result.data.sObjectList;
			this.barTextFields = result.data.fields;
			this.fieldMap = result.data.fieldMap;
			this.showGantt = true;
			this.isChartLoaded = false;
			this.paramGetRecordDynamic = undefined;
			if (this.sObject.musqotmp__DataSetName__c == "Hide Performance") {
				this.showChart = false;
			}
			let whereCaluses = [];
			if (!this.seasonableDataItem && this.sObject.musqotmp__SeasonalVariable__r) {
				whereCaluses.push({
					field: "musqotmp__DataVariable__c",
					value: this.sObject.musqotmp__SeasonalVariable__r.Id,
					clause: "=",
				});
			//	console.log("param---->246");
				if (this.sObject.musqotmp__SeasonalVariable__r.musqotmp__DataVariableType__c == "Season") {
				//	console.log("param---->248");
					this.paramGetRecords = undefined;
					this.paramGetRecords = JSON.stringify({
						component: "performanceGantt-getRecords",
						objectApiName: DATA_VARIABLE_ITEM_OBJECT.objectApiName,
						fieldMap: {
							[DATA_VARIABLE_DATE_FIELD.fieldApiName]: {},
							[DATA_VARIABLE_FIELD.fieldApiName]: {},
						},
						whereCaluses: whereCaluses,
					});
				}
			} else {
			//	console.log("param---->261");

				this.initializePgantt();
			}
		}
		if (result.error) {
			this.template.querySelector("c-toast").showToast(result.error);
		}
	}
	//Wire DataSet
	@wire(getDataSet, { param: "$paramGetRecordDynamic" })
	wiredGetDataSet(result) {
		this.dataSetWiredResult = result;
		if (result.data) {
			this.dependentDataList = [];
			this.independentDataList = [];
			let hsh = {};
			let dataSet = [];
			let chartLabels = [];
			let stackedDataSet = [];
			let lblHsh = {};
			this.dataSetResult = this.dataSetWiredResult.data;
			let sObj = this.dataSetWiredResult.data.sObj;
			let cHsh = {};
			let initToolTip = false;
			let dates = {};
			let prevData;
			let prevLabel;
			let dateKey;
			let prevKey;
			let index = 1;
			let colorIndex = 0;
			let chartColor = "";
			if (this.campaignManagerSetup.musqotmp__BarColorMarketingActivity__c) chartColor = this.campaignManagerSetup.musqotmp__BarColorMarketingActivity__c.split(",");
			let displayGroup = this.sObject.musqotmp__DisplayGroupedBy__c;
			this.displayGroupBy = this.sObject.musqotmp__DisplayGroupedBy__c;
			let momentStartDate = moment(this.startDate);
			let momentEndDate = moment(this.endDate);
			let data;
			let legendTooltipHsh = {};

			switch (this.sObject.musqotmp__DependentChartType__c) {
				case "Bar":
					this.chartType = "bar";
					this.isStacked = false;
					this.isFilled = false;
					break;
				case "Bar, Stacked":
					this.chartType = "bar";
					this.isStacked = true;
					this.isFilled = false;
					break;
				case "Area, Stacked":
					this.chartType = "line";
					this.isStacked = true;
					this.isFilled = true;
					break;
				case "Area":
					this.chartType = "line";
					this.isStacked = false;
					this.isFilled = true;
					break;
				default:
					this.chartType = "line";
					this.isStacked = false;
					this.isFilled = false;
					break;
			}
			if (this.dataSetWiredResult && this.dataSetWiredResult.data && this.dataSetWiredResult.data.sObjects.length > 0) {
				for (let sd = 0; sd < this.dataSetWiredResult.data.sObjects.length; sd++) {
					data = result.data.sObjects[sd];
					let barColors = "";
					if (data.musqotmp__DataVariable__r.musqotmp__BarColor__c) barColors = data.musqotmp__DataVariable__r.musqotmp__BarColor__c.split(",");
					let StackedField = data[sObj.musqotmp__StackedField__c];
					let momentDate = moment(data.musqotmp__Date__c);
					//We are not calculating or showing the future data if this is not forcast
					let datasetHsh = {};
					let hshKey = data.musqotmp__DataVariable__r.Name;

					if (!this.sObject.musqotmp__IncludeForecast__c && moment(new Date()).add(1, "D").format("D-M-Y") == momentDate.format("D-M-Y")) {
						break;
					}
					switch (data.musqotmp__DataVariable__r.musqotmp__DataVariableType__c) {
						case "Spend Amount":
							datasetHsh.datasetValue = parseFloat(data.musqotmp__ValueCurrency__c);
							datasetHsh.order = 1;
							break;
						case "Revenue":
							datasetHsh.datasetValue = parseFloat(data.musqotmp__ValueCurrency__c);
							datasetHsh.order = 1;
							break;
						default:
							datasetHsh.datasetValue = parseFloat(data.musqotmp__Value__c);
							datasetHsh.order = 2;
							break;
					}
					if (displayGroup == "Weekly") {
						dateKey = momentDate.format("Y ");
						dateKey += "W" + momentDate.format("W");
					} else if (displayGroup == "Monthly") {
						dateKey = momentDate.format("Y MMM");
					} else dateKey = momentDate.format("Y MMM D");

					if (dates[dateKey] == undefined) {
						dates[dateKey] = hshKey;
						chartLabels.push(dateKey);
					}
					//building data for each dataset
					if (hsh[hshKey]) {
						if (hsh[hshKey]["dateKey"][dateKey]) {
							hsh[hshKey].data[hsh[hshKey].data.length - 1] += datasetHsh.datasetValue;
							//hsh[hshKey].data[hsh[hshKey].data.length - 1] = parseFloat(parseFloat(hsh[hshKey].data[hsh[hshKey].data.length - 1]).toFixed(2));
							hsh[hshKey]["dataMap"][dateKey] = hsh[hshKey].data[hsh[hshKey].data.length - 1];
						} else {
							hsh[hshKey]["dateKey"][dateKey] = {};
							hsh[hshKey].data.push(datasetHsh.datasetValue);
							hsh[hshKey]["dataMap"][dateKey] = datasetHsh.datasetValue;
						}
						if (parseFloat(hsh[hshKey].data[hsh[hshKey].data.length - 1]) > parseFloat(hsh[hshKey].legendTooltipHsh.max))
							hsh[hshKey].legendTooltipHsh.max = parseFloat(hsh[hshKey].data[hsh[hshKey].data.length - 1]);
						if (datasetHsh.datasetValue != 0 && parseFloat(hsh[hshKey].data[hsh[hshKey].data.length - 1]) < parseFloat(hsh[hshKey].legendTooltipHsh.min))
							hsh[hshKey].legendTooltipHsh.min = parseFloat(hsh[hshKey].data[hsh[hshKey].data.length - 1]);

						if (hsh[hshKey][StackedField]) {
							if (hsh[hshKey][StackedField].legendTooltipHsh) {
								if (parseFloat(hsh[hshKey][StackedField].data[hsh[hshKey][StackedField].data.length - 1]) > parseFloat(hsh[hshKey][StackedField].legendTooltipHsh.max))
									hsh[hshKey][StackedField].legendTooltipHsh.max = parseFloat(hsh[hshKey][StackedField].data[hsh[hshKey][StackedField].data.length - 1]);
								if (parseFloat(hsh[hshKey][StackedField].data[hsh[hshKey][StackedField].data.length - 1]) < parseFloat(hsh[hshKey][StackedField].legendTooltipHsh.min))
									hsh[hshKey][StackedField].legendTooltipHsh.min = parseFloat(hsh[hshKey][StackedField].data[hsh[hshKey][StackedField].data.length - 1]);
							}
							let dValue;
							if (hsh[hshKey][StackedField][dateKey])
								hsh[hshKey][StackedField].data[hsh[hshKey][StackedField].data.length - 1] = hsh[hshKey][StackedField].data[hsh[hshKey][StackedField].data.length - 1] + datasetHsh.datasetValue;
							else {
								hsh[hshKey][StackedField][dateKey] = datasetHsh.datasetValue;
								dValue = datasetHsh.datasetValue;
								hsh[hshKey][StackedField].data.push(dValue);
							}
						} else {
							if (this.isStacked && data.musqotmp__DataVariable__r.Id == this.sObject.musqotmp__DependentData__c) {
								colorIndex += 1;
								let dataSetHsh = {
									label: StackedField,
									unit: hsh[hshKey].unit,
									data: [datasetHsh.datasetValue],
									yAxisID: "y-axis-1",
									fill: this.isFilled,
									type: this.chartType,
									dependentVariable: hshKey,
									legendTooltipHsh: {
										max: datasetHsh.datasetValue,
										min: datasetHsh.datasetValue,
										unit: data.musqotmp__DataVariable__r.musqotmp__Unit__c,
									},
									dataVariableId: hsh[hshKey].dataVariableId,
									backgroundColor: barColors ? barColors[colorIndex] : chartColor[Object.keys(hsh).length - 1],
									borderColor: barColors ? barColors[colorIndex] : chartColor[Object.keys(hsh).length - 1],
									/*
								backgroundColor: chartColor[Object.keys(hsh).length - 1],
								borderColor: chartColor[Object.keys(hsh).length - 1]*/
								};
								hsh[hshKey][StackedField] = dataSetHsh;
								hsh[hshKey][StackedField][dateKey] = datasetHsh.datasetValue;
								stackedDataSet.push(dataSetHsh);
							}
						}
					} else {
						colorIndex = 0;
						hsh[hshKey] = { label: "", backgroundColor: "", borderColor: "", fill: false, data: [], legendTooltipHsh: {}, dataMap: {}, dateKey: {} };
						hsh[hshKey]["dateKey"][dateKey] = {};
						hsh[hshKey].label = hshKey;
						hsh[hshKey].backgroundColor = barColors[0];
						hsh[hshKey].borderColor = barColors[0];
						hsh[hshKey].legendTooltipHsh.max = datasetHsh.datasetValue;
						hsh[hshKey].legendTooltipHsh.min = datasetHsh.datasetValue;
						hsh[hshKey].unit = data.musqotmp__DataVariable__r.musqotmp__Unit__c;
						hsh[hshKey].legendTooltipHsh.unit = data.musqotmp__DataVariable__r.musqotmp__Unit__c;
						hsh[hshKey].date = moment(dateKey);
						hsh[hshKey].borderDashs = [[], [], [10, 10], [20, 5], [15, 3, 3, 3]];
						//hsh[hshKey][dateKey] = datasetHsh.datasetValue;
						hsh[hshKey]["dataMap"][dateKey] = datasetHsh.datasetValue;
						hsh[hshKey].data.push(datasetHsh.datasetValue);
						hsh[hshKey].type = this.chartType; //this.sObject.musqotmp__DependentChartType__c.toLowerCase();
						hsh[hshKey].fill = this.isFilled;
						hsh[hshKey].externalId = data.musqotmp__DataVariable__r.musqotmp__ExternalId__c;
						if (data.musqotmp__DataVariable__r.Id == this.sObject.musqotmp__DependentData__c) {
							hsh[hshKey].mainDependentData = true;
							hsh[hshKey].dataVariableId = data.musqotmp__DataVariable__r.Id;
							if (this.isStacked) {
								let dataSetHsh = {
									label: StackedField,
									unit: hsh[hshKey].unit,
									data: [datasetHsh.datasetValue],
									yAxisID: "y-axis-1",
									fill: this.isFilled,
									type: this.chartType,
									legendTooltipHsh: {
										max: datasetHsh.datasetValue,
										min: datasetHsh.datasetValue,
										unit: data.musqotmp__DataVariable__r.musqotmp__Unit__c,
									},
									dependentVariable: hshKey,
									dataVariableId: hsh[hshKey].dataVariableId,
									backgroundColor: barColors ? barColors[colorIndex] : chartColor[Object.keys(hsh).length - 1],
									borderColor: barColors ? barColors[colorIndex] : chartColor[Object.keys(hsh).length - 1],
								};
								hsh[hshKey][StackedField] = dataSetHsh;
								hsh[hshKey][StackedField][dateKey] = datasetHsh.datasetValue;
								stackedDataSet.push(dataSetHsh);
							} else {
								hsh[hshKey].borderWidth = 1;
								this.dependentVariable = hshKey;
								hsh[hshKey].yAxisID = "y-axis-1";
								if (this.sObject.musqotmp__DependentChartType__c.toLowerCase() == "line") hsh[hshKey].type = "multicolorLine";
								//hsh[hshKey].backgroundColor ='rgba(255, 206, 86, 0.2)';
							}
						} else {
							hsh[hshKey].order = datasetHsh.order;
							hsh[hshKey].borderWidth = 1;
							hsh[hshKey].yAxisID = "y-axis-2";
							hsh[hshKey].type = "multicolorLine";
							hsh[hshKey].fill = false;
						}
						index += 1;
					}
					if (momentDate.diff(momentStartDate, "days") > 1) {
						let prevStackData;
						prevData = hsh[hshKey].data.pop();
						if (hsh[hshKey][StackedField] && hsh[hshKey][StackedField].data && data.musqotmp__DataVariable__r.Id == this.sObject.musqotmp__DependentData__c) {
							prevStackData = hsh[hshKey][StackedField].data.pop();
						}
						prevLabel = chartLabels.pop();
						while (momentStartDate < momentDate) {
							if (displayGroup == "Weekly") {
								dateKey = momentStartDate.format("Y ");
								dateKey += "W" + momentStartDate.format("W");
								momentStartDate.add(1, "W").startOf("W");
							} else if (displayGroup == "Monthly") {
								dateKey = momentStartDate.format("Y MMM");
								momentStartDate.add(1, "M").startOf("M");
							} else {
								dateKey = momentStartDate.format("Y MMM D");
								momentStartDate.add(1, "days");
							}
							chartLabels.push(dateKey);
							hsh[hshKey].data.push(null);
							if (hsh[hshKey][StackedField] == undefined) hsh[hshKey][StackedField] = { data: [] };
							hsh[hshKey][StackedField].data.push(null);
							hsh[hshKey][StackedField][dateKey] = null;
						}
						chartLabels.push(prevLabel);
						hsh[hshKey].data.push(prevData);
						hsh[hshKey][StackedField][dateKey] = prevStackData;
						hsh[hshKey][StackedField].data.push(prevStackData);
					}
					momentStartDate = momentDate;
				}
			}
			//If there is a no data for futuredate
			if (momentEndDate.diff(momentStartDate, "days") > 0) {
				momentStartDate.add(1, "days");
				while (momentStartDate <= momentEndDate) {
					if (displayGroup == "Weekly") {
						dateKey = momentStartDate.format("Y ");
						dateKey += "W" + momentStartDate.format("W");
						momentStartDate.add(1, "W").startOf("W");
					} else if (displayGroup == "Monthly") {
						dateKey = momentStartDate.format("Y MMM");
						momentStartDate.add(1, "M").startOf("M");
					} else {
						dateKey = momentStartDate.format("Y MMM D");
						momentStartDate.add(1, "days");
					}
					chartLabels.push(dateKey);
				}
			}
			stackedDataSet.forEach((element) => {
				hsh[element.label] = element;
			});
			for (let i in hsh) {
				if (hsh[i].dataVariableId == this.sObject.musqotmp__DependentData__c) {
					var sumOfValue = 0;
					var _n = 0;
					hsh[i].data.map((ele) => {
						if (ele) {
							sumOfValue += parseFloat(ele);
							_n += 1;
						}
					});
					var mean = sumOfValue / _n;
					var sd = Math.sqrt(hsh[i].data.map((x) => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / _n);
					hsh[i].legendTooltipHsh.n = _n;
					hsh[i].legendTooltipHsh.mean = mean;
					hsh[i].legendTooltipHsh.sd = sd;
					if (hsh[i].mainDependentData) {
						this.dependentData = hsh[i];
						if (!this.isStacked) {
							this.dependentDataList.push(hsh[i]);
							dataSet.push(hsh[i]);
						} else {
							let tempDependant = Object.assign(this.dependentData);
							tempDependant.data = [null];
							dataSet.push(tempDependant);
							this.dependentDataList.push(tempDependant);
						}
					} else {
						this.dependentDataList.push(hsh[i]);
						dataSet.push(hsh[i]);
					}
				} else {
					dataSet.unshift(hsh[i]);
					this.independentDataList.push(hsh[i]);
				}
			}
			for (let index in this.independentDataList) {
				let sum_X = 0,
					sum_Y = 0,
					sum_XY = 0,
					squareSum_X = 0,
					squareSum_Y = 0,
					n = 0;
				let scatterDataSetMap = { data: [] };
				let dependentDataMap = this.dependentData.dataMap;
				let independentDataMap = this.independentDataList[index].dataMap;
				let tData = this.independentDataList[index].data;
				scatterDataSetMap.data = [];
				scatterDataSetMap.unit = this.independentDataList[index].unit;
				scatterDataSetMap.dUnit = this.dependentData.unit;
				scatterDataSetMap.label = this.independentDataList[index].label;
				scatterDataSetMap.backgroundColor = this.independentDataList[index].backgroundColor;
				scatterDataSetMap.borderColor = this.independentDataList[index].borderColor;

				Object.keys(independentDataMap).forEach(function (key) {
					let i = key;
					let X = dependentDataMap;
					let Y = independentDataMap;
					if (X[i] && Y[i]) {
						// sum of elements of array X.
						sum_X = sum_X + X[i];

						// sum of elements of array Y.
						sum_Y = sum_Y + Y[i];

						// sum of X[i] * Y[i].
						sum_XY = sum_XY + X[i] * Y[i];

						// sum of square of array elements.
						squareSum_X = squareSum_X + X[i] * X[i];
						squareSum_Y = squareSum_Y + Y[i] * Y[i];
						n += 1;
					}
				});
				let corr = parseFloat(n * sum_XY - sum_X * sum_Y) / Math.sqrt((n * squareSum_X - sum_X * sum_X) * (n * squareSum_Y - sum_Y * sum_Y));
				var mean = sum_Y / n;
				var sd = Math.sqrt(tData.map((x) => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n);
				if (Object.keys(hsh).length != 0) {
					hsh[this.independentDataList[index].label].legendTooltipHsh.n = n;
					hsh[this.independentDataList[index].label].legendTooltipHsh.mean = mean;
					hsh[this.independentDataList[index].label].legendTooltipHsh.sd = sd;
					this.independentDataList[index].legendTooltipHsh.corr = corr.toFixed(3);
					hsh[this.independentDataList[index].label].legendTooltipHsh.corr = corr.toFixed(3);
				}
			}

			this.proccedDataSet = hsh;
			this.dataSet = dataSet;
			this.chartLabels = chartLabels;
			//Indenting the date based on data count.
			let cellWidth;
			if (this.template.querySelector(".gantt_task_cell")) cellWidth = this.template.querySelector(".gantt_task_cell").offsetWidth;
			let ganttScale;
			if (this.template.querySelector(".gantt_task_scale")) ganttScale = this.template.querySelector(".gantt_task_scale").offsetWidth;
			let totalWidth = this.chartLabels.length * cellWidth;
			if (this.chartLabels.length >= 24) {
				this.chartWidth = totalWidth + 2 * cellWidth;
			} else {
				if (this.chartLabels.length <= 16 && this.chartLabels.length >= 12) this.chartWidth = totalWidth + cellWidth;
				else if (this.chartLabels.length <= 6) this.chartWidth = totalWidth;
				else this.chartWidth = totalWidth + cellWidth + cellWidth / 2;
			}
			if (this.chartType == "bar") {
				this.chartWidth += cellWidth;
			}
			this.chartLabels.push("");
			this.halfCellWidth = cellWidth / 2;
			this.chartWidthStyle = "width:" + this.chartWidth;
			this.chartStyle = "background-color: white; max-width:" + this.chartWidth + "px;";
			this.chartPromise();
		}
		if (result.error) {
			this.template.querySelector("c-toast").showToast(result.error);
		}
	}

	//END wire section
	initializeGantt() {
		const thislwc = this;
		const mom = this.moment();
		if (this.sObject) {
			if (this.sObject.musqotmp__DateRange__c === "THIS_QUARTER") {
				this.startDate = this.moment().startOf("Q").toDate();
				this.endDate = this.moment().endOf("Q").toDate();
			} else if (this.sObject.musqotmp__DateRange__c === "NEXT_QUARTER") {
				this.startDate = this.moment().add(1, "Q").startOf("Q").toDate();
				this.endDate = this.moment().endOf("Q").toDate();
			} else if (this.sObject.musqotmp__DateRange__c === "THIS_QUARTER,NEXT_QUARTER") {
				this.startDate = this.moment().startOf("Q").toDate();
				this.endDate = this.moment().add(1, "Q").endOf("Q").toDate();
			} else if (this.sObject.musqotmp__DateRange__c === "THIS_QUARTER,LAST_QUARTER") {
				this.startDate = this.moment().subtract(1, "Q").startOf("Q").toDate();
				this.moment().startOf("Q").toDate();
				this.endDate = this.moment().endOf("Q").toDate();
			//	console.log(this.startDate);
			//	console.log(this.endDate);
			} else if (this.sObject.musqotmp__DateRange__c === "THIS_YEAR") {
				this.startDate = this.moment().startOf("Y").toDate();
				this.endDate = this.moment().endOf("Y").toDate();
			} else if (this.sObject.musqotmp__DateRange__c === "NEXT_YEAR") {
				this.startDate = this.moment().add(1, "Y").startOf("Y").toDate();
				this.endDate = this.moment().endOf("Y").toDate();
			} else if (this.sObject.musqotmp__DateRange__c === "THIS_YEAR,NEXT_YEAR") {
				this.startDate = this.moment().startOf("Y").toDate();
				this.endDate = this.moment().add(1, "Y").endOf("Y").toDate();
			} else if (this.sObject.musqotmp__DateRange__c === "THIS_YEAR,LAST_YEAR") {
				this.startDate = this.moment().subtract(1, "Y").startOf("Y").toDate();
				this.endDate = this.moment().endOf("Y").toDate();
			} else {
				this.startDate = this.moment().startOf("Q").toDate();
				this.endDate = this.moment().endOf("Q").toDate();
			}
			if (this.showChart) {
				//this.refreshData();
				this.dataSetWiredResult = undefined;
				this.paramGetRecordDynamic = JSON.stringify({
					component: "itemsToApproveFinanceChart-getRecordDynamic",
					recordId: this.performanceGanttId,
					reload: (this.isReload = !this.isReload),
					amountOfRecords: 40000,
				});
			}
		}
		if (gantt) {
			var holidays = [];
			/*	var style = document.createElement('style');
			style.type = 'text/css';
			style.innerHTML = '.cssClass { color: red; }';
      this.template.appendChild(style);*/
			gantt.config.work_time = true;
			if (this.seasonableDataItem) {
				for (var i = 0; i < this.seasonableDataItem.length; i++) {
					var converted_date = moment(this.seasonableDataItem[i].musqotmp__Date__c).format("DD-MM-YYYY");
					holidays.push(converted_date);
				}
			}

			gantt.templates.scale_cell_class = function (date) {
				if (!gantt.isWorkTime(date)) return "weekend";
			};
			if (this.sObject.musqotmp__SeasonalVariable__r) {
				let barColor = this.sObject.musqotmp__SeasonalVariable__r.musqotmp__BarColor__c ? this.sObject.musqotmp__SeasonalVariable__r.musqotmp__BarColor__c.split(",")[0] : "#9E9E9E";
				/*const style = document.createElement("style");
				style.type = "text/css";
				style.innerText =
					`.customBackground {
            background-color:` +
					barColor +
					`}`;*/
				document.getElementsByTagName("head")[0].style.innerText = `.customBackground {
					background-color:` +
							barColor +
							`}`;//appendChild(style);
				gantt.templates.timeline_cell_class = function (item, date) {
					if (barColor && holidays.includes(moment(date).format("DD-MM-YYYY"))) {
						return "customBackground";
					} else if (!gantt.isWorkTime({ date: date, task: item })) return "weekend";
				};
			}
			gantt.templates.grid_folder = function (item) {
				return "<div class='gantt_tree_icon gantt_" + item.folder_type + "'></div>";
			};

			gantt.templates.grid_file = function (item) {
				return "<div class='gantt_tree_icon gantt_" + item.folder_type + "'></div>";
			};
			gantt.templates.task_text = function (start, end, task) {
				let iconUrl = "";
				if (thislwc.standardIcons.includes(task.task_type)) {
					iconUrl = "/resource/musqotmp__Icon_" + task.task_type;
				} else {
					iconUrl = "/resource/Icon_" + task.task_type;
				}
				if (task.task_type) {
					return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="16" height="16" /> <b>' + task.barCustomText + "</b>";
				}
				return task.bartext;
			};
			/*	gantt.showLightbox = function(id) {
				if (id !== null) {
					let taskObj = gantt.getTask(id);
					thislwc.taskId = id;
					thislwc.objectApiName = taskObj.objectapiname;
					thislwc.fields = thislwc.fieldMap[taskObj.objectapiname];
					thislwc.header = taskObj.text;
					const modal = thislwc.template.querySelector('c-modal');
					modal.show();
					//document.cookie = 'taskGanttParentId=' + id;
				}
			};*/
			gantt.showLightbox = function (id) {
				if (id !== null) {
					thislwc.handleClose;
					let taskObj = gantt.getTask(id);
					thislwc.taskId = id;
					thislwc.objectApiName = taskObj.objectapiname;
					/*if(thislwc.objectApiName === 'musqotmp__MarketingPlan__c'){
						thislwc.isPlan = true;	thislwc.isCampaign = false;	thislwc.isMarketingJourney = false;
						thislwc.isMarketingActivity = false;
					}else if(thislwc.objectApiName === 'Campaign'){
						thislwc.isPlan = false;thislwc.isCampaign = true;thislwc.isMarketingJourney = false;
						thislwc.isMarketingActivity = false;
						thislwc.fieldsAllocation = thislwc.fieldMap['musqotmp__Allocation__c'];
						thislwc.taskIdCampaign = taskObj.id;
						if(thislwc.fieldsAllocation){
						  thislwc.paramDataTable = thislwc.buildParamDataTable();
						  thislwc.financeAllocation = true;
						}
					}else*/ if (
						thislwc.objectApiName === "musqotmp__MarketingActivity__c"
					) {
						thislwc.hasOpenButton = true;
						thislwc.hasCampaignTab = false;
						thislwc.isMarketingActivity = true;
						thislwc.isPlan = false;
						thislwc.isCampaign = false;
						thislwc.isMarketingJourney = false;
						thislwc.otherobjectapiname = "Campaign";
						thislwc.taskIdCampaign = taskObj.parentid;
						thislwc.fieldMap["Campaign"].forEach((element) => {
							let field = Object.assign({}, element);
							if (field.type == "reference") {
								field["isReference"] = true;
							}
							thislwc.fieldsCampaign.push(field);
						});
						thislwc.fieldsAllocation = thislwc.fieldMap["musqotmp__Allocation__c"];
						if (thislwc.fieldsAllocation) {
							thislwc.paramDataTable = thislwc.buildParamDataTable();
							thislwc.financeAllocation = true;
						}
					} else if (thislwc.objectApiName === "musqotmp__MarketingJourney__c") {
						thislwc.isMarketingJourney = true;
						thislwc.isMarketingActivity = false;
						thislwc.isPlan = false;
						thislwc.isCampaign = false;
						thislwc.otherobjectapiname = "Campaign";
						thislwc.taskIdCampaign = taskObj.parent;
						thislwc.fieldsCampaign = thislwc.fieldMap["Campaign"];
						thislwc.fieldsAllocation = thislwc.fieldMap["musqotmp__Allocation__c"];
						if (thislwc.fieldsAllocation) {
							thislwc.paramDataTable = thislwc.buildParamDataTable();
							thislwc.financeAllocation = true;
						}
					}
					thislwc.fields = [];
					thislwc.fieldMap[taskObj.objectapiname].forEach((element) => {
						let field = Object.assign({}, element);
						if (field.type == "reference") {
							field["isReference"] = true;
						}
						thislwc.fields.push(field);
					});

					thislwc.header = taskObj.text;
					const modal = thislwc.template.querySelector("c-modal");
					modal.show();
					document.cookie = "taskGanttParentId=" + id;
				}
			};

			let zoomConfig = {
				levels: [
					{
						name: "day",
						scale_height: 30,
						min_column_width: 30,
						scales: [
							{
								unit: "month",
								step: 1,
								format: function (date) {
									var dateToStr = gantt.date.date_to_str("%M %Y");
									return dateToStr(date);
								},
							},
							{ unit: "day", step: 1, format: "%d" },
						],
					},
					{
						name: "week",
						scale_height: 30,
						min_column_width: 30,
						scales: [
							{
								unit: "month",
								step: 1,
								format: function (date) {
									var dateToStr = gantt.date.date_to_str("%M %Y");
									return dateToStr(date);
								},
							},
							{ unit: "week", format: "%W", width: 30 },
						],
					},
					{
						name: "month",
						scale_height: 30,
						min_column_width: 30,
						scales: [
							{ unit: "year", format: "%Y" },
							{ unit: "month", format: "%m" },
						],
					},
				],
			};
			gantt.ext.zoom.init(zoomConfig);
			//gantt.ext.zoom.setLevel("month");
			gantt.config.row_height = 25;
			gantt.config.grid_resize = true;
			gantt.config.start_date = this.startDate;
			gantt.config.end_date = this.endDate;
			gantt.config.columns = [
				{
					name: "text",
					label: "Marketing Activities",
					tree: false,
					resize: false,
					width: 180,
				},
				{
					name: "status",
					label: "Status",
					width: 50,
					template: function (obj) {
						return "<div>" + obj.status_icon + "</div>";
					},
					align: "center",
				},
			];
			//Populate Gantt Data
			/*switch (this.sObject.musqotmp__BarText__c) {
				case 'musqotmp__Audience__c':
					break;
				case 'musqotmp__ProductGroup__c':
					break;
				case 'musqotmp__BusinessUnit__c':
					break;
				case 'musqotmp__Region__c':
					break;
				case 'musqotmp__Country__c':
					break;
				case 'musqotmp__City__c':
					break;
				case 'musqotmp__Shop__c':
					break;

				default:
					break;
			}*/
			gantt.ext.zoom.init(zoomConfig);
			window.onresize = function () {
				gantt.render();
			};
		}
	}
	populateGantt() {
		let bartextField = "Name";
		const thislwc = this;
		let tasks = {
			data: [],
			links: [],
		};
		if (this.performanceGanttResult) {
			this.performanceGanttResult.musqotmp__MarketingActivity__c.forEach((pObj) => {
				let task = {};
				let barCustomText = "";
				task["id"] = pObj.Id;
				task["objectapiname"] = "musqotmp__MarketingActivity__c";
				task["folder_type"] = "marketingactivity";
				bartextField = this.sObject.musqotmp__BarText__c;
				task["barCustomText"] = pObj["" + bartextField + ""] ? pObj["" + bartextField + ""].split(";").join(", ") : "";
				switch (bartextField) {
					case "musqotmp__Region__c":
						bartextField = "musqotmp__MarketingArea__c";
						task["barCustomText"] = pObj["" + bartextField + ""].split(";").join(", ");
						break;
					case "OwnerID":
						task["barCustomText"] = "" + pObj.CreatedBy.Name + "";
						break;
					default:
						break;
				}
				task["task_type"] = pObj.musqotmp__Type__c;
				task["text"] = pObj.Name;
				let barText = [];
				barText.push(pObj.Name);
				task["bartext"] = barText.join(" ");
				if (this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + pObj.musqotmp__Type__c]) {
					task["color"] = this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + pObj.musqotmp__Type__c];
				}
				task["status"] = pObj.musqotmp__Status__c;
				task["status_icon"] = pObj.musqotmp__StatusIcon__c;
				task["progress"] = 1;
				task["parentid"] = pObj.musqotmp__Campaign__c;
				task["start_date"] = this.moment(pObj.musqotmp__StartDate__c).format("DD-MM-YYYY");
				task["duration"] = Math.round((new Date(pObj.musqotmp__EndDate__c) - new Date(pObj.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
				tasks.data.push(task);
			});
			this.performanceGanttResult.musqotmp__MarketingJourney__c.forEach((pObj) => {
				let task = {};
				task["id"] = pObj.Id;
				task["objectapiname"] = "musqotmp__MarketingJourney__c";
				task["folder_type"] = "marketingjourney";
				bartextField = this.sObject.musqotmp__BarText__c;
				task["barCustomText"] = pObj["" + bartextField + ""] ? pObj["" + bartextField + ""].split(";").join(", ") : "";
				//task['task_type'] = pObj.musqotmp__Type__c;
				task["text"] = pObj.Name;
				let barText = [];
				barText.push(pObj.Name);
				task["bartext"] = barText.join(" ");
				/*if (this.barColorMap['musqotmp__MarketingJourney__c~~musqotmp__Type__c~~' + pObj.musqotmp__Type__c]) {
					task['color'] = this.barColorMap['musqotmp__MarketingJourney__c~~musqotmp__Type__c~~' + pObj.musqotmp__Type__c];
				}*/
				task["status"] = pObj.musqotmp__Status__c;
				task["status_icon"] = pObj.musqotmp__StatusIcon__c;
				task["progress"] = 1;
				task["parentid"] = pObj.musqotmp__Campaign__c;
				task["start_date"] = this.moment(pObj.musqotmp__StartDate__c).format("DD-MM-YYYY");
				task["duration"] = Math.round((new Date(pObj.musqotmp__EndDate__c) - new Date(pObj.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
				tasks.data.push(task);
			});
		}
		
		//gantt.config.readonly = false;
		//gantt.config.drag_resize = false;
		gantt.config.drag_move = false;
		gantt.config.drag_resize = false;
		gantt.config.drag_progress = false;
		thislwc.showGantt = true;
		gantt.init(thislwc.template.querySelector("div.gantt"), this.startDate, this.endDate);
		gantt.parse(tasks);
		switch (this.sObject && this.sObject.musqotmp__DisplayGroupedBy__c) {
			case "Weekly":
				gantt.ext.zoom.setLevel("week");
				break;
			case "Monthly":
				gantt.ext.zoom.setLevel("month");
				break;
			default:
				gantt.ext.zoom.setLevel("day");
				break;
		}
		//gantt.sort("start_date", false);
		//Scroll chart on scroll of gantt
		if (thislwc.showChart) {
			gantt.scrollTo(0, null);
			gantt.attachEvent("onGanttScroll", function (left, top) {
				const ganttDiv = thislwc.template.querySelector(".chartAreaWrapper");
				ganttDiv.scrollLeft = left;
			});
		}
	}
	createChart() {
		const tmThis = this;
		window.tempThis = this;
		const convasCnt = this.template.querySelector(".performance-chart");

		while (convasCnt.firstChild) {
			convasCnt.removeChild(convasCnt.firstChild);
		}
		if (window.lineChart != undefined) {
			window.lineChart.destroy();
		}

		let hovering = false;
		let lineType = "line";
		if (this.sObject.musqotmp__IncludeForecast__c) {
			lineType = "multicolorLine";
		}
		window.legendTooltip = this.template.querySelector(".legend-tooltip");
		const ctx = this.template.querySelector(".performance-chart").getContext("2d");
		//this.canvasBackgroundColor = 'background-color: white;';

		window.rectangleSet = false;
		ctx.canvas.width = this.chartWidth; //Set Width of the canvas
		if (Chart && this.dataSet && !this.isChartLoaded) {
			this.isChartLoaded = true;
			var formatedToday;
			var momentDate = moment(new Date());
			Chart.defaults.multicolorLine = Chart.defaults.line;
			Chart.controllers.multicolorLine = Chart.controllers.line.extend({
				draw: function (ease) {
					var startIndex = 0,
						meta = this.getMeta(),
						points = meta.data || [],
						borderDashs = this.getDataset().borderDashs,
						area = this.chart.chartArea,
						originalDatasets = meta.dataset._children.filter(function (data) {
							return !isNaN(data._view.y);
						});

					function _setDashed(borderDash, meta) {
						meta.dataset._view.borderDash = borderDash;
					}

					if (tmThis.sObject.musqotmp__DisplayGroupedBy__c == "Weekly") {
						formatedToday = momentDate.format("Y ") + "W" + momentDate.format("W");
					} else if (tmThis.sObject.musqotmp__DisplayGroupedBy__c == "Monthly") {
						formatedToday = momentDate.format("Y MMM");
					} else formatedToday = momentDate.format("Y MMM D");

					let todayIndex = meta.dataset._chart.tooltip._data.labels.indexOf(formatedToday);
					let labelLength = meta.dataset._chart.tooltip._data.labels.length;

					_setDashed(borderDashs[0], meta);
					meta.dataset._children = originalDatasets.slice(0, todayIndex);
					meta.dataset.draw();
					_setDashed([5, 5], meta);
					meta.dataset._children = originalDatasets.slice(todayIndex, labelLength);
					meta.dataset.draw();
					meta.dataset._children = originalDatasets;

					points.slice(0, todayIndex).forEach(function (point) {
						point.draw(area);
					});
				},
			});

			window.lineChart = new window.Chart(ctx, {
				type: "bar", //this.sObject.musqotmp__DependentChartType__c.toLowerCase(),
				plugins: [
					{
						afterDraw: (chartt) => {
						//	console.log("chart ====>" + chartt);
						},
					},
				],
				data: {
					labels: this.chartLabels,
					datasets: this.dataSet,
				},
				scaleOverride: true,
				scaleSteps: 10,
				scaleStepWidth: 70,
				scaleStartValue: 0,

				options: {
					legend: {
						position: "left",
						display: false,
					},
					legendCallback: function (chart) {
						var lgnt = [];
						var independentLi = [];
						var dependentLi = [];
						lgnt.push('<div class="custom-chart-legent-cnt">');
						independentLi.push("<h2><b>Independent Variable</b></h2>");
						dependentLi.push("<h2><b>Dependent Variable</b></h2>");
						independentLi.push('<ul class="' + chart.id + '-legend">');
						dependentLi.push('<ul class="' + chart.id + '-legend" >');
						for (var i = 0; i < chart.data.datasets.length; i++) {
							var text = "";
							if (chart.data.datasets[i].label) {
								if (tmThis.sObject.musqotmp__DependentData__c == chart.data.datasets[i].dataVariableId && !chart.data.datasets[i].mainDependentData) {
									text += '<li class="margin-left-10px"><span style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>';
								} else text += '<li><span style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>';
								text += chart.data.datasets[i].label;
								text += "</li>";
							}
							if (tmThis.sObject.musqotmp__DependentData__c == chart.data.datasets[i].dataVariableId) {
								dependentLi.push(text);
							} else {
								independentLi.push(text);
							}
						}
						dependentLi.push("</ul>");
						independentLi.push("</ul>");
						lgnt.push(dependentLi.join(""));
						lgnt.push(independentLi.join(""));

						lgnt.push("</div>");
						return lgnt.join("");
					},
					responsive: false,
					scaleShowValues: true,
					scales: {
						yAxes: [
							{
								type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
								display: true,
								position: "left",
								stacked: this.isStacked,
								id: "y-axis-1",
								ticks: {
									display: false,
								},
							},
							{
								type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
								display: true,
								position: "right",
								id: "y-axis-2",

								// grid line settings
								gridLines: {
									drawOnChartArea: false, // only want the grid lines for one axis to show up
								},
							},
						],
						xAxes: [
							{
								stacked: this.isStacked,
								ticks: {
									autoSkip: false,
									display: false,
								},
							},
						],
					},

					onHover: {
						animationDuration: 0,
					},
					tooltips: {
						callbacks: {
							label: function (tooltipItem, data) {
								var label = data.datasets[tooltipItem.datasetIndex].label || "";
								/*
								if (label) {
									label += ': ';
								}
								label += (Math.round(tooltipItem.yLabel * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');*/
								return [label];
							},
							afterLabel: function (tooltipItem, data) {
								return (Math.round(tooltipItem.yLabel * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " " + data.datasets[tooltipItem.datasetIndex].unit;
							},
						},
					},
					/*
					animation: {
						onComplete: function() {
							if (!window.rectangleSet) {
								tmThis.customYaxis(window.lineChart);
								window.rectangleSet = true;
							}
						},
						onProgress: function() {
							if (rectangleSet === true) {
								let copyWidth = lineChart.scales['y-axis-0'].width;
								let copyHeight = lineChart.scales['y-axis-0'].height + lineChart.scales['y-axis-0'].top + 10;
								let sourceCtx = lineChart.chart.canvas.getContext('2d');
								sourceCtx.clearRect(0, 0, copyWidth, copyHeight);
							}
						}
					}*/
				},
			});
			const targetCtx = this.template.querySelector(".chart-legend");
			while (targetCtx.firstChild) {
				targetCtx.removeChild(targetCtx.firstChild);
			}
			targetCtx.innerHTML = lineChart.generateLegend();
			let legendItems = targetCtx.getElementsByTagName("li");
			for (let i = 0; i < legendItems.length; i += 1) {
				legendItems[i].index = i;
				legendItems[i].max = this.proccedDataSet[legendItems[i].textContent].legendTooltipHsh.max;
				legendItems[i].min = this.proccedDataSet[legendItems[i].textContent].legendTooltipHsh.min;
				legendItems[i].unit = this.proccedDataSet[legendItems[i].textContent].legendTooltipHsh.unit;
				legendItems[i].r = this.proccedDataSet[legendItems[i].textContent].legendTooltipHsh.corr;
				legendItems[i].mean = this.proccedDataSet[legendItems[i].textContent].legendTooltipHsh.mean;
				legendItems[i].n = this.proccedDataSet[legendItems[i].textContent].legendTooltipHsh.n;
				legendItems[i].sd = this.proccedDataSet[legendItems[i].textContent].legendTooltipHsh.sd;
				legendItems[i].getElementsByTagName("span")[0].addEventListener("mouseover", this.handleLagentToolTipOnHover, false);
				legendItems[i].getElementsByTagName("span")[0].addEventListener("mouseleave", this.handleLagentToolTipOnHoverOnLeave, false);
				if (this.proccedDataSet[legendItems[i].textContent].dataVariableId != this.sObject.musqotmp__DependentData__c) {
					legendItems[i].addEventListener("click", this.legendClickCallback, false);
				}
			}
			this.legentStyle += "background-color: white;";
			this.showScatterButton = true;
		}
	}
	hovering = false;
	handleLagentToolTipOnHover(event) {
		if (this.hovering) {
			return;
		}
		let legendTooltip = window.legendTooltip;
		legendTooltip.style.display = "block";
		let target = event.target.parentElement || event.srcElement;
		let innerHTML = "<table>";
		innerHTML += "<tr>";
		innerHTML += "<td>Unit</td>";
		innerHTML += '<td class="padding-left-25px">' + target.unit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		+"</td>";
		innerHTML += "</tr>";

		innerHTML += "<tr>";
		innerHTML += "<td>Max</td>";
		innerHTML +=
			'<td class="padding-left-25px">' +
			parseFloat(target.max)
				.toFixed(2)
				.toString()
				.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		+"</td>";
		innerHTML += "</tr>";

		innerHTML += "<tr>";
		innerHTML += "<td>Min</td>";
		innerHTML +=
			'<td class="padding-left-25px">' +
			parseFloat(target.min)
				.toFixed(2)
				.toString()
				.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		+"</td>";
		innerHTML += "</tr>";

		innerHTML += "<tr>";
		innerHTML += "<td>STDEV</td>";
		innerHTML +=
			'<td class="padding-left-25px">' +
			target.sd
				.toFixed(2)
				.toString()
				.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		+"</td>";
		innerHTML += "</tr>";

		innerHTML += "<tr>";
		innerHTML += "<td>Mean</td>";
		innerHTML +=
			'<td class="padding-left-25px">' +
			target.mean
				.toFixed(2)
				.toString()
				.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		+"</td>";
		innerHTML += "</tr>";

		innerHTML += "<tr>";
		innerHTML += "<td>N</td>";
		innerHTML += '<td class="padding-left-25px">' + target.n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		+"</td>";
		innerHTML += "</tr>";
		if (target.r) {
			innerHTML += "<tr>";
			innerHTML += "<td>R</td>";
			innerHTML += '<td class="padding-left-25px">' + target.r.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			+"</td>";
			innerHTML += "</tr>";
		}
		innerHTML += "</table>";

		legendTooltip.innerHTML = innerHTML;
		legendTooltip.style.left = event.x + 150 + "px";
		legendTooltip.style.top = event.y - 100 + "px";
	}
	handleLagentToolTipOnHoverOnLeave() {
		let legendTooltip = window.legendTooltip;
		this.hovering = false;
		legendTooltip.innerHTML = "";
		legendTooltip.style.display = "none";
	}
	customYaxis(lineChart) {
		let scale = window.devicePixelRatio;
		let sourceCanvas = lineChart.chart.canvas;
		let copyWidth = lineChart.scales["y-axis-0"].width - 10;
		let copyHeight = lineChart.scales["y-axis-0"].height + lineChart.scales["y-axis-0"].top + 10;

		const yaxisCtx = this.template.querySelector(".yaxis-chart").getContext("2d");

		yaxisCtx.scale(scale, scale);
		yaxisCtx.canvas.width = copyWidth * scale;
		yaxisCtx.canvas.height = copyHeight * scale;
		yaxisCtx.canvas.style.width = `${copyWidth}px`;
		yaxisCtx.canvas.style.height = `${copyHeight}px`;
		yaxisCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);
		let sourceCtx = sourceCanvas.getContext("2d");
		// Normalize coordinate system to use css pixels.
		sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
		this.legentStyle = "background-color: white; width:" + (220 + this.halfCellWidth - copyWidth) + "px;";
		this.yaxisStyle = "background-color: white; margin-left:" + (220 + this.halfCellWidth - copyWidth) + "px;";
	}
	disconnectedCallback() {
		gantt.clearAll();
	}
	legendClickCallback(event) {
		const tmmThis = this;
		window.rectangleSet = false;
		event = event || window.event;
		let target = event.target || event.srcElement;
		while (target.nodeName !== "LI") {
			target = target.parentElement;
		}
		let parent = target.parentElement;
		let chartId = parseInt(parent.classList[0].split("-")[0], 10);
		let chart = Chart.instances[chartId];
		//let index = Array.prototype.slice.call(gParent.getElementsByTagName('li')).indexOf(target);
		//let index = tmmThis.dependentDataList.length + 1;
		let index = 0
		if (window.tempThis.dependentDataList) 
			target.index - window.tempThis.dependentDataList.length;
		let meta = chart.getDatasetMeta(index);
	//	console.log(index);
		let item = meta.data[index];
		if (item.strike == undefined) {
			meta.data[index].strike = true;
			target.classList.add("hidden");
		} else {
			target.classList.remove("hidden");
			meta.data[index].strike = undefined;
		}
		let ci = event.view.lineChart;
		let curr = ci.data.datasets[index]._meta[0].data[index];

		curr.hidden = !curr.hidden;
		if (meta.dataset) {
			meta.hidden = !meta.hidden;
		} else {
			meta.data[index].hidden = !meta.data[index].hidden;
		}

		// We hid a dataset ... rerender the chart
		ci.update();
	}
	//scroll gantt onscroll of chart
	handleChartScroll(event) {
	//	console.log(this.onMousePoint);
		if (this.onMousePoint == "performance-chart" || this.onMousePoint == "chartAreaWrapper") {
			gantt.scrollTo(event.target.scrollLeft, null);
		}
	}
	//Get the chart xaxis label
	getDates(startDate, endDate) {
		var dates = [],
			currentDate = startDate,
			addDays = function (days) {
				var date = new Date(this.valueOf());
				date.setDate(date.getDate() + days);
				return date;
			};
		while (currentDate <= endDate) {
			dates.push(moment(currentDate).format("DD/MM/YYYY"));
			currentDate = addDays.call(currentDate, 1);
		}
		return dates;
	}
	setClassNameOnHover(event) {
		this.onMousePoint = event.target.className;
	}
	handleRefresh() {
		if (this.wiredData.data) {
			refreshApex(this.wiredData);
		}
	}
	refreshDataSet() {
		if (this.dataSetWiredResult.data) {
			refreshApex(this.dataSetWiredResult);
		}
	}
	handleCancelModal() {
		const modal = this.template.querySelector("c-modal");

		this.isMarketingJourney = false;
		this.isMarketingActivity = false;
		this.isPlan = false;
		this.isCampaign = false;
		//this.taskIdCampaign = '';
		this.hasCampaignTab = false;
		this.hasOpenButton = false;
		this.financeAllocation = false;
		modal.hide();
	}

	handleOpen() {
		this.navigateToRecord();
	}
	handleNavigateToMarketingCloud() {
		window.open(this.mcUrlMap["" + this.taskId + ""], "MarketingCloud");
		this.handleCancelModal();
	}

	//sowmya user story W-000346
	navigateToRecord() {
		if (this.hasCampaignTab) {
			this[NavigationMixin.GenerateUrl]({
				type: "standard__recordPage",
				attributes: {
					recordId: this.taskIdCampaign,
					objectApiName: this.otherobjectapiname,
					actionName: "view",
				},
			}).then((url) => {
				window.open(url, "Musqot");
				this.handleCancelModal();
			});
		} else {
			this[NavigationMixin.GenerateUrl]({
				type: "standard__recordPage",
				attributes: {
					recordId: this.taskId,
					objectApiName: this.objectApiName,
					actionName: "view",
				},
			}).then((url) => {
				window.open(url, "Musqot");
				this.handleCancelModal();
			});
		}
	}

	handleScatterChart() {
		this.isScatter = true;
		this.toDate = new Intl.DateTimeFormat(LOCALE).format(new Date());
		this.fromDate = new Intl.DateTimeFormat(LOCALE).format(this.startDate);
	}
	regrationPoints(chartData) {
		var a, b, c, d, e, slope, yIntercept;
		var xSum = 0,
			ySum = 0,
			xySum = 0,
			xSquare = 0,
			dpsLength = chartData.data.length;
		for (var i = 0; i < dpsLength; i++) xySum += chartData.data[i].x * chartData.data[i].y;
		a = xySum * dpsLength;

		for (var i = 0; i < dpsLength; i++) {
			xSum += chartData.data[i].x;
			ySum += chartData.data[i].y;
		}
		b = xSum * ySum;

		for (var i = 0; i < dpsLength; i++) xSquare += Math.pow(chartData.data[i].x, 2);
		c = dpsLength * xSquare;

		d = Math.pow(xSum, 2);
		slope = (a - b) / (c - d);
	/*	console.log("a=>" + a);
		console.log("b=>" + b);
		console.log("c=>" + c);
		console.log("d=>" + d);
		console.log("slope=>" + slope);*/

		e = slope * xSum;
		yIntercept = (ySum - e) / dpsLength;

		var startPoint = this.getTrendLinePoint(chartData.data[0].x, slope, yIntercept);
		var endPoint = this.getTrendLinePoint(chartData.data[dpsLength - 1].x, slope, yIntercept);
		return { startPoint: startPoint, endPoint: endPoint };
	}
	getTrendLinePoint(x, slope, intercept) {
		return { x: x, y: slope * x + intercept };
	}
	createCanvas(scatterDataSetMap, div, index) {
		let annotationValue = this.regrationPoints(scatterDataSetMap);
		this.tempScratter = scatterDataSetMap;
		var canvas = document.createElement("canvas");
		canvas.maxWidth = 300;
		canvas.height = 300;
		canvas.position = "absolute";
		const ctx = canvas.getContext("2d");
		// End Defining data
		var options = {
			responsive: true,
			scales: {
				yAxes: [
					{
						scaleLabel: {
							display: true,
							labelString: this.dependentData.label,
						},
					},
				],
				xAxes: [
					{
						scaleLabel: {
							display: true,
							labelString: scatterDataSetMap.label,
						},
					},
				],
			},
			maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height
			tooltips: {
				callbacks: {
					label: function (tooltipItem, data) {
						var label = [];
						/*label = data.datasets[tooltipItem.datasetIndex][tooltipItem.index].x || '';
						label += ' ' + data.datasets[tooltipItem.datasetIndex][tooltipItem.index].y || '';*/
						label.push((Math.round(tooltipItem.yLabel * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " " + scatterDataSetMap.dUnit);

						/*
								if (label) {
									label += ': ';
								}
								label += (Math.round(tooltipItem.yLabel * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');*/
						return label;
					},
					afterLabel: function (tooltipItem, data) {
						let extraLabel = [];
						extraLabel.push((Math.round(tooltipItem.xLabel * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " " + scatterDataSetMap.unit);
						if (this.displayGroupBy == "Daily") extraLabel.push(new Intl.DateTimeFormat(LOCALE).format(new Date(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].date)));
						else extraLabel.push(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].date);
						return extraLabel;
						//return (Math.round(tooltipItem.yLabel * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ') + ' ' + data.datasets[tooltipItem.datasetIndex].unit;
					},
				},
			},
			annotation: {
				annotations: [
					{
						type: "line",
						mode: "horizontal",
						scaleID: "y-axis-1",
						value: annotationValue.startPoint,
						endValue: annotationValue.endPoint,
						borderColor: "#CCCCCC",
						borderWidth: 4,
					},
				],
			},
		};
		div.appendChild(canvas);

		// End Defining data
		var myChart = new window.Chart(ctx, {
			type: "scatter",
			plugins: [ChartjsPluginAnnotation],
			data: {
				datasets: [
					{
						label: this.dependentData.label + " — " + scatterDataSetMap.label, // Name the series
						data: scatterDataSetMap.data, // Specify the data values array
						borderColor: scatterDataSetMap.borderColor, // Ad--ustom color border
						backgroundColor: scatterDataSetMap.backgroundColor, // Add custom color background (Points and Fill)
					},
				],
			},
			options: options,
		});
	//	console.log(myChart);
		return canvas;
	}
	buildScratter() {
		//const ctx = this.template.querySelector('.chartjs-render-monitor').getContext('2d');
		const convasCnt = this.template.querySelector(".dynamic-canvas-cnt");
		var div = document.createElement("div");
		div.className = "scatter-table slds-grid slds-gutters";
		let scatterDataSets = [];
		let rowIndex = 0;
		//var row = table.insertRow(0);
		for (var index in this.independentDataList) {
			let independentData = this.independentDataList[index];
			if (index == "3") {
				var br = document.createElement("br");
				convasCnt.appendChild(br);
				div = document.createElement("div");
				div.className = "scatter-table slds-grid slds-gutters";
			}
			convasCnt.appendChild(div);
			var divCnt = document.createElement("div");
			divCnt.className = "slds-col slds-size_1-of-3";
			var cdiv = document.createElement("div");
			var tdiv = document.createElement("div");
			divCnt.appendChild(cdiv);
			divCnt.appendChild(tdiv);
			div.appendChild(divCnt);
			let scatterDataSetMap = { data: [] };
			let dependentDataMap = this.dependentData.dataMap;
			let independentDataMap = independentData.dataMap;
			scatterDataSetMap.data = [];
			scatterDataSetMap.unit = independentData.unit;
			scatterDataSetMap.dUnit = this.dependentData.unit;
			scatterDataSetMap.label = independentData.label;
			scatterDataSetMap.backgroundColor = this.independentDataList[index].backgroundColor;
			scatterDataSetMap.borderColor = this.independentDataList[index].borderColor;
			Object.keys(independentDataMap).forEach(function (key) {
				if (independentDataMap[key] && dependentDataMap[key]) scatterDataSetMap["data"].push({ x: independentDataMap[key], y: dependentDataMap[key], date: key });
			});
			var canvas = this.createCanvas(scatterDataSetMap, cdiv, index);
			/*	var div3 = document.createElement('div');
				div3.className = 'slds-col slds-grid slds-size_1-of-4';

				var div3_1 = document.createElement('div');
				div3_1.className = 'slds-col slds-size_1-of-4';
				div3_1.innerHTML='<span> ddddd</span>'
				div3.appendChild(div3_1);
				div2.appendChild(div3);*/
			//scatterDataSets.push(scatterDataSetMap);
			var table = document.createElement("TABLE");
			table.className = "slds-table  slds-table_bordered";
			table.style.tableLayout = "fixed";
			tdiv.appendChild(table);
			var thead = document.createElement("thead");
			var tr = document.createElement("TR");
			var th = document.createElement("TH");
			tr.appendChild(th);
			th = document.createElement("TH");
			var span = document.createTextNode("Mean");
			th.appendChild(span);
			tr.appendChild(th);
			th = document.createElement("TH");
			span = document.createTextNode("StDev");
			th.appendChild(span);
			tr.appendChild(th);
			th = document.createElement("TH");
			span = document.createTextNode("Min Value");
			th.appendChild(span);
			tr.appendChild(th);
			th = document.createElement("TH");
			span = document.createTextNode("Max Value");
			th.appendChild(span);
			tr.appendChild(th);
			thead.appendChild(tr);
			table.appendChild(thead);
			var tbody = document.createElement("tbody");
			table.appendChild(tbody);
			tr = document.createElement("TR");
			tbody.appendChild(tr);
			var td = document.createElement("TD");
			td.setAttribute("title", this.dependentData.label);
			span = document.createTextNode(this.dependentData.label);
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TD");
			td.setAttribute("title", this.dependentData.legendTooltipHsh.mean.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			span = document.createTextNode(this.dependentData.legendTooltipHsh.mean.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TD");
			td.setAttribute("title", this.dependentData.legendTooltipHsh.sd.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			span = document.createTextNode(this.dependentData.legendTooltipHsh.sd.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TD");
			td.setAttribute("title", this.dependentData.legendTooltipHsh.min.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			span = document.createTextNode(this.dependentData.legendTooltipHsh.min.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TD");
			td.setAttribute("title", this.dependentData.legendTooltipHsh.max.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			span = document.createTextNode(this.dependentData.legendTooltipHsh.max.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			td.appendChild(span);
			tr.appendChild(td);
			tbody.appendChild(tr);

			tr = document.createElement("TR");
			tbody.appendChild(tr);
			var td = document.createElement("TD");
			td.setAttribute("title", independentData.label);
			span = document.createTextNode(independentData.label);
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TD");
			td.setAttribute("title", independentData.legendTooltipHsh.mean.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			span = document.createTextNode(independentData.legendTooltipHsh.mean.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TD");
			td.setAttribute("title", independentData.legendTooltipHsh.sd.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			span = document.createTextNode(independentData.legendTooltipHsh.sd.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TD");
			td.setAttribute("title", independentData.legendTooltipHsh.min.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			span = document.createTextNode(independentData.legendTooltipHsh.min.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TD");
			td.setAttribute("title", independentData.legendTooltipHsh.max.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			span = document.createTextNode(independentData.legendTooltipHsh.max.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			td.appendChild(span);
			tr.appendChild(td);
			tbody.appendChild(tr);

			var tfoot = document.createElement("thead");
			table.appendChild(tfoot);
			tr = document.createElement("TR");
			td = document.createElement("TH");
			span = document.createTextNode("Correlation");
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TH");
			td.setAttribute("title", independentData.legendTooltipHsh.corr);
			span = document.createTextNode(independentData.legendTooltipHsh.corr);
			td.appendChild(span);
			tr.appendChild(td);
			//Blank tds
			td = document.createElement("TH");
			tr.appendChild(td);

			td = document.createElement("TH");
			span = document.createTextNode("N");
			td.appendChild(span);
			tr.appendChild(td);
			td = document.createElement("TH");
			td.setAttribute("title", independentData.legendTooltipHsh.n);
			span = document.createTextNode(independentData.legendTooltipHsh.n);
			td.appendChild(span);
			tr.appendChild(td);

			tfoot.appendChild(tr);
		}
	/*	const style = document.createElement("style");
		style.type = "text/css";
		style.innerText = `td {
            overflow:hidden;
				}
				tfoot {
            background: #fafaf9;
				}`;*/
		document.getElementsByTagName("head")[0].style.innerText=`td {
            overflow:hidden;
				}
				tfoot {
            background: #fafaf9;
				}`;//appendChild(style);
	}
	hideScatterModal() {
		this.isScatter = false;
	}
	//Sowmya user story W-000346
	handleClose() {
		this.isPlan = false;
		this.isCampaign = false;
		this.isMarketingActivity = false;
		this.isMarketingJourney = false;
		this.taskIdCampaign = "";
		this.hasCampaignTab = false;
		this.hasOpenButton = false;
		this.financeAllocation = false;
	}
	handleActive(event) {
		this.hasCampaignTab = true;
		this.hasOpenButton = false;
	}
	handleActiveAllocation() {
		this.hasOpenButton = true;
		this.hasCampaignTab = false;
	}
	handleActiveOpenButton() {
		this.hasOpenButton = false;
		this.hasCampaignTab = false;
	}
	//sowmya user story W-000346
	buildParamDataTable() {
		const whereCaluses = [];
		let fieldSetApiName = "musqotmp__AllocationGanttInfo";

		whereCaluses.push({
			field: "musqotmp__BeneficiaryCampaign__c",
			value: this.taskIdCampaign,
			clause: "=",
		});

		return JSON.stringify({
			component: "marketingPlanView-datatable",
			relatedListType: "finance",
			objectApiName: "musqotmp__Allocation__c",
			fieldMap: {
				//[ALLOCATION_OBJECT_ID_FIELD.fieldApiName]: {}
			},
			fieldSetApiName: fieldSetApiName,
			linkableNameField: true,
			whereCaluses: whereCaluses,
		});
	}

	@wire(datatable, {
		param: "$paramDataTable",
	})
	wiredDataTable(result) {
		if (result.data) {
			if (result.data.toast.ok) {
				this.columns = result.data.datatable.columns;
				this.data = result.data.datatable.data;
				this.template.querySelector("c-toast").showToast(result.data.toast);
			} else {
				this.data = undefined;
				this.columns = undefined;
				this.template.querySelector("c-toast").showToast(result.data.toast);
			}
		} else if (result.error) {
			this.data = undefined;
			this.columns = undefined;
			this.template.querySelector("c-toast").showToast(result.error);
		}
		this.tableLoadingState = false;
	}
	navigateToCampaignInNewTab(event) {
		let campaignId;

		if (this.dynamicRecord) {
			campaignId = this.dynamicRecord.fields.musqotmp__Campaign__c.value;
		}
		this[NavigationMixin.GenerateUrl]({
			type: "standard__recordPage",
			attributes: {
				recordId: campaignId,
				objectApiName: "Campaign",
				actionName: "view",
			},
		}).then((url) => {
			window.open(url, "Proof");
			//this.handleCancelModal();
		});
	}
	navigateToPlanInNewTab(event) {
		let planId;

		if (this.campaignObj) {
			planId = this.campaignObj.fields.musqotmp__MarketingPlan__c.value;
		}
		this[NavigationMixin.GenerateUrl]({
			type: "standard__recordPage",
			attributes: {
				recordId: planId,
				objectApiName: "musqotmp__MarketingPlan__c",
				actionName: "view",
			},
		}).then((url) => {
			window.open(url, "Proof");
			//this.handleCancelModal();
		});
	}
}