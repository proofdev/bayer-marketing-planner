import { LightningElement, wire, track, api } from "lwc";
import { updateRecord, getRecord } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { refreshApex } from '@salesforce/apex';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import postCalloutResponseContents from "@salesforce/apex/Campaign.postCalloutResponseContents";
import getCalloutResponseContents from "@salesforce/apex/Campaign.getCalloutResponseContents";
import readMarketingCloudCredential from "@salesforce/apex/Campaign.readMarketingCloudCredential";
//import getCalloutResponseTemplates from "@salesforce/apex/Campaign.getCalloutResponseTemplates";
import MARKETINGJOURNEY_OBJECT from '@salesforce/schema/MarketingJourney__c';
import MARKETINGJOURNEY_JOURNEYID_FIELD from '@salesforce/schema/MarketingJourney__c.JourneyID__c';
import MARKETINGJOURNEY_STATUS_FIELD from '@salesforce/schema/MarketingJourney__c.JourneyStatus__c';
import MARKETINGJOURNEY_NAME_FIELD from '@salesforce/schema/MarketingJourney__c.JourneyName__c';
import MARKETINGJOURNEY_DESCRIPTION_FIELD from '@salesforce/schema/MarketingJourney__c.JourneyDescription__c';
import MARKETINGJOURNEY_VERSION_FIELD from '@salesforce/schema/MarketingJourney__c.JourneyVersion__c';
import MARKETINGJOURNEY_CAMPAIGN_FIELD from '@salesforce/schema/MarketingJourney__c.Campaign__c';

import MARKETINGJOURNEY_ID_FIELD from '@salesforce/schema/MarketingJourney__c.Id';

import modalHeader from '@salesforce/label/c.modalHeader';
import btnLinkMCJourney from '@salesforce/label/c.btn_link_mc_journey';
import btnOpenInMC from '@salesforce/label/c.btn_open_in_mc';
import cancel from '@salesforce/label/c.btn_cancel';
import open from '@salesforce/label/c.btn_open';
import save from '@salesforce/label/c.btn_save';


export default class MarketingCloud extends NavigationMixin(LightningElement) {
  label = {
    btnLinkMCJourney,
    btnOpenInMC,
    modalHeader,
    cancel,
    save
  }
  marketing_journey_object = MARKETINGJOURNEY_OBJECT;
  @api recordId;
  @api journey;
  @track journeyType = "myTemplate";
  @track accessToken = "";
  @track journeyList = [];
  @track journeyId;
  @track journeys;
  @track marketingCloudContent = [];
  @track journeyRecord;
  @track isConnectedMarketingCloud = false;
  @track openMarketingCloudUrl;
  @track marketingJourney;
  @track marketingJourneyFields;
  @track isDelinked = false;
  @track marketingJourneyIconStyle = "background-color: #F28411";
  @track journeyFiled = [];
  @track marketingCloudCredential;
  @track sectioncssclass = 'slds-modal slds-fade-in-open slds-modal_medium';
  journeyOptions;
  mcJourneyObject;
  @wire(getObjectInfo, { objectApiName: MARKETINGJOURNEY_OBJECT })
  wiredGetObjectInfo({ error, data }) {
    if (data) {
      this.marketingJourney = data;
      this.marketingJourneyFields = data.fields;
      if (data.themeInfo) {
        this.marketingJourneyIconStyle = "background-color: #" + data.themeInfo.color;
      }
    } else if (error) {
    //  window.console.log(JSON.stringify(error))
    }
  }
  @wire(readMarketingCloudCredential)
  wiredReadMarketingCloudCredential(cresult) {
    if (cresult.data && cresult.data.sObj) {
      this.marketingCloudCredential = cresult.data.sObj;
      this.journeyFiled = [
        MARKETINGJOURNEY_CAMPAIGN_FIELD, MARKETINGJOURNEY_ID_FIELD,
        MARKETINGJOURNEY_JOURNEYID_FIELD, MARKETINGJOURNEY_STATUS_FIELD,
        MARKETINGJOURNEY_NAME_FIELD, MARKETINGJOURNEY_DESCRIPTION_FIELD,
        MARKETINGJOURNEY_VERSION_FIELD
      ];
      postCalloutResponseContents({
        param:
          JSON.stringify({
            URL: 'callout:MCConnect',
            Body:
              "client_id={!$Credential.UserName}" + "&client_secret={!$Credential.Password}" + "&grant_type=client_credentials" 
          })
      }).then((result) => {
        this.mcJourneyObject = result;
        if (result.resultMap) {
          if (result.toast.ok) {
            this.accessToken = result.resultMap.access_token;
           this.getResponseContents();
          }
        }
      }).catch(error => {
        if (this.template.querySelector("c-toast")) this.template.querySelector("c-toast").showToast(error);
      })
    }else if(cresult.error){
      if (this.template.querySelector("c-toast")) this.template.querySelector("c-toast").showToast(cresult.error);
    }
  }


  @wire(getRecord, { recordId: '$recordId', fields: '$journeyFiled' })
  wiredJourney(result) {
    this.journey = result;
    if (result.data) {
      this.journeyRecord = result.data;
      if (result.data.fields.musqotmp__JourneyID__c.value) {
        this.isConnectedMarketingCloud = true;
        let url = this.marketingCloudCredential.musqotmp__TargetURL__c + result.data.fields.musqotmp__JourneyID__c.value + '/' + result.data.fields.musqotmp__JourneyVersion__c.value;
        this.openMarketingCloudUrl = url;
      }
    } else if (result.error) {
      this.template.querySelector("c-toast").showToast(result.error);
    }
  }
  getResponseContents() {
    let param = JSON.stringify({
      URL:
        "https://" + this.marketingCloudCredential.musqotmp__SubDomain__c + ".rest.marketingcloudapis.com/interaction/v1/interactions?$page=" +
        1 +
        "&$pageSize=50&$orderBy=Name",
      MCAuthorization: "Bearer " + this.accessToken,
      ContentType: "application/json"
    });
    getCalloutResponseContents({ param: param })
      .then(res => {
        this.journeyList = res.resultMap.items;
        let opts = [];
        for (let i = 0; i < res.resultMap.items.length; i++) {
          if (i === 0) {
            this.journeyId = res.resultMap.items[i].id;
            opts.push({
              label:
                res.resultMap.items[i].name +
                " " +
                "(" +
                res.resultMap.items[i].status +
                ")",
              value: res.resultMap.items[i].id,
              selected: true
            });
          } else {
            opts.push({
              label:
                res.resultMap.items[i].name +
                " " +
                "(" +
                res.resultMap.items[i].status +
                ")",
              value: res.resultMap.items[i].id,
              selected: false
            });
          }
        }
        this.journeys = res.resultMap.items;
        this.journeyOptions = opts;
        //this.marketingCloudContent = this.journeys;
      })
      .catch(error => {
        this.template.querySelector("c-toast").showToast(error);
      });
  }
  get journeyTypes() {
    return [
      { label: " My Template", value: "myTemplate" },
      { label: " Salesforce Template", value: "salesforceTemplate" },
      { label: " Ready Journey", value: "readyJourney" }
    ];
  }
  /*getResponseTemplates() {
    getCalloutResponseTemplates({ token: this.accessToken })
      .then(res => {
        window.console.log(res);
      })
      .catch(error => {
        this.template.querySelector("c-toast").showToast(error);
      });
  }*/
  handleChange(event) {
    this.marketingCloudContent = [];
    this.journeyType = event.detail.value;
  //  window.console.log(this.journeyType);
    if (this.journeyType === "myTemplate") {
      //this.getResponseTemplates();
    }
    if (event.detail.value === "readyJourney") {
      this.getResponseContents();
    }
  }
  updateMarketingJourney() {
    let selectedJourney;
    this.journeys.forEach(element => {
      if (element.id === this.journeyId) {
        selectedJourney = element;
      }
    });
    const fields = {};
    fields[MARKETINGJOURNEY_ID_FIELD.fieldApiName] = this.recordId;
  //  window.console.log('this.isDelinked' + this.isDelinked);
    if (selectedJourney && !this.isDelinked) {
      fields[MARKETINGJOURNEY_NAME_FIELD.fieldApiName] = selectedJourney.name;
      fields[MARKETINGJOURNEY_JOURNEYID_FIELD.fieldApiName] = selectedJourney.id;
      fields[MARKETINGJOURNEY_VERSION_FIELD.fieldApiName] = selectedJourney.version;
      fields[MARKETINGJOURNEY_DESCRIPTION_FIELD.fieldApiName] = selectedJourney.description;
      fields[MARKETINGJOURNEY_STATUS_FIELD.fieldApiName] = selectedJourney.status;
      fields[MARKETINGJOURNEY_CAMPAIGN_FIELD.fieldApiName] = this.journeyRecord.fields.musqotmp__Campaign__c.value;
    } else {
      fields[MARKETINGJOURNEY_NAME_FIELD.fieldApiName] = null;
      fields[MARKETINGJOURNEY_JOURNEYID_FIELD.fieldApiName] = null;
      fields[MARKETINGJOURNEY_VERSION_FIELD.fieldApiName] = null;
      fields[MARKETINGJOURNEY_DESCRIPTION_FIELD.fieldApiName] = null;
      fields[MARKETINGJOURNEY_STATUS_FIELD.fieldApiName] = null;
    }
    const recordInput = { fields }
    updateRecord(recordInput)
      .then(() => {
        this.isDelinked = false;
        this.handleCancelModal();
        if (this.journeyRecord.fields && this.journeyRecord.fields.musqotmp__JourneyID__c.value) {
          this.isConnectedMarketingCloud = true;
        } else {
          this.isConnectedMarketingCloud = false;
        }
        return refreshApex(this.journey);
      })
      .catch(error => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: 'Error creating record',
            message: error.message,
            variant: 'error',
          }),
        );
      });
  }
  //Open Journey in Marketing Cloud
  navigateToMarketingCloud() {
  //  window.console.log('Navigation ' + this.openMarketingCloudUrl);
    this[NavigationMixin.Navigate]({
      "type": "standard__webPage",
      "attributes": {
        url: this.openMarketingCloudUrl
      }
    });
  }
  get marketingCloudJourneyOptions() {
    return this.journeyOptions;
  }
  handleJourneyChange(event) {
    this.journeyId = event.detail.value;
  }
  handleConnectMarketingCloud() {
    const modal = this.template.querySelector("c-modal");
    modal.show();
  }

  handleCancelModal() {
    const modal = this.template.querySelector("c-modal");
    modal.hide();
  }
  //Sync update journey dáta from marketing cloud.
  handleSync() {
    refreshApex(this.mcJourneyObject);
    this.journeyId = this.journeyRecord.fields.musqotmp__JourneyID__c.value;
    this.updateMarketingJourney();
  }
  //Delete the markeingcloud journey data from the Marketingcloud object
  handleDeLinked() {
    this.isDelinked = true;
    let updatedJourney = this.updateMarketingJourney();
  }
}