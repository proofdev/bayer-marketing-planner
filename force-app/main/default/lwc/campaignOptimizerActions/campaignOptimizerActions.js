import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, updateRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getToast } from 'c/lds';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';

import CAMPAIGNOPTIMIZER_ID_FIELD from '@salesforce/schema/CampaignOptimizer__c.Id';
import CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD from '@salesforce/schema/CampaignOptimizer__c.AcquisitionRetention__c';
import CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD from '@salesforce/schema/CampaignOptimizer__c.MoreDealsBiggerDeals__c';
import CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD from '@salesforce/schema/CampaignOptimizer__c.RevenueCashflow__c';

import frm_Acquisition_Retention from '@salesforce/label/c.frm_Acquisition_Retention';
import frm_More_Bigger_Deals from '@salesforce/label/c.frm_More_Bigger_Deals';
import frm_Revenue_cashflow from '@salesforce/label/c.frm_Revenue_cashflow';
import btn_save from '@salesforce/label/c.btn_save';
import msg_Record_Saved from '@salesforce/label/c.msg_Record_Saved';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
const CSS_CLASS = 'modal-hidden';

const fields = [CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD, CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD, CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD];

export default class CampaignOptimizerActions extends NavigationMixin(LightningElement) {
    label = {
        frm_Acquisition_Retention,
        frm_More_Bigger_Deals,
        frm_Revenue_cashflow,
        msg_Record_Saved,
        btn_save
    };
    @api recordId;
    @api objectApiName = 'musqotmp__CampaignOptimizer__c';
    @track sectioncssclass = 'slds-modal slds-fade-in-open slds-modal_small';
    @track objectTypeLabel;
    @track header;
    @track layoutSections = [];
    @track activeAccordionSections;
    @track optimizerLabel = 'Optimizer';
    @track showModal = true;

    @wire(CurrentPageReference) pageRef;

    @wire(getObjectInfo, { objectApiName: '$objectApiName' })
    wiredRecord({ error, data }) {
        if (error) {
            this.template.querySelector('c-toast').showToast(error);
        } else if (data) {
            this.objectTypeLabel = data.label;
            this.header = data.label;
        }
    }
    @wire(getRecord, { recordId: '$recordId', fields })
    wiredCampaignOptimizer;

    get acquisitionRetention() {
        return getFieldValue(this.wiredCampaignOptimizer.data, CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD);
    }

    get moreDealsBiggerDeals() {
        return getFieldValue(this.wiredCampaignOptimizer.data, CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD);
    }

    get revenueCashflow() {
        return getFieldValue(this.wiredCampaignOptimizer.data, CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD);
    }

    onLoad(event) {
        this.layoutSections = [event.detail.layout.sections[0]];
        this.activeAccordionSections = [this.layoutSections[0].id];
        //this.showModal = false;
        //const modal = this.template.querySelector('c-record-form-modal');
        //modal.show();
    }
    handleCancelRecordFormModal() {
        this.recordId = null;
        this.showModal = true;

        //const modal = this.template.querySelector('c-record-form-modal');
        //modal.hide();
    }
    handleDialogClose(event) {
        //this.showModal = false;
        this.navigateToOptimizerListViewPage();
        this.handleCancelRecordFormModal();

        //const outerDivEl = this.template.querySelector('div');
        //return outerDivEl?"display: none !important;":'display: none !important';
        //outerDivEl.classList.add(CSS_CLASS);
    }

    onSuccess(event) {
        this.recordId = event.detail.id;
        this.updateRecord();
    }
    handleError(event) {
        let customErrorField = [];
        customErrorField=event.detail.output.errors;
        if (customErrorField) {
            for (let i = 0; i < customErrorField.length; i++) {
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: customErrorField[i].message,
                    variant: 'error',
                    mode: 'sticky'
                });
                this.dispatchEvent(evt);
            }
        }
    }

    updateRecord() {
        const updateFields = {};
        updateFields[CAMPAIGNOPTIMIZER_ID_FIELD.fieldApiName] = this.recordId;
        updateFields[CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD.fieldApiName] = this.template.querySelector("[data-field='AcquisitionRetention']").value;
        updateFields[CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD.fieldApiName] = this.template.querySelector("[data-field='MoreDealsBiggerDeals']").value;
        updateFields[CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD.fieldApiName] = this.template.querySelector("[data-field='RevenueCashflow']").value;

        const recordInput = { fields: updateFields };

        updateRecord(recordInput)
            .then(() => {
                this.navigateToRecord();
                this.handleCancelRecordFormModal();
                const toast = getToast('success', 'pester', this.label.msg_Record_Saved);
                this.template.querySelector('c-toast').showToast(toast);
            })
            .catch(error => {
                this.template.querySelector('c-toast').showToast(error);
            });
    }

    navigateToRecord() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: this.objectApiName,
                actionName: 'view'
            }
        });
    }

    navigateToOptimizerListViewPage(){
        this[NavigationMixin.Navigate]({
            "type": "standard__objectPage",
            "attributes": {
                "objectApiName": this.objectApiName,
                "actionName": "home"
            }
        });     
    }
}