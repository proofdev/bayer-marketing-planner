import { LightningElement, wire, api, track } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import USER_OBJECT from '@salesforce/schema/User';
import USER_ID_FIELD from '@salesforce/schema/User.Id';
import getPlanMembers from "@salesforce/apex/PlanMemberController.getPlanMembers";
import CreateRecords from "@salesforce/apex/PlanMemberController.CreateRecords";
import datatable from '@salesforce/apex/Campaign.datatable';
import getResults from '@salesforce/apex/Campaign.getLookupResults';
import { NavigationMixin } from 'lightning/navigation';
import hl_Add_plan_members from '@salesforce/label/c.hl_Add_plan_members';
import hl_Search_People from '@salesforce/label/c.hl_Search_People';
import msg_Loading from '@salesforce/label/c.msg_Loading';
import hl_Show_More from '@salesforce/label/c.hl_Show_More';
import hl_Read from '@salesforce/label/c.hl_Read';
import btn_Next from '@salesforce/label/c.btn_Next';
import btn_cancel from '@salesforce/label/c.btn_cancel';
import btn_save from '@salesforce/label/c.btn_save';
import btn_Back from '@salesforce/label/c.btn_Back';
import hl_items_selected from '@salesforce/label/c.hl_items_selected';
import msg_No_result_found from '@salesforce/label/c.msg_No_result_found';

export default class PlanMemberAction extends NavigationMixin(LightningElement) {
    label = {
        hl_Add_plan_members,
        hl_Search_People,
        msg_Loading,
        hl_Show_More,
        hl_Read,
        btn_Next,
        btn_cancel,
        btn_save,
        btn_Back,
        hl_items_selected,
        msg_No_result_found
	};
    @api recordId;
    @api objectApiName;
    @api showModal;
    @track iconFlag = true;
    @track objInfo;
    @track showIcon = false;
    @track objIconColor;
    @track searchedData = false;
    @track userData;
    @track userList = [];
    @track showdata = false;
    @track selectedItems = false;
    @track IconColor;
    @track selectedItemList = [];
    @track lastElement;
    @track showInput = true;
    @track columns = [];
    @track columnList = [];
    @track data = [];
    @track planUserIds = [];
    @track topChecked = false;
    selectedItemMessage = this.label.hl_items_selected;
    @track selectedItemCount = 0 + this.selectedItemMessage;
    @track count = 0;
    @track hasShowData = true;
    @track selectedSet = new Set();
    @track searchData = new Set();
    @track selectedItemMap = {};
    @track hasNext = false;
    @track isNext = true;
    @track readChecked = true;
    @track rwChecked = false;
    @track isSort = false;
    @track showNext = false;
    @track dataMap = {};

    //datatable
    @track hideCheckboxColumn = false;
    @track preSelectedRows = new Set();
    @track rowOffset = 0;
    @track tableLoadingState = true;
    @track showRowNumberColumn = false;
    @track sortBy;
    @track sortDirection;
    @track selectedRows = [];

    //sorting 
    isAsc = false;
    isDsc = false;
    isNameSort = false;
    isPhoneSort = false;
    isIndustrySort = false;
    isTypeSort = false;
    isAccNumSort = false;
    sortedDirection = 'asc';
    sortedColumn;
    @wire(getObjectInfo, { objectApiName: USER_OBJECT })
    wiredGetObjectInfo({ data, error }) {
        if (data) {
            this.showIcon = true;
            this.showModal = true;
            this.objInfo = data;
            this.objIconColor = 'width:20px;height:20px; background-color: #' + this.objInfo.themeInfo.color;
            this.IconColor = 'background-color: #' + this.objInfo.themeInfo.color;
        } else if (error) {
            this.template.querySelector("c-toast").showToast(error);
        }
    }
    //reading the users who has permission plan and who doesn't include member of planmember
    @wire(getPlanMembers, { recordId: '$recordId' })
    wiredGetPlanMemberResult(result) {
        if (result.data) {
            if (result.data.toast.ok) {
                this.userData = result.data.sObjectList;
                this.planUserIds = result.data.sObjIds;
                this.lastElement = this.userData[this.userData.length - 1];
                this.buildParamDatatable();
            }
        } else if (result.error) {
            this.template.querySelector("c-toast").showToast(result.error);
        }
    }
    buildParamDatatable() {
        const whereCaluses = [];
        let fieldSetApiName = 'musqotmp__MarketingPlanMember';
        whereCaluses.push({
            field: 'Id',
            values: this.planUserIds,
            clause: 'IN'
        });
        let param = JSON.stringify({
            component: 'PlanMemberAction',
            relatedListType: 'finance',
            objectApiName: USER_OBJECT.objectApiName,
            fieldMap: {
                [USER_ID_FIELD.fieldApiName]: {},
            },
            whereCaluses: whereCaluses,
            fieldSetApiName: fieldSetApiName,
            linkableNameField: true,
            sortBy: 'LastModifiedDate',
            sortDirection: this.sortDirection,
        });
        datatable({ param: param })
            .then(result => {
                this.wiredData = result;
                if (result.toast.ok) {
                    this.sortedBy = this.sortBy;
                    this.sortedDirection = this.sortDirection;
                    this.columns = result.datatable.columns;
                    this.columns.forEach(col => {
                        if (col.label === 'Full Name' || col.label === 'Email') {
                            this.columnList.push(col);
                        }
                    });
                    this.data = result.datatable.data;
                    let dataList = [];
                    this.data.forEach(sObj => {
                        let obj = Object.assign({}, sObj);
                        this.userList.push({ label: obj.Name, value: obj.Id });
                        this.selectedItemMap[obj.Id] = { label: obj.Name, value: obj.Id, Email: obj.Email, readChecked: true, rwChecked: false };
                        dataList.push(obj);
                    })
                    this.data = dataList;
                    this.tableLoadingState = false;
                }
            })
            .catch(error => {
                this.template.querySelector("c-toast").showToast(error);
            })
    }
    handleChange(event) {
        let searchText = event.target.value;
        this.userList = [];
        this.showMessage = '';
     //   console.log('searchText :' + searchText);
        let param;
        if (searchText != null && searchText != '') {
            const whereCaluses = [];
            whereCaluses.push({
                field: 'Name',
                value: searchText,
                clause: 'LIKE',
                operator: 'AND',
            })
            whereCaluses.push({
                field: 'Id',
                values: this.planUserIds,
                clause: 'IN'
            });
            let sortedDirection = 'DESC';
            let amountOfRecords = 400000;
            let sortedBy = 'Name';
            param = JSON.stringify({
                component: 'PlanMemberAction',
                objectApiName: USER_OBJECT.objectApiName,
                whereCaluses: whereCaluses,
                sortBy: sortedBy,
                sortDirection: sortedDirection,
                amountOfRecords: amountOfRecords
            });
            getResults({ param: param })
                .then(result => {
                    this.userList = [];
                    if (result.sObjectList) {
                        if (result.sObjectList.length === 0) {
                            this.showMessage = this.label.msg_No_result_found;
                        }
                        let sObjList = result.sObjectList;
                        //searchData
                        sObjList.forEach(sObj => {
                            this.searchData.add(this.selectedItemMap[sObj.Id]);
                        });
                        this.userList = Array.from(this.searchData);
                    }
                })
                .catch(error => {
                    this.template.querySelector("c-toast").showToast(error);
                })
        } else {
            this.userList = [];
            this.showMessage = 'No result found !';
        }
    }
    handleClick(event) {
        this.showMessage = '';
        this.showdata = true;
    }
    handleClicked() {
        this.showdata = false;
    }
    handleSelect(event) {
        //let selectedName = event.currentTarget.getAttribute('name');
        let selectedId = event.currentTarget.getAttribute('data-id');
        this.showdata = false;
        this.selectedSet.add(this.selectedItemMap[selectedId]);
        this.preSelectedRows.add(selectedId);
        this.selectedRows = Array.from(this.preSelectedRows);
        this.selectedItems = true;
        this.selectedItemCount = this.selectedSet.size + this.selectedItemMessage;
    }
    removeSelectedItem(event) {
        this.showdata = false;
        let removeItemId = event.currentTarget.getAttribute("data-id");
        this.selectedSet.delete(this.selectedItemMap[removeItemId]);
        this.preSelectedRows.delete(removeItemId);
        //deselectiong the cheked value
        this.selectedRows = Array.from(this.preSelectedRows);
        this.selectedItemCount = this.selectedSet.size + this.selectedItemMessage;
    }
    handleSelectedRows(event) {
        let selectedRows = event.detail.ids;
    //    console.log('selectedRows :' + JSON.stringify(selectedRows));
        this.selectedItems = true;
        this.showdata = false;
        this.selectedSet = new Set();
        this.preSelectedRows = new Set();
        this.selectedRows = [];
        // Display that fieldName of the selected rows
        if (selectedRows.length > 0) {
            selectedRows.forEach(selectedRowId => {
                this.selectedSet.add(this.selectedItemMap[selectedRowId]);
                this.preSelectedRows.add(selectedRowId);
            });
        }
        this.selectedRows = Array.from(this.preSelectedRows);
        this.selectedItemCount = this.selectedSet.size + this.selectedItemMessage;
    }
    //Next page
    handleNext() {
        this.hasNext = true;
        this.hasSave = true;
        this.isNext = false;
    }
    handleBack() {
        this.hasNext = false;
        this.hasSave = false;
        this.isNext = true;
    }
    handleRwAll(event) {
        //event.preventDefault();
        let selectedId = event.currentTarget.getAttribute("data-id");
        this.rwChecked = false;
        if (event.target.checked) {
            this.selectedSet.forEach(item => {
                this.rwChecked = true;
                item.rwChecked = true;
            });
        } else if (!event.target.checked) {
            this.selectedSet.forEach(item => {
                item.rwChecked = false;
                item.rwChecked = false;
            });
        }
        //console.log('selectedItemList RwAll :' + JSON.stringify(Array.from(this.selectedSet)));
    }
    handleRWCheck(event) {
        //event.preventDefault();
        let selectedId = event.currentTarget.getAttribute("data-id");
        this.rwChecked = false;
        if (event.target.checked) {
            this.selectedSet.forEach(item => {
                if (item.value === selectedId) {
                    item.rwChecked = true;
                }
            });
        } else if (!event.target.checked) {
            this.selectedSet.forEach(item => {
                if (item.value === selectedId) {
                    item.rwChecked = false;
                }
            });
        }
        //console.log('selectedItemList handleRWCheck :' + JSON.stringify(Array.from(this.selectedSet)));
    }
    handleSave() {
        let sObjList = [];
        this.selectedSet.forEach(sObj => {
            if (sObj.rwChecked) {
                sObjList.push({
                    musqotmp__MarketingPlan__c: this.recordId,
                    musqotmp__User__c: sObj.value,
                    musqotmp__PlanAccess__c: 'Read Write',
                });
            } else {
                sObjList.push({
                    musqotmp__MarketingPlan__c: this.recordId,
                    musqotmp__User__c: sObj.value,
                    musqotmp__PlanAccess__c: 'Read Only',
                });
            }
        })
        CreateRecords({ sObjectList: sObjList })
            .then(record => {
                this.record = record;
                this.handleDialogClose();
            })
            .catch(error => {
                this.template.querySelector("c-toast").showToast(error);
            })
    }
    handleDialogClose() {
        /*this.showModal = false;
        const v = this.showModal;
        const onClickEvent = new CustomEvent('show', {
            detail: { v },
        });
        //firing the event.
        this.dispatchEvent(onClickEvent);*/

        this.selectedSet = new Set();
        this.selectedRows = [];
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: 'musqotmp__MarketingPlan__c',
                actionName: 'view'
            }
        });
    }
    handleColumnSort(event) {
        // field name
        this.sortBy = event.detail.sortBy;
        // sort direction
        this.sortDirection = event.detail.sortDirection;
        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.sortBy, event.detail.sortDirection);
    }
    sortData(fieldname, direction) {
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.data));
        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };
        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1 : -1;
        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        // set the sorted data to data table data
        this.data = parseData;
    }
    get getShowNext() {
        return this.selectedSet.size > 0 ? this.showNext = true : this.showNext = false;
    }
    handleSort(event) {
        //this.handleSortData(event.currentTarget.dataset.id);
    }
    handleSortData(sortColumnName) {
        let data = Array.from(this.selectedSet);
        //console.log('data sort:' + JSON.stringify(data));
        // check previous column and direction
        if (this.sortedColumn === sortColumnName) {
            this.sortedDirection = this.sortedDirection === 'asc' ? 'desc' : 'asc';
        }
        else {
            this.sortedDirection = 'asc';
        }
        // check arrow direction
        if (this.sortedDirection === 'asc') {
            this.isAsc = true;
            this.isDsc = false;
        }
        else {
            this.isAsc = false;
            this.isDsc = true;
        }
        // check reverse direction
        let isReverse = this.sortedDirection === 'asc' ? 1 : -1;
        this.sortedColumn = sortColumnName;
        // sort the data
        this.selectedSet = JSON.parse(JSON.stringify(this.selectedSet)).sort((a, b) => {
            a = a[sortColumnName] ? a[sortColumnName].toLowerCase() : ''; // Handle null values
            b = b[sortColumnName] ? b[sortColumnName].toLowerCase() : '';
            return a > b ? 1 * isReverse : -1 * isReverse;
        });
    }
}