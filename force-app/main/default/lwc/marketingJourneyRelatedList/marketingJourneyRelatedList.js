import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { refreshApex } from '@salesforce/apex';
import { getRecord, deleteRecord, createRecord, updateRecord } from 'lightning/uiRecordApi';
import datatable from '@salesforce/apex/Campaign.datatable';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import LOCALE from '@salesforce/i18n/locale';
import MOMENT_JS from '@salesforce/resourceUrl/moment';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import { getToast, readCookie } from 'c/lds';
import MARKETING_JOURNEY_OBJECT from '@salesforce/schema/MarketingJourney__c';
import MARKETING_JOURNEY_ID_FIELD from '@salesforce/schema/MarketingJourney__c.Id';
//labels
import msg_Delete from '@salesforce/label/c.msg_Delete';
import modalHeader from '@salesforce/label/c.modalHeader';
import btn_save from '@salesforce/label/c.btn_save';
import btn_New from '@salesforce/label/c.btn_new';
import btn_cancel from '@salesforce/label/c.btn_cancel';
import btn_Delete from '@salesforce/label/c.btn_Delete';
import msg_Record_Delete from '@salesforce/label/c.msg_Record_Delete';
import msg_Record_Saved from '@salesforce/label/c.msg_Record_Saved';

export default class MarketingJourneyRelatedList extends LightningElement {
	label = {
		msg_Delete,
		modalHeader,
		btn_save,
		btn_New,
		btn_cancel,
		btn_Delete,
		msg_Record_Delete,
		msg_Record_Saved,
	};
	@api recordId;
	@track objectApiName = "musqotmp__MarketingJourney__c";
	@track objectInfo;
	@track wiredGetRecord;
	@track header = false;
	@track modalHeader = modalHeader;
	@track content = msg_Delete;
	@track searchKey = "";
	@track data = [];
	@track columns = [];
	@track rowOffset = 0;
	@track tableLoadingState = true;
	@track showRowNumberColumn = false;
	@track sortBy = "LastModifiedDate";
	@track sortDirection = "desc";
	@track sortedBy = "LastModifiedDate";
	@track sortedDirection = "desc";
	@track selectedRows = [];
	@track hideCheckboxColumn = true;
	@track objectTypeLabel = "";
	@track listFields = [];
	@track dataLength;
	@track layoutSections = [];
	@track activeAccordionSections;
	@track sectioncssclass = "slds-modal slds-fade-in-open slds-modal_small";
	@track sectionModalCssClass = "slds-modal slds-fade-in-open slds-modal_medium";
	@track recordFormId;
	@track hasDisabled = false;
	@track journeyIconColor;
	@track showEdit = false;
	@track typeLayoutSections = [];
	@track customGetRecordId;
	@track Country;
	@track Product;
	@track TherapeuticArea;
	@track countryCode;
    @track portfolioCommercial;
    @track productCommercial;
    @track productCodeCommercial;
    @track portfolioMedical;
    @track productMedical;
    @track productCodeMedical;
	@track TacticalCampaignType;
	@track TacticalCamID;
	


	wiredData;

	connectedCallback() {
		//this.recordId = this.recordId;
		//this.objectApiName = this.objectApiName;
		this.paramDataTable = this.buildParamDataTable();
	}
	buildTypeLayoutSectionWithMoment(layoutSections, wiredGetRecord) {
		loadScript(this, MOMENT_JS)
			.then(() => {
				this.moment;
				this.buildTypeLayoutSections(layoutSections, wiredGetRecord);
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	//object info
	@wire(getObjectInfo, { objectApiName: MARKETING_JOURNEY_OBJECT })
	wiredObjectInfo({ data, error }) {
		if (data) {
			this.objectInfo = data;
			this.header = true;
			this.journeyIconColor = "background-color: #" + this.objectInfo.themeInfo.color;
		//	console.log("recordId :" + this.recordId);
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}
	getJourneyIconColor() {
		return "background-color: #" + this.objectInfo.themeInfo.color;
	}
	buildParamDataTable() {
		const whereCaluses = [];
		let fieldSetApiName = "musqotmp__MarketingJourneyRelatedList";
		whereCaluses.push({
			field: "musqotmp__Campaign__c",
			value: this.recordId,
			clause: "=",
		});
		const rowLevelactions = [
			{ label: "Edit", name: "edit", iconName: "utility:edit" },
			{ label: "Delete", name: "delete", iconName: "utility:delete" },
		];
		return JSON.stringify({
			component: "MarketingActivityRelatedList",
			relatedListType: "finance",
			objectApiName: MARKETING_JOURNEY_OBJECT.objectApiName,
			fieldMap: {
				[MARKETING_JOURNEY_ID_FIELD.fieldApiName]: {},
			},
			whereCaluses: whereCaluses,
			fieldSetApiName: fieldSetApiName,
			linkableNameField: true,
			sortBy: this.sortBy,
			sortDirection: this.sortDirection,
			rowLevelactions: rowLevelactions,
		});
	}
	//generating datatable
	@wire(datatable, { param: "$paramDataTable" })
	wiredGetDatatable(result) {
		this.wiredData = result;
		if (result.data) {
			if (result.data.toast.ok) {
				this.dataLength = result.data.datatable.data.length;
				this.sortedBy = this.sortBy;
				this.sortedDirection = this.sortDirection;
				this.columns = result.data.datatable.columns;
				let colList = [];
				this.columns.forEach((column) => {
					let obj = Object.assign({}, column);
					if (obj.type === "date") {
						obj.type = "string";
					}
					colList.push(obj);
				});
				this.columns = colList;
				//console.log('columns :' + JSON.stringify(this.columns));
				this.data = result.data.datatable.data;
				this.listFields = result.data.fields;
				this.objectTypeLabel = result.data.objectTypeLabel;
				this.template.querySelector("c-toast").showToast(result.data.toast);
			} else {
				this.data = undefined;
				this.columns = undefined;
				this.template.querySelector("c-toast").showToast(result.data.toast);
			}
		} else if (result.error) {
			this.data = undefined;
			this.columns = undefined;
			this.template.querySelector("c-toast").showToast(result.error);
		}
		this.tableLoadingState = false;
	}
	handleSelect(event) {
		this.recordFormId = event.detail.id;
		if (event.detail.actionName === "edit") {
			const modal = this.template.querySelector("c-record-form-modal");
			modal.show();
		} else {
			const modal = this.template.querySelector("c-modal");
			modal.show();
		}
		this.modalHeader = event.detail.actionLabel + " " + this.objectTypeLabel;
	}
	handleColumnSort(event) {
		this.sortBy = event.detail.sortBy;
		this.sortDirection = event.detail.sortDirection;
		this.paramDataTable = this.buildParamDataTable();
	}
	// parent record data
	@wire(getRecord, { recordId: "$customGetRecordId", layoutTypes: ["Full"], modes: ["Edit"] })
	wiredGetRecordData({ data, error }) {
		if (data) {
			this.parentCampaignData = data;
			this.wiredGetRecord = data;
			this.buildTypeLayoutSectionWithMoment(this.layoutSections, this.wiredGetRecord);
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}
	@wire(getRecord, { recordId: "$recordFormId", layoutTypes: ["Full"], modes: ["Edit"] })
	wiredGetMJRecord({ data, error }) {
		if (data) {
			this.objData = data;
			this.buildTypeLayoutSectionWithMoment(this.layoutSections, null);
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}
	//loading layout information
	onLoad(event) {
		this.layoutSections = [event.detail.layout.sections[0]];
		this.activeAccordionSections = [this.layoutSections[0].id];
		this.customGetRecordId = this.recordId;
		//this.buildTypeLayoutSections(this.layoutSections, null);
	}
	// creating new action
	buildTypeLayoutSections(layoutSections, record) {
		this.typeLayoutSections = [];
		this.editTypeLayoutSections = [];
		this.parentValueMap = {};
		let parentStartDate;
		let rightNow = new Date();
		let yyyyMmDd = rightNow.toISOString().slice(0, 10);
		let todayDate = moment(yyyyMmDd).format("YYYY-MM-DD");
		layoutSections.forEach((layoutSection) => {
			let typeLayoutSection = {};
			typeLayoutSection.id = layoutSection.id;
			typeLayoutSection.heading = layoutSection.heading;
			let layoutRows = [];
			layoutSection.layoutRows.forEach((layoutRow) => {
				let typeLayoutRows = {};
				let layoutItems = [];
				layoutRow.layoutItems.forEach((layoutItem) => {
					let typeLayoutItems = {};
					let layoutComponents = [];
					layoutItem.layoutComponents.forEach((layoutComponent) => {
						let typeLayoutComponent = {};
						typeLayoutComponent.apiName = layoutComponent.apiName;
						if (!this.recordFormId) {
							if (layoutComponent.apiName === "musqotmp__StartDate__c" && record !== null) {
								typeLayoutComponent.label = layoutComponent.label;
								parentStartDate = record.fields.StartDate.value;
							//	console.log("parentStartDate :" + parentStartDate);
								typeLayoutComponent["dateFormate"] = true;
								if (parentStartDate > todayDate) {
									typeLayoutComponent.value = parentStartDate;
									this.stDate = parentStartDate;
								} else {
									typeLayoutComponent.value = todayDate;
									this.stDate = todayDate;
								}
							} else if (layoutComponent.apiName === "musqotmp__EndDate__c" && record !== null) {
								let parentEndDate = record.fields.EndDate.value;
								typeLayoutComponent.label = layoutComponent.label;
								typeLayoutComponent["dateFormate"] = true;
								if (parentStartDate > todayDate) {
									typeLayoutComponent.value = parentEndDate;
									this.enDate = parentEndDate;
								} else {
									const endDate = moment(yyyyMmDd).add(7, "days").format("YYYY-MM-DD");
									typeLayoutComponent.value = endDate;
									this.enDate = endDate;
								}
								//console.log('enDate :' + this.enDate);
							} else if (layoutComponent.apiName === 'musqotmp__Country__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Country__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Country__c.value;
								//this.Country = record.fields.musqotmp__Country__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Country_Code__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Country_Code__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Country_Code__c.value;
								//this.countryCode = record.fields.musqotmp__Country_Code__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Therapeutic_Area__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Therapeutic_Area__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Therapeutic_Area__c.value;
								//this.TherapeuticArea = record.fields.musqotmp__Therapeutic_Area__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Portfolio_Commercial__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Portfolio_Commercial__c.value;
								//this.portfolioCommercial = record.fields.musqotmp__Portfolio_Commercial__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Portfolio_Commercial__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Product_Commercial__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Product_Commercial__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Product_Commercial__c.value;
								//this.productCommercial = record.fields.musqotmp__Product_Commercial__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Product_Code_Commercial__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Product_Code_Commercial__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Product_Code_Commercial__c.value;
								//this.productCodeCommercial = record.fields.musqotmp__Product_Code_Commercial__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Portfolio_Medical__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Portfolio_Medical__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Portfolio_Medical__c.value;
								//this.portfolioMedical = record.fields.musqotmp__Portfolio_Medical__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Product_Medical__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Product_Medical__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Product_Medical__c.value;
								//this.productMedical = record.fields.musqotmp__Product_Medical__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Product_Code_Medical__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Product_Code_Medical__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Product_Code_Medical__c.value;
								//this.productCodeMedical = record.fields.musqotmp__Product_Code_Medical__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__TC_ID__c') {
								typeLayoutComponent.value = record.fields.musqotmp__TC_ID_text__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__TC_ID_text__c.value;
								//this.TacticalCamID = record.fields.musqotmp__TC_ID__c.value;
							} else if (layoutComponent.apiName === 'musqotmp__Tactical_Campaign_Type__c') {
								typeLayoutComponent.value = record.fields.musqotmp__Tactical_Campaign_Type__c.value;
								this.parentValueMap[layoutComponent.apiName] = record.fields.musqotmp__Tactical_Campaign_Type__c.value;
								//this.TacticalCampaignType = record.fields.musqotmp__Tactical_Campaign_Type__c.value;
							} else if (record.fields[layoutComponent.apiName] && record !== null) {
								typeLayoutComponent.value = record.fields[layoutComponent.apiName].value;
							}
						} else {
							if (layoutComponent.apiName === "musqotmp__StartDate__c" && this.objData !== null) {
								typeLayoutComponent["dateFormate"] = true;
								typeLayoutComponent.label = layoutComponent.label;
								typeLayoutComponent.value = this.objData.fields.musqotmp__StartDate__c.value;
							} else if (layoutComponent.apiName === "musqotmp__EndDate__c" && this.objData !== null) {
								typeLayoutComponent.label = layoutComponent.label;
								typeLayoutComponent["dateFormate"] = true;
								typeLayoutComponent.value = this.objData.fields.musqotmp__EndDate__c.value;
							} else {
								if (this.objData !== null && this.objData.fields[layoutComponent.apiName]) typeLayoutComponent.value = this.objData.fields[layoutComponent.apiName].value;
							}
						}
						if (this.objectApiName === "musqotmp__MarketingJourney__c" && layoutComponent.apiName === "musqotmp__Campaign__c") {
							typeLayoutComponent.value = this.recordId;
						} else if (record !== null && layoutComponent.apiName !== null && layoutComponent.apiName !== "Name" && record.fields[layoutComponent.apiName]) {
							//console.log('layoutComponent.apiName :' + layoutComponent.apiName);
							//typeLayoutComponent.value = record.fields[layoutComponent.apiName].value;
						}
						layoutComponents.push(typeLayoutComponent);
					});
					typeLayoutItems.layoutComponents = layoutComponents;
					layoutItems.push(typeLayoutItems);
				});
				typeLayoutRows.layoutItems = layoutItems;
				layoutRows.push(typeLayoutRows);
			});
			typeLayoutSection.layoutRows = layoutRows;
			this.typeLayoutSections.push(typeLayoutSection);
			if (this.recordFormId) {
				this.editTypeLayoutSections.push(typeLayoutSection);
				this.showEdit = true;
			}
			//console.log('this.typeLayoutSections :' + JSON.stringify(this.typeLayoutSections));
			//console.log('this.editTypeLayoutSections :' + JSON.stringify(this.editTypeLayoutSections));
			this.typeLayoutSectionsCopy = Array.from(this.typeLayoutSections);
		});
	}
	handleNew() {
		this.handleReset();
		this.recordFormId = null;
		this.modalHeader = "New " + this.objectTypeLabel;
		const modal = this.template.querySelector("c-record-form-modal");
		modal.show();
	}
	handleSubmit(event) {
		event.preventDefault(); // stop the form from submitting
		const fields = event.detail.fields;
		let stDate = new Date(this.template.querySelector("[data-field='musqotmp__StartDate__c']").value);
		let yyyyMmDd = stDate.toISOString().slice(0, 10);
		let StartDate = moment(yyyyMmDd).format("YYYY-MM-DD");
		//console.log('StartDate :' + StartDate);
		let enDate = new Date(this.template.querySelector("[data-field='musqotmp__EndDate__c']").value);
		let endyyyyMmDd = enDate.toISOString().slice(0, 10);
		let endDate = moment(endyyyyMmDd).format("YYYY-MM-DD");
		//console.log('endDate :' + endDate);
		fields["musqotmp__StartDate__c"] = StartDate;
		fields["musqotmp__EndDate__c"] = endDate;
		this.stDate = this.template.querySelector("[data-field='musqotmp__StartDate__c']").value;
		this.enDate = this.template.querySelector("[data-field='musqotmp__EndDate__c']").value;
		this.hasDisabled = true;
		let valid = true;
		let validateMessage = "These Required fields must be completed: ";
		let validateField = "";
		if (!fields.Name) {
			validateField = " " + this.objectInfo.fields.Name.label + ",";
			valid = false;
			validateMessage += validateField;
		}
		if (!fields.musqotmp__Campaign__c) {
			valid = false;
			validateField = " " + this.objectInfo.fields.musqotmp__Campaign__c.label + ",";
			validateMessage += validateField;
		}
		if (!this.stDate) {
			valid = false;
			validateField = " " + this.objectInfo.fields.musqotmp__StartDate__c.label + ",";
			validateMessage += validateField;
		}
		if (!this.enDate) {
			valid = false;
			validateField = " " + this.objectInfo.fields.musqotmp__EndDate__c.label + ",";
			validateMessage += validateField;
		}
		//console.log('fields :' + JSON.stringify(fields));
		if (valid) {
			if (this.recordFormId) {
				fields["Id"] = this.recordFormId;
				this.updateRecord(fields);
				//this.template.querySelector('lightning-record-edit-form').submit(fields);
			} else {
				this.createRecord(fields);
			}
		} else {
			this.hasDisabled = false;
			validateMessage = validateMessage.substring(0, validateMessage.length - 1);
			const evt = new ShowToastEvent({
				title: "Error",
				message: validateMessage,
				variant: "error",
				mode: "pester",
			});
			this.dispatchEvent(evt);
		}
	}
	createRecord(fields) {
		const recordInput = { apiName: MARKETING_JOURNEY_OBJECT.objectApiName, fields };
		createRecord(recordInput)
			.then((record) => {
				this.recordFormId = record.id;
				this.hasDisabled = false;
				this.handleCancelRecordFormModal();
				const toast = getToast("success", "pester", this.label.msg_Record_Saved);
				this.template.querySelector("c-toast").showToast(toast);
			})
			.catch((error) => {
				let customErrorField = error.body.output.errors;
				if (customErrorField) {
					for (let i = 0; i < customErrorField.length; i++) {
						const evt = new ShowToastEvent({
							title: "Error",
							message: customErrorField[i].message,
							variant: "error",
							mode: "sticky",
						});
						this.dispatchEvent(evt);
					}
				} else {
					this.template.querySelector("c-toast").showToast(error);
				}
			});
	}
	updateRecord(fields) {
		const recordInput = { fields };
		updateRecord(recordInput)
			.then((record) => {
				this.recordFormId = record.id;
				this.hasDisabled = false;
				const toast = getToast("success", "pester", this.label.msg_Record_Saved);
				this.template.querySelector("c-toast").showToast(toast);
				this.handleCancelRecordFormModal();
			})
			.catch((error) => {
				let customErrorField = error.body.output.errors;
				if (customErrorField) {
					for (let i = 0; i < customErrorField.length; i++) {
						const evt = new ShowToastEvent({
							title: "Error",
							message: customErrorField[i].message,
							variant: "error",
							mode: "sticky",
						});
						this.dispatchEvent(evt);
					}
				} else {
					this.template.querySelector("c-toast").showToast(error);
				}
			});
	}
	//reset the form
	handleReset() {
		let parent = this.template.querySelector("[data-field='musqotmp__Campaign__c']");
		if (parent) {
			parent = parent.value;
		}
		const inputFields = this.template.querySelectorAll("lightning-input-field");
		if (inputFields.length > 0) {
			if (inputFields) {
				inputFields.forEach((field) => {
					field.reset();
				});
			}
			if (this.template.querySelector("[data-field='musqotmp__Campaign__c']")) this.template.querySelector("[data-field='musqotmp__Campaign__c']").value = parent || this.recordId;
			if (this.template.querySelector("[data-field='musqotmp__StartDate__c']")) this.template.querySelector("[data-field='musqotmp__StartDate__c']").value = this.stDate;
			if (this.template.querySelector("[data-field='musqotmp__EndDate__c']")) this.template.querySelector("[data-field='musqotmp__EndDate__c']").value = this.enDate;
			//Changes for Bayer
			if(this.parentValueMap['musqotmp__Country__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Country__c']").value = this.parentValueMap['musqotmp__Country__c'];}
		    if(this.parentValueMap['musqotmp__Country_Code__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Country_Code__c']").value = this.parentValueMap['musqotmp__Country_Code__c'];}
			if(this.parentValueMap['musqotmp__Therapeutic_Area__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Therapeutic_Area__c']").value = this.parentValueMap['musqotmp__Therapeutic_Area__c'];}
			if(this.parentValueMap['musqotmp__Portfolio_Commercial__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Portfolio_Commercial__c']").value = this.parentValueMap['musqotmp__Portfolio_Commercial__c'];}
			if(this.parentValueMap['musqotmp__Portfolio_Medical__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Portfolio_Medical__c']").value = this.parentValueMap['musqotmp__Portfolio_Medical__c'];}
			if(this.parentValueMap['musqotmp__Product_Commercial__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Product_Commercial__c']").value = this.parentValueMap['musqotmp__Product_Commercial__c'];}
			if(this.parentValueMap['musqotmp__Product_Medical__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Product_Medical__c']").value = this.parentValueMap['musqotmp__Product_Medical__c'];}
			if(this.parentValueMap['musqotmp__Product_Code_Commercial__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Product_Code_Commercial__c']").value = this.parentValueMap['musqotmp__Product_Code_Commercial__c'];}
			if(this.parentValueMap['musqotmp__Product_Code_Medical__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Product_Code_Medical__c']").value = this.parentValueMap['musqotmp__Product_Code_Medical__c'];}
            if(this.parentValueMap['musqotmp__TC_ID__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__TC_ID__c']").value = this.parentValueMap['musqotmp__TC_ID__c'];}
            if(this.parentValueMap['musqotmp__Tactical_Campaign_Type__c'] != undefined){	this.template.querySelector("[data-field='musqotmp__Tactical_Campaign_Type__c']").value = this.parentValueMap['musqotmp__Tactical_Campaign_Type__c'];}

			let excludeField = ["Name"];
			if (this.wiredGetRecord) {
				Object.keys(this.wiredGetRecord.fields).forEach((field) => {
					if (this.template.querySelector("[data-field='" + field + "']") && !excludeField.includes(field)) {
						this.template.querySelector("[data-field='" + field + "']").value = this.wiredGetRecord.fields[field].value;
					}
				});
			}
		}
	}
	//deleting the record
	handleDelete() {
		const recordId = this.recordFormId;
		deleteRecord(recordId)
			.then(() => {
				const toast = getToast("success", "pester", this.label.msg_Record_Delete);
				this.template.querySelector("c-toast").showToast(toast);
				this.handleCancelModal();
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	handleCancelModal() {
		this.recordFormId = null;
		const modal = this.template.querySelector("c-modal");
		modal.hide();
		this.handleRefresh();
	}
	handleFormChange(event) {
		this.hasDisabled = false;
	}
	cancelModal() {
		this.handleCancelRecordFormModal();
	}
	handleCancelRecordFormModal() {
		this.recordFormId = null;
		const modal = this.template.querySelector("c-record-form-modal");
		this.handleRefresh();
		modal.hide();
		//this.handleReset();
	}
	handleRefresh() {
		this.selectedRows = [];
		if (this.wiredData.data) {
			refreshApex(this.wiredData);
		}
	}
}