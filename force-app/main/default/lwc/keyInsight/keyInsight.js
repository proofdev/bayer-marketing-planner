import { LightningElement, wire, track, api } from "lwc";
import { updateRecord, getRecord } from "lightning/uiRecordApi";
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";

import { loadScript, loadStyle } from "lightning/platformResourceLoader";
import { CurrentPageReference, NavigationMixin } from "lightning/navigation";
import { refreshApex, getSObjectValue } from "@salesforce/apex";
import highchart from "@salesforce/resourceUrl/highchart_8";
import getRecords from "@salesforce/apex/keyInsightDashBoard.getRecords";
import KEYINSIGHTDASHBOARD_OBJECT from "@salesforce/schema/KeyInsightDashboard__c";
import KEYINSIGHTDASHBOARD_ID_FIELD from "@salesforce/schema/KeyInsightDashboard__c.Id";

import putCalloutResponseContents from "@salesforce/apex/keyInsightDashBoard.putCalloutResponseContents";
import getCalloutResponseContentList from "@salesforce/apex/keyInsightDashBoard.getCalloutResponseContentList";
import getCalloutResponseContentMap from "@salesforce/apex/keyInsightDashBoard.getCalloutResponseContentMap";
import parseDataFromJwtToken from "@salesforce/apex/keyInsightDashBoard.parseDataFromJwtToken";
import userId from "@salesforce/user/Id";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import customCSS from "@salesforce/resourceUrl/customCSS";
import plusCircle from "@salesforce/resourceUrl/plusCircle";
import chevronDown from "@salesforce/resourceUrl/chevronDown";
import chevronUp from "@salesforce/resourceUrl/chevronUp";
import readKIDSetupSettings from "@salesforce/apex/keyInsightDashBoard.readKIDSetupSettings";
import 	down_arrow from "@salesforce/resourceUrl/down_arrow";
import up_arrow from "@salesforce/resourceUrl/up_arrow"; 



export default class KeyInsight extends LightningElement {
  @api recordId;
  @track paramRouteToValueGetRecords;
  @track paramMovingValueItemGetRecords;
  @track paramRegressionGetRecords;
  @track wiredRegressionData;
  @track regressionMovingValueList = [];

  @track altHsh = {};
  @track altLabel;
  @track showInputSection = false;
  @track altScenarioHsh = {};
  @track keyInsightData;
  @track routeToValueMap = {};
  activeSections = ["A", "B", "C"];
  @track causes = [];
  @track showCauses = false;
  @track effect = [];
  @track comment = [];
  @track insights = [];
  @track summery;
  @track sfUserId = userId;
  @track proofUserId;
  @track proofOrgId;
  @track proofToken;
  @track proofIdToken;
  //@track proofURL = "https://testapp.get-proof.com";
  @track paramParseDataFromJwtToken;
  @track domainURL;
  @track externalFactorData = [];
  @track assumptionList = [];
  @track newAssumptionList = [];
  @track keyIndex = 0;
  @track showAddAssumption = true;
  @track enableAssumptionEdit = false;
  @track enableAssumptionCancel = false;
  @track enableAssumptionSave = false;
  @track enableAssumptionIcon = false;
  @track assumptionResponceList;
  @track kids = [];
  @track plusCircle = plusCircle;
  @track chevronDown = chevronDown;
  @track chevronUp = chevronUp;
  @track proofURL;
  @track causesStatus = false;
  @track downarrow = down_arrow;
  @track totalcauses;
  @track remainingCauses = [];
  @track firstCauses = {};
  @track effectName;
  @track upArrow = up_arrow;
  @track dateRange;
  @track frequency;
  @track showAssumptionList = true;

  @track enableExFactorSave = false;
  @track enableExFactorEdit = false;
  @track enableExFactorCancel = false;
  @track showExFactorList = false;
  @track exFactorList = [];
  @track showAddExFactor = false;
  @track showExternalFactorData = false
  renderedCallback() {
    Promise.all([loadStyle(this, customCSS + "/keyInsight.css")])
      .then(() => {
        //console.log("Files loaded");
      })
      .catch((e) => {
        //console.log(e);
      });
  }
  handleAssumptionAccordion() {
    this.showAssumptionList = this.showAssumptionList ? false : true;
    if (this.showAssumptionList) {
      this.template.querySelector(".rectengle-header").classList.add("padding-top-zero");
    } else {
      this.template.querySelector(".rectengle-header").classList.remove("padding-top-zero");
    }
  }
  handleExFactorAccordion() {
    this.showExFactorList = this.showExFactorList ? false : true;
    if (this.showExFactorList) {
      //this.template.querySelector(".rectengle-header-ExFactor").classList.add("padding-top-zero");
    } else {
      //this.template.querySelector(".rectengle-header-ExFactor").classList.remove("padding-top-zero");
    }
  }
  addRow() {
    this.enableAssumptionSave = true;
    this.enableAssumptionEdit = false;
    this.enableAssumptionCancel = true;
    //this.enableAssumptionIcon = false;
    this.keyIndex += 1;
    this.showAddAssumption = true;
    let assumptionList = this.newAssumptionList;
    assumptionList.push({ assumption: "", value: "", keyInsightDashboardId: this.kid });
    this.newAssumptionList = assumptionList;
    if (this.keyIndex >= 4) {
      this.showAddAssumption = false;
    }
  }
  changeHandler(event) {
    this.newAssumptionList[event.target.accessKey].assumption = event.target.value;
    this.newAssumptionList[event.target.accessKey].value = event.target.value;
  }
  handleSaveAssumption() {
    let param;
    let newArr = [];
    if(this.newAssumptionList.length>0){
      this.newAssumptionList.forEach((ele) => {
        delete ele["value"];
        newArr.push(ele);
      });
    }
    let bodydata = JSON.stringify({
      deleteResourceIds: this.kids,
      keyInsightAssumptions: newArr,
      keyInsightDashboardId: this.kid,
    });
    param = JSON.stringify({
      URL: this.domainURL + "/assumptions?resource_type=KEY_INSIGHT_DASHBOARD",
      Body: bodydata,
      Authorization: this.proofIdToken,
    });
    this.saveAssumption(param);
  }
  saveAssumption(param) {
    putCalloutResponseContents({ param: param })
      .then((res) => {
        if (res.resultMap) {
          let varient = 'error';
          if(res.resultMap.code == 200){
            varient = 'success';
          }
          const event = new ShowToastEvent({
            //	title: 'Get Help',
            message: res.resultMap.description,
            variant: varient,
          });
          this.dispatchEvent(event);
          this.newAssumptionList = [];
          this.keyIndex = 0;
          this.enableAssumptionSave = false;
          this.enableAssumptionEdit = true;
          this.enableAssumptionCancel = false;
        }
        let param = JSON.stringify({
          URL: this.domainURL + "/assumptions?resource_type=KEY_INSIGHT_DASHBOARD",
          Authorization: this.proofIdToken,
        });
        this.getAssumption(param);
      })
      .catch((error) => {
        this.template.querySelector("c-toast").showToast(error);
      });
  }

  @wire(parseDataFromJwtToken, { param: "$paramParseDataFromJwtToken" })
  wiredParseDataFromJwtToken(result) {
    if (result.data) {
      let proofAuthMap = result.data.proofAuthMap;
      this.proofToken = result.data.proofToken;
      this.proofIdToken = result.data.proofIdToken;
      this.proofOrgId = result.data.proofAuthMap.orgId;
      this.proofUserId = result.data.proofAuthMap.sub.split("|")[1];
      this.domainURL = this.proofURL + "/api/organisations/" + this.proofOrgId + "/users/" + this.proofUserId + "/key_insight_dashboard/" + this.kid;
      this.getExternalFactor();
      let param = JSON.stringify({
        URL: this.domainURL + "/assumptions?resource_type=KEY_INSIGHT_DASHBOARD",
        Authorization: this.proofIdToken,
      });
      this.getAssumption(param);
      this.getResponseContents();
    }
  }
  @wire(getRecord, { recordId: "$recordId", layoutTypes: ["Full"], modes: ["Edit"] })
  wiredGetRecordData({ data, error }) {
    if (data) {
      this.kid = data.fields.musqotmp__keyInsightDashboardId__c.value;
      this.paramParseDataFromJwtToken = JSON.stringify({
        recordId: this.sfUserId,
      });
    } else if (error) {
      this.template.querySelector("c-toast").showToast(error);
    }
  }
  connectedCallback() {
    this.getKIDSetupSettings();
  }

  getKIDSetupSettings() {
    let paramdata = JSON.stringify({ Param: "TestParam" });
    readKIDSetupSettings({ paramdata }).then((res) => {
      //console.log("res" + res);
      this.KIDSettings = res;
      this.proofURL = this.KIDSettings.sObj.musqotmp__proofURL__c;
    });
  }
  /*connectedCallback() {
    //this.getAssumptions();
    //	this.getExternalFactor();
  }*/
  getExternalFactor() {
    let paramdata = JSON.stringify({
      URL: this.domainURL + "/external_factor?resource_type=KEY_INSIGHT_DASHBOARD",
      Authorization: this.proofIdToken,
    });
    getCalloutResponseContentMap({ param: paramdata }).then((res) => {
      this.externalFactorData = res.resultMap;
      //	console.log("getExternalFactor === " + JSON.stringify(this.externalFactorData));
    });
  }
  getAssumption(param) {
    //	console.log("Param data for Assumption == "+JSON.stringify(param));
    getCalloutResponseContentList({ param: param }).then((res) => {
      this.assumptionResponceList = res;
      this.enableAssumptionEdit = false;
      if (this.assumptionResponceList.resultList && this.assumptionResponceList.resultList.length > 0) {
        this.enableAssumptionEdit = true;
      }
      this.handleCancelAssumption();
    });
  }
  handleDeleteAssumptions(event) {
    //console.log(event.target.value);
    let index = event.target.value;
    let assumption = this.assumptionList[index];
    this.kids.push(assumption.keyInsightAssumptionId);
    if (index > -1) {
      this.assumptionList.splice(index, 1);
    }
  }
  handleEditAssumption(event) {
    this.enableAssumptionEdit = false;
    this.enableAssumptionSave = true;
    this.enableAssumptionCancel = true;
    this.enableAssumptionIcon = true;
  }
  handleCancelAssumption() {
    //this.enableAssumptionEdit = false;
    this.enableAssumptionEdit = true;
    this.enableAssumptionCancel = false;
    this.enableAssumptionSave = false;
    this.enableAssumptionIcon = false;
    this.newAssumptionList = [];
    this.kids = [];

    this.assumptionList = this.assumptionResponceList && this.assumptionResponceList.resultList.map((obj, index) => ({
      ...obj,
      index: index + 1,
    }));
    this.keyIndex = this.assumptionList.length - 1;
    if (this.assumptionList.length == 0) {
      this.enableAssumptionEdit = false;
    }
    if (this.keyIndex >= 4) {
      this.showAddAssumption = false;
    } else {
      this.showAddAssumption = true;
    }
  }
  getResponseContents() {
    let param = JSON.stringify({
      URL: this.domainURL + "?resource_type=KEY_INSIGHT_DASHBOARD",
      Authorization: this.proofIdToken,
    });
    getCalloutResponseContentMap({ param: param }).then((res) => {
      //console.log("getResponseContents for  Key Insights right sight === " + JSON.stringify(res));
      let arrCauses = [];
      let arrEffect = [];
      if (res.resultMap) {
        this.comment = res.resultMap.comment;
        this.insights = res.resultMap.causes;
        this.firstCauses = res.resultMap.causes[0];
        this.totalcauses = res.resultMap.causes.length;
        this.remainingCauses = res.resultMap.causes.slice(1);
        this.effectName = res.resultMap.effect.name;
        if (res.resultMap.causes) {
          res.resultMap.causes.forEach((ele, i) => {
            let sTree = {};
            sTree["label"] = ele.name;
            sTree["name"] = i + 1;
            sTree["expanded"] = true;
            sTree["items"] = [{}];
            arrCauses.push(sTree);
          });
        }
        this.causes = arrCauses;
        if (res.resultMap.effect) {
          let sTree = {};
          sTree["label"] = res.resultMap.effect.name;
          sTree["expanded"] = false;
          sTree["items"] = [{}];
          arrEffect.push(sTree);
        }
      }
      this.effect = arrEffect;

      this.showCauses = true;
      this.summery = res.resultMap;
      if (this.summery) {
        //this.dateRange = this.summery.startDate + " — " + this.summery.endDate;
        if (this.summery.frequency == "MONTHLY") {
          /*let startdate = this.summery.startDate;
          let endDate = this.summery.endDate;
          let startYear = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(startdate);
          let startMonth = new Intl.DateTimeFormat('en', { month: 'short' }).format(startdate);
          let endYear = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(endDate);
          let endMonth = new Intl.DateTimeFormat('en', { month: 'short' }).format(endDate);
          this.dateRange = startMonth + "—" + startYear +" — "+ endMonth + "—" + endYear;*/
          this.dateRange = this.summery.startDate.substring(3, 11) + " - " + this.summery.endDate.substring(3, 11);
        } else {
          this.dateRange = this.summery.startDate + " - " + this.summery.endDate;
        }
        if (this.summery.frequency == "MONTHLY") {
          this.frequency = "Monthly";
        } else if (this.summery.frequency == "QUARTERLY") {
          this.frequency = "Quarterly";
        } else if (this.summery.frequency == "WEEKLY") {
          this.frequency = "Weekly";
        } else if (this.summery.frequency == "DAILY") {
          this.frequency = "Daily";
        }
      }
      //this.effect = res.resultMap.effect;
    });
  }

  changeStatusTrue() {
    this.causesStatus = false;
  }
  causesStatusFalse() {
    this.causesStatus = true;
  }
}