import { LightningElement, track, wire, api } from 'lwc';
import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
import { CurrentPageReference } from 'lightning/navigation';

import DATA_VARIABLE_OBJECT from '@salesforce/schema/DataVariable__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { updateRecord, getRecord } from 'lightning/uiRecordApi';

export default class DataVariableForm extends NavigationMixin(LightningElement) {
	@track modalTitle;
	@track displayMessage = '';
	@track showModal = false;
	@track wireObjectInfo;
	@track layoutSections;
	@track typeLayoutSections = [];
	@track activeAccordionSections = [];
	@track recordFormId;
	@track showModal = false;
	@track showForm = false;
	@track barColors;
	@track barColorList = [];
	@track dataVariableId;
	@track recordId = '';

	@wire(CurrentPageReference) currentPageReference;
	get recordIdFromState() {
		return this.currentPageReference && this.currentPageReference.attributes.recordId;
	}

	renderedCallback() {
		if (this.getRecordData && this.showModal) {
			const colorCnt = this.template.querySelector('.colors-cnt');
			if (colorCnt) {
				colorCnt.innerText = '';
				this.barColors.split(',').forEach((element) => {
					var div = document.createElement('div');
					div.classList.add('square');
					div.style.backgroundColor = element;
					div.style.height = '20px';
					div.style.width = '20px';
					div.style.float = 'left';
					div.style.marginLeft = '5px';
					colorCnt.appendChild(div);
				});
			} 
		}
	}
	@wire(getObjectInfo, { objectApiName: DATA_VARIABLE_OBJECT })
	wiredData(result) {
		if (result.data) {
			this.wireObjectInfo = result.data;
			this.showModal = true;
		}
	}
	@wire(getRecord, { recordId: '$recordId', layoutTypes: [ 'Full' ], modes: [ 'View' ] })
	wiredGetRecord(result) {
		if (result.data) {
			this.getRecordData = result.data;
			this.barColors = result.data.fields.musqotmp__BarColor__c.value;
			this.buildFormData();
			//this.showModal = true;
		}
	}
	handleError(event){
		this.dataVariableId = this.recordId;
		//this.navigateToRecordEditPage();
	}
	handleSubmit(event) {
		event.preventDefault(); // stop the form from submitting
		const fields = event.detail.fields;
		this.template.querySelector('lightning-record-edit-form.dataVariableForm').submit(fields);
	}
	handleSuccess(event) {
		this.dataVariableId = event.detail.id;
		this.navigateToRecordEditPage();
	}
	navigateToRecordEditPage() {
		// Opens the Account record modal
		// to view a particular record.
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: this.dataVariableId,
				objectApiName: this.wireObjectInfo.apiName, // objectApiName is optional
				actionName: 'view'
			}
		});
	}
	onEditFormLoad(event) {
		this.recordId = this.recordIdFromState;
		this.layoutSections = event.detail.layout.sections;
		if(this.recordId == undefined || this.getRecordData){
			this.buildFormData();
		}

	}
	buildFormData(){
		if (this.getRecordData) {
			this.modalTitle = 'Edit ' + this.getRecordData.fields.Name.value;
		} else {
			this.modalTitle = 'New ' + this.wireObjectInfo.label;
		}
		//If The changed any value in plan, that'll reflect in campaign without reloading the page
		this.wiredGetRecord = [];
		this.buildTypeLayoutSections(this.layoutSections, this.wiredGetRecord);
	}
	onHandleSave(event) {
		event.preventDefault();
		this.template.querySelector('.saverecord').click();
		/*const dataVariableForm = this.template.querySelector('lightning-record-edit-form.dataVariableForm');
		if(dataVariableForm)
			dataVariableForm.submit();*/
	}
	onHandleSaveNew(event) {
		const inputFields = this.template.querySelectorAll('lightning-input-field');
		if (inputFields) {
			inputFields.forEach((field) => {
				field.reset();
			});
		}
		//refreshApex(this.wireRecordType);
	}
	handleChangedBarColorInput(event) {
		this.barColors = event.target.value;
		/*this.barColorList = this.barColors.split(',').map(function(e) {
			return 'background-color:' + e;
		});*/
		const colorCnt = this.template.querySelector('.colors-cnt');
		colorCnt.innerText = '';
		this.barColors.split(',').forEach((element) => {
			var div = document.createElement('div');
			div.classList.add('square');
			div.style.backgroundColor = element;
			div.style.height = '20px';
			div.style.width = '20px';
			div.style.float = 'left';
			div.style.marginLeft = '5px';
			colorCnt.appendChild(div);
		});
	}
	handleChangedBarColor(event) {
		this.barColor = event.target.value;
		if (this.barColors) {
			this.barColors += ',' + this.barColor;
		} else {
			this.barColors = this.barColor;
		}
		/*this.barColorList = this.barColors.split(',').map(function(e) {
			return 'background-color:' + e;
		});*/
		const colorCnt = this.template.querySelector('.colors-cnt');
		colorCnt.innerText = '';
		this.barColors.split(',').forEach((element) => {
			var div = document.createElement('div');
			div.classList.add('square');
			div.style.backgroundColor = element;
			div.style.height = '20px';
			div.style.width = '20px';
			div.style.float = 'left';
			div.style.marginLeft = '5px';
			colorCnt.appendChild(div);
		});
	}
	getBackgroundColor(value) {
		return 'background-color:' + value;
	}
	updateColors(event) {
		this.barColors = event.target.value;
	}
	buildTypeLayoutSections(layoutSections, record) {
	//	window.console.log(this.objectApiName);
	//	window.console.log(JSON.stringify(this.wireData));

	//	window.console.log(JSON.stringify(this.propertyOrFunction));
		//refreshApex(this.relatedListObject);
		this.typeLayoutSections = [];
		layoutSections.forEach((layoutSection) => {
			let typeLayoutSection = {};
			typeLayoutSection['id'] = layoutSection.id;
			typeLayoutSection['heading'] = layoutSection.heading;
			let layoutRows = [];
			layoutSection.layoutRows.forEach((layoutRow) => {
				let typeLayoutRows = {};
				let layoutItems = [];
				layoutRow.layoutItems.forEach((layoutItem) => {
					let typeLayoutItems = {};
					let layoutComponents = [];
					layoutItem.layoutComponents.forEach((layoutComponent) => {
						let typeLayoutComponent = {};
						typeLayoutComponent['class'] = '';
						typeLayoutComponent['apiName'] = layoutComponent.apiName;
						typeLayoutComponent['label'] = layoutComponent.label;
						typeLayoutComponent['musqotmp__BarColor__c'] = false;
						if (layoutComponent.apiName == 'musqotmp__BarColor__c') {
							typeLayoutComponent['musqotmp__BarColor__c'] = true;
							typeLayoutComponent['class'] = 'lightning-bar-color-field';
							typeLayoutComponent['variant'] = 'label-hidden';
							//this.barColors = layoutComponent.value;
						}
						layoutComponents.push(typeLayoutComponent);
					});
					typeLayoutItems['layoutComponents'] = layoutComponents;
					typeLayoutItems['required'] = layoutItem.required;
					layoutItems.push(typeLayoutItems);
				});
				typeLayoutRows['layoutItems'] = layoutItems;
				layoutRows.push(typeLayoutRows);
			});
			typeLayoutSection['layoutRows'] = layoutRows;
			this.typeLayoutSections.push(typeLayoutSection);
			this.activeAccordionSections.push(layoutSection.id);
		});
		this.showForm = true;
	}
	get recordTypeId() {
		// Returns a map of record type Ids
		const rtis = this.objectInfo.data.recordTypeInfos;
		return Object.keys(rtis).find((rti) => rtis[rti].name === 'Master');
	}
	save() {
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Success',
				message: 'some message here.',
				variant: 'success'
			})
		);
		//this.dispatchEvent(new CustomEvent('close'));
	}
	cancel() {
		this.dispatchEvent(new CustomEvent('close'));
	}
	closeModal() {
		/*const closeQA = new CustomEvent('close');
		// Dispatches the event.
		this.dispatchEvent(closeQA);*/
		if (this.recordId) {
			this[NavigationMixin.Navigate]({
				type: 'standard__recordPage',
				attributes: {
					recordId: this.recordId,
					objectApiName: this.wireObjectInfo.apiName, // objectApiName is optional
					actionName: 'view'
				}
			});
		} else {
			this[NavigationMixin.Navigate]({
				type: 'standard__objectPage',
				attributes: {
					objectApiName: this.wireObjectInfo.apiName,
					actionName: 'list'
				},
				state: {
					filterName: 'Recent'
				}
			});
		}
		/*var homeEvt = eval("$A.get('e.force:navigateToObjectHome')");
		homeEvt.setParams({
			scope: 'musqotmp__DataVariable__c'
		});
		homeEvt.fire();*/
	}
}