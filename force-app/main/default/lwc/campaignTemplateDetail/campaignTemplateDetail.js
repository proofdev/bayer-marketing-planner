import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD from '@salesforce/schema/CampaignTemplate__c.AcquisitionRetention__c';
import CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD from '@salesforce/schema/CampaignTemplate__c.MoreDealsBiggerDeals__c';
import CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD from '@salesforce/schema/CampaignTemplate__c.RevenueCashflow__c';

import frm_Acquisition_Retention from '@salesforce/label/c.frm_Acquisition_Retention';
import frm_More_Bigger_Deals from '@salesforce/label/c.frm_More_Bigger_Deals';
import frm_Revenue_cashflow from '@salesforce/label/c.frm_Revenue_cashflow';
import msg_Optimizer from '@salesforce/label/c.msg_Optimizer';

const fields = [CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD, CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD, CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD];

export default class CampaignTemplateDetail extends LightningElement {
    label = {frm_Acquisition_Retention,
        frm_More_Bigger_Deals,
        frm_Revenue_cashflow,
        msg_Optimizer      
    };
    @api recordId;
    @api objectApiName;
    @track layoutSections = [];
    @track activeAccordionSections;
    @track optimizerLabel = this.label.msg_Optimizer;

    @wire(getRecord, { recordId: '$recordId', fields })
    wiredCampaignTemplate;

    get acquisitionRetention() {
        return getFieldValue(this.wiredCampaignTemplate.data, CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD);
    }

    get moreDealsBiggerDeals() {
        return getFieldValue(this.wiredCampaignTemplate.data, CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD);
    }

    get revenueCashflow() {
        return getFieldValue(this.wiredCampaignTemplate.data, CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD);
    }

    onLoad(event){
        this.layoutSections = [event.detail.layout.sections[0]];
        this.activeAccordionSections = [this.layoutSections[0].id, this.label.optimizerLabel];
    }
}