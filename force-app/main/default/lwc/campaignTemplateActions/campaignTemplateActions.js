import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, updateRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getToast } from 'c/lds';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';

import CAMPAIGNTEMPLATE_ID_FIELD from '@salesforce/schema/CampaignTemplate__c.Id';
import CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD from '@salesforce/schema/CampaignTemplate__c.AcquisitionRetention__c';
import CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD from '@salesforce/schema/CampaignTemplate__c.MoreDealsBiggerDeals__c';
import CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD from '@salesforce/schema/CampaignTemplate__c.RevenueCashflow__c';

import frm_Acquisition_Retention from '@salesforce/label/c.frm_Acquisition_Retention';
import frm_More_Bigger_Deals from '@salesforce/label/c.frm_More_Bigger_Deals';
import frm_Revenue_cashflow from '@salesforce/label/c.frm_Revenue_cashflow';
import msg_Optimizer from '@salesforce/label/c.msg_Optimizer';
import msg_Record_Saved from '@salesforce/label/c.msg_Record_Saved';
import btn_save from '@salesforce/label/c.btn_save';

const fields = [CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD, CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD, CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD];

export default class CampaignTemplateActions extends NavigationMixin(LightningElement) {
    label = {frm_Acquisition_Retention,
        frm_More_Bigger_Deals,
        frm_Revenue_cashflow,
        msg_Optimizer,
        msg_Record_Saved,
        btn_save
    };

    @api recordId;
    @api objectApiName = 'musqotmp__CampaignTemplate__c';
    @track objectTypeLabel;
    @track sectioncssclass = 'slds-modal slds-fade-in-open slds-modal_small';
    @track header;
    @track layoutSections = [];
    @track activeAccordionSections;
    @track optimizerLabel = this.label.msg_Optimizer;

    @wire(CurrentPageReference) pageRef;

    @wire(getObjectInfo, { objectApiName: '$objectApiName' })
    wiredRecord({ error, data }) {
        if (error) {
            this.template.querySelector('c-toast').showToast(error);
        } else if (data) {
            this.objectTypeLabel = data.label;
            this.header = data.label;
        }
    }

    @wire(getRecord, { recordId: '$recordId', fields })
    wiredCampaignTemplate;

    get acquisitionRetention() {
        return getFieldValue(this.wiredCampaignTemplate.data, CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD);
    }

    get moreDealsBiggerDeals() {
        return getFieldValue(this.wiredCampaignTemplate.data, CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD);
    }

    get revenueCashflow() {
        return getFieldValue(this.wiredCampaignTemplate.data, CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD);
    }

    onLoad(event){
        this.layoutSections = [event.detail.layout.sections[0]];
        this.activeAccordionSections = [this.layoutSections[0].id];
        const modal = this.template.querySelector('c-record-form-modal');
        modal.show();
    }

    handleCancelRecordFormModal() {
        this.recordId = null;
        const modal = this.template.querySelector('c-record-form-modal');
        modal.hide();
    }

    onSuccess(event){
        this.recordId = event.detail.id;
        this.updateRecord();
    }

    updateRecord() {
        const updateFields = {};
        updateFields[CAMPAIGNTEMPLATE_ID_FIELD.fieldApiName] = this.recordId;
        updateFields[CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD.fieldApiName] = this.template.querySelector("[data-field='AcquisitionRetention']").value;
        updateFields[CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD.fieldApiName] = this.template.querySelector("[data-field='MoreDealsBiggerDeals']").value;
        updateFields[CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD.fieldApiName] = this.template.querySelector("[data-field='RevenueCashflow']").value;

        const recordInput = { fields: updateFields };

        updateRecord(recordInput)
            .then(() => {
                this.navigateToRecord();
                this.handleCancelRecordFormModal();
                const toast = getToast('success', 'pester', this.label.msg_Record_Saved);
                this.template.querySelector('c-toast').showToast(toast);
            })
            .catch(error => {
                this.template.querySelector('c-toast').showToast(error);
            });
    }

    navigateToRecord() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: this.objectApiName,
                actionName: 'view'
            }
        });
    }
}