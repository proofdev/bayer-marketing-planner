/**
 * Gets a field's label from a fields.
 * @param fields The Fields.
 * @param field Object-qualified API name of the field to return.
 * @returns The field's label (which may be a record in the case of spanning fields), or undefined if the field isn't found.
 */
export function getFieldLabel(fields, field){
    return fields[field].label;
}

/**
 * Creates a new cookie using the properties.
 * @param name The name of the cookie.
 * @param value The value of the cookie.
 * @param days Number of days to expires.
 * @returns null.
 */
export function createCookie(name, value, days){
    var expires;
    if (days) {
        const date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name+"="+value+expires+"; path=/";
}

/**
 * Gets a cookie's value.
 * @param name The name of the cookie.
 * @returns A promise that cookie's value or undefined.
 */
export function readCookie(name){
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    var value;
    ca.forEach(c => {
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) value = c.substring(nameEQ.length, c.length);
    });
    return value;
}

/**
 * Erase a cookie.
 * @param name The name of the cookie.
 * @returns null.
 */
export function eraseCookie(name){
    createCookie(name, "" , -1);
}

/**
 * Generate a rgb color.
 * @returns rgb.
 */
export function generateRandomRGB(){
    const x = Math.floor(Math.random() * 256);
    const y = Math.floor(Math.random() * 256);
    const z = Math.floor(Math.random() * 256);
    return  "rgb(" + x + "," + y + "," + z + ")";
}

/**
 * Gets a hex color.
 * @param labels The Labels.
 * @returns hex color.
 */
export function getHexColor(labels){
    const hexColors = [];
    const colors = [];
    colors.push('#ff9933');
    colors.push('#ff5c33');
    colors.push('#6ce02d');
    colors.push('#eb2f4e');
    colors.push('#d62bc8');
    colors.push('#722bd6');
    colors.push('#2b3fd6');
    colors.push('#666666');
    colors.push('#784599');
    colors.push('#007FB1');
    colors.push('#FF6F00');
    colors.push('#941751');
    colors.push('#999999');
    colors.push('#8760A1');
    colors.push('#00A1E0');
    colors.push('#FF9933');
    colors.push('#CB2A7B');
    colors.push('#CCCCCC');
    colors.push('#B69FC6');
    colors.push('#89D0F0');
    colors.push('#FFD3AB');
    colors.push('#E080AF');
    colors.push('#ffeb33');
    const iterator = labels.keys(); 
    for (let key of iterator) {
        if(colors[key] === undefined){
            hexColors.push('#' + Math.floor(Math.random()*16777215).toString(16));
        }else{
            hexColors.push(colors[key]);
        }
    }
    return  hexColors;
}

/**
 * Gets a title of the toast, displayed as a heading.
 * @param mode Determines how persistent the toast is. Valid values are: dismissable (default), remains visible until you press the close button or 3 seconds has elapsed, whichever comes first; pester, remains visible until the close button is clicked; sticky, remains visible for 3 seconds.
 * @returns title.
 */
export function getTitle(mode){
    return mode === 'error' ? 'Error' 
    : mode === 'warning' ? 'Warning' 
    : mode === 'success' ? 'Success' 
    : 'Information';
}

/**
 * Gets a toast that display toasts to provide feedback to a user following an action, such as after a record is created..
 * @param variant Changes the appearance of the notice. Toasts inherit styling from toasts in the Lightning Design System. Valid values are: info (default), success, warning, and error.
 * @param mode Determines how persistent the toast is. Valid values are: dismissable (default), remains visible until you press the close button or 3 seconds has elapsed, whichever comes first; pester, remains visible until the close button is clicked; sticky, remains visible for 3 seconds.
 * @param message A string representing the body of the message.
 * @returns toast.
 */
export function getToast(variant, mode, message, title = 'Information'){
    if(title === 'Information'){
        title = getTitle(mode);
    }
    const toast = {
        showToast: true,
        body: {
            title: title,
            variant: variant,
            mode: mode,
            message: message
        }
    }
    return toast;
}