/* eslint-disable no-dupe-keys*/
/* eslint-disable consistent-return */
/* eslint-disable no-dupe-class-members */
import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CAMPAIGNCONTRIBUTOR_OBJECT from '@salesforce/schema/CampaignContributor__c';
import CAMPAIGNCONTRIBUTOR_ID_FIELD from '@salesforce/schema/CampaignContributor__c.Id';
import CAMPAIGNCONTRIBUTOR_ROLEINCAMPAIGN_FIELD from '@salesforce/schema/CampaignContributor__c.RoleInCampaign__c';
import CAMPAIGNCONTRIBUTOR_WORKINGTIME_FIELD from '@salesforce/schema/CampaignContributor__c.WorkingTime__c';
import Id from '@salesforce/user/Id';
import datatable from '@salesforce/apex/Campaign.datatable';
import getRecords from '@salesforce/apex/Campaign.getRecords';
import { getRecord, deleteRecord, createRecord } from 'lightning/uiRecordApi';
import { getToast } from 'c/lds';
import { refreshApex } from '@salesforce/apex';
import DynamicObjectInfo from '@salesforce/apex/Campaign.DynamicObjectInfo';
import btn_group_message from '@salesforce/label/c.btn_group_message';
import hl_Search from '@salesforce/label/c.hl_Search';
import msg_Delete from '@salesforce/label/c.msg_Delete';
import modalHeader from '@salesforce/label/c.modalHeader';
import btn_save from '@salesforce/label/c.btn_save';
import btn_New from '@salesforce/label/c.btn_new';
import btn_cancel from '@salesforce/label/c.btn_cancel';
import btn_Delete from '@salesforce/label/c.btn_Delete';
import msg_Record_Delete from '@salesforce/label/c.msg_Record_Delete';
import msg_Record_Saved from '@salesforce/label/c.msg_Record_Saved';
import msg_Duplicate_Record from '@salesforce/label/c.msg_Duplicate_Record';
import msg_Required_Fields from "@salesforce/label/c.msg_Required_Fields";

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;

export default class CampaignContributorRelatedList extends LightningElement {
  label = {
    msg_Delete,
    hl_Search,
    modalHeader,
    btn_save,
    btn_New,
    btn_cancel,
    btn_Delete,
    msg_Record_Delete,
    msg_Record_Saved,
    btn_group_message,
    msg_Duplicate_Record,
    msg_Required_Fields,
  };
  @api recordId;
  userId = Id;
  @track objectApiName = "musqotmp__CampaignContributor__c";
  @track showModal = false;
  @track roleInCampaign = CAMPAIGNCONTRIBUTOR_ROLEINCAMPAIGN_FIELD;
  @track workingTime = CAMPAIGNCONTRIBUTOR_WORKINGTIME_FIELD;
  @track searchKey = "";
  @track data = [];
  @track columns = [];
  @track rowOffset = 0;
  @track tableLoadingState = true;
  @track showRowNumberColumn = false;
  @track sortBy = "LastModifiedDate";
  @track sortDirection = "desc";
  @track sortedBy = "LastModifiedDate";
  @track sortedDirection = "desc";
  @track selectedRows = [];
  @track hideCheckboxColumn = true;
  @track objectTypeLabel = "";
  @track listFields = [];
  @track paramDataTable;
  @track sectionCssClass = "slds-modal slds-fade-in-open slds-modal_small";
  @track recordFormId;
  @track content = msg_Delete;
  @track header = modalHeader;
  @track layoutSections = [];
  @track activeAccordionSections;
  @track showNew = true;
  @track paramGetRecords;
  @track typeLayoutSections = [];
  @track typeLayoutSectionsCopy = [];
  @track isGroupMessage = false;
  @track contributorObjectInfo;
  @track header = false;
  @track sectioncssclass = "slds-modal slds-fade-in-open slds-modal_small";
  @track hasDisabled = false;
  /** Wired Apex result so it may be programmatically refreshed. */
  wiredData;
  wiredGetRecord;

  @wire(getObjectInfo, { objectApiName: CAMPAIGNCONTRIBUTOR_OBJECT })
  campaignContributorObjectInfo({ data, error }) {
    if (data) {
      this.contributorObjectInfo = data;
      this.header = true;
      this.paramGetRecords = this.buildParamGetRecords();
      this.paramDataTable = this.buildParamDataTable();
    } else if (error) {
      this.template.querySelector("c-toast").showToast(error);
    }
  }
  get getContributorIconColor() {
    return "background-color: #" + this.contributorObjectInfo.themeInfo.color;
  }

  /*@wire(DynamicObjectInfo, {
        param: JSON.stringify({ component: 'CampaignContributorRelatedList-DynamicObjectInfo' }),
        recordId: '$recordId'
    })
    wiredGetDynamicObjectInfo(result) {
        if (result.data) {
            if (result.data.toast.ok) {
                this.objectApiName = result.data.objectApiName;
                //window.console.log('API NAME ' + this.objectApiName);
                this.objectTypeLabel = result.data.objectTypeLabel;
                this.recordId = result.data.recordId;
                //window.console.log('recordId :' + this.recordId);
                this.paramGetRecords = this.buildParamGetRecords();
                this.paramDataTable = this.buildParamDataTable();
                this.template.querySelector('c-toast').showToast(result.data.toast);
            } else {
                this.template.querySelector('c-toast').showToast(result.data.toast);
            }
        } else if (result.error) {
            this.template.querySelector('c-toast').showToast(result.error);
        }
    }*/
  buildParamGetRecords() {
    const whereCaluses = [];
    whereCaluses.push({
      field: "musqotmp__Campaign__c",
      value: this.recordId,
      clause: "=",
    });
    if (this.objectApiName === "musqotmp__CampaignContributor__c") {
      return JSON.stringify({
        component: "CampaignContributorRelatedList-getRecords",
        recordId: this.recordId,
        objectApiName: this.objectApiName,
        fieldMap: {
          [CAMPAIGNCONTRIBUTOR_ROLEINCAMPAIGN_FIELD.fieldApiName]: {},
        },
        whereCaluses: whereCaluses,
      });
    }
  }
  @wire(getRecords, { param: "$paramGetRecords" })
  buildParamDataTable() {
    const whereCaluses = [];
    const whereCaluseSubs = [];
    const operator = "AND";
    let fieldSetApiName = "musqotmp__CampaignContributorRelatedList";
    if (this.objectApiName === "musqotmp__CampaignContributor__c") {
      whereCaluses.push({
        field: "musqotmp__Campaign__c",
        value: this.recordId,
        clause: "=",
      });
    }
    if (this.searchKey !== "") {
      this.listFields.forEach((listField) => {
        let name;
        if (listField.type === "string") {
          name = listField.name;
        } else if (listField.type === "combobox") {
          name = listField.name;
        } else if (listField.type === "picklist") {
          name = listField.name;
        } else if (listField.type === "reference") {
          name = listField.name;
          if (name === "OwnerId") {
            name = "Owner.Name";
          } else if (name === "CreatedById") {
            name = "CreatedBy.Name";
          } else if (name === "LastModifiedById") {
            name = "LastModifiedBy.Name";
          } else if (name.includes("__c")) {
            name = name.replace("__c", "__r.Name");
          }
        }
        if (name) {
          whereCaluseSubs.push({
            field: name,
            value: this.searchKey,
            clause: "LIKE",
            operator: "OR",
          });
        }
      });
    }
    const rowLevelactions = [
      { label: "Edit", name: "edit", iconName: "utility:edit" },
      { label: "Delete", name: "delete", iconName: "utility:delete" },
    ];
    return JSON.stringify({
      component: "CampaignContributorRelatedList-datatable",
      relatedListType: "finance",
      objectApiName: CAMPAIGNCONTRIBUTOR_OBJECT.objectApiName,
      fieldMap: {
        [CAMPAIGNCONTRIBUTOR_ID_FIELD.fieldApiName]: {},
      },
      fieldSetApiName: fieldSetApiName,
      linkableNameField: true,
      whereCaluses: whereCaluses,
      operator: operator,
      whereCaluseSubs: whereCaluseSubs,
      sortBy: this.sortBy,
      sortDirection: this.sortDirection,
      rowLevelactions: rowLevelactions,
    });
  }

  @wire(datatable, {
    param: "$paramDataTable",
  })
  wiredDataTable(result) {
    this.wiredData = result;
    if (result.data) {
      if (result.data.toast.ok) {
        this.sortedBy = this.sortBy;
        this.sortedDirection = this.sortDirection;
        this.columns = result.data.datatable.columns;
        this.data = result.data.datatable.data;
        this.listFields = result.data.fields;
        this.objectTypeLabel = result.data.objectTypeLabel;
        this.template.querySelector("c-toast").showToast(result.data.toast);
      } else {
        this.data = undefined;
        this.columns = undefined;
        this.template.querySelector("c-toast").showToast(result.data.toast);
      }
    } else if (result.error) {
      this.data = undefined;
      this.columns = undefined;
      this.template.querySelector("c-toast").showToast(result.error);
    }
    this.tableLoadingState = false;
  }

  handleColumnSort(event) {
    this.sortBy = event.detail.sortBy;
    this.sortDirection = event.detail.sortDirection;
    this.paramDataTable = this.buildParamDataTable();
  }

  handleSearchKeyChange(event) {
    // Debouncing this method: Do not update the reactive property as long as this function is
    // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
    window.clearTimeout(this.delayTimeout);
    const searchKey = event.target.value;
    // eslint-disable-next-line @lwc/lwc/no-async-operation
    this.delayTimeout = setTimeout(() => {
      this.searchKey = searchKey;
      this.paramDataTable = this.buildParamDataTable();
    }, DELAY);
  }

  handleNew() {
    this.handleReset();
    this.recordFormId = null;
    this.header = this.label.btn_New + " " + this.objectTypeLabel;
    const modal = this.template.querySelector("c-record-form-modal");
    modal.show();
  }
  //reset the form
  handleReset() {
    const inputFields = this.template.querySelectorAll("lightning-input-field");
    if (inputFields) {
      inputFields.forEach((field) => {
        field.reset();
      });
    }
    this.template.querySelector(
      "[data-field='musqotmp__Campaign__c']"
    ).value = this.recordId;
  }

  handleSelect(event) {
    this.recordFormId = event.detail.id;
    if (event.detail.actionName === "edit") {
      const modal = this.template.querySelector("c-record-form-modal");
      modal.show();
    } else {
      const modal = this.template.querySelector("c-modal");
      modal.show();
    }
    this.header = event.detail.actionLabel + " " + this.objectTypeLabel;
  }

  handleCancelRecordFormModal() {
    this.recordFormId = null;
    const modal = this.template.querySelector("c-record-form-modal");
    this.handleRefresh();
    modal.hide();
  }
  CancelModal() {
    this.handleCancelRecordFormModal();
  }
  handleCancelModal() {
    this.recordFormId = null;
    const modal = this.template.querySelector("c-modal");
    modal.hide();
  }
  handleDelete() {
    const recordId = this.recordFormId;
    deleteRecord(recordId)
      .then(() => {
        const toast = getToast(
          "success",
          "pester",
          this.label.msg_Record_Delete
        );
        this.template.querySelector("c-toast").showToast(toast);
        this.handleRefresh();
        this.handleCancelModal();
      })
      .catch((error) => {
        this.template.querySelector("c-toast").showToast(error);
        this.handleCancelModal();
      });
  }
  @wire(getRecord, {
    recordId: "$recordId",
    layoutTypes: ["Full"],
    modes: ["Create"],
  })
  wiredRecord({ error, data }) {
    if (error) {
      this.template.querySelector("c-toast").showToast(error);
    } else if (data) {
      this.wiredGetRecord = data;
      this.buildTypeLayoutSections(this.layoutSections, this.wiredGetRecord);
    }
  }

  onLoad(event) {
    this.layoutSections = [event.detail.layout.sections[0]];
    this.activeAccordionSections = [this.layoutSections[0].id];
    this.buildTypeLayoutSections(this.layoutSections, null);
  }
  buildTypeLayoutSections(layoutSections, record) {
    this.typeLayoutSections = [];
    layoutSections.forEach((layoutSection) => {
      let typeLayoutSection = {};
      typeLayoutSection.id = layoutSection.id;
      typeLayoutSection.heading = layoutSection.heading;
      let layoutRows = [];
      layoutSection.layoutRows.forEach((layoutRow) => {
        let typeLayoutRows = {};
        let layoutItems = [];
        layoutRow.layoutItems.forEach((layoutItem) => {
          let typeLayoutItems = {};
          let layoutComponents = [];
          layoutItem.layoutComponents.forEach((layoutComponent) => {
            let typeLayoutComponent = {};
            typeLayoutComponent.apiName = layoutComponent.apiName;
            if (
              this.objectApiName === "musqotmp__CampaignContributor__c" &&
              layoutComponent.apiName === "musqotmp__Campaign__c"
            ) {
              typeLayoutComponent.value = this.recordId;
            } else if (
              record !== null &&
              layoutComponent.apiName !== null &&
              layoutComponent.apiName !== "Name" &&
              record.fields[layoutComponent.apiName]
            ) {
              typeLayoutComponent.value =
                record.fields[layoutComponent.apiName].value;
            }
            layoutComponents.push(typeLayoutComponent);
          });
          typeLayoutItems.layoutComponents = layoutComponents;
          layoutItems.push(typeLayoutItems);
        });
        typeLayoutRows.layoutItems = layoutItems;
        layoutRows.push(typeLayoutRows);
      });
      typeLayoutSection.layoutRows = layoutRows;
      this.typeLayoutSections.push(typeLayoutSection);
      this.typeLayoutSectionsCopy = Array.from(this.typeLayoutSections);
    });
  }

  handleFormChange() {
    this.hasDisabled = false;
  }
  handleSubmit(event) {
    event.preventDefault(); // stop the form from submitting
    const fields = event.detail.fields;
    this.hasDisabled = true;
    let valid = true;
    let validateMessage = this.label.msg_Required_Fields;
    let validateField = "";
    if (!fields.musqotmp__User__c) {
      validateField =
        " " + this.contributorObjectInfo.fields.musqotmp__User__c.label + ",";
      valid = false;
      validateMessage += validateField;
    }
    if (!fields.musqotmp__Campaign__c) {
      valid = false;
      validateField =
        " " +
        this.contributorObjectInfo.fields.musqotmp__Campaign__c.label +
        ",";
      validateMessage += validateField;
    }
    if (!fields.musqotmp__RoleInCampaign__c) {
      valid = false;
      validateField =
        " " +
        this.contributorObjectInfo.fields.musqotmp__RoleInCampaign__c.label +
        ",";
      validateMessage += validateField;
    }
    //console.log('fields :' + JSON.stringify(fields));
    if (valid) {
      if (this.recordFormId) {
        this.template
          .querySelector("lightning-record-edit-form")
          .submit(fields);
      } else {
        this.createRecord(fields);
      }
    } else {
      this.hasDisabled = false;
      validateMessage = validateMessage.substring(
        0,
        validateMessage.length - 1
      );
      const evt = new ShowToastEvent({
        title: "Error",
        message: validateMessage,
        variant: "error",
        mode: "pester",
      });
      this.dispatchEvent(evt);
    }
  }
  createRecord(fields) {
    const recordInput = {
      apiName: CAMPAIGNCONTRIBUTOR_OBJECT.objectApiName,
      fields,
    };
    createRecord(recordInput)
      .then((record) => {
        this.recordFormId = record.id;
        this.hasDisabled = false;
        this.handleCancelRecordFormModal();
        const toast = getToast(
          "success",
          "pester",
          this.label.msg_Record_Saved
        );
        this.template.querySelector("c-toast").showToast(toast);
      })
      .catch((error) => {
        if (error.body.output.errors) {
          let errorMessage = error.body.output.errors[0].message;
          if (errorMessage.includes("duplicate value found")) {
            //console.log('errorMessage :' + errorMessage);
            const evt = new ShowToastEvent({
              title: "Error",
              message: this.label.msg_Duplicate_Record,
              variant: "error",
              mode: "sticky",
            });
            this.dispatchEvent(evt);
          } else {
            const evt = new ShowToastEvent({
              title: "Error",
              message: errorMessage,
              variant: "error",
              mode: "sticky",
            });
            this.dispatchEvent(evt);
          }
        } else {
          this.template.querySelector("c-toast").showToast(error);
        }
      });
  }
  onSuccess(event) {
    this.recordFormId = event.detail.id;
   // window.console.log("onSuccess -recordFormId :" + this.recordFormId);
    this.handleCancelRecordFormModal();
    const toast = getToast("success", "pester", this.label.msg_Record_Saved);
    this.template.querySelector("c-toast").showToast(toast);
  }
  handleSave(event) {
    this.isGroupMessage = true;
  //  window.console.log(this.isGroupMessage);
  }
  hanldeIsgroupmessageChange(event) {
    this.isGroupMessage = event.isGroupMessage;
  }
  handleRefresh() {
    this.selectedRows = [];
    if (this.wiredData.data) {
      refreshApex(this.wiredData);
    }
  }
}