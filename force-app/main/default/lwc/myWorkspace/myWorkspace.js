import { LightningElement, wire, track, api } from "lwc";
import getMyWorkspace from "@salesforce/apex/Gantt.getMyWorkspace";
import getRecords from "@salesforce/apex/Campaign.getRecords";
import getResults from "@salesforce/apex/Gantt.getResults"; //Sowmya
import { updateRecord, createRecord } from "lightning/uiRecordApi";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import { loadScript, loadStyle } from "lightning/platformResourceLoader";
import Gantt from "@salesforce/resourceUrl/gantt_6";
import { CurrentPageReference, NavigationMixin } from "lightning/navigation";
import { refreshApex, getSObjectValue } from "@salesforce/apex";
import MOMENT_JS from "@salesforce/resourceUrl/moment";
import { getPicklistValues } from "lightning/uiObjectInfoApi";
import { getToast, readCookie } from "c/lds";
import { getRecord, getFieldValue } from "lightning/uiRecordApi";
import LOCALE from "@salesforce/i18n/locale";
import getDynamicObjectInfo from "@salesforce/apex/Campaign.getDynamicObjectInfo";
import cloneCampaignTemplate from "@salesforce/apex/Gantt.cloneCampaignTemplate";
import readMarketingCloudCredential from "@salesforce/apex/Campaign.readMarketingCloudCredential";
import CAMPAIGNOPTIMIZER_OBJECT from "@salesforce/schema/CampaignOptimizer__c";
import CAMPAIGNOPTIMIZER_ID_FIELD from "@salesforce/schema/CampaignOptimizer__c.Id";
import CAMPAIGNOPTIMIZER_NAME_FIELD from "@salesforce/schema/CampaignOptimizer__c.Name";
import CAMPAIGNOPTIMIZER_PLAN_FIELD from "@salesforce/schema/CampaignOptimizer__c.MarketingPlan__c";
import CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD from "@salesforce/schema/CampaignOptimizer__c.RevenueCashflow__c";
import CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD from "@salesforce/schema/CampaignOptimizer__c.MoreDealsBiggerDeals__c";
import CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD from "@salesforce/schema/CampaignOptimizer__c.AcquisitionRetention__c";
import CAMPAIGNOPTIMIZER_FISCALYEAR_FIELD from "@salesforce/schema/CampaignOptimizer__c.FiscalYear__c";
import CAMPAIGNOPTIMIZER_PERIOD_FIELD from "@salesforce/schema/CampaignOptimizer__c.Period__c";
import CAMPAIGNOPTIMIZER_YEAR_QUATER_FIELD from "@salesforce/schema/CampaignOptimizer__c.YearQuarter__c";
import CAMPAIGNOPTIMIZER_MARKETING_PLAN_FIELD from "@salesforce/schema/CampaignOptimizer__c.MarketingPlan__c";
import MARKETINGPLAN_OBJECT from "@salesforce/schema/MarketingPlan__c";
import CAMPAIGNTEMPLATE_OBJECT from "@salesforce/schema/CampaignTemplate__c";
import CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD from "@salesforce/schema/CampaignTemplate__c.RevenueCashflow__c";
import CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD from "@salesforce/schema/CampaignTemplate__c.MoreDealsBiggerDeals__c";
import CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD from "@salesforce/schema/CampaignTemplate__c.AcquisitionRetention__c";
import CAMPAIGNTEMPLATE_ID_FIELD from "@salesforce/schema/CampaignTemplate__c.Id";
import CAMPAIGNTEMPLATE_NAME_FIELD from "@salesforce/schema/CampaignTemplate__c.Name";
import CAMPAIGNTEMPLATE_DESCRIPTION_FIELD from "@salesforce/schema/CampaignTemplate__c.Description__c";
import CAMPAIGNTEMPLATE_REVENUE_FIELD from "@salesforce/schema/CampaignTemplate__c.Revenue__c";
import CAMPAIGNTEMPLATE_RETENTION_FIELD from "@salesforce/schema/CampaignTemplate__c.Retention__c";
import CAMPAIGNTEMPLATE_ACQUISITION_FIELD from "@salesforce/schema/CampaignTemplate__c.Acquisition__c";
import CAMPAIGNTEMPLATE_AWARNESS_FIELD from "@salesforce/schema/CampaignTemplate__c.Awareness__c";
import CAMPAIGNTEMPLATE_ACTIVE_FIELD from "@salesforce/schema/CampaignTemplate__c.Active__c";
import CAMPAIGN_OBJECT from "@salesforce/schema/Campaign";
import CAMPAIGN_ID_FIELD from "@salesforce/schema/Campaign.Id";
import CAMPAIGN_NAME_FIELD from "@salesforce/schema/Campaign.Name";
import CAMPAIGN_MARKETING_PLAN_FIELD from "@salesforce/schema/Campaign.MarketingPlan__c";
import CAMPAIGN_CAMPAIGNTEMPLATE_FIELD from "@salesforce/schema/Campaign.MarketingPlan__r.CampaignTemplate__c";
import CAMPAIGN_TYPE_FIELD from "@salesforce/schema/Campaign.Type";
import CAMPAIGN_SD_FIELD from "@salesforce/schema/Campaign.StartDate";
import CAMPAIGN_ED_FIELD from "@salesforce/schema/Campaign.EndDate";
import RECORD_TYPE_FIELD from "@salesforce/schema/Campaign.RecordTypeId";
import MARKETINGACTIVITY_OBJECT from "@salesforce/schema/MarketingActivity__c";
import MARKETINGACTIVITY_ID_FIELD from "@salesforce/schema/MarketingActivity__c.Id";
import MARKETINGACTIVITY_SD_FIELD from "@salesforce/schema/MarketingActivity__c.StartDate__c";
import MARKETINGACTIVITY_ED_FIELD from "@salesforce/schema/MarketingActivity__c.EndDate__c";
import MARKETINGACTIVITY_CAMPAIGNTEMPLATE_FIELD from "@salesforce/schema/MarketingActivity__c.Campaign__r.MarketingPlan__r.CampaignTemplate__c";
import MARKETINGACTIVITY_TYPE_FIELD from "@salesforce/schema/MarketingActivity__c.Type__c";

import MARKETINGJOURNEY_ID_FIELD from "@salesforce/schema/MarketingJourney__c.Id";
import MARKETINGJOURNEY_SD_FIELD from "@salesforce/schema/MarketingJourney__c.StartDate__c";
import MARKETINGJOURNEY_ED_FIELD from "@salesforce/schema/MarketingJourney__c.EndDate__c";
import MARKETINGJOURNEY_OBJECT from "@salesforce/schema/MarketingJourney__c";

import MARKETING_ACTIVITY_CAMPAIGN_NAME_FIELD from "@salesforce/schema/MarketingActivity__c.Campaign__r.Name";
import MARKETING_JOURNEY_CAMPAIGN_NAME_FIELD from "@salesforce/schema/MarketingJourney__c.Campaign__r.Name";
import MARKETING_ACTIVITY_PLAN_FIELD from "@salesforce/schema/MarketingActivity__c.Campaign__r.MarketingPlan__r.Name";
import CAMPAIGN_MARKETINGPLAN_FIELD from "@salesforce/schema/Campaign.MarketingPlan__r.Name";
import MARKETING_JOURNEY_PLAN_FIELD from "@salesforce/schema/MarketingJourney__c.Campaign__r.MarketingPlan__r.Name";

import btn_cancel from "@salesforce/label/c.btn_cancel";
import btn_open from "@salesforce/label/c.btn_open";
import btn_continue from "@salesforce/label/c.btn_continue";
import btn_save from "@salesforce/label/c.btn_save";
import btn_edit from "@salesforce/label/c.btn_edit";
import statusHeader from "@salesforce/label/c.gantt_StatusHeader";

import hl_Campaign_Template from "@salesforce/label/c.hl_Campaign_Template";
import hl_Campaign from "@salesforce/label/c.hl_Campaign";
import hl_Activities from "@salesforce/label/c.hl_Activities";
import hl_Journey from "@salesforce/label/c.hl_Journey";
import hl_Optimizing_Schema from "@salesforce/label/c.hl_Optimizing_Schema";
import hl_Select_Schema from "@salesforce/label/c.hl_Select_Schema";
import msg_Record_Saved from "@salesforce/label/c.msg_Record_Saved";
import hl_New_Marketing_Activity from "@salesforce/label/c.hl_New_Marketing_Activity";
import hl_Choose_Option from "@salesforce/label/c.hl_Choose_Option";
import btnOpenInMC from "@salesforce/label/c.btn_open_in_mc";
import msg_template_availablity from "@salesforce/label/c.msg_template_availablity";
import hl_new from "@salesforce/label/c.btn_new";
import datatable from "@salesforce/apex/Campaign.datatable";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
const CSS_SLDSISACTIVE = "slds-is-active";

export default class MyWorkspace extends NavigationMixin(LightningElement) {
	label = {
		btn_cancel,
		btn_continue,
		btn_edit,
		btn_save,
		btn_open,
		hl_Campaign_Template,
		hl_Campaign,
		hl_Activities,
		hl_Journey,
		hl_new,
		hl_Optimizing_Schema,
		hl_Select_Schema,
		msg_Record_Saved,
		hl_New_Marketing_Activity,
		hl_Choose_Option,
		statusHeader,
		btnOpenInMC,
		msg_template_availablity,
	};
	@api recordId;
	ganttInitialized = false;
	@track sObject;
	@track sObjects = [];
	@track barTextFields = [];
	@track tasks;
	@track header = "The modal header";
	@track objectApiName;
	@track taskId;
	@track fields = [];
	@track fieldMap = {};
	@track momentjsInitialized;
	@track selectedDateTime = new Date();
	@track startDate;
	@track endDate;
	@track campaignManagerSetup;
	@track barColorMap = {};
	@track addHeader = this.label.hl_Choose_Option;
	@track createRecord = [];
	@track addSectionCssClass = "slds-modal slds-fade-in-open slds-modal_small";
	@track addSearch;
	@track parent;
	@track campaignOptimizers;
	@track campaignOptimizerRecordId;
	@track paramGetCampaign;
	@track showCampaignOptimizer = false;
	@track typeId;
	@track typeSectionCssClass = "slds-modal slds-fade-in-open slds-modal_small";
	@track typeHeader = "";
	@track layoutSections = [];
	@track activeAccordionSections;
	@track typeLayoutSections = [];
	@track typeSectionCssClassMarketingActivity = "slds-modal slds-fade-in-open slds-modal_small";
	@track typeHeaderMarketingActivity = "";
	@track layoutSectionsMarketingActivity = [];
	@track activeAccordionSectionsMarketingActivity;
	@track typeLayoutSectionsMarketingActivity = [];
	@track sectionObjectApiName = "";
	@track paramCloneCampaignTemplate;
	@track sObjectCampaign = [];
	@track isCampaignOptimizer = false;
	@track sObjOptimizer = [];
	@track revenueCashFlow;
	@track moreDealsBiggerDeals;
	@track acquisitionRetention;
	@track templateRevenueCashflow;
	@track templateMoreDealsBiggerDeals;
	@track templateAcquisitionRetention;
	@track mcUrlMap = {};
	@track cloneTemplate = false;
	@track newTemplateInfo = false;
	@track recordFormId;
	@track campaignName;
	@track campaignStartDate;
	@track campaignEndDate;
	@track sortBy = "musqotmp__FiscalYear__c,musqotmp__Period__c,musqotmp__MarketingPlan__c";
	@track selectedValue;
	@track marketingJourney;
	@track marketingActivity;
	@track campaign;
	@track campaignTemplate;
	@track showFormFooter = false;
	@track ganttTasksMap = {};
	@track paramGetRecords;
	@track sObjectCampaignDao;

	@track layoutSectionsJourney;
	@track activeAccordionSectionsJourney;
	@track typeLayoutSectionsJourney = [];
	@track typeSectionCssClassJourney = "slds-modal slds-fade-in-open slds-modal_small";
	@track typeHeaderJourney = "";
	@track showJourney = false;
	@track useMarketingCloud = false;
	@track marketingPlanIds = [];
	@track isEmptyTemplate = false;
	@track showOptimizerSchema = false;
	@track templatePlan;
	@track parentCampaign;
	// @api iconName = 'action:new_campaign';
	@api LoadingText = false;
	@track txtclassname = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
	@track messageFlag = false;
	@track iconFlag = true;
	@track clearIconFlag = false;
	@track inputReadOnly = false;
	// Campaign
	@track selectCampaignRecordId = "";
	@api LoadingTextCampaign = false;
	@track inputCampaignReadOnly = false;
	@track iconFlagCampaign = true;
	@track clearIconFlagCampaign = false;
	@track messageFlagCampaign = false;
	@track showDropdownPlan = false;
	@track showDropdownCampaign = false;
	@track templateCloneId;
	@track wiredGetRecord = null;
	@track marketingCloudCredential;
	@track recordTypeId;
	@track folderRecordTypeId;
	@track action = "";
	@track url;
	@track hasExecutionCampaign = false;
	@track parentStartDate;
	@track parentEndDate;
	@track campaignHeader;
	@track marketingPlan;
	@track campaignValue;
	@track hasMarketingPlan = false;
	@track newfieldList = [];
	@track data = [];
	@track url = "";
	@track campaignUrl;
	@track objectFileds;
	@track taskRecId;
	@track sectioncssclass = "slds-modal slds-fade-in-open slds-modal_medium";

	//Sowmya user stroy W-000385 Gantt pop-up
	@track sectioncssclass = "slds-modal slds-fade-in-open slds-modal_medium";
	@track fieldsCampaign = [];
	@track isMarketingActivity = false;
	@track isMarketingJourney = false;
	@track taskIdCampaign;
	@track otherobjectapiname;
	@track dtdata = [];
	@track dtcolumns = [];
	@track tableLoadingState = true;
	@track hasCampaignTab = false;
	@track hasOpenButton = false;
	@track paramDataTable;
	@track hideCheckboxColumn = true;
	@track financeAllocation = false;
	@track submitAction;
	@track showOtherObjectTab = false;
	//Remove if not needed
	@api objectName = "";
	@api selectRecordNameCamp;
	@api Label;
	@api searchRecords = [];
	@api searchRecordsCampaign = [];
	@api required = false;

	// @api iconName = 'action:new_campaign';
	//@api LoadingText = false;
	//custom lookup
	@track topLevelStartDate;
	@track topLevelEndDate;
	@api fieldName = "Name";
	@track objInfo = [];
	@track campaignObjInfo = [];
	@api fieldLabel;
	@api selectRecordName;
	@track fieldValue;
	@track fields;
	@api selectRecordId;
	@track hasDisabled = false;
	@track required = true;
	@track clearIconFlag;
	@api currentText;
	@api lastView;
	@track showIcon;
	@track templatePlan;
	//disable action
	@track disabledAction = false;
	@track showAddModal = true;
	@track showTemplate = false;
	@track paramGetCampaignOptimizer;
	@track workspaceId;
	@track campaignPlanMap = {};
	@track campaignOptimizerPlanMap = {};
	@track campaignOptimizerOption = [];
	@track requiredCampaign=true;
	@track requiredPlan = true;


	/** Wired Apex result so it may be programmatically refreshed. */
	wiredData;
	//TODO Need to make dynamic to validate icon is standard or custom
	@track standardIcons = ["Adwords", "Banner", "Event", "Email", "Mobile", "Outdoor", "Postal", "Print", "Radio", "Social", "Sponsor", "Telemarketing", "Tv", "Web", "Other"];

	@wire(getRecord, { recordId: "$recordId", layoutTypes: ["Full"], modes: ["View"] })
	wiredGetWorkSpaceRecord(result) {
		if (result.data) {
			if (this.wiredData) {
				this.handleRefresh();
			}
			//this.tempGanttId = result.data.id;
			//this.loadMoment();
			//this.loadGantt();
		}
	}
	@wire(getObjectInfo, { objectApiName: MARKETINGJOURNEY_OBJECT })
	marketingJourney;
	get getMarketingJourneyIconColor() {
		return "width:32px;height:32px; background-color: #" + this.marketingJourney.data.themeInfo.color;
	}
	@wire(getObjectInfo, { objectApiName: MARKETINGACTIVITY_OBJECT })
	marketingActivity;
	get getMarketingActivityIconColor() {
		return "width:32px;height:32px; background-color: #" + this.marketingActivity.data.themeInfo.color;
	}
	@wire(getObjectInfo, { objectApiName: CAMPAIGNTEMPLATE_OBJECT })
	campaignTemplate;
	get getCampaignTemplateIconColor() {
		return "width:32px;height:32px; background-color: #" + this.campaignTemplate.data.themeInfo.color;
	}
	@wire(getObjectInfo, { objectApiName: CAMPAIGN_OBJECT })
	campaign;
	get getCampaignIconColor() {
		return "width:32px;height:32px; background-color: #" + this.campaign.data.themeInfo.color;
	}
	@wire(getObjectInfo, { objectApiName: CAMPAIGN_OBJECT })
	wiredObjectInfo({ data, error }) {
		if (data) {
			this.campaignObjInfo = data;
			// map of record type Info
			const rtInfos = data.recordTypeInfos;
			// getting map values
			let rtValues = Object.values(rtInfos);
			for (let i = 0; i < rtValues.length; i++) {
				if (rtValues[i].name !== "Master") {
					if (rtValues[i].name === "Execution Campaign") {
						this.recordTypeId = rtValues[i].recordTypeId;
					} else if (rtValues[i].name === "Folder Campaign") {
						this.folderRecordTypeId = rtValues[i].recordTypeId;
					}
				}
			}
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}
	//custom lookup
	//Anji
	@wire(getObjectInfo, { objectApiName: MARKETINGPLAN_OBJECT })
	wiredPlanInfo({ data, error }) {
		if (data) {
			this.objInfo = data;
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}
	/*get moment() {
		if (moment) return moment;
		this.moment;
	}*/
	//renderedCallback() {
	renderedCallback() {
		try {
			Promise.all([loadScript(this, MOMENT_JS)])
				.then(() => {
					if (moment) {
						this.workspaceId = this.recordId;
						this.momentjsInitialized = true;
						this.handleRefresh();
					}
				})
				.catch((error) => {
					this.template.querySelector("c-toast").showToast(error);
				});
		} catch (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}

	@wire(CurrentPageReference) pageRef;
	@wire(readMarketingCloudCredential)
	wiredReadMarketingCloudCredential(cresult) {
		if (cresult.data && cresult.data.sObj) {
			this.marketingCloudCredential = cresult.data.sObj;
		}
	}
	@wire(getMyWorkspace, {
		param: JSON.stringify({ component: "myWorkspace-getMyWorkspace" }),
		recordId: "$workspaceId",
	})
	wiredGetMyWorkspace(result) {
		if (result.data) {
			if (result.data.toast.ok) {
				this.wiredData = result;
				this.campaignManagerSetup = result.data.customSetting;
				this.useMarketingCloud = this.campaignManagerSetup.musqotmp__UseMarketingCloud__c;
				this.barColorMap = result.data.stringMap;
				this.sObject = result.data.sObj;
				this.sObjects = result.data.sObjects;
				//window.console.log('this.sObjects :' + JSON.stringify(this.sObjects));
				this.barTextFields = result.data.fields;
				this.fieldMap = result.data.fieldMap;
				this.marketingPlanIds = result.data.sObjIds;
				//window.console.log('marketingPlanIds :' + JSON.stringify(this.marketingPlanIds));
				this.tasks = this.buildGanttData();
				this.ganttPromise();
				this.paramGetCampaignOptimizer = this.buildParamCampaignOptimizer();
				this.template.querySelector("c-toast").showToast(result.data.toast);
			} else {
				this.template.querySelector("c-toast").showToast(result.data.toast);
			}
		} else if (result.error) {
			this.template.querySelector("c-toast").showToast(result.error);
		}
	}
	buildParamCampaignOptimizer() {
		let whereCaluses = this.optimizerWhereClauses();
		return JSON.stringify({
			component: "myWorkspace-getRecords",
			objectApiName: CAMPAIGNOPTIMIZER_OBJECT.objectApiName,
			fieldMap: {
				[CAMPAIGNOPTIMIZER_ID_FIELD.fieldApiName]: {},
				[CAMPAIGNOPTIMIZER_NAME_FIELD.fieldApiName]: {},
				[CAMPAIGNOPTIMIZER_PLAN_FIELD.fieldApiName]: {},
				[CAMPAIGNOPTIMIZER_REVENUECASHFLOW_FIELD.fieldApiName]: {},
				[CAMPAIGNOPTIMIZER_MOREDEALSBIGGERDEALS_FIELD.fieldApiName]: {},
				[CAMPAIGNOPTIMIZER_ACQUISITIONRETENTION_FIELD.fieldApiName]: {},
			},
			whereCaluses: whereCaluses,
			sortBy: this.sortBy,
		});
	}
	optimizerWhereClauses() {
		let today = new Date();
		let month = today.getMonth() + 1;
		let thisYear = today.getFullYear();
		let thisQuarter = Math.ceil(month / 3);
		const whereCaluses = [];
		whereCaluses.push({
			field: CAMPAIGNOPTIMIZER_MARKETING_PLAN_FIELD.fieldApiName,
			values: this.marketingPlanIds,
			clause: "IN",
			operator: "AND",
		});
		whereCaluses.push({
			field: CAMPAIGNOPTIMIZER_YEAR_QUATER_FIELD.fieldApiName,
			value: parseFloat(thisYear + "" + thisQuarter),
			clause: ">=",
		});
		return whereCaluses;
	}
	@wire(getRecords, { param: "$paramGetCampaignOptimizer" })
	wiredGetRecords(result) {
		if (result.data) {
			this.campaignOptimizers = [];
			if (result.data.toast.ok) {
				this.campaignOptimizerOption = this.buildCampaignOptimizers(result.data.sObjects);
				this.sObjOptimizer = result.data.sObjects;
				this.campaignOptimizerSchema();
				this.template.querySelector("c-toast").showToast(result.data.toast);
			} else if (result.error) {
				this.campaignOptimizers = undefined;
				this.template.querySelector("c-toast").showToast(result.error);
			}
		}
	}
	@wire(getRecord, {
		recordId: "$parent",
		layoutTypes: ["Full"],
		modes: ["Create"],
	})
	wiredGetRecordData({ data, error }) {
		if (data) {
			this.wiredGetRecord = data;
			this.parentCampaignName = data.fields.Name.value;
			this.campaignHeader = "New Campaign in " + this.parentCampaignName;
			let parentStartDate = data.fields.StartDate.value;
			const date = new Date(parentStartDate);
			const dateTimeFormat = new Intl.DateTimeFormat(LOCALE).format(date);
			let dateString = dateTimeFormat;
			this.parentStartDate = dateString;
			let parentEndDate = data.fields.EndDate.value;
			const endDate = new Date(parentEndDate);
			const endDatedateTimeFormat = new Intl.DateTimeFormat(LOCALE).format(endDate);
			let endDateString = endDatedateTimeFormat;
			this.parentEndDate = endDateString;
			if (data.recordTypeInfo) {
				this.recordTypeName = data.recordTypeInfo.name;
				if (this.recordTypeName === "Execution Campaign") {
					this.hasExecutionCampaign = true;
				} else {
					this.hasExecutionCampaign = false;
				}
			}
			//this.buildTypeLayoutSections(this.layoutSections, this.wiredGetRecord);
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}

	buildParamCampaign(objectApiName) {
		const whereCaluses = [];
		if (objectApiName === "musqotmp__MarketingActivity__c") {
			whereCaluses.push({
				field: MARKETINGACTIVITY_CAMPAIGNTEMPLATE_FIELD.fieldApiName,
				value: true,
				clause: "=",
			});
		} else if (objectApiName === "Campaign") {
			whereCaluses.push({
				field: CAMPAIGN_CAMPAIGNTEMPLATE_FIELD.fieldApiName,
				value: true,
				clause: "=",
			});
		} else {
			whereCaluses.push({
				field: CAMPAIGN_CAMPAIGNTEMPLATE_FIELD.fieldApiName,
				value: true,
				clause: "=",
			});
		}
		return JSON.stringify({
			component: "myWorkspace-getRecords",
			objectApiName: objectApiName,
			fieldMap: {
				Id: {},
				Name: {},
			},
			whereCaluses: whereCaluses,
		});
	}
	//Anji
	buildParamCampaignTemplate(selectedValue) {
		const whereCaluses = [];
		if (selectedValue === "Acquisition") {
			whereCaluses.push({
				field: CAMPAIGNTEMPLATE_ACQUISITION_FIELD.fieldApiName,
				value: true,
				clause: "=",
				operator: "AND",
			});
			whereCaluses.push({
				field: CAMPAIGNTEMPLATE_ACTIVE_FIELD.fieldApiName,
				value: true,
				clause: "=",
			});
		} else if (selectedValue === "Awareness") {
			whereCaluses.push({
				field: CAMPAIGNTEMPLATE_AWARNESS_FIELD.fieldApiName,
				value: true,
				clause: "=",
				operator: "AND",
			});
			whereCaluses.push({
				field: CAMPAIGNTEMPLATE_ACTIVE_FIELD.fieldApiName,
				value: true,
				clause: "=",
			});
		} else if (selectedValue === "Retention") {
			whereCaluses.push({
				field: CAMPAIGNTEMPLATE_RETENTION_FIELD.fieldApiName,
				value: true,
				clause: "=",
				operator: "AND",
			});
			whereCaluses.push({
				field: CAMPAIGNTEMPLATE_ACTIVE_FIELD.fieldApiName,
				value: true,
				clause: "=",
			});
		} else if (selectedValue === "Revenue") {
			whereCaluses.push({
				field: CAMPAIGNTEMPLATE_REVENUE_FIELD.fieldApiName,
				value: true,
				clause: "=",
				operator: "AND",
			});
			whereCaluses.push({
				field: CAMPAIGNTEMPLATE_ACTIVE_FIELD.fieldApiName,
				value: true,
				clause: "=",
			});
		}
		return JSON.stringify({
			component: "myWorkspace-getRecords",
			objectApiName: CAMPAIGNTEMPLATE_OBJECT.objectApiName,
			fieldMap: {
				[CAMPAIGNTEMPLATE_ID_FIELD.fieldApiName]: {},
				[CAMPAIGNTEMPLATE_NAME_FIELD.fieldApiName]: {},
				[CAMPAIGNTEMPLATE_REVENUECASHFLOW_FIELD.fieldApiName]: {},
				[CAMPAIGNTEMPLATE_MOREDEALSBIGGERDEALS_FIELD.fieldApiName]: {},
				[CAMPAIGNTEMPLATE_ACQUISITIONRETENTION_FIELD.fieldApiName]: {},
				[CAMPAIGNTEMPLATE_DESCRIPTION_FIELD.fieldApiName]: {},
			},
			whereCaluses: whereCaluses,
		});
	}
	@wire(getRecords, { param: "$paramGetRecords" })
	wiredcampaigntemplateRecords(result) {
		this.sObjectCampaignDao = result;
		if (result.data) {
			if (result.data.toast.ok) {
				refreshApex(this.sObjectCampaignDao);
				//if campaign optimizer is seleted and on change
				//of radio button we are loading the campaign templates
				if (this.campaignOptimizerRecordId) {
					this.getCampaignTemplateRecords();
				}
			}
		}
	}
	getCampaignTemplateRecords() {
		this.addItems = [];
		if (this.isCampaignOptimizer && this.sObjectCampaignDao.data && this.sObjectCampaignDao.data.sObjects.length > 0) {
			this.sObjectCampaignDao.data.sObjects.forEach((sObject) => {
				this.templateRevenueCashflow = sObject.musqotmp__RevenueCashflow__c;
				this.templateMoreDealsBiggerDeals = sObject.musqotmp__MoreDealsBiggerDeals__c;
				this.templateAcquisitionRetention = sObject.musqotmp__AcquisitionRetention__c;
				let optimizerSum = 0;
				let revenue = this.revenueCashFlow - this.templateRevenueCashflow;
				let deal = this.moreDealsBiggerDeals - this.templateMoreDealsBiggerDeals;
				let acquisition = this.acquisitionRetention - this.templateAcquisitionRetention;
				optimizerSum = revenue + deal + acquisition;
				optimizerSum = Math.abs(revenue) + Math.abs(deal) + Math.abs(acquisition);
				let color = "#eb2f4e";
				if (optimizerSum < 25) {
					color = "#6ce02d";
				} else if (optimizerSum < 50) {
					color = "#ff9933";
				} else if (optimizerSum < 100) {
					color = "#ff5c33";
				}
				let metatext = "";
				let description = "";
				if (optimizerSum > 0) {
					metatext += "Rank " + optimizerSum;
				} else {
					metatext += "Rank " + 1;
					color = "#ff9933";
				}
				if (sObject.musqotmp__Description__c) {
					description = sObject.musqotmp__Description__c;
				}
				this.addItems.push({
					label: sObject.Name,
					name: sObject.Id,
					rank: optimizerSum,
					metatext: metatext,
					color: "background-color:" + color,
					description: description,
					icon: "/resource/musqotmp__slds_icons/utility/campaign.svg",
				});
			});
			this.addItems.sort((a, b) => (a.rank > b.rank ? 1 : b.rank > a.rank ? -1 : 0));
		}
		if (this.addItems.length > 0) {
			this.isEmptyTemplate = false;
			this.showTemplate = true;
		} else {
			this.isEmptyTemplate = true;
		}
	}
	get options() {
		return [
			{ label: "Acquisition", value: "Acquisition" },
			{ label: "Awareness", value: "Awareness" },
			{ label: "Retention", value: "Retention" },
			{ label: "Revenue", value: "Revenue" },
		];
	}
	handleOptimizerChange(event) {
		this.typeId = "";
		this.showTemplate = false;
		//this.sObjectCampaign = [];
		this.selectedValue = "";
		//this.campaignOptimizers=[];
		this.addItems = [];
		this.selectedValue = event.detail.value;
		if (this.selectedValue) {
			this.showOptimizerSchema = true;
			this.paramGetRecords = this.buildParamCampaignTemplate(this.selectedValue);
		}
	}

	@wire(getPicklistValues, {
		recordTypeId: "012000000000000AAA",
		fieldApiName: CAMPAIGN_TYPE_FIELD,
	})
	picklistValues;

	@wire(getPicklistValues, {
		recordTypeId: "012000000000000AAA",
		fieldApiName: MARKETINGACTIVITY_TYPE_FIELD,
	})
	picklistMarketingActivityTypeValues;

	buildCampaignOptimizers(sObjects) {
		const options = [];
		if (sObjects.length > 0) {
			this.campaignOptimizerRecordId = sObjects[0].Id;
		}
		sObjects.forEach((sObject) => {
			if (this.campaignOptimizerPlanMap[sObject.musqotmp__MarketingPlan__c]) {
				this.campaignOptimizerPlanMap[sObject.musqotmp__MarketingPlan__c].push({ label: sObject.Name, value: sObject.Id });
			} else {
				this.campaignOptimizerPlanMap[sObject.musqotmp__MarketingPlan__c] = [];
				this.campaignOptimizerPlanMap[sObject.musqotmp__MarketingPlan__c].push({ label: sObject.Name, value: sObject.Id });
			}
			options.push({ label: sObject.Name, value: sObject.Id });
		});
		return options;
	}
	disconnectedCallback() {
		gantt.clearAll();
	}
	buildGanttData() {
		let tasks = {
			data: [],
			links: [],
		};
		this.sObjects.forEach((sObjectCampaign) => {
			let task = {};
			let link = {};
			/*let sD=this.moment(sObjectCampaign.StartDate).toDate();
			let eD=this.moment(sObjectCampaign.EndDate).toDate();
			if (!this.startDate || sD < this.startDate) {
				this.startDate = sD;
			}
			if (!this.endDate || eD > this.endDate) {
				this.endDate = eD;
			}*/
			task.id = sObjectCampaign.Id;
			task.objectapiname = "Campaign";
			task.folder_type = "campaign";
			task.view = this.sObject.musqotmp__GanttView__c;
			task.task_type = sObjectCampaign.Type;
			task.text = sObjectCampaign.Name;
			this.campaignPlanMap[sObjectCampaign.Id] = sObjectCampaign.musqotmp__MarketingPlan__c;
			let barText = [];
			if (this.sObject.musqotmp__GanttView__c === "Compact") {
				let name = this.barTextFields[0].name;
				if (this.barTextFields[0].type === "reference") {
					if (name === "OwnerId") {
						name = "Owner";
					} else if (name === "CreatedById") {
						name = "CreatedBy";
					} else if (name === "LastModifiedById") {
						name = "LastModifiedBy";
					} else if (name.includes("__c")) {
						name = name.replace("__c", "__r");
					}
					barText.push(sObjectCampaign[name].Name === undefined ? "-" : sObjectCampaign[name].Name);
				} else {
					barText.push(sObjectCampaign[name] === undefined ? "-" : sObjectCampaign[name]);
				}
			} else {
				this.barTextFields.forEach((barTextField, index) => {
					let name = barTextField.name;
					if (barTextField.type === "reference") {
						if (name === "OwnerId") {
							name = "Owner";
						} else if (name === "CreatedById") {
							name = "CreatedBy";
						} else if (name === "LastModifiedById") {
							name = "LastModifiedBy";
						} else if (name.includes("__c")) {
							name = name.replace("__c", "__r");
						}
						barText.push(sObjectCampaign[name].Name === undefined ? "-" : sObjectCampaign[name].Name);
					} else {
						barText.push(sObjectCampaign[name] === undefined ? "-" : sObjectCampaign[name]);
					}
					if (this.barTextFields[index + 1]) {
						barText.push("<br />");
					}
				});
			}
			task.bartext = barText.join(" ");
			if (this.barColorMap["Campaign~~Type~~" + sObjectCampaign.Type]) {
				task.color = this.barColorMap["Campaign~~Type~~" + sObjectCampaign.Type];
			}
			task.status = sObjectCampaign.Status;
			task.status_icon = sObjectCampaign.musqotmp__StatusIcon__c;
			task.progress = 1;
			task.start_date = moment(sObjectCampaign.StartDate).format("DD-MM-YYYY");
			task.duration = Math.round((new Date(sObjectCampaign.EndDate) - new Date(sObjectCampaign.StartDate)) / (1000 * 60 * 60 * 24)) + 1;
			if (sObjectCampaign.ParentId) {
				task.parent = sObjectCampaign.ParentId;
				link.source = sObjectCampaign.ParentId;
				link.target = sObjectCampaign.Id;
				link.type = 0;
				tasks.links.push(link);
			}
			tasks.data.push(task);
			if (sObjectCampaign.musqotmp__MarketingActivities__r) {
				sObjectCampaign.musqotmp__MarketingActivities__r.forEach((sObjectMarketingActivity) => {
					task = {};
					link = {};
					task.id = sObjectMarketingActivity.Id;
					task.objectapiname = "musqotmp__MarketingActivity__c";
					task.folder_type = "marketingactivity";
					task.view = this.sObject.musqotmp__GanttView__c;
					task.task_type = sObjectMarketingActivity.musqotmp__Type__c;
					task.text = sObjectMarketingActivity.Name;
					let barText = [];
					//Sowmya user stroy W-000365 BarText
					if (this.sObject.musqotmp__GanttView__c === "Compact") {
						let name = this.barTextFields[0].name;
						if (this.barTextFields[0].type === "reference") {
							if (name === "OwnerId") {
								// name = 'Owner';
								name = "CreatedBy";
							} else if (name === "CreatedById") {
								name = "CreatedBy";
							} else if (name === "LastModifiedById") {
								name = "LastModifiedBy";
							} else if (name.includes("__c")) {
								name = name.replace("__c", "__r");
							}
							barText.push(sObjectMarketingActivity[name]["Name"] === undefined ? "-" : sObjectMarketingActivity[name]["Name"]);
						} else {
							barText.push(sObjectMarketingActivity[name] === undefined ? "-" : sObjectMarketingActivity[name]);
						}
					} else {
						this.barTextFields.forEach((barTextField, index) => {
							let name = barTextField.name;
							if (barTextField.type === "reference") {
								if (name === "OwnerId") {
									//name = 'Owner';
									name = "CreatedBy";
								} else if (name === "CreatedById") {
									name = "CreatedBy";
								} else if (name === "LastModifiedById") {
									name = "LastModifiedBy";
								} else if (name.includes("__c")) {
									name = name.replace("__c", "__r");
								}
								barText.push(sObjectMarketingActivity[name]["Name"] === undefined ? "-" : sObjectMarketingActivity[name]["Name"]);
							} else {
								barText.push(sObjectMarketingActivity[name] === undefined ? "-" : sObjectMarketingActivity[name]);
							}
							if (this.barTextFields[index + 1]) {
								barText.push("<br />");
							}
						});
					}
					//Sowmya End
					//barText.push(sObjectMarketingActivity.Name);
					task.bartext = barText.join(" ");
					if (this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + sObjectMarketingActivity.musqotmp__Type__c]) {
						task.color = this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + sObjectMarketingActivity.musqotmp__Type__c];
					}
					task.status = sObjectMarketingActivity.musqotmp__Status__c;
					task.status_icon = sObjectMarketingActivity.musqotmp__StatusIcon__c;
					task.progress = 1;
					task.start_date = moment(sObjectMarketingActivity.musqotmp__StartDate__c).format("DD-MM-YYYY");
					task.duration = Math.round((new Date(sObjectMarketingActivity.musqotmp__EndDate__c) - new Date(sObjectMarketingActivity.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
					task.parent = sObjectMarketingActivity.musqotmp__Campaign__c;
					tasks.data.push(task);
					link.source = sObjectMarketingActivity.musqotmp__Campaign__c;
					link.target = sObjectMarketingActivity.Id;
					link.type = 0;
					tasks.links.push(link);
				});
			}
			if (sObjectCampaign.musqotmp__MarketingJourneys__r) {
				sObjectCampaign.musqotmp__MarketingJourneys__r.forEach((sObjectMarketingJourney) => {
					task = {};
					link = {};
					task.id = sObjectMarketingJourney.Id;
					task.objectapiname = "musqotmp__MarketingJourney__c";
					task.folder_type = "marketingjourney";
					task.view = this.sObject.musqotmp__GanttView__c;
					task.task_type = sObjectCampaign.Type;
					task.text = sObjectMarketingJourney.Name;
					let barText = [];
					//Sowmya user stroy W-000365 BarText
					if (this.sObject.musqotmp__GanttView__c === "Compact") {
						let name = this.barTextFields[0].name;
						if (this.barTextFields[0].type === "reference") {
							if (name === "OwnerId") {
								name = "Owner";
							} else if (name === "CreatedById") {
								name = "CreatedBy";
							} else if (name === "LastModifiedById") {
								name = "LastModifiedBy";
							} else if (name.includes("__c")) {
								name = name.replace("__c", "__r");
							}
							barText.push(sObjectMarketingJourney[name]["Name"] === undefined ? "-" : sObjectMarketingJourney[name]["Name"]);
						} else {
							barText.push(sObjectMarketingJourney[name] === undefined ? "-" : sObjectMarketingJourney[name]);
						}
					} else {
						this.barTextFields.forEach((barTextField, index) => {
							let name = barTextField.name;
							if (barTextField.type === "reference") {
								if (name === "OwnerId") {
									name = "Owner";
								} else if (name === "CreatedById") {
									name = "CreatedBy";
								} else if (name === "LastModifiedById") {
									name = "LastModifiedBy";
								} else if (name.includes("__c")) {
									name = name.replace("__c", "__r");
								}
								barText.push(sObjectMarketingJourney[name]["Name"] === undefined ? "-" : sObjectMarketingJourney[name]["Name"]);
							} else {
								barText.push(sObjectMarketingJourney[name] === undefined ? "-" : sObjectMarketingJourney[name]);
							}
							if (this.barTextFields[index + 1]) {
								barText.push("<br />");
							}
						});
					}
					//Sowmya End
					//barText.push(sObjectMarketingJourney.Name);
					task.bartext = barText.join(" ");
					task.status = sObjectMarketingJourney.musqotmp__Status__c;
					task.status_icon = sObjectMarketingJourney.musqotmp__StatusIcon__c;
					task.progress = 1;
					task.start_date = moment(sObjectMarketingJourney.musqotmp__StartDate__c).format("DD-MM-YYYY");
					task.duration = Math.round((new Date(sObjectMarketingJourney.musqotmp__EndDate__c) - new Date(sObjectMarketingJourney.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
					task.parent = sObjectMarketingJourney.musqotmp__Campaign__c;
					tasks.data.push(task);
					link.source = sObjectMarketingJourney.musqotmp__Campaign__c;
					link.target = sObjectMarketingJourney.Id;
					link.type = 0;
					tasks.links.push(link);
					if (sObjectMarketingJourney.musqotmp__JourneyID__c && this.marketingCloudCredential) {
						this.mcUrlMap[sObjectMarketingJourney.Id] =
							this.marketingCloudCredential.musqotmp__TargetURL__c + sObjectMarketingJourney.musqotmp__JourneyID__c + "/" + sObjectMarketingJourney.musqotmp__JourneyVersion__c;
					}
				});
			}
		});
		return tasks;
	}

	ganttPromise() {
		//if (this.ganttInitialized) {
		//return;
		//}
		//this.ganttInitialized = true;
		Promise.all([loadScript(this, Gantt + "/codebase/dhtmlxgantt.js"), loadStyle(this, Gantt + "/codebase/dhtmlxgantt.css")])
			.then(() => {
				this.initializeGantt();
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}

	initializeGantt() {
		const thislwc = this;
		const mom = moment();
		let today = new Date();

		if (this.sObject.musqotmp__DateRange__c === "THIS_MONTH") {
			this.startDate = mom.startOf("M").toDate();
			this.endDate = mom.endOf("M").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "NEXT_MONTH") {
			this.startDate = mom.add(1, "M").startOf("M").toDate();
			this.endDate = mom.endOf("M").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "THIS_MONTH,NEXT_MONTH") {
			this.startDate = mom.startOf("M").toDate();
			this.endDate = mom.add(1, "M").endOf("M").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "THIS_QUARTER") {
			this.startDate = mom.startOf("Q").toDate();
			this.endDate = mom.endOf("Q").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "NEXT_QUARTER") {
			this.startDate = mom.add(1, "Q").startOf("Q").toDate();
			this.endDate = mom.endOf("Q").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "THIS_QUARTER,NEXT_QUARTER") {
			this.startDate = mom.startOf("Q").toDate();
			this.endDate = mom.add(1, "Q").endOf("Q").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "LAST_QUARTER,THIS_QUARTER") {
			this.startDate = mom.subtract(1, "Q").startOf("Q").toDate();
			this.endDate = mom.add(1, "Q").endOf("Q").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "THIS_YEAR") {
			this.startDate = mom.startOf("Y").toDate();
			this.endDate = mom.endOf("Y").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "NEXT_YEAR") {
			this.startDate = mom.add(1, "Y").startOf("Y").toDate();
			this.endDate = mom.endOf("Y").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "THIS_YEAR,NEXT_YEAR") {
			this.startDate = mom.startOf("Y").toDate();
			this.endDate = mom.add(1, "Y").endOf("Y").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "LAST_YEAR,THIS_YEAR") {
			this.startDate = mom.subtract(1, "Y").startOf("Y").toDate();
			this.endDate = mom.add(1, "Y").endOf("Y").toDate();
		} else {
			this.startDate = mom.startOf("M").toDate();
			this.endDate = mom.endOf("M").toDate();
		}

		gantt.init(this.template.querySelector("div.gantt"), this.startDate, this.endDate);
		let zoomConfig = {
			levels: [
				{
					name: "day",
					scale_height: 27,
					min_column_width: 45,
					scales: [{ unit: "day", step: 1, format: "%d %M" }],
				},
				{
					name: "week",
					scale_height: 50,
					min_column_width: 50,
					scales: [
						{
							unit: "week",
							step: 1,
							format: function (date) {
								var dateToStr = gantt.date.date_to_str("%d %M");
								var endDate = gantt.date.add(date, -6, "day");
								var weekNum = gantt.date.date_to_str("%W")(date);
								return "#" + weekNum + ", " + dateToStr(date) + " - " + dateToStr(endDate);
							},
						},
						{ unit: "day", step: 1, format: "%j %D" },
					],
				},
				{
					name: "month",
					scale_height: 35,
					scales: [
						{ unit: "month", format: "%F" },
						{ unit: "week", format: "%W" },
					],
				},
				{
					name: "quarter",
					height: 50,
					min_column_width: 90,
					scales: [
						{ unit: "month", step: 1, format: "%M" },
						{
							unit: "quarter",
							step: 1,
							format: function (date) {
								var dateToStr = gantt.date.date_to_str("%M");
								var endDate = gantt.date.add(gantt.date.add(date, 3, "month"), -1, "day");
								return dateToStr(date) + " - " + dateToStr(endDate);
							},
						},
					],
				},
				{
					name: "year",
					scale_height: 50,
					min_column_width: 30,
					scales: [{ unit: "year", step: 1, format: "%Y" }],
				},
			],
		};
		gantt.ext.zoom.init(zoomConfig);
		//gantt.ext.zoom.setLevel("month");
		gantt.config.row_height = this.sObject.musqotmp__GanttView__c === "Compact" ? 25 : 35;
		gantt.config.columns = [
			{ name: "text", label: this.label.hl_Campaign, tree: true, resize: true },
			{
				name: "status",
				label: this.label.statusHeader,
				width: 50,
				template: function (obj) {
					return "<div>" + obj.status_icon + "</div>";
				},
				align: "center",
			},
			{ name: "add", label: "", width: 36 },
		];

		gantt.templates.grid_folder = function (item) {
			return "<div class='gantt_tree_icon gantt_" + item.folder_type + "'></div>";
		};

		gantt.templates.grid_file = function (item) {
			return "<div class='gantt_tree_icon gantt_" + item.folder_type + "'></div>";
		};

		gantt.templates.task_text = function (start, end, task) {
			let iconUrl = "";
			switch (task.folder_type) {
				case "campaign":
					if (task.view === "Compact") {
						return task.bartext;
					}
					return '<div class="campaign">' + task.bartext + "</div>";
					break;
				/*	case 'marketingactivity':
						if (thislwc.standardIcons.includes(task.task_type)) {
							iconUrl = '/resource/musqotmp__Icon_' + task.task_type;
						} else {
							iconUrl = '/resource/Icon_' + task.task_type;
						}
						if (task.view === 'Compact') {
							return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="16" height="16" />';
						}
						return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="24" height="24" />';
						break;*/
				case "marketingactivity":
					if (thislwc.standardIcons.includes(task.task_type)) {
						iconUrl = "/resource/musqotmp__Icon_" + task.task_type;
					} else {
						iconUrl = "/resource/Icon_" + task.task_type;
					}
					if (task.view === "Compact") {
						return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="16" height="16" /> ' + task.bartext + "";
					}
					return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="24" height="24" /> ' + task.bartext + "";
					break;
				//Sowmya End
				case "marketingjourney":
					if (thislwc.standardIcons.includes(task.task_type)) {
						iconUrl = "/resource/musqotmp__Icon_" + task.task_type;
					} else {
						iconUrl = "/resource/Icon_" + task.task_type;
					}
					if (task.view === "Compact") {
						return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="16" height="16" /> ' + task.bartext + "";
					}
					return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="24" height="24" /> ' + task.bartext + "";
					break;
				default:
					return task.text;
					break;
			}
		};

		gantt.showLightbox = function (id) {
			if (id !== null) {
				let taskObj = gantt.getTask(id);
				thislwc.taskId = id;
				thislwc.objectApiName = taskObj.objectapiname;
				//Sowmya Start user story W-000385
				if (thislwc.objectApiName === "Campaign") {
					thislwc.isPlan = false;
					thislwc.isCampaign = true;
					thislwc.isMarketingJourney = false;
					thislwc.isMarketingActivity = false;
					thislwc.fieldsAllocation = thislwc.fieldMap["musqotmp__Allocation__c"];
					thislwc.taskIdCampaign = taskObj.id;
					thislwc.objectFileds = [CAMPAIGN_MARKETINGPLAN_FIELD];
					if (thislwc.fieldsAllocation) {
						thislwc.paramDataTable = thislwc.buildParamDataTable();
						thislwc.financeAllocation = true;
					}
				} else if (thislwc.objectApiName === "musqotmp__MarketingActivity__c") {
					thislwc.isMarketingActivity = true;
					thislwc.isPlan = false;
					thislwc.isCampaign = false;
					thislwc.isMarketingJourney = false;
					thislwc.otherobjectapiname = "Campaign";
					thislwc.taskIdCampaign = taskObj.parent;
					thislwc.fieldsCampaign = thislwc.fieldMap["Campaign"];
					thislwc.fieldsAllocation = thislwc.fieldMap["musqotmp__Allocation__c"];
					thislwc.objectFileds = [MARKETING_ACTIVITY_CAMPAIGN_NAME_FIELD, MARKETING_ACTIVITY_PLAN_FIELD];
					if (thislwc.fieldsAllocation) {
						thislwc.paramDataTable = thislwc.buildParamDataTable();
						thislwc.financeAllocation = true;
					}
				} else if (thislwc.objectApiName === "musqotmp__MarketingJourney__c") {
					thislwc.isMarketingJourney = true;
					thislwc.isMarketingActivity = false;
					thislwc.isPlan = false;
					thislwc.isCampaign = false;
					thislwc.otherobjectapiname = "Campaign";
					thislwc.taskIdCampaign = taskObj.parent;
					thislwc.fieldsCampaign = thislwc.fieldMap["Campaign"];
					thislwc.fieldsAllocation = thislwc.fieldMap["musqotmp__Allocation__c"];
					thislwc.objectFileds = [MARKETING_JOURNEY_CAMPAIGN_NAME_FIELD, MARKETING_JOURNEY_PLAN_FIELD];
					if (thislwc.fieldsAllocation) {
						thislwc.paramDataTable = thislwc.buildParamDataTable();
						thislwc.financeAllocation = true;
					}
				}
				//Sowmya End W-000385
				/*	if (thislwc.objectApiName === 'Campaign') {
						thislwc.objectFileds = [CAMPAIGN_MARKETINGPLAN_FIELD];
					} else if (thislwc.objectApiName === 'musqotmp__MarketingActivity__c') {
						thislwc.objectFileds = [MARKETING_ACTIVITY_CAMPAIGN_NAME_FIELD, MARKETING_ACTIVITY_PLAN_FIELD];
					} else if (thislwc.objectApiName === 'musqotmp__MarketingJourney__c') {
						thislwc.objectFileds = [MARKETING_JOURNEY_CAMPAIGN_NAME_FIELD, MARKETING_JOURNEY_PLAN_FIELD];
					}*/

				thislwc.fields = thislwc.fieldMap[taskObj.objectapiname];
				thislwc.taskRecId = thislwc.taskId;
				thislwc.header = taskObj.text;
				const modal = thislwc.template.querySelector("c-modal");
				modal.show();
				document.cookie = "taskWorkSpaceParentId=" + id;
			}
		};
		gantt.attachEvent("onTaskCreated", function (task) {
			thislwc.parent = task.parent;
			this.parent = thislwc.parent;
			thislwc.showAddModal = true;
			thislwc.campaignOptimizers = undefined;
			if (task.parent) {
				let planId = thislwc.campaignPlanMap[task.parent];
				thislwc.campaignOptimizers = thislwc.campaignOptimizerPlanMap[planId];
			} else {
				thislwc.campaignOptimizers = thislwc.campaignOptimizerOption;
			}
			const modal = thislwc.template.querySelector("c-my-workspace-add-modal");
			modal.show();
		});
		gantt.attachEvent("onTaskDrag", function (id, mode, task, original) {
			var modes = gantt.config.drag_mode;
			if (mode == modes.move) {
				var diff = task.start_date - original.start_date;
				gantt.eachTask(function (child) {
					child.start_date = new Date(+child.start_date + diff);
					child.end_date = new Date(+child.end_date + diff);
					gantt.refreshTask(child.id, true);
				}, id);
			}
			return true;
		});
		gantt.attachEvent("onAfterTaskDrag", function (id, mode, e) {
			let taskObj = gantt.getTask(id);
			let tsk = {};
			var sdate = new Date(taskObj.start_date);
			//window.console.log(sdate);
			sdate.setDate(sdate.getDate() + 1);

			if (taskObj.objectapiname === "Campaign") {
				tsk[CAMPAIGN_ID_FIELD.fieldApiName] = id;
				tsk[CAMPAIGN_SD_FIELD.fieldApiName] = sdate.toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
				tsk[CAMPAIGN_ED_FIELD.fieldApiName] = new Date(taskObj.end_date).toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
			}
			if (taskObj.objectapiname === "musqotmp__MarketingActivity__c") {
				tsk[MARKETINGACTIVITY_ID_FIELD.fieldApiName] = id;
				tsk[MARKETINGACTIVITY_SD_FIELD.fieldApiName] = sdate.toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
				tsk[MARKETINGACTIVITY_ED_FIELD.fieldApiName] = new Date(taskObj.end_date).toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
			}
			if (taskObj.objectapiname === "musqotmp__MarketingJourney__c") {
				tsk[MARKETINGJOURNEY_ID_FIELD.fieldApiName] = id;
				tsk[MARKETINGJOURNEY_SD_FIELD.fieldApiName] = sdate.toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
				tsk[MARKETINGJOURNEY_ED_FIELD.fieldApiName] = new Date(taskObj.end_date).toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
			}
			thislwc.ganttTasksMap[id] = tsk;
			gantt.eachTask(function (child) {
				var cdate = new Date(child.start_date);
				cdate.setDate(cdate.getDate() + 1);
				tsk = {};
				child.start_date = gantt.roundDate({
					date: child.start_date,
					unit: gantt.config.duration_unit,
					step: gantt.config.duration_step,
				});
				child.end_date = gantt.calculateEndDate(child.start_date, child.duration, gantt.config.duration_unit);
				if (child.objectapiname === "Campaign") {
					tsk[CAMPAIGN_ID_FIELD.fieldApiName] = child.id;
					tsk[CAMPAIGN_SD_FIELD.fieldApiName] = cdate.toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
					tsk[CAMPAIGN_ED_FIELD.fieldApiName] = new Date(child.end_date).toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
				}
				if (child.objectapiname === "musqotmp__MarketingActivity__c") {
					tsk[MARKETINGACTIVITY_ID_FIELD.fieldApiName] = child.id;
					tsk[MARKETINGACTIVITY_SD_FIELD.fieldApiName] = cdate.toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
					tsk[MARKETINGACTIVITY_ED_FIELD.fieldApiName] = new Date(child.end_date).toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
				}
				if (child.objectapiname === "musqotmp__MarketingJourney__c") {
					tsk[MARKETINGJOURNEY_ID_FIELD.fieldApiName] = child.id;
					tsk[MARKETINGJOURNEY_SD_FIELD.fieldApiName] = cdate.toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
					tsk[MARKETINGJOURNEY_ED_FIELD.fieldApiName] = new Date(child.end_date).toISOString(); //Sowmya Fix: Not Saving drag and drop of bar
				}
				thislwc.ganttTasksMap[child.id] = tsk;
				gantt.updateTask(child.id);
			}, id);

			if (thislwc.showFormFooter === false) {
				thislwc.showFormFooter = true;
			}
		});

		//gantt.config.readonly = true;
		gantt.config.drag_move = true;
		gantt.config.drag_resize = true;
		gantt.config.drag_progress = true;
		gantt.templates.grid_row_class = function (start, end, task) {
			if (task.objectapiname === "musqotmp__MarketingActivity__c" || task.objectapiname === "musqotmp__MarketingJourney__c") {
			//	console.log("gantt.templates.grid_row_class ==== " + task.$level);
				if (task.$level >= 1) {
					return "nested_task";
				}
				//	return 'hide_add_task_button';
			}
			return "";
		};
		gantt.init(this.template.querySelector("div.gantt"), this.startDate, this.endDate);
		gantt.config.grid_resize = true;
		gantt.config.start_date = this.startDate;
		gantt.config.end_date = this.endDate;
		gantt.clearAll();
		gantt.parse(this.tasks);
		if (thislwc.ganttInitialized) {
			gantt.refreshData();
		}
		thislwc.ganttInitialized = true;
		gantt.config.show_errors = false;
		let taskWorkSpaceParentId = readCookie("taskWorkSpaceParentId");
		while (taskWorkSpaceParentId) {
			taskWorkSpaceParentId = gantt.getParent(taskWorkSpaceParentId);
			if (taskWorkSpaceParentId) {
				gantt.open(taskWorkSpaceParentId);
			}
		}

		window.onresize = function () {
			gantt.render();
		};
	}
	//Anji
	@wire(getRecord, { recordId: "$taskRecId", fields: "$objectFileds" })
	wiredGetTaskRecord({ data, error }) {
		if (data) {
			this.data = data;
			if (this.objectApiName === "Campaign") {
				this.marketingPlan = this.data.fields.musqotmp__MarketingPlan__r.value.fields.Name.value;
			} else if (this.objectApiName === "musqotmp__MarketingActivity__c" || this.objectApiName === "musqotmp__MarketingJourney__c") {
				this.marketingPlan = this.data.fields.musqotmp__Campaign__r.value.fields.musqotmp__MarketingPlan__r.value.fields.Name.value;
				this.campaignValue = this.data.fields.musqotmp__Campaign__r.value.fields.Name.value;
			}
			let newfieldList = [];
			let newfieldsCampaign = [];
			if (this.objectApiName === "Campaign") {
				newfieldList = Object.assign([], this.fields);
				for (let i = 0; i < newfieldList.length; i++) {
					if (newfieldList[i].name === "musqotmp__MarketingPlan__c") {
						newfieldList.splice(i, 1, {
							label: "Marketing Plan",
							name: "Plan",
							hasMarketingPlan: true,
						});
						let objectName = "musqotmp__MarketingPlan__c";
						let taskrecordId = data.fields.musqotmp__MarketingPlan__r.value.id;
						this.navigateToDetailViewPage(objectName, taskrecordId);
					}
				}
			}
			if (this.objectApiName === "musqotmp__MarketingActivity__c" || this.objectApiName === "musqotmp__MarketingJourney__c") {
				newfieldList = Object.assign([], this.fields);
				for (let i = 0; i < newfieldList.length; i++) {
					if (newfieldList[i].name === "musqotmp__Campaign__c") {
						newfieldList.splice(i, 1, {
							label: "Campaign",
							name: "Campaign",
							hasCampaign: true,
						});
						let objectName = "musqotmp__Campaign__c";
						let taskrecordId = this.data.fields.musqotmp__Campaign__r.value.id;
						this.navigateToDetailViewPage(objectName, taskrecordId);
					}
					if (newfieldList[i].name === "musqotmp__Campaign__r.musqotmp__MarketingPlan__c") {
						newfieldList.splice(i, 1, {
							label: "Marketing Plan",
							name: "musqotmp__Campaign__r.musqotmp__MarketingPlan__c",
							hasMarketingPlan: true,
						});
						let objectName = "musqotmp__MarketingPlan__c";
						let taskrecordId = this.data.fields.musqotmp__Campaign__r.value.fields.musqotmp__MarketingPlan__r.value.id;
						this.navigateToDetailViewPage(objectName, taskrecordId);
					}
				}

				newfieldsCampaign = Object.assign([], this.fieldsCampaign);
				for (let i = 0; i < newfieldsCampaign.length; i++) {
					if (newfieldsCampaign[i].name === "musqotmp__MarketingPlan__c") {
						newfieldsCampaign.splice(i, 1, {
							label: "Marketing Plan",
							name: "musqotmp__MarketingPlan__c",
							hasMarketingPlan: true,
						});
						let objectName = "musqotmp__MarketingPlan__c";
						let taskrecordId = this.data.fields.musqotmp__Campaign__r.value.fields.musqotmp__MarketingPlan__r.value.id;
						this.navigateToDetailViewPage(objectName, taskrecordId);
					}
				}
			}
			this.fields = newfieldList;
			this.fieldsCampaign = newfieldsCampaign;
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}
	navigateToDetailViewPage(objectName, taskrecordId) {
		this[NavigationMixin.GenerateUrl]({
			type: "standard__recordPage",
			attributes: {
				recordId: taskrecordId,
				objectApiName: objectName,
				actionName: "view",
			},
		})
			.then((url) => {
				//window.open(url)
				if (objectName === "musqotmp__Campaign__c") {
					this.campaignUrl = url;
				}
				if (objectName === "musqotmp__MarketingPlan__c") {
					this.url = url;
				}
				//this[NavigationMixin.Navigate](url);
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}

	handleRefresh() {
		if (this.wiredData) {
			refreshApex(this.wiredData);
		}
	}

	handleCancelModal() {
		const modal = this.template.querySelector("c-modal");
		modal.hide();
		this.isMarketingJourney = false;
		this.isPlan = false;
		this.isCampaign = false;
		this.isMarketingJourney = false;
		this.taskIdCampaign = "";
		this.hasCampaignTab = false;
		this.hasOpenButton = false;
		this.financeAllocation = false;
	}

	handleOpen() {
		this.navigateToRecord();
	}

	/*navigateToRecord() {
		this[NavigationMixin.GenerateUrl]({
			type: 'standard__recordPage',
			attributes: {
				recordId: this.taskId,
				objectApiName: this.objectApiName,
				actionName: 'view'
			}
		}).then((url) => {
			window.open(url, 'Musqot');
			this.handleCancelModal();
		});
	}*/
	handleNavigateToMarketingCloud() {
		/*this[NavigationMixin.Navigate]({
	  type: "standard__webPage",
	  attributes: {
		url: this.mcUrlMap["" + this.taskId + ""]
	  }
	});*/
		window.open(this.mcUrlMap["" + this.taskId + ""], "MarketingCloud");
		this.handleCancelModal();
	}
	get mcUrl() {
		return this.mcUrlMap[this.taskId] ? this.mcUrlMap[this.taskId] : null;
	}
	handleCancelAddModal() {
		this.showTemplate = false;
		//this.typeId = '';
		this.selectedValue = "";
		const modal = this.template.querySelector("c-my-workspace-add-modal");
		modal.hide();
	}
	handleAddSection(obj) {
		this.typeId = "";
		this.selectedValue = "";
		this.showTemplate = false;
		this.template.querySelectorAll(".slds-nav-vertical__item").forEach((ip) => {
			ip.classList.remove(CSS_SLDSISACTIVE);
		});
		obj.currentTarget.parentElement.classList.add(CSS_SLDSISACTIVE);
		this.addItems = [];
		let objectApiName = obj.currentTarget.name;
		this.sectionObjectApiName = objectApiName;
		this.showCampaignOptimizer = false;
		this.isEmptyTemplate = false;
		if (objectApiName === "Campaign") {
			this.showCampaignOptimizer = true;
			//this.getCampaignTemplateRecords();
		} else if (objectApiName === "Campaign.Type") {
			this.paramGetCampaign = undefined;
			this.picklistValues.data.values.forEach((item) => {
				if (item.value !== "Folder" && item.value !== "Other") {
					//Sowmya 13/11/2019
					this.addItems.push({
						label: item.label,
						name: item.value,
						rank: 0,
						metatext: "",
						color: "background-color:" + this.barColorMap["Campaign~~Type~~" + item.value],
						icon: "/resource/musqotmp__slds_icons/utility/campaign.svg",
					});
				}
			});
		} else if (objectApiName === "musqotmp__MarketingActivity__c.musqotmp__Type__c") {
			this.paramGetCampaign = undefined;
			this.addItems = [];
			this.picklistMarketingActivityTypeValues.data.values.forEach((item) => {
				this.addItems.push({
					label: item.label,
					name: item.value,
					rank: 0,
					metatext: "",
					color: "background-color:" + this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + item.value],
					icon: "/resource/musqotmp__Icon_" + item.value,
				});
			});
		} else {
			this.paramGetCampaign = this.buildParamCampaign(objectApiName);
		}
		this.showTemplate = true;
	}

	handleSelectType(obj) {
		this.typeId = "";
		this.template.querySelectorAll(".types li").forEach((li) => {
			li.classList.remove(CSS_SLDSISACTIVE);
		});
		obj.currentTarget.classList.add(CSS_SLDSISACTIVE);
		this.typeId = obj.currentTarget.getAttribute("data-name");
		this.templateCloneId = this.typeId;
		this.resetTypeId = this.typeId;
		if (this.sectionObjectApiName === "musqotmp__MarketingActivity__c.musqotmp__Type__c") {
			this.buildTypeLayoutSectionsMarketingActivity(this.layoutSectionsMarketingActivity, this.wiredGetRecord);
		} else {
			this.buildTypeLayoutSections(this.layoutSections, this.wiredGetRecord);
		}
	}
	handleCampaignOptimizerChange(event) {
		this.campaignOptimizerRecordId = null;
		this.campaignOptimizerRecordId = event.detail.value;
		this.campaignOptimizerSchema();
	}
	campaignOptimizerSchema() {
		this.isCampaignOptimizer = true;
		// campaignOptimizers
		this.sObjOptimizer.forEach((sObject) => {
			if (sObject.Id === this.campaignOptimizerRecordId) {
				this.revenueCashFlow = sObject.musqotmp__RevenueCashflow__c;
				this.moreDealsBiggerDeals = sObject.musqotmp__MoreDealsBiggerDeals__c;
				this.acquisitionRetention = sObject.musqotmp__AcquisitionRetention__c;
			}
		});
		this.getCampaignTemplateRecords();
	}
	onLoad(event) {
		this.layoutSections = [event.detail.layout.sections[0]];
		this.activeAccordionSections = [this.layoutSections[0].id];
		this.buildTypeLayoutSections(this.layoutSections, null);
	}
	buildTypeLayoutSections(layoutSections, record) {
		//window.console.log('parent :' + this.parent);
		this.typeLayoutSections = [];
		let apiStartDate;
		let apiEndDate;
		let rightNow = new Date();
		let yyyyMmDd = rightNow.toISOString().slice(0, 10);
		let toDayDate = moment(yyyyMmDd).format("YYYY-MM-DD");
		if (record !== null) {
			apiEndDate = record.fields.EndDate.value;
			apiStartDate = record.fields.StartDate.value;
		}
		layoutSections.forEach((layoutSection) => {
			let typeLayoutSection = {};
			typeLayoutSection.id = layoutSection.id;
			typeLayoutSection.heading = layoutSection.heading;
			let layoutRows = [];
			layoutSection.layoutRows.forEach((layoutRow) => {
				let typeLayoutRows = {};
				let layoutItems = [];
				layoutRow.layoutItems.forEach((layoutItem) => {
					let typeLayoutItems = {};
					let layoutComponents = [];
					if (this.parent !== undefined) {
						layoutItem.layoutComponents.forEach((layoutComponent) => {
							let typeLayoutComponent = {};
							typeLayoutComponent.apiName = layoutComponent.apiName;
							if (layoutComponent.apiName === "Type" && this.typeId) {
								typeLayoutComponent.value = this.typeId;
							} else if (layoutComponent.apiName === "ParentId") {
								typeLayoutComponent.value = this.parent;
							} else if (record !== null && layoutComponent.apiName !== null && layoutComponent.apiName !== "Name" && record.fields[layoutComponent.apiName]) {
								if (layoutComponent.apiName === "musqotmp__MarketingPlan__c") {
									this.selectedPlanRecordId = record.fields.musqotmp__MarketingPlan__c.value;
									this.templatePlan = record.fields.musqotmp__MarketingPlan__r.value.fields.Name.value;
									typeLayoutComponent.hasPlan = true;
								} else if (layoutComponent.apiName == "StartDate") {
									typeLayoutComponent["dateFormate"] = true;
									typeLayoutComponent["label"] = layoutComponent.label;
									if (apiStartDate <= toDayDate) {
										this.stDate = toDayDate;
										typeLayoutComponent.value = toDayDate;
									}
								} else if (layoutComponent.apiName == "EndDate") {
									typeLayoutComponent["dateFormate"] = true;
									typeLayoutComponent["label"] = layoutComponent.label;
									if (apiStartDate <= toDayDate) {
										this.edDate = moment(yyyyMmDd).add(30, "days").format("YYYY-MM-DD");
										typeLayoutComponent.value = moment(yyyyMmDd).add(30, "days").format("YYYY-MM-DD");
									}
								} else {
									typeLayoutComponent.value = record.fields[layoutComponent.apiName].value;
								}
							}
							layoutComponents.push(typeLayoutComponent);
						});
					} else {
						layoutItem.layoutComponents.forEach((layoutComponent) => {
							let typeLayoutComponent = {};
							typeLayoutComponent.apiName = layoutComponent.apiName;
							if (layoutComponent.apiName === "Type" && this.typeId) {
								typeLayoutComponent.value = this.typeId;
							} else if (layoutComponent.apiName === "ParentId") {
								typeLayoutComponent.value = "";
							} else if (layoutComponent.apiName === "musqotmp__MarketingPlan__c") {
								typeLayoutComponent.hasPlan = true;
								typeLayoutComponent.value = "";
							} else if (layoutComponent.apiName === "StartDate") {
								typeLayoutComponent.value = "";
								typeLayoutComponent["dateFormate"] = true;
								typeLayoutComponent["label"] = layoutComponent.label;
							} else if (layoutComponent.apiName === "EndDate") {
								typeLayoutComponent.value = "";
								typeLayoutComponent["dateFormate"] = true;
								typeLayoutComponent["label"] = layoutComponent.label;
							}
							layoutComponents.push(typeLayoutComponent);
						});
					}
					typeLayoutItems.layoutComponents = layoutComponents;
					layoutItems.push(typeLayoutItems);
				});
				typeLayoutRows.layoutItems = layoutItems;
				layoutRows.push(typeLayoutRows);
			});
			//window.console.log('layoutRows :' + JSON.stringify(layoutRows));
			typeLayoutSection.layoutRows = layoutRows;
			this.typeLayoutSections.push(typeLayoutSection);
			//window.console.log('typeLayoutSections :' + JSON.stringify(this.typeLayoutSections));
		});
	}
	handleSubmit(event) {
		event.preventDefault(); // stop the form from submitting
		this.fields = event.detail.fields;
		const fields = this.fields;
		this.disabledAction = true;
		let recordInput;
		let startDt;
		let endDt;
		if (event.currentTarget.getAttribute("data-action")) {
			this.submitAction = event.currentTarget.getAttribute("data-action");
		}
		if (this.submitAction == "NewCampaign") {
			//validation
			this.stDate = this.template.querySelector("[data-field='StartDate']").value;
			this.edDate = this.template.querySelector("[data-field='EndDate']").value;

			startDt = new Date(this.template.querySelector("[data-field='StartDate']").value);
			endDt = new Date(this.template.querySelector("[data-field='EndDate']").value);
			fields[CAMPAIGN_SD_FIELD.fieldApiName] = startDt;
			fields[CAMPAIGN_ED_FIELD.fieldApiName] = endDt;
			fields[RECORD_TYPE_FIELD.fieldApiName] = this.recordTypeId;
			if (this.selectedPlanRecordId) {
				fields[CAMPAIGN_MARKETING_PLAN_FIELD.fieldApiName] = this.selectedPlanRecordId;
			} else {
				fields[CAMPAIGN_MARKETING_PLAN_FIELD.fieldApiName] = null;
			}
			recordInput = { apiName: CAMPAIGN_OBJECT.objectApiName, fields };
		} else if (this.submitAction == "NewMarketingActivity") {
			//validation
			this.stDate = this.template.querySelector("[data-field='musqotmp__StartDate__c']").value;
			this.edDate = this.template.querySelector("[data-field='musqotmp__EndDate__c']").value;

			startDt = new Date(this.template.querySelector("[data-field='musqotmp__StartDate__c']").value);
			endDt = new Date(this.template.querySelector("[data-field='musqotmp__EndDate__c']").value);
			fields[MARKETINGACTIVITY_SD_FIELD.fieldApiName] = startDt;
			fields[MARKETINGACTIVITY_ED_FIELD.fieldApiName] = endDt;
			recordInput = { apiName: MARKETINGACTIVITY_OBJECT.objectApiName, fields };
		} else if (this.submitAction == "NewMarketingJourney") {
			//validation
			this.stDate = this.template.querySelector("[data-field='Start Date']").value;
			this.edDate = this.template.querySelector("[data-field='End Date']").value;

			startDt = new Date(this.template.querySelector("[data-field='Start Date']").value);
			endDt = new Date(this.template.querySelector("[data-field='End Date']").value);
			fields[MARKETINGJOURNEY_SD_FIELD.fieldApiName] = startDt;
			fields[MARKETINGJOURNEY_ED_FIELD.fieldApiName] = endDt;
			recordInput = { apiName: MARKETINGJOURNEY_OBJECT.objectApiName, fields };
		}
		let validateMessage = "These required fields must be completed: ";
		let validateField = "";
		let valid = true;
		if (this.submitAction == "NewCampaign") {
			if (!fields.Name) {
				validateField = " " + this.campaignObjInfo.fields.Name.label + ",";
				valid = false;
				validateMessage += validateField;
			}
			if (this.stDate === "") {
				validateField = " " + this.campaignObjInfo.fields.StartDate.label + ",";
				valid = false;
				validateMessage += validateField;
			}
			if (this.edDate === "") {
				validateField = " " + this.campaignObjInfo.fields.EndDate.label + ",";
				valid = false;
				validateMessage += validateField;
			}
			if (!fields.musqotmp__MarketingPlan__c) {
				validateField = " " + this.campaignObjInfo.fields.musqotmp__MarketingPlan__c.label + ",";
				valid = false;
				validateMessage += validateField;
			}
		} else if (this.submitAction == "NewMarketingActivity") {
			if (!fields.Name) {
				validateField = " " + this.marketingActivity.data.fields.Name.label + ",";
				valid = false;
				validateMessage += validateField;
			}
			if (this.stDate === "") {
				validateField = " " + this.marketingActivity.data.fields.musqotmp__StartDate__c.label + ",";
				valid = false;
				validateMessage += validateField;
			}
			if (this.edDate === "") {
				validateField = " " + this.marketingActivity.data.fields.musqotmp__EndDate__c.label + ",";
				valid = false;
				validateMessage += validateField;
			}
		} else if (this.submitAction == "NewMarketingJourney") {
			if (!fields.Name) {
				validateField = " " + this.marketingJourney.data.fields.Name.label + ",";
				valid = false;
				validateMessage += validateField;
			}
			if (this.stDate === "") {
				validateField = " " + this.marketingJourney.data.fields.musqotmp__StartDate__c.label + ",";
				valid = false;
				validateMessage += validateField;
			}
			if (this.edDate === "") {
				validateField = " " + this.marketingJourney.data.fields.musqotmp__EndDate__c.label + ",";
				valid = false;
				validateMessage += validateField;
			}
		}
		if (valid) {
			createRecord(recordInput)
				.then((result) => {
					this.result = result;
					this.disabledAction = false;
					this.showAddModal = false;
					this.addItems = [];
					this.handleCancelAddModal();
					if (this.submitAction == "NewCampaign") {
						this.cancelCreateCampaign();
					} else if (this.submitAction == "NewMarketingActivity") {
						this.cancelCreateMarketingActivity();
					} else if (this.submitAction == "NewMarketingJourney") {
						this.cancelCreateJourney();
					}
					this.handleRefresh();
					const toast = getToast("success", "pester", this.label.msg_Record_Saved);
					this.template.querySelector("c-toast").showToast(toast);
				})
				.catch((error) => {
					let customErrorField = [];
					customErrorField = error.body.output.errors;
					let standardErrorField = error.body.output.fieldErrors;
					if (standardErrorField) {
						if (standardErrorField.StartDate) {
							let customvalidation = error.body.output.fieldErrors.StartDate[0].message;
							const evt = new ShowToastEvent({
								title: "Error",
								message: customvalidation,
								variant: "error",
								mode: "sticky",
							});
							this.dispatchEvent(evt);
						}
						if (standardErrorField.EndDate) {
							let customvalidation = error.body.output.fieldErrors.EndDate[0].message;
							const evt = new ShowToastEvent({
								title: "Error",
								message: customvalidation,
								variant: "error",
								mode: "sticky",
							});
							this.dispatchEvent(evt);
						}
						if (standardErrorField.ParentId) {
							let customvalidation = error.body.output.fieldErrors.ParentId[0].message;
							const evt = new ShowToastEvent({
								title: "Error",
								message: customvalidation,
								variant: "error",
								mode: "sticky",
							});
							this.dispatchEvent(evt);
						}
					}
					if (customErrorField) {
						for (let i = 0; i < customErrorField.length; i++) {
							const evt = new ShowToastEvent({
								title: "Error",
								message: customErrorField[i].message,
								variant: "error",
								mode: "sticky",
							});
							this.dispatchEvent(evt);
						}
					}
				});
		} else {
			this.disabledAction = false;
			validateMessage = validateMessage.substring(0, validateMessage.length - 1);
			const evt = new ShowToastEvent({
				title: "Error",
				message: validateMessage,
				variant: "error",
				mode: "sticky",
			});
			this.dispatchEvent(evt);
			//const toast = getToast('error', 'sticky', validateMessage);
			//this.template.querySelector('c-toast').showToast(toast);
		}
	}
	handleFormChange() {
		this.disabledAction = false;
	}
	// restting the form
	handleReset(selectedObjectAmiName) {
		const inputFields = this.template.querySelectorAll("lightning-input-field");
		if (inputFields) {
			inputFields.forEach((field) => {
				field.reset();
			});
		}
		if (selectedObjectAmiName == "Campaign.Type") {
			if (this.parent) {
				this.template.querySelector("[data-field='StartDate']").value = this.stDate;
				this.template.querySelector("[data-field='EndDate']").value = this.edDate;
				this.template.querySelector("[data-field='ParentId']").value = this.parent;
				this.template.querySelector("[data-field='Type']").value = this.resetTypeId;
				if (this.wiredGetRecord) {
					//this.template.querySelector("[data-field='musqotmp__Countries__c']").value = this.wiredGetRecord.fields.musqotmp__Countries__c.value;
					this.template.querySelector("[data-field='musqotmp__MarketingBusinessUnit__c']").value = this.wiredGetRecord.fields.musqotmp__MarketingBusinessUnit__c.value;
					this.template.querySelector("[data-field='musqotmp__MarketingArea__c']").value = this.wiredGetRecord.fields.musqotmp__MarketingArea__c.value;
					//this.template.querySelector("[data-field='musqotmp__ProductGroup__c']").value = this.wiredGetRecord.fields.musqotmp__ProductGroup__c.value;
					//this.template.querySelector("[data-field='musqotmp__Shop__c']").value = this.wiredGetRecord.fields.musqotmp__Shop__c.value;
					this.template.querySelector("[data-field='musqotmp__Audience__c']").value = this.wiredGetRecord.fields.musqotmp__Audience__c.value;
					//this.template.querySelector("[data-field='musqotmp__City__c']").value = this.wiredGetRecord.fields.musqotmp__City__c.value;
				}
			} else {
				this.template.querySelector("[data-field='StartDate']").value = "";
				this.template.querySelector("[data-field='EndDate']").value = "";
				this.template.querySelector("[data-field='ParentId']").value = "";
			}
		} else if (selectedObjectAmiName == "musqotmp__MarketingActivity__c.musqotmp__Type__c") {
			//console.log('typeId :' + this.typeId);
			//console.log('parent :' + this.parent);
			this.template.querySelector("[data-field='musqotmp__Type__c']").value = this.resetTypeId;
			this.template.querySelector("[data-field='musqotmp__Campaign__c']").value = this.parent;
			this.template.querySelector("[data-field='musqotmp__StartDate__c']").value = this.stDate;
			this.template.querySelector("[data-field='musqotmp__EndDate__c']").value = this.edDate;
			this.template.querySelector("[data-field='musqotmp__Countries__c']").value = this.wiredGetRecord.fields.musqotmp__Countries__c.value;
			this.template.querySelector("[data-field='musqotmp__MarketingBusinessUnit__c']").value = this.wiredGetRecord.fields.musqotmp__MarketingBusinessUnit__c.value;
			this.template.querySelector("[data-field='musqotmp__MarketingArea__c']").value = this.wiredGetRecord.fields.musqotmp__MarketingArea__c.value;
			this.template.querySelector("[data-field='musqotmp__ProductGroup__c']").value = this.wiredGetRecord.fields.musqotmp__ProductGroup__c.value;
			this.template.querySelector("[data-field='musqotmp__Shop__c']").value = this.wiredGetRecord.fields.musqotmp__Shop__c.value;
			this.template.querySelector("[data-field='musqotmp__Audience__c']").value = this.wiredGetRecord.fields.musqotmp__Audience__c.value;
			this.template.querySelector("[data-field='musqotmp__City__c']").value = this.wiredGetRecord.fields.musqotmp__City__c.value;
		} else if (selectedObjectAmiName === "musqotmp__MarketingJourney__c") {
			this.template.querySelector("[data-field='musqotmp__Campaign__c']").value = this.parent;
			this.template.querySelector("[data-field='musqotmp__StartDate__c']").value = this.stDate;
			this.template.querySelector("[data-field='musqotmp__EndDate__c']").value = this.edDate;
			this.template.querySelector("[data-field='musqotmp__Countries__c']").value = this.wiredGetRecord.fields.musqotmp__Countries__c.value;
			this.template.querySelector("[data-field='musqotmp__BusinessUnit__c']").value = this.wiredGetRecord.fields.musqotmp__MarketingBusinessUnit__c.value;
			this.template.querySelector("[data-field='musqotmp__MarketingRegion__c']").value = this.wiredGetRecord.fields.musqotmp__MarketingArea__c.value;
			this.template.querySelector("[data-field='musqotmp__ProductGroup__c']").value = this.wiredGetRecord.fields.musqotmp__ProductGroup__c.value;
			this.template.querySelector("[data-field='musqotmp__Shop__c']").value = this.wiredGetRecord.fields.musqotmp__Shop__c.value;
			this.template.querySelector("[data-field='musqotmp__Audience__c']").value = this.wiredGetRecord.fields.musqotmp__Audience__c.value;
			this.template.querySelector("[data-field='musqotmp__City__c']").value = this.wiredGetRecord.fields.musqotmp__City__c.value;
		}
	}
	handleContinue() {
		let selectedObjectAmiName;
		this.handleCancelAddModal();
		if (this.sectionObjectApiName === "musqotmp__MarketingActivity__c.musqotmp__Type__c") {
			this.typeHeaderMarketingActivity = this.label.hl_New_Marketing_Activity;
			const modal = this.template.querySelector("c-my-workspace-marketing-activity-modal");
			modal.show();
			selectedObjectAmiName = this.sectionObjectApiName;
			this.handleReset(selectedObjectAmiName);
		} else if (this.sectionObjectApiName === "Campaign") {
			this.newTemplateInfo = true;
			this.cloneTemplate = true;
			let rightNow = new Date();
			// Adjust for the user's time zone
			rightNow.setMinutes(new Date().getMinutes() - new Date().getTimezoneOffset());
			// Return the date in "YYYY-MM-DD" format
			let yyyyMmDd = rightNow.toISOString().slice(0, 10);
			this.topLevelStartDate = yyyyMmDd;
			let endDate = new Date(this.topLevelStartDate);
			endDate.setDate(endDate.getDate() + 30);
			let yyyyMmDdEnd = endDate.toISOString().slice(0, 10);
			this.topLevelEndDate = yyyyMmDdEnd;
			selectedObjectAmiName = this.sectionObjectApiName;
			this.handleReset(selectedObjectAmiName);
		} else {
			this.typeHeader = "New Campaign";
			const modal = this.template.querySelector("c-record-form-modal");
			modal.show();
			selectedObjectAmiName = this.sectionObjectApiName;
			this.handleReset(selectedObjectAmiName);
		}
	}
	handleClone(event) {
		this.action = event.currentTarget.getAttribute("data-action");
		if (this.cloneTemplate) {
			if (this.parent !== undefined) {
				this.paramCloneCampaignTemplate = JSON.stringify({
					component: "myWorkspace-getMyWorkspace",
					recordId: this.parent,
					recordTypeId: this.recordTypeId,
					folderRecordTypeId: this.folderRecordTypeId,
					campaignName: this.campaignName,
					startDate: this.campaignStartDate,
				});
				this.cloneCampaignTemplate();
				this.closeModal();
			}
			if (this.parent === undefined) {
				let valid = false;
				if (this.selectCampaignRecordId && this.campaignName && this.topLevelStartDate && this.topLevelEndDate) {
					valid = true;
				}
				if (valid) {
					this.paramCloneCampaignTemplate = JSON.stringify({
						component: "myWorkspace-getMyWorkspace",
						recordId: this.selectCampaignRecordId,
						recordTypeId: this.recordTypeId,
						folderRecordTypeId: this.folderRecordTypeId,
						campaignName: this.campaignName,
						startDate: this.topLevelStartDate,
						endDate: this.topLevelEndDate,
					});
					this.cloneCampaignTemplate();
					this.closeModal();
				} else {
					const toast = getToast("error", "sticky", "Please fill the required fields.");
					this.template.querySelector("c-toast").showToast(toast);
				}
			}
		}
	}
	cloneCampaignTemplate() {
		cloneCampaignTemplate({
			param: this.paramCloneCampaignTemplate,
			recordId: this.templateCloneId,
		})
			.then((result) => {
				if (result.toast.ok) {
					if (this.action === "open") {
						this.newCampaignId = result.recordId;
						this.navigateToRecordViewPage(this.newCampaignId);
					}
					this.selectRecordId = "";
					this.handleRefresh();
					this.template.querySelector("c-toast").showToast(result.toast);
				} else {
					let validationMessage = result.toast.body.title;
					let occurence;
					if (validationMessage.includes("MAXIMUM_HIERARCHY_LEVELS_REACHED")) {
						occurence = validationMessage.indexOf("MAXIMUM_HIERARCHY_LEVELS_REACHED,") + 34;
						validationMessage = validationMessage.substring(occurence, validationMessage.length);
						occurence = validationMessage.lastIndexOf(":");
						validationMessage = validationMessage.substring(0, occurence);
						//console.log('validationMessage :' + validationMessage);
						//	validationMessage = validationMessage.slice(81, 216);
						const evt = new ShowToastEvent({
							title: "Error",
							message: validationMessage,
							variant: "error",
							mode: "sticky",
						});
						this.dispatchEvent(evt);
					} else {
						const evt = new ShowToastEvent({
							title: "Error",
							message: validationMessage,
							variant: "error",
							mode: "sticky",
						});
						this.dispatchEvent(evt);
					}
				}
			})
			//showing error
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	navigateToRecordViewPage(newCampaignId) {
		this[NavigationMixin.GenerateUrl]({
			type: "standard__recordPage",
			attributes: {
				recordId: newCampaignId,
				objectApiName: "Campaign",
				actionName: "view",
			},
		})
			.then((url) => {
				window.open(url);
				//this[NavigationMixin.Navigate](url);
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	handleFormInputChange(event) {
		if (event.target.name === "campaignName") {
			this.campaignName = event.target.value;
		} else if (event.target.name === "StartDate") {
			this.campaignStartDate = event.target.value;
			this.topLevelStartDate = event.target.value;
		} else if (event.target.name === "EndDate") {
			this.campaignEndDate = event.target.value;
			this.topLevelEndDate = event.target.value;
		}
	}
	closeModal() {
		this.newTemplateInfo = false;
		this.selectRecordId = "";
		const whereCaluses = [];
		whereCaluses.push({
			field: this.objInfo.apiName,
			value: this.selectRecordId,
			clause: "=",
		});
		let sortedDirection = "DESC";
		let amountOfRecords = 400000;
		let sortedBy = "Name";
		let param = JSON.stringify({
			component: "campaign-lookup",
			objectApiName: "Campaign",
			whereCaluses: whereCaluses,
			sortBy: sortedBy,
			sortDirection: sortedDirection,
			amountOfRecords: amountOfRecords,
		});
		this.template.querySelector("c-campaign-lookup").getResultData(param);
	}
	onSuccess(event) {
		this.handleRefresh();
		this.handleCancelRecordFormModal();
		this.recordFormId = event.detail.id;
		this.updateRecord();
		const toast = getToast("success", "pester", this.label.msg_Record_Saved);
		this.template.querySelector("c-toast").showToast(toast);
	}
	updateRecord() {
		const fields = {};
		fields[CAMPAIGN_ID_FIELD.fieldApiName] = this.recordFormId;
		fields[RECORD_TYPE_FIELD.fieldApiName] = this.recordTypeId;
		const recordInput = { fields: fields };
		updateRecord(recordInput)
			.then(() => {
				const toast = getToast("success", "pester", this.label.msg_Record_Saved);
				this.template.querySelector("c-toast").showToast(toast);
			})
			.catch((error) => {
				let customErrorField = [];
				customErrorField = error.body.output.errors;
				if (customErrorField) {
					for (let i = 0; i < customErrorField.length; i++) {
						const evt = new ShowToastEvent({
							title: "Error",
							message: customErrorField[i].message,
							variant: "error",
							mode: "sticky",
						});
						this.dispatchEvent(evt);
					}
				}
				//this.template.querySelector('c-toast').showToast(error);
			});
	}
	handleCancelRecordFormModal() {
		this.recordFormId = null;
		const modal = this.template.querySelector("c-record-form-modal");
		modal.hide();
	}

	onLoadMarketingActivity(event) {
		this.layoutSectionsMarketingActivity = [event.detail.layout.sections[0]];
		this.activeAccordionSectionsMarketingActivity = [this.layoutSectionsMarketingActivity[0].id];
		this.buildTypeLayoutSectionsMarketingActivity(this.layoutSectionsMarketingActivity, null);
	}

	buildTypeLayoutSectionsMarketingActivity(layoutSections, record) {
		this.typeLayoutSectionsMarketingActivity = [];
		let rightNow = new Date();
		let yyyyMmDd = rightNow.toISOString().slice(0, 10);
		let toDayDate = moment(yyyyMmDd).format("YYYY-MM-DD");
		let apiStartDate;
		let apiEndDate;
		if (record !== null) {
			apiStartDate = record.fields.StartDate.value;
			apiEndDate = record.fields.EndDate.value;
		}
		layoutSections.forEach((layoutSection) => {
			let typeLayoutSection = {};
			typeLayoutSection.id = layoutSection.id;
			typeLayoutSection.heading = layoutSection.heading;
			let layoutRows = [];
			layoutSection.layoutRows.forEach((layoutRow) => {
				let typeLayoutRows = {};
				let layoutItems = [];
				layoutRow.layoutItems.forEach((layoutItem) => {
					let typeLayoutItems = {};
					let layoutComponents = [];
					layoutItem.layoutComponents.forEach((layoutComponent) => {
						let typeLayoutComponent = {};
						typeLayoutComponent.apiName = layoutComponent.apiName;
						if (layoutComponent.apiName === "musqotmp__Type__c" && this.typeId) {
							typeLayoutComponent.value = this.typeId;
						} else if (layoutComponent.apiName === "musqotmp__Campaign__c") {
							typeLayoutComponent.value = this.parent;
						} else if (record !== null && layoutComponent.apiName !== null && layoutComponent.apiName !== "Name" && record.fields[layoutComponent.apiName]) {
							//console.log("record.fields[layoutComponent.apiName] :" + JSON.stringify(record.fields[layoutComponent.apiName]));
							typeLayoutComponent.value = record.fields[layoutComponent.apiName].value;
						} else if (typeLayoutComponent.apiName == "musqotmp__StartDate__c") {
							typeLayoutComponent["dateFormate"] = true;
							typeLayoutComponent["label"] = layoutComponent.label;
							if (apiStartDate <= toDayDate) {
								this.stDate = toDayDate;
								typeLayoutComponent.value = toDayDate;
							} else {
								this.stDate = apiStartDate;
								typeLayoutComponent.value = apiStartDate;
							}
						} else if (typeLayoutComponent.apiName == "musqotmp__EndDate__c") {
							typeLayoutComponent["dateFormate"] = true;
							typeLayoutComponent["label"] = layoutComponent.label;
							if (apiStartDate <= toDayDate) {
								this.edDate = moment(yyyyMmDd).add(7, "days").format("YYYY-MM-DD");
								typeLayoutComponent.value = moment(yyyyMmDd).add(7, "days").format("YYYY-MM-DD");
							} else {
								this.edDate = apiEndDate;
								typeLayoutComponent.value = apiEndDate;
							}
						}
						layoutComponents.push(typeLayoutComponent);
					});
					typeLayoutItems.layoutComponents = layoutComponents;
					layoutItems.push(typeLayoutItems);
				});
				typeLayoutRows.layoutItems = layoutItems;
				layoutRows.push(typeLayoutRows);
			});
			typeLayoutSection.layoutRows = layoutRows;
			this.typeLayoutSectionsMarketingActivity.push(typeLayoutSection);
		});
	}

	onSuccessMarketingActivity(event) {
		this.handleRefresh();
		this.handleCancelRecordFormModalMarketingActivity();
		const toast = getToast("success", "pester", this.label.msg_Record_Saved);
		this.template.querySelector("c-toast").showToast(toast);
	}

	handleCancelRecordFormModalMarketingActivity() {
		this.recordFormId = null;
		const modal = this.template.querySelector("c-my-workspace-marketing-activity-modal");
		modal.hide();
	}
	handleSaveTasks() {
		let recordArr = [];
		for (let i in this.ganttTasksMap) {
			recordArr.push(this.ganttTasksMap[i]);
		}
		const recordInputs = recordArr.map((draft) => {
			const fields = Object.assign({}, draft);
			return { fields };
		});

		const promises = recordInputs.map((recordInput) => updateRecord(recordInput));
		Promise.all(promises)
			.then((sObj) => {
				this.template.querySelector("c-toast").showToast("Updated");
				this.handleCloseFooterForm();
				//window.console.log("success");
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
				//window.console.log("failure");
			});
	}
	handleCloseFooterForm() {
		this.showFormFooter = false;
		//this.tasks = this.buildGanttData();
		this.ganttPromise();
	}
	get isCampaignOptimizers() {
		return this.sObjectCampaign.length > 0 ? true : false;
	}

	//Journey - Start
	onLoadJourney(event) {
		//window.console.log('onLoadJourney Called');
		this.layoutSectionsJourney = [event.detail.layout.sections[0]];
		this.activeAccordionSectionsJourney = [this.layoutSectionsJourney[0].id];
		this.buildTypeLayoutSectionsJourney(this.layoutSectionsJourney, null);
		//window.console.log('onLoadJourney Exit');
	}

	buildTypeLayoutSectionsJourney(layoutSections, record) {
		//window.console.log('buildTypeLayoutSectionsJourney Called');
		this.typeLayoutSectionsJourney = [];
		//window.console.log('this.parent***' + this.parent);
		let apiStartDate;
		let apiEndDate;
		let rightNow = new Date();
		let yyyyMmDd = rightNow.toISOString().slice(0, 10);
		let toDayDate = moment(yyyyMmDd).format("YYYY-MM-DD");
		if (this.wiredGetRecord !== null) {
			apiEndDate = this.wiredGetRecord.fields.EndDate.value;
			apiStartDate = this.wiredGetRecord.fields.StartDate.value;
			const modal = this.template.querySelector("c-my-workspace-journey-modal");
			modal.show();
		}
		layoutSections.forEach((layoutSection) => {
			let typeLayoutSection = {};
			typeLayoutSection.id = layoutSection.id;
			typeLayoutSection.heading = layoutSection.heading;
			let layoutRows = [];
			layoutSection.layoutRows.forEach((layoutRow) => {
				let typeLayoutRows = {};
				let layoutItems = [];
				layoutRow.layoutItems.forEach((layoutItem) => {
					let typeLayoutItems = {};
					let layoutComponents = [];
					layoutItem.layoutComponents.forEach((layoutComponent) => {
						let typeLayoutComponent = {};
						typeLayoutComponent.apiName = layoutComponent.apiName;
						//window.console.log('layoutComponent.apiName***' + layoutComponent.apiName);
						if (layoutComponent.apiName === "musqotmp__Campaign__c") {
							typeLayoutComponent.value = this.parent;
						} else if (layoutComponent.apiName === "musqotmp__StartDate__c") {
							typeLayoutComponent["dateFormate"] = true;
							typeLayoutComponent["label"] = layoutComponent.label;
							if (apiStartDate <= toDayDate) {
								this.stDate = toDayDate;
								typeLayoutComponent.value = toDayDate;
							} else {
								this.stDate = apiStartDate;
								typeLayoutComponent.value = apiStartDate;
							}
						} else if (layoutComponent.apiName === "musqotmp__EndDate__c") {
							typeLayoutComponent["dateFormate"] = true;
							typeLayoutComponent["label"] = layoutComponent.label;
							if (apiStartDate <= toDayDate) {
								this.edDate = moment(yyyyMmDd).add(20, "days").format("YYYY-MM-DD");
								typeLayoutComponent.value = moment(yyyyMmDd).add(20, "days").format("YYYY-MM-DD");
							} else {
								this.edDate = apiEndDate;
								typeLayoutComponent.value = apiEndDate;
							}
						} else if (layoutComponent.apiName === "musqotmp__Audience__c" && this.wiredGetRecord !== null) {
							typeLayoutComponent.value = this.wiredGetRecord.fields.musqotmp__Audience__c.value;
						} else if (layoutComponent.apiName === "musqotmp__City__c" && this.wiredGetRecord !== null) {
							typeLayoutComponent.value = this.wiredGetRecord.fields.musqotmp__City__c.value;
						} else if (layoutComponent.apiName === "musqotmp__Countries__c" && this.wiredGetRecord !== null) {
							typeLayoutComponent.value = this.wiredGetRecord.fields.musqotmp__Countries__c.value;
						} else if (layoutComponent.apiName === "musqotmp__ProductGroup__c" && this.wiredGetRecord !== null) {
							typeLayoutComponent.value = this.wiredGetRecord.fields.musqotmp__ProductGroup__c.value;
						} else if (layoutComponent.apiName === "musqotmp__Shop__c" && this.wiredGetRecord !== null) {
							typeLayoutComponent.value = this.wiredGetRecord.fields.musqotmp__Shop__c.value;
						} else if (layoutComponent.apiName === "musqotmp__BusinessUnit__c" && this.wiredGetRecord !== null) {
							typeLayoutComponent.value = this.wiredGetRecord.fields.musqotmp__MarketingBusinessUnit__c.value;
						} else if (layoutComponent.apiName === "musqotmp__MarketingRegion__c" && this.wiredGetRecord !== null) {
							typeLayoutComponent.value = this.wiredGetRecord.fields.musqotmp__MarketingArea__c.value;
						} else if (record !== null && layoutComponent.apiName !== null && layoutComponent.apiName !== "Name" && record.data.fields[layoutComponent.apiName]) {
							//typeLayoutComponent.value = record.data.fields[layoutComponent.apiName].value;
						}
						layoutComponents.push(typeLayoutComponent);
					});
					typeLayoutItems.layoutComponents = layoutComponents;
					layoutItems.push(typeLayoutItems);
				});
				typeLayoutRows.layoutItems = layoutItems;
				layoutRows.push(typeLayoutRows);
			});
			typeLayoutSection.layoutRows = layoutRows;
			this.typeLayoutSectionsJourney.push(typeLayoutSection);
		});
	}
	handleAddJourney(obj) {
		//window.console.log('handleAddJourney called2');
		//window.console.log('typeHeaderJourney' + this.typeHeaderJourney);
		this.typeHeaderJourney = this.label.hl_new + " " + this.label.hl_Journey;
		this.buildTypeLayoutSectionsJourney(this.layoutSectionsJourney, null);
		//window.console.log('typeHeaderJourney ' + this.typeHeaderJourney);
		this.showCampaignOptimizer = false;
		let objectApiName = obj.currentTarget.name;
		this.sectionObjectApiName = objectApiName;
		this.paramGetCampaign = undefined;
		//const modal = this.template.querySelector('c-my-workspace-journey-modal');
		//this.showJourney = true;
		//if (this.sectionObjectApiName === "musqotmp__MarketingJourney__c") {
		//modal.show();
		this.handleCancelAddModal();
		this.handleReset(objectApiName);
		//}
	}
	onSuccessJourney(event) {
		//window.console.log('onSuccessJourney');
		this.handleRefresh();
		this.handleCancelRecordFormModalJourney();
		const toast = getToast("success", "pester", this.label.msg_Record_Saved);
		this.template.querySelector("c-toast").showToast(toast);
	}

	handleCancelRecordFormModalJourney() {
		//this.isJourney = false;
		//this.workspaceModelShow = true;
		this.recordFormId = null;
		const modal = this.template.querySelector("c-my-workspace-journey-modal");
		modal.hide();
	}

	cancelCreateJourney() {
		//window.console.log('showcMyWorkspaceAddModal parent value' + this.parent);
		this.handleCancelRecordFormModalJourney();
		//const modal1 = this.template.querySelector("c-my-workspace-add-modal");
		//modal1.show();
		this.showcMyWorkspaceAddModal();
	}
	//Journey - End

	showcMyWorkspaceAddModal() {
		//window.console.log('showcMyWorkspaceAddModal parent value' + this.parent);
		const modal1 = this.template.querySelector("c-my-workspace-add-modal");
		modal1.show();
	}

	cancelCreateMarketingActivity() {
		this.handleCancelRecordFormModalMarketingActivity();
		this.showcMyWorkspaceAddModal();
	}

	cancelCreateCampaign() {
		this.handleCancelRecordFormModal();
		this.showcMyWorkspaceAddModal();
		//this.handleReset();
	}
	//Sowmya user stroy W-000385 Start
	navigateToRecord() {
		if (this.hasCampaignTab) {
			this[NavigationMixin.GenerateUrl]({
				type: "standard__recordPage",
				attributes: {
					recordId: this.taskIdCampaign,
					objectApiName: this.otherobjectapiname,
					actionName: "view",
				},
			}).then((url) => {
				window.open(url, "Musqot");
				this.handleCancelModal();
			});
		} else {
			this[NavigationMixin.GenerateUrl]({
				type: "standard__recordPage",
				attributes: {
					recordId: this.taskId,
					objectApiName: this.objectApiName,
					actionName: "view",
				},
			}).then((url) => {
				window.open(url, "Musqot");
				this.handleCancelModal();
			});
		}
	}

	handleClose() {
		this.isPlan = false;
		this.isCampaign = false;
		this.isMarketingActivity = false;
		this.isMarketingJourney = false;
		this.hasCampaignTab = false;
		this.hasOpenButton = false;
		this.financeAllocation = false;
	}

	handleActive(event) {
		this.hasCampaignTab = true;
		this.hasOpenButton = false;
		this.showOtherObjectTab = true;
	}
	handleActiveAllocation() {
		this.hasOpenButton = true;
		this.hasCampaignTab = false;
	}
	handleActiveOpenButton() {
		this.hasOpenButton = false;
		this.hasCampaignTab = false;
		this.showOtherObjectTab = false;
	}

	buildParamDataTable() {
		const whereCaluses = [];
		let fieldSetApiName = "musqotmp__AllocationGanttInfo";

		whereCaluses.push({
			field: "musqotmp__BeneficiaryCampaign__c",
			value: this.taskIdCampaign,
			clause: "=",
		});

		return JSON.stringify({
			component: "marketingPlanView-datatable",
			relatedListType: "finance",
			objectApiName: "musqotmp__Allocation__c",
			fieldMap: {
				//[ALLOCATION_OBJECT_ID_FIELD.fieldApiName]: {}
			},
			fieldSetApiName: fieldSetApiName,
			linkableNameField: true,
			whereCaluses: whereCaluses,
		});
	}

	@wire(datatable, {
		param: "$paramDataTable",
	})
	wiredDataTable(result) {
		if (result.data) {
			if (result.data.toast.ok) {
				this.dtcolumns = result.data.datatable.columns;
				this.dtdata = result.data.datatable.data;
				this.template.querySelector("c-toast").showToast(result.data.toast);
			} else {
				this.data = undefined;
				this.columns = undefined;
				this.template.querySelector("c-toast").showToast(result.data.toast);
			}
		} else if (result.error) {
			this.data = undefined;
			this.columns = undefined;
			this.template.querySelector("c-toast").showToast(result.error);
		}
		this.tableLoadingState = false;
	}
	//custom lookup
	//sumbitting the record
	get getDisabled() {
		if (this.hasDisabled) {
			this.clearIconFlag = false;
		}
		return this.hasDisabled;
	}
	get getshowIcons() {
		if (this.fieldValue === "" || this.fieldValue === undefined) {
			this.showIcon = false;
			this.clearIconFlag = false;
		} else {
			this.showIcon = true;
			this.clearIconFlag = true;
		}
		return this.showIcon, this.clearIconFlag;
	}
	handelSelect(event) {
		this.selectRecordId =undefined
		if (event.detail.recordId) {
			this.selectRecordId = event.detail.recordId;
			this.showParent = true;
		}
	}
	handelSearch(event) {
		if (event.detail.currentText) {
			this.currentText = event.detail.currentText;
			let objectApiName = event.detail.objectApiName;
			if (this.currentText != null && this.currentText != "") {
				const whereCaluses = [];
				whereCaluses.push({
					field: this.fieldName,
					value: this.currentText,
					clause: "LIKE",
				});
				let sortedDirection = "DESC";
				let amountOfRecords = 400000;
				let sortedBy = "Name";
				let param = JSON.stringify({
					component: "Customlookup",
					objectApiName: objectApiName,
					whereCaluses: whereCaluses,
					sortBy: sortedBy,
					sortDirection: sortedDirection,
					amountOfRecords: amountOfRecords,
				});
				this.template.querySelector("c-customlookup").getResultData(param);
			}
		}
	}
	handleClick(event) {
		if (event.detail.lastView) {
			this.lastView = event.detail.lastView;
			let objectApiName = event.detail.objectApiName;
			if (this.lastView) {
				const whereCaluses = [];
				whereCaluses.push({
					field: "LastViewedDate",
					clause: "!=",
					value: null,
				});
				let sortedDirection = "DESC";
				let amountOfRecords = 400000;
				let sortedBy = "Name";
				let param = JSON.stringify({
					component: "Customlookup",
					objectApiName: objectApiName,
					whereCaluses: whereCaluses,
					sortBy: sortedBy,
					sortDirection: sortedDirection,
					amountOfRecords: amountOfRecords,
				});
				this.template.querySelector("c-customlookup").getResultData(param);
			}
		}
	}
	handleClickValue(event) {
		this.onClickValue = event.detail.onClickValue;
		let objectApiName = event.detail.objectApiName;
		const whereCaluses = [];
		whereCaluses.push({
			field: "LastViewedDate",
			clause: "!=",
			value: null,
		});
		let sortedDirection = "DESC";
		let amountOfRecords = 400000;
		let sortedBy = "Name";
		let param = JSON.stringify({
			component: "Customlookup",
			objectApiName: objectApiName,
			whereCaluses: whereCaluses,
			sortBy: sortedBy,
			sortDirection: sortedDirection,
			amountOfRecords: amountOfRecords,
		});
		this.template.querySelector("c-customlookup").getResultData(param);
	}
	//campaign
	handelSelectCampaign(event) {
		this.selectCampaignRecordId = undefined
		if (event.detail.recordId) {
			this.selectCampaignRecordId = event.detail.recordId;
			this.showParent = true;
		}
	}
	handleClickParent(event) {
		if (event.detail.lastView) {
			this.lastView = event.detail.lastView;
			let objectApiName = event.detail.objectApiName;
			if (this.lastView) {
				const whereCaluses = [];
				whereCaluses.push({
					field: this.objInfo.apiName,
					value: this.selectRecordId,
					clause: "=",
				});
				let sortedDirection = "DESC";
				let amountOfRecords = 400000;
				let sortedBy = "Name";
				let param = JSON.stringify({
					component: "campaign-lookup",
					objectApiName: objectApiName,
					whereCaluses: whereCaluses,
					sortBy: sortedBy,
					sortDirection: sortedDirection,
					amountOfRecords: amountOfRecords,
				});
				this.template.querySelector("c-campaign-lookup").getResultData(param);
			}
		}
	}
	handleClickCampaign(event) {
		this.onClickValue = event.detail.onClickValue;
		let objectApiName = event.detail.objectApiName;
		const whereCaluses = [];
		whereCaluses.push({
			field: this.objInfo.apiName,
			value: this.selectRecordId,
			clause: "=",
		});
		let sortedDirection = "DESC";
		let amountOfRecords = 400000;
		let sortedBy = "Name";
		let param = JSON.stringify({
			component: "campaign-lookup",
			objectApiName: objectApiName,
			whereCaluses: whereCaluses,
			sortBy: sortedBy,
			sortDirection: sortedDirection,
			amountOfRecords: amountOfRecords,
		});
		this.template.querySelector("c-campaign-lookup").getResultData(param);
	}
	handelSearched(event) {
		if (event.detail.currentText) {
			this.currentText = event.detail.currentText;
			let objectApiName = event.detail.objectApiName;
			if (this.currentText != null && this.currentText != "") {
				const whereCaluses = [];
				whereCaluses.push({
					field: this.fieldName,
					value: this.currentText,
					clause: "LIKE",
					operator: "AND",
				});
				whereCaluses.push({
					field: this.objInfo.apiName,
					value: this.selectRecordId,
					clause: "=",
				});
				let sortedDirection = "DESC";
				let amountOfRecords = 400000;
				let sortedBy = "Name";
				let param = JSON.stringify({
					component: "campaign-lookup",
					objectApiName: objectApiName,
					whereCaluses: whereCaluses,
					sortBy: sortedBy,
					sortDirection: sortedDirection,
					amountOfRecords: amountOfRecords,
				});
				this.template.querySelector("c-campaign-lookup").getResultData(param);
			}
		}
	}
	//custom lookup for campaign plan
	get getshowPlanIcons() {
		if (this.templatePlan === "" || this.templatePlan === undefined) {
			this.showIcon = false;
			this.clearIconFlag = false;
		} else {
			this.showIcon = true;
			this.clearIconFlag = true;
		}
		return this.showIcon, this.clearIconFlag;
	}
	handelSelectPlan(event) {
		this.selectedPlanRecordId = undefined
		if (event.detail.recordId) {
			this.selectedPlanRecordId = event.detail.recordId;
			this.showParent = true;
		}
	}
	handleClickPlan(event) {
		if (event.detail.lastView) {
			this.lastView = event.detail.lastView;
			let objectApiName = event.detail.objectApiName;
			if (this.lastView) {
				const whereCaluses = [];
				whereCaluses.push({
					field: "LastViewedDate",
					clause: "!=",
					value: null,
				});
				let sortedDirection = "DESC";
				let amountOfRecords = 400000;
				let sortedBy = "Name";
				let param = JSON.stringify({
					component: "my-workspace-plan-lookup",
					objectApiName: objectApiName,
					whereCaluses: whereCaluses,
					sortBy: sortedBy,
					sortDirection: sortedDirection,
					amountOfRecords: amountOfRecords,
				});
				this.template.querySelector("c-my-workspace-plan-lookup").getResultData(param);
			}
		}
	}
	handleClickValuePlan(event) {
		this.onClickValue = event.detail.onClickValue;
		let objectApiName = event.detail.objectApiName;
		const whereCaluses = [];
		whereCaluses.push({
			field: "LastViewedDate",
			clause: "!=",
			value: null,
		});
		let sortedDirection = "DESC";
		let amountOfRecords = 400000;
		let sortedBy = "Name";
		let param = JSON.stringify({
			component: "my-workspace-plan-lookup",
			objectApiName: objectApiName,
			whereCaluses: whereCaluses,
			sortBy: sortedBy,
			sortDirection: sortedDirection,
			amountOfRecords: amountOfRecords,
		});
		this.template.querySelector("c-my-workspace-plan-lookup").getResultData(param);
	}
	handelSearchedPlan(event) {
		if (event.detail.currentText) {
			this.currentText = event.detail.currentText;
			let objectApiName = event.detail.objectApiName;
			if (this.currentText != null && this.currentText != "") {
				const whereCaluses = [];
				whereCaluses.push({
					field: this.fieldName,
					value: this.currentText,
					clause: "LIKE",
				});
				let sortedDirection = "DESC";
				let amountOfRecords = 400000;
				let sortedBy = "Name";
				let param = JSON.stringify({
					component: "my-workspace-plan-lookup",
					objectApiName: objectApiName,
					whereCaluses: whereCaluses,
					sortBy: sortedBy,
					sortDirection: sortedDirection,
					amountOfRecords: amountOfRecords,
				});
				this.template.querySelector("c-my-workspace-plan-lookup").getResultData(param);
			}
		}
	}
	//end
}