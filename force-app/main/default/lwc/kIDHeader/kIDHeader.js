import { LightningElement,track, wire,api } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import KID_OBJECT from '@salesforce/schema/KeyInsightDashboard__c';
import { getRecord } from 'lightning/uiRecordApi';
import NAME_FIELD from '@salesforce/schema/KeyInsightDashboard__c.Name';

const FIELDS = [
    NAME_FIELD
];

export default class KIDHeader extends LightningElement {
    @api recordId;
    @track header;
    @track objectInfo;
    kidIconColor = "background-color: #"+"eff3fb";
    @track dataLength = 10;

    @wire(getObjectInfo, { objectApiName: KID_OBJECT })
	wiredObjectInfo({ data, error }) {
		if (data) {
			this.objectInfo = data;
			this.header = true;
			//this.kidIconColor = "background-color: #" + this.objectInfo.themeInfo.color;
		//	console.log("recordId :" + this.recordId);
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
    }
    
    /*@wire(getRecord, {
        recordId: "$recordId",
        layoutTypes: ["Full"],
        modes: ["Create"],
    })
    wiredRecord({ error, data }) {
        if (error) {
            this.template.querySelector("c-toast").showToast(error);
        } else if (data) {
            this.wiredGetRecord = data;
            this.buildTypeLayoutSections(this.layoutSections, this.wiredGetRecord);
        }
    }
    */

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredKidRecord;

	getKidIconColor() {
		return "background-color: #" + "eff3fb";
    }
    
    get name() {
        //console.log('wiredKidRecord Name'+this.wiredKidRecord.data.fields.Name.value);
        return this.wiredKidRecord.data.fields.Name.value;
    }
}