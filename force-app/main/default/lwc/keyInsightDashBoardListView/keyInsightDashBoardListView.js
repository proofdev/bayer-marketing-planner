import { LightningElement, wire, track, api } from "lwc";
import { updateRecord, getRecord } from "lightning/uiRecordApi";
import userId from "@salesforce/user/Id";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { refreshApex, getSObjectValue } from "@salesforce/apex";
import { loadScript, loadStyle } from "lightning/platformResourceLoader";
import MOMENT_JS from "@salesforce/resourceUrl/moment";

import getCalloutResponseContentList from "@salesforce/apex/keyInsightDashBoard.getCalloutResponseContentList";
import postCalloutResponseContents from "@salesforce/apex/keyInsightDashBoard.postCalloutResponseContents";
import parseDataFromJwtToken from "@salesforce/apex/keyInsightDashBoard.parseDataFromJwtToken";

import KEYINSIGHTDASHBOARD_OBJECT from "@salesforce/schema/KeyInsightDashboard__c";
import KEYINSIGHTDASHBOARD_ID_FIELD from "@salesforce/schema/KeyInsightDashboard__c.Id";
import KEYINSIGHTDASHBOARD_CREATED_BY_ID_FIELD from "@salesforce/schema/KeyInsightDashboard__c.CreatedById";
import KEYINSIGHTDASHBOARD_PROOFUSERID_FIELD from "@salesforce/schema/KeyInsightDashboard__c.ProofUserId__c";
import KEYINSIGHTDASHBOARD_STATUS_FIELD from "@salesforce/schema/KeyInsightDashboard__c.Status__c";
import KEYINSIGHTDASHBOARD_NAME_FIELD from "@salesforce/schema/KeyInsightDashboard__c.Name";
import USER_ID_FIELD from "@salesforce/schema/User.Id";
import JWTTOKEN_FIELD from "@salesforce/schema/User.ProofIdToken__c";
import saveKidRecords from "@salesforce/apex/keyInsightDashBoard.saveKidRecords";
import getRecords from "@salesforce/apex/keyInsightDashBoard.getRecords";
import datatable from "@salesforce/apex/Campaign.datatable";
import { CurrentPageReference } from "lightning/navigation";
import readKIDSetupSettings from "@salesforce/apex/keyInsightDashBoard.readKIDSetupSettings";

export default class KeyInsightDashBoardListView extends LightningElement {
  @api recordId;
  @track data = [];
  @track rowOffset;
  @track columns = [];
  @track tableLoadingState;
  @track sortedBy;
  @track sortedDirection;
  @track hideCheckboxColumn = true;
  @track selectedRows = [];
  @track showRowNumberColumn;
  @track totalRecordTitle;
  @track paramKidGetRecords;
  @track kidRecList = []; //= KEYINSIGHTDASHBOARD_OBJECT;
  @track sortBy = "LastModifiedDate";
  @track sortDirection = "desc";
  @track sortedBy = "LastModifiedDate";
  @track sortedDirection = "desc";

  @track wiredData;
  @track dataLength;
  @track sortedBy;
  @track sortedDirection;
  @track sortedDirection;
  @track columns;
  @track data;
  @track listFields;
  @track objectTypeLabel;
  @track tableLoadingState;

  @track paramDataTable;
  @track paramGetRecords;
  @track kidMap = {};

  @track domainURL;
  @track sfUserId = userId;
  @track proofUserId;
  @track proofOrgId;
  @track proofIdToken;
  @track KIDSettings;
  @track proofURL; // = readKIDSetupSettings.musqotmp__proofURL__c;
  @track auth0URL; // = readKIDSetupSettings.musqotmp__auth0URL__c;
  @track redirectURI; // = readKIDSetupSettings.musqotmp__redirectURI__c;
  @track additionalParam; // = readKIDSetupSettings.musqotmp__AdditionalParam__c;
  @track clientId; // = readKIDSetupSettings.musqotmp__ClientId__c;
  //@track proofURL = "https://testapp.get-proof.com";
  //@track proofURL = readKIDSetupSettings.musqotmp__proofURL__c;
  @track paramParseDataFromJwtToken;
  @track calloutResponseContentMapData;
  @track authenticated = true;
  //currentPageReference = null;
  urlStateParameters = null;
  //@track clientId = "6rTadwLO454MAdHvAhtWgpR53me2qsPM";
  //@track clientSecret = "zQMFQtxz3bDh7GxgiagV9S18DI0okx0R4F1g9-_MfROE_cRPQfC06WzeuSZTRSFL";
  @track jwtToken;
  //@track idToken = 'b3vFf3zZODsOO-x5';
  /* Params from Url */
  urlcode = null;
  @track pageReference = false;
  @track isLoading = false;
  //@track isCurrentRef = false;
  @track customSettingsParam;
  @track sourceUpdateToken;
  /*@wire(CurrentPageReference)
	getStateParameters(currentPageReference) {
		if (currentPageReference) {
			this.urlStateParameters = currentPageReference.state;
			this.urlcode = this.urlStateParameters.c__code || null;
			//window.console.log("urlcode" + this.urlcode);
			this.isCurrentRef = true;
			this.customSettingsParam = JSON.stringify({"Param":"TestParam"}); 
		}
		
	}*/
  get moment() {
    if (moment) return moment;
    else this.moment;
  }
  connectedCallback() {
    loadScript(this, MOMENT_JS)
      .then(() => {
        this.moment;
        this.customSettingsParam = JSON.stringify({ Param: "TestParam" });
      })
      .catch((error) => {
        this.template.querySelector("c-toast").showToast(error);
      });
  }

  getURLParam() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get("c__code");
    return c;
  }

  /*readKIDSetup(){
		if(this.urlcode && this.clientId){
				let param = JSON.stringify({
					URL: 'callout:KeyInsightDashboardConnect',
					"content-type": "application/x-www-form-urlencoded",
					Body: "clientId={!$Credential.UserName}" + "&clientSecret={!$Credential.Password}"+ "&code=" + this.urlcode + "&redirect_uri="+this.redirectURI + "&grant_type=authorization_code",
				});
				this.getTokenFromProof(param);
				this.authenticated = true;
		}
		
	}*/

  @wire(readKIDSetupSettings, { param: "$customSettingsParam" })
  wiredReadKIDSetupSettings(cresult) {
    if (cresult.data && cresult.data.sObj) {
      this.KIDSettings = cresult.data.sObj;
      this.proofURL = this.KIDSettings.musqotmp__proofURL__c;
      this.auth0URL = this.KIDSettings.musqotmp__auth0URL__c;
      this.redirectURI = this.KIDSettings.musqotmp__redirectURI__c;
      this.additionalParam = this.KIDSettings.musqotmp__AdditionalParam__c;
      this.clientId = this.KIDSettings.musqotmp__ClientId__c;
      this.paramParseDataFromJwtToken = JSON.stringify({
        recordId: this.sfUserId,
      });
      /*if(this.urlcode != null && jwtToken){
					this.readKIDSetup();
				} else {
					this.paramParseDataFromJwtToken = JSON.stringify({
					recordId: this.sfUserId,
					});
				}*/
    }
  }
  wiredJwtToken;
  @wire(parseDataFromJwtToken, { param: "$paramParseDataFromJwtToken" })
  wiredParseDataFromJwtToken(result) {
    this.sourceUpdateToken = undefined;
    this.wiredJwtToken = result;
    //console.log("Result of wiredParseDataFromJwtToken " + JSON.stringify(result));
    if (result.data) {
      let proofAuthMap = result.data.proofAuthMap;
      //console.log("Proof iD token == " + result.data.proofIdToken);
      if (result.data.proofIdToken) {
        this.proofIdToken = result.data.proofIdToken;
        this.proofOrgId = result.data.proofAuthMap.orgId;
        this.proofUserId = result.data.proofAuthMap.sub.split("|")[1];
        this.domainURL = this.proofURL + "/api/organisations/" + this.proofOrgId + "/key_insight_dashboards";
        const whereCaluses = [];
        whereCaluses.push({
          field: KEYINSIGHTDASHBOARD_CREATED_BY_ID_FIELD.fieldApiName,
          value: this.sfUserId,
          clause: "=",
        });
        this.paramGetRecords = JSON.stringify({
          component: "Regrassion-getRecords",
          objectApiName: KEYINSIGHTDASHBOARD_OBJECT.objectApiName,
          fieldMap: {
            [KEYINSIGHTDASHBOARD_ID_FIELD.fieldApiName]: {},
            [KEYINSIGHTDASHBOARD_NAME_FIELD.fieldApiName]: {},
            [KEYINSIGHTDASHBOARD_CREATED_BY_ID_FIELD.fieldApiName]: {},
          },
          whereCaluses: whereCaluses,
        });
      } else {
        this.urlcode = this.getURLParam();
        if (!this.urlcode) {
          window.open(this.auth0URL + "/authorize?client_id=" + this.clientId + "&response_type=code&redirect_uri=" + this.redirectURI + this.additionalParam, "_self");
        } else {
          let param = JSON.stringify({
            URL: "callout:KID_Connection",
            "content-type": "application/x-www-form-urlencoded",
            Body: "clientId={!$Credential.UserName}" + "&clientSecret={!$Credential.Password}" + "&code=" + this.urlcode + "&redirect_uri=" + this.redirectURI + "&grant_type=authorization_code",
          });
          this.getTokenFromProof(param);
          this.authenticated = true;
        }
      }
    }
    if (result.error) {
      this.urlcode = this.getURLParam();
      if (!this.urlcode) {
        window.open(this.auth0URL + "/authorize?client_id=" + this.clientId + "&response_type=code&redirect_uri=" + this.redirectURI + this.additionalParam, "_self");
      } else {
        let param = JSON.stringify({
          URL: "callout:KID_Connection",
          "content-type": "application/x-www-form-urlencoded",
          Body: "clientId={!$Credential.UserName}" + "&clientSecret={!$Credential.Password}" + "&code=" + this.urlcode + "&redirect_uri=" + this.redirectURI + "&grant_type=authorization_code",
        });
        this.getTokenFromProof(param);
        this.authenticated = true;
      }
      /*window.open(this.auth0URL + "/authorize?client_id=" + this.clientId + "&response_type=code&redirect_uri=" + this.redirectURI + this.additionalParam, "_self");*/
    }
    /*else {
			window.open(
				"https://proof-test.us.auth0.com/authorize?client_id=6rTadwLO454MAdHvAhtWgpR53me2qsPM&response_type=code&redirect_uri=https://www.salesforce.com?connection=proof-test&scope=openid%20email%20offline_access",
				"_blank"
			);
		//	this.template.querySelector("c-toast").showToast(result.data.toast);
		}*/
  }
  getTokenFromProof(param) {
    postCalloutResponseContents({ param: param })
      .then((res) => {
        this.jwtToken = res.resultMap.id_token;
        //window.console.log("jwtToken" + this.jwtToken);
        if (this.jwtToken) {
          this.updateUser();
        } else {
          window.open(this.auth0URL + "/authorize?client_id=" + this.clientId + "&response_type=code&redirect_uri=" + this.redirectURI + this.additionalParam, "_self");
          this.isLoading = true;
          this.dispatchEvent(
            new ShowToastEvent({
              title: res.resultMap.error,
              message: "Redirecting to proof for authentication",
              variant: "error",
            })
          );
        }
      })
      .catch((error) => {
        this.template.querySelector("c-toast").showToast(error);
      });
  }
  updateUser() {
    this.paramParseDataFromJwtToken = undefined;
    const fields = {};
    fields[USER_ID_FIELD.fieldApiName] = this.sfUserId;
    fields[JWTTOKEN_FIELD.fieldApiName] = this.jwtToken;
    const recordInput = { fields };
    updateRecord(recordInput)
      .then(() => {
        //refreshApex(this.paramParseDataFromJwtToken);
        if (this.sourceUpdateToken == "expire") {
          window.open(this.auth0URL + "/authorize?client_id=" + this.clientId + "&response_type=code&redirect_uri=" + this.redirectURI + this.additionalParam, "_self");
        } else {
          this.paramParseDataFromJwtToken = JSON.stringify({
            recordId: this.sfUserId,
          });
        }
      })
      .catch((error) => {
        this.isLoading = true;
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Error creating record",
            message: error.body.message,
            variant: "error",
          })
        );
      });
  }

  /*getJWTToken(){
		let paramdata = JSON.stringify({
					recordId: this.sfUserId,
					});
		parseDataFromJwtToken({paramdata})
			.then((res) => {
				//console.log("res"+res);
				this.jwtToken = res.data.proofIdToken;
            })
	}*/

  getResponseContents() {
    let param = JSON.stringify({
      URL: this.domainURL,
      Authorization: this.proofIdToken,
      recordId: this.sfUserId,
    });
    getCalloutResponseContentList({ param: param })
      .then((res) => {
        if (res.resultMap) {
          let reloadNumber = this.getCookie("reload");
          if (reloadNumber == "1") {
            this.setCookie("reload", "0", 1);
            this.isLoading = true;
            this.dispatchEvent(
              new ShowToastEvent({
                title: res.resultMap.error,
                message: "Something went wrong, Please try again after sometimes",
                variant: "error",
              })
            );
          } else {
            this.setCookie("reload", "1", 1);
            this.jwtToken = null;
            this.sourceUpdateToken = "expire";
            this.authenticated = false;
            this.isLoading = true;
            this.updateUser();
          }
          //console.log("unhandled error code");
        } else {
          let nameUrl;
          this.kidRecList = [];
          res.resultList.map((row) => {
            if (row.visibility.toLowerCase() == "public" && row.ownerId == this.proofUserId) {
              let kidRec = {};
              kidRec.objectApiName = "musqotmp__KeyInsightDashboard__c";
              if (this.kidMap[row.businessQuestion]) {
                kidRec = Object.assign({}, this.kidMap[row.businessQuestion]);
              }
              kidRec.Name = row.businessQuestion;
              kidRec.musqotmp__keyInsightDashboardId__c = row.keyInsightDashboardId;
              kidRec.musqotmp__Frequency__c = row.associatedModelFrequency;
              kidRec.musqotmp__CreatedOn__c = moment(row.createdAt).format("YYYY-MM-DD");
              kidRec.musqotmp__Status__c = row.visibility;
              kidRec.musqotmp__ProofUserId__c = this.proofUserId;
              kidRec.musqotmp__KIDUser__c = this.sfUserId;
              if (row.isAccessible) {
                kidRec.musqotmp__isAccessible__c = row.isAccessible;
              }
              this.kidRecList.push(kidRec);
            }
          });

          this.saveKidRecList();
        }
      })
      .catch((error) => {
        this.template.querySelector("c-toast").showToast(error);
      });
  }
  @wire(getRecords, { param: "$paramGetRecords" })
  wiredGetRecords(result) {
    if (result.data) {
      result.data.sObjects.forEach((e) => {
        this.kidMap[e.Name] = e;
      });
      this.getResponseContents();
    }
  }
  saveKidRecList() {
    saveKidRecords({
      kidRecList: this.kidRecList,
    }).then((result) => {
      this.paramDataTable = this.buildParamDataTable();
    });
  }

  buildParamDataTable() {
    const whereCaluses = [];
    whereCaluses.push({
      field: KEYINSIGHTDASHBOARD_CREATED_BY_ID_FIELD.fieldApiName,
      value: this.sfUserId,
      clause: "=",
      operator: "AND",
    });
    whereCaluses.push({
      field: KEYINSIGHTDASHBOARD_PROOFUSERID_FIELD.fieldApiName,
      value: this.proofUserId,
      clause: "=",
    });
    let fieldSetApiName = "musqotmp__ListView";
    return JSON.stringify({
      component: "KeyInsightDashBoard",
      objectApiName: KEYINSIGHTDASHBOARD_OBJECT.objectApiName,
      fieldSetApiName: fieldSetApiName,
      fieldMap: {
        [KEYINSIGHTDASHBOARD_ID_FIELD.fieldApiName]: {},
      },
      whereCaluses: whereCaluses,
      linkableNameField: true,
      sortBy: this.sortBy,
      sortDirection: this.sortDirection,
    });
  }

  @wire(datatable, { param: "$paramDataTable" })
  wiredGetDatatable(result) {
    this.wiredData = result;
    if (result.data) {
      if (result.data.toast.ok) {
        this.isLoading = true;
        this.dataLength = result.data.datatable.data.length;
        this.sortedBy = this.sortBy;
        this.sortedDirection = this.sortDirection;
        let colArr = [];

        result.data.datatable.columns.forEach((col) => {
          let tempCol = Object.assign({}, col);
          if (tempCol.type == "date") {
            tempCol.type = "text";
          }
          if (col.fieldName == "musqotmp__KeyInsightDashboard__c.Name") {
            tempCol.fixedWidth = 500;
            //colArr.push(Object.assign({}, col, { fixedWidth: 500 }));
            colArr.push(tempCol);
          } else {
            colArr.push(tempCol);
          }
        });
        this.columns = colArr;
        this.data = result.data.datatable.data;
        this.listFields = result.data.fields;
        this.objectTypeLabel = result.data.objectTypeLabel;
        if (this.data.length == 0 || this.data.length == 1) {
          this.totalRecordTitle = this.data.length + " item";
        } else {
          this.totalRecordTitle = this.data.length + " items";
        }
      }
    } else if (result.error) {
      this.template.querySelector("c-toast").showToast(result.error);
    }
    this.tableLoadingState = false;
  }
  setCookie(name, value, days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
  }
  getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }
  eraseCookie(name) {
    document.cookie = name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  }
}