import { LightningElement, api, track } from 'lwc';
import getOrganizationId from '@salesforce/apex/Organization.getOrganizationId';

export default class AmmmView extends LightningElement {
    @api recordId;
    @api title;
    @api staticResourceName;
    @track staticResourceUrl;

    connectedCallback() {
        if(this.staticResourceName.indexOf('[planid]') > -1){
            this.staticResourceUrl = '/resource/musqotmp__' + this.staticResourceName.replace('[planid]', this.recordId);
        }else if(this.staticResourceName.indexOf('[orgid]') > -1){
            this.staticResourceUrl = '/resource/musqotmp__' + this.staticResourceName.replace('[orgid]', getOrganizationId);
        }else {
            this.staticResourceUrl = '/resource/musqotmp__' + this.staticResourceName
        }
    }
}