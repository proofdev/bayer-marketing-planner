import { LightningElement, api } from 'lwc';
export default class RecordViewForm extends LightningElement {
    @api objectapiname;
    @api fields;
    @api recordid;
}