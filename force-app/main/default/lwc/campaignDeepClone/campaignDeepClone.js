/* eslint-disable @lwc/lwc/no-document-query */
/* eslint-disable no-mixed-spaces-and-tabs */
import { LightningElement, api, track, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import getRecords from "@salesforce/apex/Campaign.getRecords";
import CAMPAIGN_MARKETING_PLAN from "@salesforce/schema/Campaign.MarketingPlan__c";
import CAMPAIGN_OBJECT from '@salesforce/schema/Campaign';
import CAMPAIGN_ID_FIELD from '@salesforce/schema/Campaign.Id';
import CAMAPIGN_NAME_FIELD from "@salesforce/schema/Campaign.Name";
import deepCloneCampaignTemplate from "@salesforce/apex/Gantt.deepCloneCampaignTemplate";
import btn_cancel from '@salesforce/label/c.btn_cancel';
import btn_clone from '@salesforce/label/c.btn_clone';


export default class CampaignDeepClone extends LightningElement {
	label = {
		btn_cancel,
		btn_clone	
	}
	@api recordId;
	@track objectApiName = 'Campaign';
	@track layoutSections = [];
	@track activeAccordionSections;
	@track objectTypeLabel;
	@track header;
	@track campaignName;
	@track showModal = true;
	@track fields;
	@track marketingPlanId;
	@track campaignOptions;
	@track ParentId;
	@track paramParentCampaign;
	@track campaign;
	@track parent = false;
	@wire(getRecord, { recordId: '$recordId', fields: [CAMPAIGN_MARKETING_PLAN] })
	wiredRecord({  data,error }) {
		if (data) {
			this.campaign = data;
			this.marketingPlanId = this.campaign.fields.musqotmp__MarketingPlan__c.value;
			this.paramParentCampaign = this.buildParamParentCampaign(this.marketingPlanId);
		}else if(error) {
			this.template.querySelector('c-toast').showToast(error);
	}
	}
	handleInputForm(event) {
		this.marketingPlanId = event.detail.value[0];
		this.parent = false;
		if (this.marketingPlanId)
			this.paramParentCampaign = this.buildParamParentCampaign(this.marketingPlanId);
	}
	handleSubmit(event) {
		event.preventDefault(); // stop the form from submitting
		this.fields = event.detail.fields;
		this.showModal = false;
		//setting param
		this.paramCampaignClone();
		
	}
	paramCampaignClone() {
		this.paramCampaignCloneTemplate = JSON.stringify({
			component: 'campaignDeepClone-cloneCampaignTemplate',
			recordId: this.ParentId || this.recordId,
			campaignName: this.fields.Name,
			startDate: this.fields.StartDate,
			endDate: this.fields.EndDate,
			marketingPlan: this.fields.musqotmp__MarketingPlan__c
		});
		//calling campaign cloning
		this.campaignDeepClone();
	}
	// deep clone campaign
	campaignDeepClone() {
		deepCloneCampaignTemplate({
			param: this.paramCampaignCloneTemplate,
			recordId: this.recordId
		})
			.then(result => {
				if (result.toast.ok) {
					this.template.querySelector("c-toast").showToast(result.toast);
					//custom event to close the modal
					const v = false;
		            const onClickEvent = new CustomEvent('show', {
			        detail : {v},
				  });
				  //firing the event.
		           this.dispatchEvent(onClickEvent);
				} else {
					this.template.querySelector("c-toast").showToast(result.toast);
				}
			})
			.catch(error => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	//building parent campaign
	buildParamParentCampaign(marketingPlanId) {
		let whereCaluses = [];
		whereCaluses.push({
			field: CAMPAIGN_MARKETING_PLAN.fieldApiName,
			value: marketingPlanId,
			clause: '='
		});
		return JSON.stringify({
			component: 'campaignDeepClone-cloneCampaignTemplate',
			objectApiName: CAMPAIGN_OBJECT.objectApiName,
			fieldMap: {
				[CAMPAIGN_ID_FIELD.fieldApiName]: {},
				[CAMAPIGN_NAME_FIELD.fieldApiName]: {},
			},
			whereCaluses: whereCaluses
		});
	}
	@wire(getRecords, { param: "$paramParentCampaign" })
	wiredCampaignRecords(result) {
		if (result.data) {
			if (result.data.toast.ok) {
				this.campaignOptions = this.buildCampaigns(result.data.sObjects);
				this.template.querySelector("c-toast").showToast(result.data.toast);
			} else if (result.error) {
				this.campaignOptions = undefined;
				this.template.querySelector("c-toast").showToast(result.error);
			}
		}
	}
	buildCampaigns(sObjects) {
		const options = [];
		sObjects.forEach(sObject => {
			options.push({ label: sObject.Name, value: sObject.Id })
		});
		return options;
	}
	handleChange(event) {
		this.ParentId = event.detail.value;
		this.parent = true;
	}
	handleDialogClose(event){
		this.showModal = false;
		const v = this.showModal ;
		const onClickEvent = new CustomEvent('show', {
			    detail : {v},
			});
		//firing the event.
		this.dispatchEvent(onClickEvent);
	}
	get buttonCss(){
		return this.parent?"margin-right: 13%;":'margin-right:0%';
	}
}