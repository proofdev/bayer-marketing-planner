import { LightningElement, wire, api, track } from 'lwc';
import getResults from '@salesforce/apex/Campaign.getLookupResults';

export default class Customlookup extends LightningElement {
	@api objectName;
	@api hasDisabled;
	@api objInfo;
	@api fieldName;
	@api selectRecordId;
	@api fieldLabel;
	@api fieldValue;
	@api currentText;
	@api required = false;
	@api iconName = "action:new_account";
	@api LoadingText = false;
	@track txtclassname = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
	@track messageFlag = false;
	@track iconFlag = false;
	@api clearIconFlag;
	@track inputReadOnly = false;
	@track sObjectList = [];
	@api showIcon;
	@track showSpinner = false;
	@api lastView;
	@track onClickValue = "";
	@track recentItems;

	connectedCallback() {
		this.objInfo = this.objInfo;
		let objName = this.objectName;
		if (objName.includes("musqotmp__")) {
			objName = objName.replace(/musqotmp__/, " ");
		}
		if (objName.includes("__c")) {
			objName = objName.replace(/__c/, " ");
		}
		if (this.fieldValue === undefined) {
			this.iconFlag = true;
		} else {
			this.iconFlag = false;
		}
		//this.fieldLabel = objName;
	}

	searchField(event) {
		this.sObjectList = [];
		this.lastView = false;
		this.iconFlag = false;
		this.recentItems = "";
		this.currentText = event.target.value;
		if (this.currentText === "" || this.currentText === undefined || this.currentText === null) {
			this.txtclassname =
				this.sObjectList.length > 0 ? "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open" : "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
			this.LoadingText = false;
			this.messageFlag = false;
			this.iconFlag = true;
		} else {
			this.LoadingText = true;
			this.clearIconFlag = true;
		}
		this.dispatchEvent(
			new CustomEvent("searched", {
				detail: {
					currentText: this.currentText,
					objectApiName: this.objectName,
				},
			})
		);
		this.dispatchEvent(
      new CustomEvent("selected", {
        detail: {
          recordId: undefined,
        },
      })
    );
	}
	handleClick(event) {
		this.showIcon = false;
		this.onClickValue = event.target.value;
		this.dispatchEvent(
			new CustomEvent("clicked", {
				detail: {
					objectApiName: this.objectName,
					onClickValue: this.onClickValue,
				},
			})
		);
	}
	@api
	getResultData(param) {
		getResults({ param: param })
			.then((result) => {
				this.sObjectList = result.sObjectList;
				this.LoadingText = false;
				this.showSpinner = false;
				if (result.sObjectList) {
					this.txtclassname =
						result.sObjectList.length > 0 ? "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open" : "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
				}
				if (this.currentText && result.sObjectList.length == 0) {
					this.messageFlag = true;
				} else {
					this.messageFlag = false;
				}
				if (this.selectRecordId != null && this.selectRecordId.length > 0) {
					this.iconFlag = false;
					this.clearIconFlag = true;
				} else {
					this.iconFlag = true;
					this.clearIconFlag = false;
					this.showIcon = false;
				}
				this.handleRefresh();
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	setSelectedRecord(event) {
		let currentRecId = event.currentTarget.dataset.recordid;
		let selectName = event.currentTarget.dataset.recordname;
		this.txtclassname = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
		this.iconFlag = false;
		this.clearIconFlag = true;
		this.fieldValue = event.currentTarget.dataset.recordname;
		this.selectRecordId = currentRecId;
		if (this.selectRecordId) {
			this.showIcon = true;
		}
		this.inputReadOnly = false;

		this.dispatchEvent(
			new CustomEvent("selected", {
				detail: {
					recordId: this.selectRecordId,
				},
			})
		);
	}
	get getIconColor() {
		return "width:20px;height:20px; margin-top: -11px;background-color: #" + this.objInfo.themeInfo.color;
	}

	closeIcon(event) {
		//this.fieldValue = undefined;
		this.selectRecordId = "";
		this.inputReadOnly = false;
		this.iconFlag = true;
		this.clearIconFlag = false;
		this.currentText = "";
		this.showIcon = false;
		this.showSpinner = true;
		this.LoadingText = true;
		this.lastView = true;
		if (this.fieldLabel === "Marketing Plan") {
			this.fieldLabel = this.fieldLabel.slice(10, 14);
			this.recentItems = "Recent " + this.fieldLabel + "s";
		} else if (this.fieldLabel === "Parent Campaigns") {
			let label = this.fieldLabel.slice(7, 16);
			this.recentItems = "Recent " + label;
		}
		this.sObjectList = [];
		this.dispatchEvent(
			new CustomEvent("click", {
				detail: {
					lastView: this.lastView,
					objectApiName: this.objectName,
				},
			})
		);
		this.dispatchEvent(
      new CustomEvent("selected", {
        detail: {
          recordId: undefined,
        },
      })
    );
	//	window.console.log("getResultList :" + JSON.stringify(this.getResultList));
		/* if (this.whereCaluseList) {
             this.getResultData();
         }*/
	}
	handleRefresh() {
		this.whereCaluseList = [];
		this.sortedBy = "";
		this.sortedDirection = "";
		this.amountOfRecords = 0;
		this.fieldValue=''
	}
    get placeholder(){
        return "Search " + this.objInfo.label + "...";
    }
}