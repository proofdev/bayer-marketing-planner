/**
 * Reduces one or more LDS toasts into a string[] of toast messages.
 * @param {FetchResponse|FetchResponse[]} toasts
 * @return {String[]} Toast messages
 */
export function reduceToasts(toasts) {
    if (!Array.isArray(toasts)) {
        toasts = [toasts];
    }

    return (
        toasts
            // Remove null/undefined items
            .filter(toast => !!toast)
            // Extract an toast message
            .map(toast => {
                // UI API read toast custom
                if (toast.showToast && typeof toast.showToast === 'boolean') {
                    return toast;
                }
                // UI API read toasts
                else if (Array.isArray(toast.body)) {
                    return toast.body.map(e => e.message);
                }
                // UI API DML, Apex and network toasts
                else if (toast.body && typeof toast.body.message === 'string') {
                    return toast.body.message;
                }
                // JS toasts
                else if (typeof toast.message === 'string') {
                    return toast.message;
                }
                // Unknown toast shape so try HTTP status text
                return toast.statusText;
            })
            // Flatten
            .reduce((prev, curr) => prev.concat(curr), [])
            // Remove empty strings
            .filter(message => !!message)
    );
}