import { LightningElement, api, track } from 'lwc';
import close from '@salesforce/label/c.btn_close';

const CSS_CLASS = 'modal-hidden';

export default class MyWorkspaceMarketingActivityModal extends LightningElement {
    label = {
        close
    }
    @api
    set header(value) {
        this.hasHeaderString = value !== '';
        this._headerPrivate = value;
    }
    get header() {
        return this._headerPrivate;
    }

    @track hasHeaderString = false;
    _headerPrivate;

    @api
    set sectioncssclass(value) {
        this.hasSectionCssClass = value !== '';
        if (value === undefined) {
            this._sectionCssClassPrivate = 'slds-modal slds-fade-in-open';
        }
        this._sectionCssClassPrivate = value;
    }
    get sectioncssclass() {
        return this._sectionCssClassPrivate;
    }

    @track hasSectionCssClass = false;
    _sectionCssClassPrivate;

    @api show() {
        const outerDivEl = this.template.querySelector('div');
        outerDivEl.classList.remove(CSS_CLASS);
    }

    @api hide() {
        const outerDivEl = this.template.querySelector('div');
        outerDivEl.classList.add(CSS_CLASS);
    }

    handleDialogClose() {
        this.hide();
    }

    handleSlotTaglineChange() {
        const taglineEl = this.template.querySelector('p');
        taglineEl.classList.remove(CSS_CLASS);
    }

    handleSlotFooterChange() {
        const footerEl = this.template.querySelector('footer');
        footerEl.classList.remove(CSS_CLASS);
    }
}