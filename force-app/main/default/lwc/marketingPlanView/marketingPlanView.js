import { LightningElement, wire, track, api } from "lwc";
import { getRecord } from "lightning/uiRecordApi";
import LOCALE from "@salesforce/i18n/locale";
import getMarketingPlanView from "@salesforce/apex/Gantt.getMarketingPlanView";
import readMarketingCloudCredential from "@salesforce/apex/Campaign.readMarketingCloudCredential";
import { loadScript, loadStyle } from "lightning/platformResourceLoader";
import Gantt from "@salesforce/resourceUrl/gantt_6";
import { CurrentPageReference, NavigationMixin } from "lightning/navigation";
import { readCookie } from "c/lds";
import { refreshApex } from "@salesforce/apex";
import MOMENT_JS from "@salesforce/resourceUrl/moment";
import modalHeader from "@salesforce/label/c.modalHeader";
import statusHeader from "@salesforce/label/c.gantt_StatusHeader";
import campaignHeader from "@salesforce/label/c.gantt_columnCampaignHeader";
import cancel from "@salesforce/label/c.btn_cancel";
import open from "@salesforce/label/c.btn_open";
import btnOpenInMC from "@salesforce/label/c.btn_open_in_mc";
import datatable from "@salesforce/apex/Campaign.datatable";

import MARKETING_ACTIVITY_CAMPAIGN_NAME_FIELD from "@salesforce/schema/MarketingActivity__c.Campaign__r.Name";
import MARKETING_JOURNEY_CAMPAIGN_NAME_FIELD from "@salesforce/schema/MarketingJourney__c.Campaign__r.Name";
import MARKETING_ACTIVITY_PLAN_FIELD from "@salesforce/schema/MarketingActivity__c.Campaign__r.MarketingPlan__r.Name";
import CAMPAIGN_MARKETINGPLAN_FIELD from "@salesforce/schema/Campaign.MarketingPlan__r.Name";
import MARKETING_JOURNEY_PLAN_FIELD from "@salesforce/schema/MarketingJourney__c.Campaign__r.MarketingPlan__r.Name";

export default class MarketingPlanView extends NavigationMixin(LightningElement) {
	label = {
		modalHeader,
		statusHeader,
		campaignHeader,
		cancel,
		open,
		btnOpenInMC,
	};
	@api recordId;
	ganttInitialized = false;
	@track sObject;
	@track sObjects = [];
	@track sObjectList = [];
	@track sObjectListMA = [];
	@track sObjectUnMap;
	@track sObjectListUn = [];
	@track barTextFields = [];
	@track tasks;
	@track header = modalHeader;
	@track objectApiName;
	@track taskId;
	@track fields = [];
	@track fieldsAllocation = [];
	@track fieldMap = {};
	@track momentjsInitialized;
	@track selectedDateTime = new Date();
	@track startDate;
	@track endDate;
	@track campaignManagerSetup;
	@track barColorMap = {};
	@track mcUrlMap = {};
	@track marketingCloudCredential;

	//Sowmya user stroy W-000365 Gantt pop-up
	@track sectioncssclass = "slds-modal slds-fade-in-open slds-modal_medium";
	@track fieldsCampaign = [];
	@track isMarketingActivity = false;
	@track isMarketingActivityInJourney = false;
	@track isMarketingJourney = false;
	@track taskIdCampaign;
	@track taskIdJourney;
	@track otherobjectapiname;
	@track data = [];
	@track columns = [];
	@track tableLoadingState = true;
	@track hasCampaignTab = false;
	@track hasOpenButton = false;
	@track paramDataTable;
	@track hideCheckboxColumn = true;
	@track financeAllocation = false;
	@track ganttId;
	@track tempGanttId;
	@track objectFileds;
	@track taskRecId;
	@track campaignUrl;
	@track marketingPlanUrl;
	@track marketingPlan;
	@track campaignValue;

	AggregateColumns = [
		{ label: "Cost center", fieldName: "nameUrl", type: "text" },
		//{ label: 'Financial year', fieldName: 'musqotmp__FiscalYear__c', type: 'text', sortable: true },
		{ label: "Approved Amount", fieldName: "ApprovedAmount", type: "currency" },
	];

	//TODO Need to make dynamic to validate icon is standard or custom
	@track standardIcons = ["Adwords", "Banner", "Event", "Email", "Mobile", "Outdoor", "Postal", "Print", "Radio", "Social", "Sponsor", "Telemarketing", "Tv", "Web", "Other"];
	/** Wired Apex result so it may be programmatically refreshed. */
	wiredData;

	@wire(getRecord, { recordId: "$taskRecId", fields: "$objectFileds" })
	wiredGetTaskRecord({ data, error }) {
		if (data) {
			this.data = data;
			if (this.objectApiName === "Campaign") {
				this.marketingPlan = this.data.fields.musqotmp__MarketingPlan__r.value.fields.Name.value;
				let taskrecordId = data.fields.musqotmp__MarketingPlan__r.value.id;
				let objectName = "musqotmp__MarketingPlan__c";
				this.navigateToDetailViewPage(objectName, taskrecordId);
			} else if (this.objectApiName === "musqotmp__MarketingActivity__c" || this.objectApiName === "musqotmp__MarketingJourney__c") {
				this.marketingPlan = this.data.fields.musqotmp__Campaign__r.value.fields.musqotmp__MarketingPlan__r.value.fields.Name.value;
				this.campaignValue = this.data.fields.musqotmp__Campaign__r.value.fields.Name.value;

				let objectName = "musqotmp__Campaign__c";
				let taskrecordId = this.data.fields.musqotmp__Campaign__r.value.id;
				this.navigateToDetailViewPage(objectName, taskrecordId);
				if (this.data.fields.musqotmp__Campaign__r.value.fields.musqotmp__MarketingPlan__r) {
					this.navigateToDetailViewPage("musqotmp__MarketingPlan__c", this.data.fields.musqotmp__Campaign__r.value.fields.musqotmp__MarketingPlan__r.value.id);
				}
			}
		} else if (error) {
			this.template.querySelector("c-toast").showToast(error);
		}
	}
	@wire(getRecord, { recordId: "$recordId", layoutTypes: ["Full"], modes: ["View"] })
	wiredGetRecord(result) {
		if (result.data) {
			//this.tempGanttId = result.data.id;
			this.loadGantt();
		}
	}
	disconnectedCallback() {
		gantt.clearAll();
	}
	get moment() {
		return moment;
	}
	//renderedCallback renamed to loadGantt By Subhasis
	loadGantt() {
		loadScript(this, MOMENT_JS)
			.then(() => {
				this.moment;
				this.ganttId = this.recordId;
				this.handleRefresh();
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	@wire(CurrentPageReference) pageRef;
	@wire(readMarketingCloudCredential)
	wiredReadMarketingCloudCredential(cresult) {
		if (cresult.data && cresult.data.sObj) {
			this.marketingCloudCredential = cresult.data.sObj;
		}
	}
	@wire(getMarketingPlanView, {
		param: JSON.stringify({
			component: "marketingPlanView-getMarketingPlanView",
		}),
		recordId: "$ganttId",
	})
	wiredGetMarketingPlanView(result) {
		if (result.data) {
			if (result.data.toast.ok) {
				this.wiredData = result;
				this.campaignManagerSetup = result.data.customSetting;
				this.barColorMap = result.data.stringMap;
				this.sObject = result.data.sObj;
				this.sObjects = result.data.sObjects;
				this.sObjectList = result.data.sObjectList;
				this.sObjectListMA = result.data.sObjectListMarketingActivity;
				this.sObjectUnMap = result.data.sObjectUnMap;
				this.barTextFields = result.data.fields;
				this.fieldMap = result.data.fieldMap;
				this.tasks = this.buildGanttData();
				this.ganttPromise();
				this.template.querySelector("c-toast").showToast(result.data.toast);
			} else {
				this.template.querySelector("c-toast").showToast(result.data.toast);
			}
		} else if (result.error) {
			this.template.querySelector("c-toast").showToast(result.error);
		}
	}

	buildGanttData() {
		let campaignMap = {};
		
		this.sObjectList.forEach((sObjectCampaign) => {
			campaignMap[sObjectCampaign.Id] = sObjectCampaign;
		});
		//MA under MJ start
		let marketingJouenyMap = {};
        this.sObjectListMA.forEach((sObjectMJ) => {
			marketingJouenyMap[sObjectMJ.Id] = sObjectMJ;
		});
		//Bayer end
		let tasks = {
			data: [],
			links: [],
		};

		this.sObjects.forEach((sObjectMaketingPlan) => {
			let task = {};
			let link = {};
			task["id"] = sObjectMaketingPlan.Id;
			task["objectapiname"] = "musqotmp__MarketingPlan__c";
			task["folder_type"] = "marketingplan";
			task["view"] = this.sObject.musqotmp__GanttView__c;
			task["task_type"] = sObjectMaketingPlan.musqotmp__PlanType__c;
			task["text"] = sObjectMaketingPlan.Name;
			let barText = [];
			// barText.push(sObjectMaketingPlan.Name);
			//Sowmya user stroy W-000365 BarText
			if (this.sObject.musqotmp__GanttView__c === "Compact") {
				let name = this.barTextFields[0].name;
				if (this.barTextFields[0].type === "reference") {
					if (name === "OwnerId") {
						name = "Owner";
					} else if (name === "CreatedById") {
						name = "CreatedBy";
					} else if (name === "LastModifiedById") {
						name = "LastModifiedBy";
					} else if (name.includes("__c")) {
						name = name.replace("__c", "__r");
					}
					barText.push(sObjectMaketingPlan[name]["Name"] === undefined ? "-" : sObjectMaketingPlan[name]["Name"]);
				} else {
					barText.push(sObjectMaketingPlan[name] === undefined ? "-" : sObjectMaketingPlan[name]);
				}
			} else {
				this.barTextFields.forEach((barTextField, index) => {
					let name = barTextField.name;
					if (barTextField.type === "reference") {
						if (name === "OwnerId") {
							name = "Owner";
						} else if (name === "CreatedById") {
							name = "CreatedBy";
						} else if (name === "LastModifiedById") {
							name = "LastModifiedBy";
						} else if (name.includes("__c")) {
							name = name.replace("__c", "__r");
						}
						barText.push(sObjectMaketingPlan[name]["Name"] === undefined ? "-" : sObjectMaketingPlan[name]["Name"]);
					} else {
						barText.push(sObjectMaketingPlan[name] === undefined ? "-" : sObjectMaketingPlan[name]);
					}
					if (this.barTextFields[index + 1]) {
						barText.push("<br />");
					}
				});
			}
			task["bartext"] = barText.join(" ");
			//Sowmya End
			if (this.barColorMap["musqotmp__MarketingPlan__c~~musqotmp__PlanType__c~~" + sObjectMaketingPlan.musqotmp__PlanType__c]) {
				task["color"] = this.barColorMap["musqotmp__MarketingPlan__c~~musqotmp__PlanType__c~~" + sObjectMaketingPlan.musqotmp__PlanType__c];
			}
			task["status"] = sObjectMaketingPlan.musqotmp__Status__c;
			task["status_icon"] = sObjectMaketingPlan.musqotmp__StatusIcon__c;
			task["progress"] = 1;
			task["start_date"] = this.moment(sObjectMaketingPlan.musqotmp__StartDate__c).format("DD-MM-YYYY");
			task["duration"] = Math.round((new Date(sObjectMaketingPlan.musqotmp__EndDate__c) - new Date(sObjectMaketingPlan.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
			tasks.data.push(task);
			if (sObjectMaketingPlan.musqotmp__Campaigns__r) {
				sObjectMaketingPlan.musqotmp__Campaigns__r.forEach((sObjectCampaign) => {
					task = {};
					link = {};
					task["id"] = sObjectCampaign.Id;
					task["objectapiname"] = "Campaign";
					task["folder_type"] = "campaign";
					task["view"] = this.sObject.musqotmp__GanttView__c;
					task["task_type"] = sObjectCampaign.Type;
					task["text"] = sObjectCampaign.Name;
					let barText = [];
					if (this.sObject.musqotmp__GanttView__c === "Compact") {
						let name = this.barTextFields[0].name;
						if (this.barTextFields[0].type === "reference") {
							if (name === "OwnerId") {
								name = "Owner";
							} else if (name === "CreatedById") {
								name = "CreatedBy";
							} else if (name === "LastModifiedById") {
								name = "LastModifiedBy";
							} else if (name.includes("__c")) {
								name = name.replace("__c", "__r");
							}
							barText.push(sObjectCampaign[name]["Name"] === undefined ? "-" : sObjectCampaign[name]["Name"]);
						} else {
							barText.push(sObjectCampaign[name] === undefined ? "-" : sObjectCampaign[name]);
						}
					} else {
						this.barTextFields.forEach((barTextField, index) => {
							let name = barTextField.name;
							if (barTextField.type === "reference") {
								if (name === "OwnerId") {
									name = "Owner";
								} else if (name === "CreatedById") {
									name = "CreatedBy";
								} else if (name === "LastModifiedById") {
									name = "LastModifiedBy";
								} else if (name.includes("__c")) {
									name = name.replace("__c", "__r");
								}
								barText.push(sObjectCampaign[name]["Name"] === undefined ? "-" : sObjectCampaign[name]["Name"]);
							} else {
								barText.push(sObjectCampaign[name] === undefined ? "-" : sObjectCampaign[name]);
							}

							if (this.barTextFields[index + 1]) {
								barText.push("<br />");
							}
						});
					}

					task["bartext"] = barText.join(" ");

					if (this.barColorMap["Campaign~~Type~~" + sObjectCampaign.Type]) {
						task["color"] = this.barColorMap["Campaign~~Type~~" + sObjectCampaign.Type];
					}
					task["status"] = sObjectCampaign.Status;
					task["status_icon"] = sObjectCampaign.musqotmp__StatusIcon__c;
					task["progress"] = this.ganttProgress(sObjectCampaign);
					task["start_date"] = this.moment(sObjectCampaign.StartDate).format("DD-MM-YYYY");
					task["duration"] = Math.round((new Date(sObjectCampaign.EndDate) - new Date(sObjectCampaign.StartDate)) / (1000 * 60 * 60 * 24)) + 1;
					if (sObjectCampaign.ParentId) {
						task["parent"] = sObjectCampaign.ParentId;
						link["source"] = sObjectCampaign.ParentId;
						link["target"] = sObjectCampaign.Id;
						link["type"] = 0;
						//tasks.links.push(link);
					} else {
						task["parent"] = sObjectCampaign.musqotmp__MarketingPlan__c;
						link["source"] = sObjectCampaign.musqotmp__MarketingPlan__c;
						link["target"] = sObjectCampaign.Id;
						link["type"] = 0;
						//tasks.links.push(link);
					}
					tasks.data.push(task);
					if (campaignMap[sObjectCampaign.Id]) {
						let sObjectChildCampaign = campaignMap[sObjectCampaign.Id];
						if (sObjectChildCampaign.musqotmp__MarketingActivities__r) {
							sObjectChildCampaign.musqotmp__MarketingActivities__r.forEach((sObjectMarketingActivity) => {
								task = {};
								link = {};
								task["id"] = sObjectMarketingActivity.Id;
								task["objectapiname"] = "musqotmp__MarketingActivity__c";
								task["folder_type"] = "marketingactivity";
								task["view"] = this.sObject.musqotmp__GanttView__c;
								task["task_type"] = sObjectMarketingActivity.musqotmp__Type__c;
								task["text"] = sObjectMarketingActivity.Name;
								let barText = [];
								//Sowmya user stroy W-000365 BarText
								if (this.sObject.musqotmp__GanttView__c === "Compact") {
									let name = this.barTextFields[0].name;
									if (this.barTextFields[0].type === "reference") {
										if (name === "OwnerId") {
											// name = 'Owner';
											name = "CreatedBy";
										} else if (name === "CreatedById") {
											name = "CreatedBy";
										} else if (name === "LastModifiedById") {
											name = "LastModifiedBy";
										} else if (name.includes("__c")) {
											name = name.replace("__c", "__r");
										}
										barText.push(sObjectMarketingActivity[name]["Name"] === undefined ? "-" : sObjectMarketingActivity[name]["Name"]);
									} else {
										barText.push(sObjectMarketingActivity[name] === undefined ? "-" : sObjectMarketingActivity[name]);
									}
								} else {
									this.barTextFields.forEach((barTextField, index) => {
										let name = barTextField.name;
										if (barTextField.type === "reference") {
											if (name === "OwnerId") {
												//name = 'Owner';
												name = "CreatedBy";
											} else if (name === "CreatedById") {
												name = "CreatedBy";
											} else if (name === "LastModifiedById") {
												name = "LastModifiedBy";
											} else if (name.includes("__c")) {
												name = name.replace("__c", "__r");
											}
											barText.push(sObjectMarketingActivity[name]["Name"] === undefined ? "-" : sObjectMarketingActivity[name]["Name"]);
										} else {
											barText.push(sObjectMarketingActivity[name] === undefined ? "-" : sObjectMarketingActivity[name]);
										}
										if (this.barTextFields[index + 1]) {
											barText.push("<br />");
										}
									});
								}
								//Sowmya End
								task["bartext"] = barText.join(" ");
								if (this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + sObjectMarketingActivity.musqotmp__Type__c]) {
									task["color"] = this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + sObjectMarketingActivity.musqotmp__Type__c];
								}
								task["status"] = sObjectMarketingActivity.musqotmp__Status__c;
								task["status_icon"] = sObjectMarketingActivity.musqotmp__StatusIcon__c;
								task["progress"] = 1;
								task["start_date"] = this.moment(sObjectMarketingActivity.musqotmp__StartDate__c).format("DD-MM-YYYY");
								task["duration"] = Math.round((new Date(sObjectMarketingActivity.musqotmp__EndDate__c) - new Date(sObjectMarketingActivity.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
								task["parent"] = sObjectMarketingActivity.musqotmp__Campaign__c;
								tasks.data.push(task);
								link["source"] = sObjectMarketingActivity.musqotmp__Campaign__c;
								link["target"] = sObjectMarketingActivity.Id;
								link["type"] = 0;
								//tasks.links.push(link);
							});
						}
						if (sObjectChildCampaign.musqotmp__MarketingJourneys__r) {
							sObjectChildCampaign.musqotmp__MarketingJourneys__r.forEach((sObjectMarketingJourney) => {
								task = {};
								link = {};
								task["id"] = sObjectMarketingJourney.Id;
								task["objectapiname"] = "musqotmp__MarketingJourney__c";
								task["folder_type"] = "marketingjourney";
								task["view"] = this.sObject.musqotmp__GanttView__c;
								//task['task_type'] = sObjectMaketingPlan.musqotmp__PlanType__c;
								task["task_type"] = sObjectMarketingJourney.musqotmp__Type__c;
								task["text"] = sObjectMarketingJourney.Name;
								let barText = [];
								if (sObjectMarketingJourney.musqotmp__JourneyID__c && this.marketingCloudCredential) {
									this.mcUrlMap[sObjectMarketingJourney.Id] =
										this.marketingCloudCredential.musqotmp__TargetURL__c + sObjectMarketingJourney.musqotmp__JourneyID__c + "/" + sObjectMarketingJourney.musqotmp__JourneyVersion__c;
								}
								//barText.push(sObjectMarketingJourney.Name);
								//Sowmya user stroy W-000365 BarText
								if (this.sObject.musqotmp__GanttView__c === "Compact") {
									let name = this.barTextFields[0].name;
									if (this.barTextFields[0].type === "reference") {
										if (name === "OwnerId") {
											name = "Owner";
										} else if (name === "CreatedById") {
											name = "CreatedBy";
										} else if (name === "LastModifiedById") {
											name = "LastModifiedBy";
										} else if (name.includes("__c")) {
											name = name.replace("__c", "__r");
										}
										barText.push(sObjectMarketingJourney[name]["Name"] === undefined ? "-" : sObjectMarketingJourney[name]["Name"]);
									} else {
										barText.push(sObjectMarketingJourney[name] === undefined ? "-" : sObjectMarketingJourney[name]);
									}
								} else {
									this.barTextFields.forEach((barTextField, index) => {
										let name = barTextField.name;
										if (barTextField.type === "reference") {
											if (name === "OwnerId") {
												name = "Owner";
											} else if (name === "CreatedById") {
												name = "CreatedBy";
											} else if (name === "LastModifiedById") {
												name = "LastModifiedBy";
											} else if (name.includes("__c")) {
												name = name.replace("__c", "__r");
											}
											barText.push(sObjectMarketingJourney[name]["Name"] === undefined ? "-" : sObjectMarketingJourney[name]["Name"]);
										} else {
											barText.push(sObjectMarketingJourney[name] === undefined ? "-" : sObjectMarketingJourney[name]);
										}
										if (this.barTextFields[index + 1]) {
											barText.push("<br />");
										}
									});
								}
								
								task["bartext"] = barText.join(" ");
								task["status"] = sObjectMarketingJourney.musqotmp__Status__c;
								task["status_icon"] = sObjectMarketingJourney.musqotmp__StatusIcon__c;
								task["progress"] = 1;
								task["start_date"] = this.moment(sObjectMarketingJourney.musqotmp__StartDate__c).format("DD-MM-YYYY");
								task["duration"] = Math.round((new Date(sObjectMarketingJourney.musqotmp__EndDate__c) - new Date(sObjectMarketingJourney.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
								task["parent"] = sObjectMarketingJourney.musqotmp__Campaign__c;
								tasks.data.push(task);
								link["source"] = sObjectMarketingJourney.musqotmp__Campaign__c;
								link["target"] = sObjectMarketingJourney.Id;
								link["type"] = 0;
								//tasks.links.push(link);
								//Start MA uder MJ Bayer
								if(marketingJouenyMap[sObjectMarketingJourney.Id]){
									let sObjectChildMA = marketingJouenyMap[sObjectMarketingJourney.Id];
								     if (sObjectChildMA.musqotmp__Marketing_Activities__r) {
									     sObjectChildMA.musqotmp__Marketing_Activities__r.forEach((sObjectMarketingActivity) => {
											task = {};
								            link = {};
								            task["id"] = sObjectMarketingActivity.Id;
								            task["objectapiname"] = "musqotmp__MarketingActivity__c";
								            task["folder_type"] = "marketingactivityInJourney";
								            task["view"] = this.sObject.musqotmp__GanttView__c;
								            task["task_type"] = sObjectMarketingActivity.musqotmp__Type__c;
								            task["text"] = sObjectMarketingActivity.Name;
								            let barText = [];
											if (this.sObject.musqotmp__GanttView__c === "Compact") {
												let name = this.barTextFields[0].name;
												if (this.barTextFields[0].type === "reference") {
													if (name === "OwnerId") {
														// name = 'Owner';
														name = "CreatedBy";
													} else if (name === "CreatedById") {
														name = "CreatedBy";
													} else if (name === "LastModifiedById") {
														name = "LastModifiedBy";
													} else if (name.includes("__c")) {
														name = name.replace("__c", "__r");
													}
													barText.push(sObjectMarketingActivity[name]["Name"] === undefined ? "-" : sObjectMarketingActivity[name]["Name"]);
												} else {
													barText.push(sObjectMarketingActivity[name] === undefined ? "-" : sObjectMarketingActivity[name]);
												}
											} else {
												this.barTextFields.forEach((barTextField, index) => {
													let name = barTextField.name;
													if (barTextField.type === "reference") {
														if (name === "OwnerId") {
															//name = 'Owner';
															name = "CreatedBy";
														} else if (name === "CreatedById") {
															name = "CreatedBy";
														} else if (name === "LastModifiedById") {
															name = "LastModifiedBy";
														} else if (name.includes("__c")) {
															name = name.replace("__c", "__r");
														}
														barText.push(sObjectMarketingActivity[name]["Name"] === undefined ? "-" : sObjectMarketingActivity[name]["Name"]);
													} else {
														barText.push(sObjectMarketingActivity[name] === undefined ? "-" : sObjectMarketingActivity[name]);
													}
													if (this.barTextFields[index + 1]) {
														barText.push("<br />");
													}
												});
											}
											
											task["bartext"] = barText.join(" ");
											if (this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + sObjectMarketingActivity.musqotmp__Type__c]) {
												task["color"] = this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + sObjectMarketingActivity.musqotmp__Type__c];
											}
											task["status"] = sObjectMarketingActivity.musqotmp__Status__c;
											task["status_icon"] = sObjectMarketingActivity.musqotmp__StatusIcon__c;
											task["progress"] = 1;
											task["start_date"] = this.moment(sObjectMarketingActivity.musqotmp__StartDate__c).format("DD-MM-YYYY");
											task["duration"] = Math.round((new Date(sObjectMarketingActivity.musqotmp__EndDate__c) - new Date(sObjectMarketingActivity.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
											task["parent"] = sObjectMarketingActivity.musqotmp__Campaign__c;
											tasks.data.push(task);
											link["source"] = sObjectMarketingActivity.musqotmp__Campaign__c;
											link["target"] = sObjectMarketingActivity.Id;
											link["type"] = 0;
											//tasks.links.push(link);
										});
									 }
								}
							});
						}
					}
				});
			} else {
				if (this.sObjectUnMap[sObjectMaketingPlan.Id]) {
					this.sObjectUnMap[sObjectMaketingPlan.Id].forEach((sObjectCampaign) => {
						task = {};
						link = {};
						task["id"] = sObjectCampaign.Id;
						task["objectapiname"] = "Campaign";
						task["folder_type"] = "campaign";
						task["view"] = this.sObject.musqotmp__GanttView__c;
						task["task_type"] = sObjectCampaign.Type;
						task["text"] = sObjectCampaign.Name;
						let barText = [];
						if (this.sObject.musqotmp__GanttView__c === "Compact") {
							let name = this.barTextFields[0].name;
							if (this.barTextFields[0].type === "reference") {
								if (name === "OwnerId") {
									name = "Owner";
								} else if (name === "CreatedById") {
									name = "CreatedBy";
								} else if (name === "LastModifiedById") {
									name = "LastModifiedBy";
								} else if (name.includes("__c")) {
									name = name.replace("__c", "__r");
								}
								barText.push(sObjectCampaign[name]["Name"] === undefined ? "-" : sObjectCampaign[name]["Name"]);
							} else {
								barText.push(sObjectCampaign[name] === undefined ? "-" : sObjectCampaign[name]);
							}
						} else {
							this.barTextFields.forEach((barTextField, index) => {
								let name = barTextField.name;
								if (barTextField.type === "reference") {
									if (name === "OwnerId") {
										name = "Owner";
									} else if (name === "CreatedById") {
										name = "CreatedBy";
									} else if (name === "LastModifiedById") {
										name = "LastModifiedBy";
									} else if (name.includes("__c")) {
										name = name.replace("__c", "__r");
									}
									barText.push(sObjectCampaign[name]["Name"] === undefined ? "-" : sObjectCampaign[name]["Name"]);
								} else {
									barText.push(sObjectCampaign[name] === undefined ? "-" : sObjectCampaign[name]);
								}
								if (this.barTextFields[index + 1]) {
									barText.push("<br />");
								}
							});
						}
						task["bartext"] = barText.join(" ");
						if (this.barColorMap["Campaign~~Type~~" + sObjectCampaign.Type]) {
							task["color"] = this.barColorMap["Campaign~~Type~~" + sObjectCampaign.Type];
						}
						task["status"] = sObjectCampaign.Status;
						task["status_icon"] = sObjectCampaign.musqotmp__StatusIcon__c;
						task["progress"] = this.ganttProgress(sObjectCampaign);
						task["start_date"] = this.moment(sObjectCampaign.StartDate).format("DD-MM-YYYY");
						task["duration"] = Math.round((new Date(sObjectCampaign.EndDate) - new Date(sObjectCampaign.StartDate)) / (1000 * 60 * 60 * 24)) + 1;
						if (sObjectCampaign.ParentId) {
							task["parent"] = sObjectCampaign.ParentId;
							link["source"] = sObjectCampaign.ParentId;
							link["target"] = sObjectCampaign.Id;
							link["type"] = 0;
							//tasks.links.push(link);
						} else {
							task["parent"] = sObjectMaketingPlan.Id;
							link["source"] = sObjectMaketingPlan.Id;
							link["target"] = sObjectCampaign.Id;
							link["type"] = 0;
							//tasks.links.push(link);
						}
						tasks.data.push(task);
						if (campaignMap[sObjectCampaign.Id]) {
							let sObjectChildCampaign = campaignMap[sObjectCampaign.Id];
							if (sObjectChildCampaign.musqotmp__MarketingActivities__r) {
								sObjectChildCampaign.musqotmp__MarketingActivities__r.forEach((sObjectMarketingActivity) => {
									task = {};
									link = {};
									task["id"] = sObjectMarketingActivity.Id;
									task["objectapiname"] = "musqotmp__MarketingActivity__c";
									task["folder_type"] = "marketingactivity";
									task["view"] = this.sObject.musqotmp__GanttView__c;
									task["task_type"] = sObjectMarketingActivity.musqotmp__Type__c;
									task["text"] = sObjectMarketingActivity.Name;
									let barText = [];
									//barText.push(sObjectMarketingActivity.Name);
									//Sowmya user stroy W-000365 BarText
									if (this.sObject.musqotmp__GanttView__c === "Compact") {
										let name = this.barTextFields[0].name;
										if (this.barTextFields[0].type === "reference") {
											if (name === "OwnerId") {
												name = "Owner";
											} else if (name === "CreatedById") {
												name = "CreatedBy";
											} else if (name === "LastModifiedById") {
												name = "LastModifiedBy";
											} else if (name.includes("__c")) {
												name = name.replace("__c", "__r");
											}
											barText.push(sObjectMarketingActivity[name]["Name"] === undefined ? "-" : sObjectMarketingActivity[name]["Name"]);
										} else {
											barText.push(sObjectMarketingActivity[name] === undefined ? "-" : sObjectMarketingActivity[name]);
										}
									} else {
										this.barTextFields.forEach((barTextField, index) => {
											let name = barTextField.name;
											if (barTextField.type === "reference") {
												if (name === "OwnerId") {
													name = "Owner";
												} else if (name === "CreatedById") {
													name = "CreatedBy";
												} else if (name === "LastModifiedById") {
													name = "LastModifiedBy";
												} else if (name.includes("__c")) {
													name = name.replace("__c", "__r");
												}
												barText.push(sObjectMarketingActivity[name]["Name"] === undefined ? "-" : sObjectMarketingActivity[name]["Name"]);
											} else {
												barText.push(sObjectMarketingActivity[name] === undefined ? "-" : sObjectMarketingActivity[name]);
											}
											if (this.barTextFields[index + 1]) {
												barText.push("<br />");
											}
										});
									}

									task["bartext"] = barText.join(" ");
									if (this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + sObjectMarketingActivity.musqotmp__Type__c]) {
										task["color"] = this.barColorMap["musqotmp__MarketingActivity__c~~musqotmp__Type__c~~" + sObjectMarketingActivity.musqotmp__Type__c];
									}
									task["status"] = sObjectMarketingActivity.musqotmp__Status__c;
									task["status_icon"] = sObjectMarketingActivity.musqotmp__StatusIcon__c;
									task["progress"] = 1;
									task["start_date"] = this.moment(sObjectMarketingActivity.musqotmp__StartDate__c).format("DD-MM-YYYY");
									task["duration"] = Math.round((new Date(sObjectMarketingActivity.musqotmp__EndDate__c) - new Date(sObjectMarketingActivity.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
									task["parent"] = sObjectMarketingActivity.musqotmp__Campaign__c;
									tasks.data.push(task);
									link["source"] = sObjectMarketingActivity.musqotmp__Campaign__c;
									link["target"] = sObjectMarketingActivity.Id;
									link["type"] = 0;
									//tasks.links.push(link);
								});
							}
							if (sObjectChildCampaign.musqotmp__MarketingJourneys__r) {
								sObjectChildCampaign.musqotmp__MarketingJourneys__r.forEach((sObjectMarketingJourney) => {
									task = {};
									link = {};
									task["id"] = sObjectMarketingJourney.Id;
									task["objectapiname"] = "musqotmp__MarketingJourney__c";
									task["folder_type"] = "marketingjourney";
									task["view"] = this.sObject.musqotmp__GanttView__c;
									//task['task_type'] = sObjectMaketingPlan.musqotmp__PlanType__c;
									//Sowmya user stroy W-000365 BarText
									task["task_type"] = sObjectMarketingJourney.musqotmp__PlanType__c;
									task["text"] = sObjectMarketingJourney.Name;
									let barText = [];
									//barText.push(sObjectMarketingJourney.Name);
									if (sObjectMarketingJourney.musqotmp__JourneyID__c && this.marketingCloudCredential) {
										this.mcUrlMap[sObjectMarketingJourney.Id] =
											this.marketingCloudCredential.musqotmp__TargetURL__c + sObjectMarketingJourney.musqotmp__JourneyID__c + "/" + sObjectMarketingJourney.musqotmp__JourneyVersion__c;
									}
									//Sowmya user stroy W-000365 BarText
									if (this.sObject.musqotmp__GanttView__c === "Compact") {
										let name = this.barTextFields[0].name;
										if (this.barTextFields[0].type === "reference") {
											if (name === "OwnerId") {
												name = "Owner";
											} else if (name === "CreatedById") {
												name = "CreatedBy";
											} else if (name === "LastModifiedById") {
												name = "LastModifiedBy";
											} else if (name.includes("__c")) {
												name = name.replace("__c", "__r");
											}
											barText.push(sObjectMarketingJourney[name]["Name"] === undefined ? "-" : sObjectMarketingJourney[name]["Name"]);
										} else {
											barText.push(sObjectMarketingJourney[name] === undefined ? "-" : sObjectMarketingJourney[name]);
										}
									} else {
										this.barTextFields.forEach((barTextField, index) => {
											let name = barTextField.name;
											if (barTextField.type === "reference") {
												if (name === "OwnerId") {
													name = "Owner";
												} else if (name === "CreatedById") {
													name = "CreatedBy";
												} else if (name === "LastModifiedById") {
													name = "LastModifiedBy";
												} else if (name.includes("__c")) {
													name = name.replace("__c", "__r");
												}
												barText.push(sObjectMarketingJourney[name]["Name"] === undefined ? "-" : sObjectMarketingJourney[name]["Name"]);
											} else {
												barText.push(sObjectMarketingJourney[name] === undefined ? "-" : sObjectMarketingJourney[name]);
											}
											if (this.barTextFields[index + 1]) {
												barText.push("<br />");
											}
										});
									}
									//Sowmya End
									task["bartext"] = barText.join(" ");
									task["status"] = sObjectMarketingJourney.musqotmp__Status__c;
									task["status_icon"] = sObjectMarketingJourney.musqotmp__StatusIcon__c;
									task["progress"] = 1;
									task["start_date"] = this.moment(sObjectMarketingJourney.musqotmp__StartDate__c).format("DD-MM-YYYY");
									task["duration"] = Math.round((new Date(sObjectMarketingJourney.musqotmp__EndDate__c) - new Date(sObjectMarketingJourney.musqotmp__StartDate__c)) / (1000 * 60 * 60 * 24)) + 1;
									task["parent"] = sObjectMarketingJourney.musqotmp__Campaign__c;
									tasks.data.push(task);
									link["source"] = sObjectMarketingJourney.musqotmp__Campaign__c;
									link["target"] = sObjectMarketingJourney.Id;
									link["type"] = 0;
									//tasks.links.push(link);
								});
							}
						}
					});
				}
			}
		});
		return tasks;
	}

	ganttProgress(campaign) {
		if (campaign.musqotmp__IsFolder__c && this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c !== undefined) {
			if (this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c === 0) {
				return 1;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c === 1) {
				const percent = (campaign.HierarchyActualCost ? campaign.HierarchyActualCost : 0) / (campaign.HierarchyBudgetedCost ? campaign.HierarchyBudgetedCost : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c === 2) {
				const percent = (campaign.HierarchyAmountWonOpportunities ? campaign.HierarchyAmountWonOpportunities : 0) / (campaign.HierarchyExpectedRevenue ? campaign.HierarchyExpectedRevenue : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c === 3) {
				// const percent = (campaign.HierarchyNumberOfConvertedLeads ? campaign.HierarchyNumberOfConvertedLeads : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent = (campaign.NumberOfConvertedLeads ? campaign.NumberOfConvertedLeads : 0) / (campaign.NumberOfLeads ? campaign.NumberOfLeads : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c === 4) {
				// const percent = (campaign.HierarchyNumberOfLeads ? campaign.HierarchyNumberOfLeads : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent = (campaign.NumberOfWonOpportunities ? campaign.NumberOfWonOpportunities : 0) / (campaign.NumberOfOpportunities ? campaign.NumberOfOpportunities : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c === 5) {
				//const percent = (campaign.HierarchyNumberOfWonOpportunities ? campaign.HierarchyNumberOfWonOpportunities : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent =
					(campaign.HierarchyNumberOfWonOpportunities ? campaign.HierarchyNumberOfWonOpportunities : 0) / (campaign.HierarchyNumberOfOpportunities ? campaign.HierarchyNumberOfOpportunities : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c === 6) {
				// const percent = (campaign.HierarchyNumberOfOpportunities ? campaign.HierarchyNumberOfOpportunities : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent = (campaign.NumberOfResponses ? campaign.NumberOfResponses : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressTypeFolder__c === 7) {
				// const percent = (campaign.HierarchyNumberOfContacts ? campaign.HierarchyNumberOfContacts : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent =
					(campaign.HierarchyNumberOfResponses ? campaign.HierarchyNumberOfResponses : 0) /
					((campaign.HierarchyNumberSent ? campaign.HierarchyNumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				return percent > 1 ? 1 : percent;
			}
		} else if (!campaign.musqotmp__IsFolder__c && this.campaignManagerSetup.musqotmp__GanttProgressType__c !== undefined) {
			if (this.campaignManagerSetup.musqotmp__GanttProgressType__c === 0) {
				return 1;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressType__c === 1) {
				const percent = (campaign.ActualCost ? campaign.ActualCost : 0) / (campaign.BudgetedCost ? campaign.BudgetedCost : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressType__c === 2) {
				const percent = (campaign.HierarchyAmountWonOpportunities ? campaign.HierarchyAmountWonOpportunities : 0) / (campaign.HierarchyExpectedRevenue ? campaign.HierarchyExpectedRevenue : 0);
				//const percent = (campaign.AmountWonOpportunities ? campaign.AmountWonOpportunities : 0) / (campaign.ExpectedRevenue ? campaign.ExpectedRevenue : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressType__c === 3) {
				//const percent = (campaign.NumberOfConvertedLeads ? campaign.NumberOfConvertedLeads : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent = (campaign.NumberOfConvertedLeads ? campaign.NumberOfConvertedLeads : 0) / (campaign.NumberOfLeads ? campaign.NumberOfLeads : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressType__c === 4) {
				//const percent = (campaign.NumberOfLeads ? campaign.NumberOfLeads : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent = (campaign.NumberOfWonOpportunities ? campaign.NumberOfWonOpportunities : 0) / (campaign.NumberOfOpportunities ? campaign.NumberOfOpportunities : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressType__c === 5) {
				//const percent = (campaign.NumberOfWonOpportunities ? campaign.NumberOfWonOpportunities : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent =
					(campaign.HierarchyNumberOfWonOpportunities ? campaign.HierarchyNumberOfWonOpportunities : 0) / (campaign.HierarchyNumberOfOpportunities ? campaign.HierarchyNumberOfOpportunities : 0);
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressType__c === 6) {
				//const percent = (campaign.NumberOfOpportunities ? campaign.NumberOfOpportunities : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent = (campaign.NumberOfResponses ? campaign.NumberOfResponses : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				return percent > 1 ? 1 : percent;
			} else if (this.campaignManagerSetup.musqotmp__GanttProgressType__c === 7) {
				//const percent = (campaign.NumberOfContacts ? campaign.NumberOfContacts : 0) / ((campaign.NumberSent ? campaign.NumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				const percent =
					(campaign.HierarchyNumberOfResponses ? campaign.HierarchyNumberOfResponses : 0) /
					((campaign.HierarchyNumberSent ? campaign.HierarchyNumberSent : 0) * (campaign.ExpectedResponse ? campaign.ExpectedResponse : 0));
				return percent > 1 ? 1 : percent;
			}
		}
		return 1;
	}

	ganttPromise() {
		this.ganttInitialized = true;
		Promise.all([loadScript(this, Gantt + "/codebase/dhtmlxgantt.js"), loadStyle(this, Gantt + "/codebase/dhtmlxgantt.css")])
			.then(() => {
						this.initializeGantt();
					
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}

	initializeGantt() {
		const thislwc = this;
		const mom = thislwc.moment();

		if (this.sObject.musqotmp__DateRange__c === "THIS_QUARTER") {
			this.startDate = mom.startOf("Q").toDate();
			this.endDate = mom.endOf("Q").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "NEXT_QUARTER") {
			this.startDate = mom.add(1, "Q").startOf("Q").toDate();
			this.endDate = mom.endOf("Q").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "THIS_QUARTER,NEXT_QUARTER") {
			this.startDate = mom.startOf("Q").toDate();
			this.endDate = mom.add(1, "Q").endOf("Q").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "LAST_QUARTER,THIS_QUARTER") {
			this.startDate = mom.subtract(1, "Q").startOf("Q").toDate();
			this.endDate = mom.add(1, "Q").endOf("Q").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "THIS_YEAR") {
			this.startDate = mom.startOf("Y").toDate();
			this.endDate = mom.endOf("Y").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "NEXT_YEAR") {
			this.startDate = mom.add(1, "Y").startOf("Y").toDate();
			this.endDate = mom.endOf("Y").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "THIS_YEAR,NEXT_YEAR") {
			this.startDate = mom.startOf("Y").toDate();
			this.endDate = mom.add(1, "Y").endOf("Y").toDate();
		} else if (this.sObject.musqotmp__DateRange__c === "LAST_YEAR,THIS_YEAR") {
			this.startDate = mom.subtract(1, "Y").startOf("Y").toDate();
			this.endDate = mom.add(1, "Y").endOf("Y").toDate();
		} else {
			this.startDate = mom.startOf("Q").toDate();
			this.endDate = mom.endOf("Q").toDate();
		}
		let zoomConfig = {
			levels: [
				{
					name: "day",
					scale_height: 27,
					min_column_width: 80,
					scales: [{ unit: "day", step: 1, format: "%d %M" }],
				},
				{
					name: "week",
					scale_height: 50,
					min_column_width: 50,
					scales: [
						{
							unit: "week",
							step: 1,
							format: function (date) {
								var dateToStr = gantt.date.date_to_str("%d %M");
								var endDate = gantt.date.add(date, -6, "day");
								var weekNum = gantt.date.date_to_str("%W")(date);
								return "#" + weekNum + ", " + dateToStr(date) + " - " + dateToStr(endDate);
							},
						},
						{ unit: "day", step: 1, format: "%j %D" },
					],
				},
				{
					name: "month",
					scale_height: 35,
					scales: [
						{ unit: "month", format: "%F, %Y" },
						{ unit: "week", format: "%W" },
					],
				},
				{
					name: "quarter",
					height: 50,
					min_column_width: 90,
					scales: [
						{ unit: "month", step: 1, format: "%M" },
						{
							unit: "quarter",
							step: 1,
							format: function (date) {
								var dateToStr = gantt.date.date_to_str("%M");
								var endDate = gantt.date.add(gantt.date.add(date, 3, "month"), -1, "day");
								return dateToStr(date) + " - " + dateToStr(endDate);
							},
						},
					],
				},
				{
					name: "year",
					scale_height: 50,
					min_column_width: 30,
					scales: [{ unit: "year", step: 1, format: "%Y" }],
				},
			],
		};
		gantt.ext.zoom.init(zoomConfig);
		gantt.ext.zoom.setLevel("month");
		gantt.config.row_height = this.sObject.musqotmp__GanttView__c === "Compact" ? 25 : 35;
		gantt.config.columns = [
			{
				name: "text",
				label: this.label.campaignHeader,
				tree: true,
				resize: true,
			},
			{
				name: "status",
				label: this.label.statusHeader,
				width: 50,
				template: function (obj) {
					return "<div>" + obj.status_icon + "</div>";
				},
				align: "center",
			},
		];

		gantt.templates.grid_folder = function (item) {
			return "<div class='gantt_tree_icon gantt_" + item.folder_type + "'></div>";
		};

		gantt.templates.grid_file = function (item) {
			return "<div class='gantt_tree_icon gantt_" + item.folder_type + "'></div>";
		};

		gantt.templates.task_text = function (start, end, task) {
			let iconUrl = "";
			switch (task.folder_type) {
				case "campaign":
					if (task.view === "Compact") {
						return task.bartext;
					}
					return '<div class="campaign">' + task.bartext + "</div>";
					break;
				//Sowmya user stroy W-000365 BarText
				case "marketingplan":
					if (task.view === "Compact") {
						return task.bartext;
					}
					return '<div class="campaign">' + task.bartext + "</div>";
					break;
				case "marketingjourney":
						if (task.view === "Compact") {
							return task.bartext;
						}
						return '<div class="campaign">' + task.bartext + "</div>";
						break;
						//Bayer start
				case "marketingactivityInJourney":
							if (thislwc.standardIcons.includes(task.task_type)) {
								iconUrl = "/resource/musqotmp__Icon_" + task.task_type;
							} else {
								iconUrl = "/resource/Icon_" + task.task_type;
							}
							if (task.view === "Compact") {
								return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="16" height="16" /> ' + task.bartext + "";
							}
							return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="24" height="24" /> ' + task.bartext + "";
							break;	
						//Bayer End
				case "marketingactivity":
					if (thislwc.standardIcons.includes(task.task_type)) {
						iconUrl = "/resource/musqotmp__Icon_" + task.task_type;
					} else {
						iconUrl = "/resource/Icon_" + task.task_type;
					}
					if (task.view === "Compact") {
						return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="16" height="16" /> ' + task.bartext + "";
					}
					return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="24" height="24" /> ' + task.bartext + "";
					break;
				//Sowmya End
				/*case "marketingjourney":
					if (thislwc.standardIcons.includes(task.task_type)) {
						iconUrl = "/resource/musqotmp__Icon_" + task.task_type;
					} else {
						iconUrl = "/resource/Icon_" + task.task_type;
					}
					if (task.view === "Compact") {
						return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="16" height="16" /> ' + task.bartext + "";
					}
					return '<img style="margin-bottom: 3px;" src="' + iconUrl + '" width="24" height="24" /> ' + task.bartext + "";
					break;*/
				default:
					return task.text;
					break;
					
			}
		};
		//sowmya user story W-000340
		gantt.showLightbox = function (id) {
			if (id !== null) {
				let taskObj = gantt.getTask(id);
				thislwc.taskId = id;
				thislwc.objectApiName = taskObj.objectapiname;
				thislwc.taskRecId = thislwc.taskId;
				if (thislwc.objectApiName === "musqotmp__MarketingPlan__c") {
					thislwc.isPlan = true;
					thislwc.isCampaign = false;
					thislwc.isMarketingJourney = false;
					thislwc.isMarketingActivity = false;
					thislwc.taskRecId=undefined;
				} else if (thislwc.objectApiName === "Campaign") {
					thislwc.isPlan = false;
					thislwc.isCampaign = true;
					thislwc.isMarketingJourney = false;
					thislwc.isMarketingActivity = false;
					thislwc.fieldsAllocation = thislwc.fieldMap["musqotmp__Allocation__c"];
					thislwc.taskIdCampaign = taskObj.id;
					thislwc.objectFileds = [CAMPAIGN_MARKETINGPLAN_FIELD];
					if (thislwc.fieldsAllocation) {
						thislwc.paramDataTable = thislwc.buildParamDataTable();
						thislwc.financeAllocation = true;
					}
				} else if (thislwc.objectApiName === "musqotmp__MarketingActivity__c") {
					thislwc.isMarketingActivity = true;
					thislwc.isPlan = false;
					thislwc.isCampaign = false;
					thislwc.isMarketingJourney = false;
					thislwc.otherobjectapiname = "Campaign";
					thislwc.taskIdCampaign = taskObj.parent;
					//thislwc.taskIdJourney = taskObj.parent;//Bayer
					thislwc.fieldsCampaign = thislwc.fieldMap["Campaign"];
					thislwc.fieldsAllocation = thislwc.fieldMap["musqotmp__Allocation__c"];
					thislwc.objectFileds = [MARKETING_ACTIVITY_CAMPAIGN_NAME_FIELD, MARKETING_ACTIVITY_PLAN_FIELD];

					if (thislwc.fieldsAllocation) {
						thislwc.paramDataTable = thislwc.buildParamDataTable();
						thislwc.financeAllocation = true;
					}
				} else if (thislwc.objectApiName === "musqotmp__MarketingJourney__c") {
					thislwc.isMarketingJourney = true;
					thislwc.isMarketingActivity = false;
					thislwc.isPlan = false;
					thislwc.isCampaign = false;
					thislwc.otherobjectapiname = "Campaign";
					thislwc.taskIdCampaign = taskObj.parent;
					thislwc.fieldsCampaign = thislwc.fieldMap["Campaign"];
					
					thislwc.fieldsAllocation = thislwc.fieldMap["musqotmp__Allocation__c"];
					thislwc.objectFileds = [MARKETING_JOURNEY_CAMPAIGN_NAME_FIELD, MARKETING_JOURNEY_PLAN_FIELD];
					if (thislwc.fieldsAllocation) {
						thislwc.paramDataTable = thislwc.buildParamDataTable();
						thislwc.financeAllocation = true;
					}
				}
				let fieldList = [];
				thislwc.fieldsCampaign.forEach((element) => {
					let field = Object.assign({}, element);
					if (field.name == "musqotmp__Campaign__c") {
						field["hasCampaign"] = true;
						field["hasReference"] = true;
					} else if (field.name == "musqotmp__MarketingPlan__c") {
						field["hasMarketingPlan"] = true;
						field["hasReference"] = true;
					}
					fieldList.push(field);
				});
				thislwc.fieldsCampaign = fieldList;
				fieldList=[];
				thislwc.fieldMap[taskObj.objectapiname].forEach((element) => {
					let field = Object.assign({}, element);
					if (field.name == "musqotmp__Campaign__c") {
						field["hasCampaign"] = true;
						field["hasReference"] = true;
					} else if (field.name == "musqotmp__Campaign__r.musqotmp__MarketingPlan__c" || field.name == "musqotmp__MarketingPlan__c") {
						field["hasMarketingPlan"] = true;
						field["hasReference"] = true;
					}
					fieldList.push(field);
				});
				thislwc.fields = fieldList;

				thislwc.header = taskObj.text;
				const modal = thislwc.template.querySelector("c-modal");
				modal.show();
				document.cookie = "taskGanttParentId=" + id;
			}
		};
		//Sowmya user stroy W-000365 End
		gantt.config.drag_move = false;
		gantt.config.drag_resize = false;
		gantt.config.drag_progress = false;
		/*gantt.config.layout = {
            css: "gantt_container",
            rows: [
                {
                    cols: [
                        {view: "grid", width: 320, scrollY: "scrollVer"},
                        {resizer: true, width: 1},
                        {view: "timeline", scrollX: "scrollHor", scrollY: "scrollVer"},
                        {resizer: true, width: 1}
                    ]
    
                },
                {view: "scrollbar", id: "scrollHor", height: 20}
            ]
        };*/
		gantt.init(this.template.querySelector("div.gantt"), this.startDate, this.endDate);
		gantt.config.grid_resize = true;
		gantt.config.start_date = this.startDate;
		gantt.config.end_date = this.endDate;
		gantt.clearAll();
		gantt.parse(this.tasks);
		let taskGanttParentId = readCookie("taskGanttParentId");
		if (taskGanttParentId) {
			while (taskGanttParentId) {
				let parentId = taskGanttParentId;
				taskGanttParentId = gantt.getParent(taskGanttParentId);
				if (taskGanttParentId == 0) {
					gantt.open(parentId);
				} else {
					gantt.open(taskGanttParentId);
				}
			}
		} else {
			if (this.tasks.data.length > 0) {
				gantt.open(this.tasks.data[0].id);
			}
		}
		window.onresize = function () {
			gantt.render();
		};
	}

	handleRefresh() {
		if (this.wiredData) {
			refreshApex(this.wiredData);
		}
	}

	handleCancelModal() {
		const modal = this.template.querySelector("c-modal");
		modal.hide();
		this.isMarketingJourney = false;
		this.isPlan = false;
		this.isCampaign = false;
		this.isMarketingJourney = false;
		//this.taskIdCampaign = '';
		this.hasCampaignTab = false;
		this.hasOpenButton = false;
		this.financeAllocation = false;
	}

	handleOpen() {
		this.navigateToRecord();
	}
	handleNavigateToMarketingCloud() {
		window.open(this.mcUrlMap["" + this.taskId + ""], "MarketingCloud");
		this.handleCancelModal();
	}
	get mcUrl() {
		return this.mcUrlMap[this.taskId] ? this.mcUrlMap[this.taskId] : null;
	}

	navigateToDetailViewPage(objectName, taskrecordId) {
		this[NavigationMixin.GenerateUrl]({
			type: "standard__recordPage",
			attributes: {
				recordId: taskrecordId,
				objectApiName: objectName,
				actionName: "view",
			},
		})
			.then((url) => {
				//window.open(url)
				if (objectName === "musqotmp__Campaign__c") {
					this.campaignUrl = url;
				}
				if (objectName === "musqotmp__MarketingPlan__c") {
					this.marketingPlanUrl = url;
				}
				//this[NavigationMixin.Navigate](url);
			})
			.catch((error) => {
				this.template.querySelector("c-toast").showToast(error);
			});
	}
	//Sowmya user stroy W-000365 Start
	navigateToRecord() {
		if (this.hasCampaignTab) {
			this[NavigationMixin.GenerateUrl]({
				type: "standard__recordPage",
				attributes: {
					recordId: this.taskIdCampaign,
					objectApiName: this.otherobjectapiname,
					actionName: "view",
				},
			}).then((url) => {
				window.open(url, "Musqot");
				this.handleCancelModal();
			});
		} else {
			this[NavigationMixin.GenerateUrl]({
				type: "standard__recordPage",
				attributes: {
					recordId: this.taskId,
					objectApiName: this.objectApiName,
					actionName: "view",
				},
			}).then((url) => {
				window.open(url, "Musqot");
				this.handleCancelModal();
			});
		}
	}

	handleClose() {
		this.isPlan = false;
		this.isCampaign = false;
		this.isMarketingActivity = false;
		this.isMarketingJourney = false;
		this.taskIdCampaign = "";
		this.hasCampaignTab = false;
		this.hasOpenButton = false;
		this.financeAllocation = false;
	}

	handleActive(event) {
		this.hasCampaignTab = true;
		this.hasOpenButton = false;
	}
	handleActiveAllocation() {
		this.hasOpenButton = true;
		this.hasCampaignTab = false;
	}
	handleActiveOpenButton() {
		this.hasOpenButton = false;
		this.hasCampaignTab = false;
	}

	buildParamDataTable() {
		const whereCaluses = [];
		let fieldSetApiName = "musqotmp__AllocationGanttInfo";

		whereCaluses.push({
			field: "musqotmp__BeneficiaryCampaign__c",
			value: this.taskIdCampaign,
			clause: "=",
		});

		return JSON.stringify({
			component: "marketingPlanView-datatable",
			relatedListType: "finance",
			objectApiName: "musqotmp__Allocation__c",
			fieldMap: {
				//[ALLOCATION_OBJECT_ID_FIELD.fieldApiName]: {}
			},
			fieldSetApiName: fieldSetApiName,
			linkableNameField: true,
			whereCaluses: whereCaluses,
		});
	}

	@wire(datatable, {
		param: "$paramDataTable",
	})
	wiredDataTable(result) {
		if (result.data) {
			if (result.data.toast.ok) {
				this.columns = result.data.datatable.columns;
				this.data = result.data.datatable.data;
				this.template.querySelector("c-toast").showToast(result.data.toast);
			} else {
				this.data = undefined;
				this.columns = undefined;
				this.template.querySelector("c-toast").showToast(result.data.toast);
			}
		} else if (result.error) {
			this.data = undefined;
			this.columns = undefined;
			this.template.querySelector("c-toast").showToast(result.error);
		}
		this.tableLoadingState = false;
	}
	//Sowmya user stroy W-000365 End
}