/* eslint-disable no-unused-vars */
import { LightningElement, api, track, wire } from 'lwc';
import emailMessageToContributors from '@salesforce/apex/SendMessageToContributors.emailMessageToContributors';
import allContributors from '@salesforce/apex/SendMessageToContributors.allContributors';
import { getToast } from 'c/lds';
import saveFile from '@salesforce/apex/SendMessageToContributors.saveFile';
import CAMPAIGNCONTRIBUTOR_OBJECT from '@salesforce/schema/CampaignContributor__c';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import ROLEINCAMPAIGN_FIELD from '@salesforce/schema/CampaignContributor__c.RoleInCampaign__c';
import label_roleInCampaign from '@salesforce/label/c.hl_role_in_campaign';
import msg_contributorRole from '@salesforce/label/c.msg_contributor_role';
import msg_contributorMessage from '@salesforce/label/c.msg_is_contributor';
import msg_record_send	 from '@salesforce/label/c.msg_record_send';
import msg_file_upload		 from '@salesforce/label/c.msg_file_upload';
import btn_send from '@salesforce/label/c.btn_send';
import btn_cancel from '@salesforce/label/c.btn_cancel';

export default class Email extends LightningElement {
    label = {
        btn_send,
        msg_contributorRole,
        msg_contributorMessage,
        label_roleInCampaign,
        btn_cancel,
        msg_record_send,
        msg_file_upload	
    };
    @api recordId;
    @api objectApiName = 'musqotmp__CampaignContributor__c';
    @track fromaddresses;
    @track toaddresses = [];
    @track bcc;
    @track subject = '' ;
    @track body = '';
    @track contributorEmail;
    @track userEmail;
    @track fileName = '';
    @track UploadFile = 'Save File';
    @track showLoadingSpinner = false;
    @track isTrue = false;
    @track fileNames = []; 
    @track filesUploaded = [];
    @track file;
    @track fileContents;
    @track fileReader;
    @track content;
    @track contentIds= [];
    @track status;
    @track picklistValues = [];
    @track value = '---All---';
    @track campaignInRole;
    @track isContributor;
    /** Wired Apex result so it may be programmatically refreshed. */
    @track wiredData;

    @wire(getObjectInfo, { objectApiName: CAMPAIGNCONTRIBUTOR_OBJECT })
    objectInfo;
    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: ROLEINCAMPAIGN_FIELD})
    RoleInCampaignPicklistValues;

    handleChange(event) {
        this.value = event.detail.value;
        this.campaignInRole = event.detail.value;
     //   window.console.log('value :'+this.value);
     //   window.console.log('campaignInRole :'+this.campaignInRole);
    }
    @wire(allContributors, {
        param: JSON.stringify({ component: 'Email-allContributors'}),
        recordId: '$recordId'
    })
    wiredCampaignContributor(result){
        this.wiredData = result;
        if (result.data) {
            if (result.data.toast.ok) {             
                this.toaddresses = result.data.contributorEmails.join(', ');
                //this.recordId = result.data.recordId;
             //   window.console.log('recordIdEmail :'+this.recordId);
                this.userEmail = result.data.userEmail;
            }else{
                this.toaddresses = undefined;
                this.template.querySelector('c-toast').showToast(result.data.toast);
            }  
        }else if(result.error){
            this.toaddresses = undefined;
            this.template.querySelector('c-toast').showToast(result.error);
        }
       
    }
    handleFormInputChange(event){
        if(event.target.name === 'ToAddresses'){
            this.toaddresses = event.target.value;
        }
        else if( event.target.name === 'Bcc' ){
            this.bcc = event.target.value;
        }
        else if( event.target.name === 'Subject' ){
           this.subject= event.target.value;
        }
        else if( event.target.label === 'Body'){
            this.body = event.target.value;
        }
    }
    sendEmail() {
        if(this.toaddresses.length > 0){
            emailMessageToContributors({idParent:this.recordId, campaignInRole:this.value, contributorEmails: this.toaddresses.split(','), subject:this.subject, body:this.body, contentversionIds:this.contentIds})
            .then(result => {
            if(result.isContributor){
                this.isContributor  = result.isContributor; 
             //   window.console.log('isContributor :'+this.isContributor);
            }else{
                const toast = getToast('success', 'pester', this.label.msg_record_send);
                this.template.querySelector('c-toast').showToast(toast);
                this.closeModal();
            }    
        })
        .catch(error => {
            this.template.querySelector('c-toast').showToast(error);
        });
        }else if(this.campaignInRole){
            this.isContributor = this.label.msg_contributorRole + this.campaignInRole;
         //   window.console.log('isContributor :'+this.isContributor);
        }else{
                this.isContributor = this.label.msg_contributorMessage;
            //    window.console.log('isContributor :'+this.isContributor);
            }
        }
    // getting file 
    handleFilesChange(event) {
        if(event.target.files.length > 0) {
            this.filesUploaded = event.target.files;
            this.fileName = event.target.files[0].name;
        }
        this.handleSave();
    }
    handleSave() {
        if(this.filesUploaded.length > 0) {
            this.uploadHelper();
        }
        else {
            this.fileName = 'Please select file to upload!!';
        }
    }
    uploadHelper() {
        
        this.showLoadingSpinner = true;
        //this.file = this.filesUploaded[0];
        for(let i = 0; i < this.filesUploaded.length; i++){
            this.file = this.filesUploaded[i];
            this.fileNames.push(this.filesUploaded[i].name);
        // create a FileReader object 
        this.fileReader= new FileReader();
        // set onload function of FileReader object  
        this.fileReader.onloadend = (() => {
            this.fileContents = this.fileReader.result;
            let base64 = 'base64,';
            this.content = this.fileContents.indexOf(base64) + base64.length;
            this.fileContents = this.fileContents.substring(this.content);
            // call the uploadProcess method 
            this.saveToFile();
        })
        //Read the image
        this.fileReader.readAsDataURL(this.file);
    }
    }
    // Calling apex class to insert the file
    saveToFile() {
        saveFile({ idParent: this.recordId, strFileName: this.fileNames, base64Data: encodeURIComponent(this.fileContents)})
        .then(result => {
            this.contentIds = result.contentIds;
        //    window.console.log('contentIds :'+this.contentIds);
            this.isTrue = true;
            this.showLoadingSpinner = false;
            const toast = getToast('success', 'pester', this.label.msg_file_upload);
            this.template.querySelector('c-toast').showToast(toast);
        })
        .catch(error => {
            this.template.querySelector('c-toast').showToast(error);
            
        });
    }
    closeModal() {
        const selectedEvent = new CustomEvent("isgroupmessage", {
        isGroupMessage: false 
        });
        this.dispatchEvent(selectedEvent);
        this.handleRefresh();
    } 
    handleRefresh(){
        this.subject = '';
        this.body = '';
        this.isContributor = '';
        if(this.wiredData.data){
            refreshApex(this.wiredData);
        }
    }
}