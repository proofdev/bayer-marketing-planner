import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { reduceToasts } from 'c/toastUtils';
import sendToast from '@salesforce/apex/Campaign.sendToast';

export default class Toast extends LightningElement {
    @track paramSendToast;

    @api
    showToast(toasts) {
        this.toasts = reduceToasts(toasts);
        this.toasts.forEach(toast => {
            // UI API read toasts custom
            if (toast.showToast && typeof toast.showToast === 'boolean') {
                const evt = new ShowToastEvent({
                    title: toast.body.title,
                    message: toast.body.message,
                    variant: toast.body.variant,
                    mode: toast.body.mode
                });
                this.dispatchEvent(evt);

                if (toast.sendToast && typeof toast.sendToast === 'boolean') {
                    this.paramSendToast = JSON.stringify({
                        component: 'toast-sendToast',
                        toast: toast
                    });
    
                    sendToast({ param: this.paramSendToast })
                        .then(result => {
                            if (result.toast.ok) {
                                this.toast = result.toast;
                                this.showToast(this.toast);
                            }else{
                                this.toast = result.toast;
                                this.showToast(this.toast);
                            }
                        })
                        .catch(error => {
                            this.toast = error;
                            this.showToast(this.toast);
                        });
                }
            }
            // UI API read toasts
            else if (Array.isArray(toast.body)) {
                toast.body.forEach(message => {
                    const evt = new ShowToastEvent({
                        title: 'Error',
                        message: message,
                        variant: 'error',
                        mode: 'sticky'
                    });
                    this.dispatchEvent(evt);
                });
            }
            // UI API DML, Apex and network toasts
            else if (toast.body && typeof toast.body.message === 'string') {
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: toast.body.message,
                    variant: 'error',
                    mode: 'sticky'
                });
                this.dispatchEvent(evt);
            }
            // JS toasts
            else if (typeof toast.message === 'string') {
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: toast.message,
                    variant: 'error',
                    mode: 'sticky'
                });
                this.dispatchEvent(evt);
            }else if (typeof toast === 'string') {
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: toast,
                    variant: 'error',
                    mode: 'sticky'
                });
                this.dispatchEvent(evt);
            }else{
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: toast.statusText,
                    variant: 'error',
                    mode: 'sticky'
                });
                this.dispatchEvent(evt);
            }
        });
    }
}