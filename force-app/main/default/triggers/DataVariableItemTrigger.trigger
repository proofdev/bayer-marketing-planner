/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              07/27/2020          Created for W-000396
**************************************************************************************************************************************/
trigger DataVariableItemTrigger on DataVariableItem__c (before insert, before update) {
    DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance();
    if(!disableTrigger.musqotmp__DataVariableItem__c){
        if(trigger.isInsert){
            if(trigger.isBefore){
                //Added for W-000396
                DataVariableItemTriggerHelper.beforeInsert(Trigger.new);
            }    
        } 
        if(trigger.isUpdate){
            if(trigger.isBefore){
                //Added for W-000396
                DataVariableItemTriggerHelper.beforeUpdate(Trigger.new,Trigger.oldMap);
            }    
        }
    }
}