/* Date          :26-Sep-2019
** Author        :Alka Kumari 
** Description   :Created for W-000096
*/
trigger TaskTrigger on Task (after insert, after update){
    musqotmp__DisableTrigger__c disableTrigger = new musqotmp__DisableTrigger__c();
    if(!disableTrigger.musqotmp__Task__c){
    if(Trigger.isAfter){
        try{
            List<Task> campaignTaskList = new List<Task>();
            List<String> campaignContributerList = new List<String>();
            List<Id> campignIdList = new List<Id>();
            for(Task tsk: Trigger.new){
                if(tsk.WhatId != null){
                    String whatIdStr = String.valueof(tsk.WhatId);
                    if(whatIdStr.startsWith('70') && tsk.Status != 'Completed'){
                        campignIdList.add(tsk.WhatId);
                        campaignTaskList.add(tsk);
                        String camId = String.valueOf(tsk.whatId).substring(0, 15);
                        String userId = String.valueOf(tsk.OwnerId).substring(0, 15);
                        campaignContributerList.add(camId+userId);
                    }    
                }
            }
            if(Trigger.isInsert){
                if(campaignTaskList.size()>0)
                    TaskTriggerHelper.createCampaignContributer(campaignTaskList,campaignContributerList);
            }
            if(Trigger.isUpdate){
                if(campaignTaskList.size()>0)
                    TaskTriggerHelper.updateCampaignContributer(campaignTaskList,Trigger.oldMap,campaignContributerList);
            }
        } Catch (Exception e){
            ExceptionHandler.logError('Apex', 'TaskTrigger', 'Trigger.isAfter', e);
        }
    }
    }
}