/************************************************************************************************************************************
* Developer			Date				Description
* ---------------------------------------------------------------------------------------------------------------------------------------	                                                               												campaigncontributorrelatedlist 
* Abhishek          24/12/2020			Trigger for MarketingPlanMember validation.
**************************************************************************************************************************************/
trigger PlanMemberTrigger on musqotmp__MarketingPlanMember__c (before insert, after insert, before update, after update, before delete, after delete) {
    try{
        DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance();
        if(!disableTrigger.musqotmp__MarketingPlanMember__c )
        {
            if(trigger.isInsert){
                if(trigger.isBefore){
                   // system.debug('inside trigger Before Insert');
                    PlanMemberTriggerHelper.processPlanMemberBeforeInsert(Trigger.New);}
                if(trigger.isAfter){
                   // system.debug('inside trigger After Insert');
                    PlanMemberTriggerHelper.processPlanMemberAfterInsert(Trigger.New);}
                }
            if(trigger.isUpdate){
                //system.debug('inside trigger.isAfter');
                if(trigger.isBefore){
                    PlanMemberTriggerHelper.processPlanMemberBeforeUpdate(Trigger.new,Trigger.oldMap);}
                if(trigger.isAfter){
                  // system.debug('inside trigger.isAfter');
                   PlanMemberTriggerHelper.processPlanMemberAfterUpdate(Trigger.new, Trigger.oldMap);}
                }
            if(trigger.isDelete){
                if(trigger.isBefore){
                  //  system.debug('inside trigger.isAfter');
                    PlanMemberTriggerHelper.processPlanMemberBeforeDelete(Trigger.Old);}
                if(trigger.isAfter){
                  //  system.debug('inside trigger.isAfter');
                    PlanMemberTriggerHelper.processPlanMemberAfterDelete(Trigger.Old);}
                }
        	}
    	}
        catch(Exception ex){
            ExceptionHandler.logError('Trigger','PlanMemberTrigger','',ex);
        }
	}