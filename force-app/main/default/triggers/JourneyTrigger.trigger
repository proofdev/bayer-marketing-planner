/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              06/04/2020          Created for W-000339 (Deleted Existing Data Variable Job and Data Variable Item records and 
create new Data Variable Job and Data Variable Item records as per the updated values if applicable)
**************************************************************************************************************************************/
trigger JourneyTrigger on MarketingJourney__c (after Update, after insert, after delete, after undelete,before insert) {
    DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance();
     set<string> dbJourneySet = new set<string>();
    if(!disableTrigger.MarketingJourney__c){
         if(trigger.isInsert){
            if(trigger.isBefore){
              for(musqotmp__MarketingJourney__c journey : [Select id, Name, musqotmp__JourneyID__c  from musqotmp__MarketingJourney__c]){
                    dbJourneySet.add(journey.musqotmp__JourneyID__c);
                }
                for(musqotmp__MarketingJourney__c newJourney : trigger.new){
                    if(dbJourneySet.contains(newJourney.musqotmp__JourneyID__c)){
                        newJourney.addError('Journey ID should be unique. A journey with this ID already exists.');
                    }
                }
            }
        }
        if(trigger.isUpdate){
            if(trigger.isAfter){
                JourneyTriggerHelper.processDataVarForecastsAfterUpdate(Trigger.new,Trigger.oldMap);    
            }
        }
    }
    
    List<schema.Campaign> campaign = new List<schema.Campaign>();
    set<Id> custord = new Set<Id>();
    if(Trigger.isDelete){
        for(MarketingJourney__c activity: Trigger.Old){
            custord.add(activity.musqotmp__Campaign__c);
        }
    } else if(Trigger.isUpdate){
        for(MarketingJourney__c test:Trigger.New) {
            custord.add(test.musqotmp__Campaign__c);   
        }
        for(MarketingJourney__c MA:Trigger.Old) {
            custord.add(MA.musqotmp__Campaign__c);   
        }   
    } else {
        for(MarketingJourney__c MA:Trigger.New) {
            custord.add(MA.musqotmp__Campaign__c); 
        } 
    }
    AggregateResult[] groupedResults = [SELECT COUNT(Id), musqotmp__Campaign__c FROM musqotmp__MarketingJourney__c where musqotmp__Campaign__c IN :custord GROUP BY musqotmp__Campaign__c  ];
    if(groupedResults.size() > 0){
        for(AggregateResult ar:groupedResults) {
            Id custid = (ID)ar.get('musqotmp__Campaign__c');
            Integer count = (INTEGER)ar.get('expr0');
            Schema.Campaign cust1 = new Schema.Campaign();
            cust1.Id = custid;
            cust1.musqotmp__Total_number_of_journeys__c  = count;
            campaign.add(cust1);
        }
        update campaign;
    }else if(groupedResults.size() == 0){
        // List<schema.Campaign> campaignList = new List<schema.Campaign>();
        List<schema.Campaign> campaignList = [SELECT id, musqotmp__Total_number_of_journeys__c FROM Campaign where id IN :custord];
        for(schema.Campaign camp:campaignList){
            camp.musqotmp__Total_number_of_journeys__c = 0;
        }
        if(campaignList.size() > 0){
            update campaignList;
        }
    }
}