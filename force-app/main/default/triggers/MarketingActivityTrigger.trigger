/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              06/05/2020          Created for W-000339 (Deleted Existing Data Variable Job and Data Variable Item records and 
create new Data Variable Job and Data Variable Item records as per the updated values if applicable)
**************************************************************************************************************************************/
trigger MarketingActivityTrigger on MarketingActivity__c (after Update, after insert, after delete, after undelete) {
    DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance();
    if(!disableTrigger.musqotmp__MarketingActivity__c){
        if(trigger.isUpdate){
            if(trigger.isAfter){
                MarketingActivityTriggerHelper.processDataVarForecastsAfterUpdate(Trigger.new,Trigger.oldMap);    
            }
        }
    }
    List<schema.Campaign> campaign = new List<schema.Campaign>();
    set<Id> custord = new Set<Id>();
    if(Trigger.isDelete){
        for(musqotmp__MarketingActivity__c activity: Trigger.Old){
            custord.add(activity.musqotmp__Campaign__c);
        }
    } else if(Trigger.isUpdate){
        for(musqotmp__MarketingActivity__c test:Trigger.New) {
            custord.add(test.musqotmp__Campaign__c);   
        }
        for(musqotmp__MarketingActivity__c MA:Trigger.Old) {
            custord.add(MA.musqotmp__Campaign__c);   
        }   
    } else {
        for(musqotmp__MarketingActivity__c MA:Trigger.New) {
            custord.add(MA.musqotmp__Campaign__c); 
        } 
    }
    AggregateResult[] groupedResults = [SELECT COUNT(Id), musqotmp__Campaign__c FROM musqotmp__MarketingActivity__c where musqotmp__Campaign__c IN :custord GROUP BY musqotmp__Campaign__c  ];
 
    if(groupedResults.size() > 0){
        for(AggregateResult ar:groupedResults) {
            Schema.Campaign cust1 = new Schema.Campaign();
            Id custid = (ID)ar.get('musqotmp__Campaign__c');
            Integer  count = (INTEGER)ar.get('expr0');
            cust1.Id = custid;
            system.debug('count'+count);
            cust1.musqotmp__Total_number_of_components__c  = count;
            campaign.add(cust1);
        }
        update campaign;
    }else if(groupedResults.size() == 0){
       // List<schema.Campaign> campaignList = new List<schema.Campaign>();
         List<schema.Campaign> campaignList = [SELECT id, musqotmp__Total_number_of_components__c FROM Campaign where id IN :custord];
        for(schema.Campaign camp:campaignList){
           camp.musqotmp__Total_number_of_components__c = 0;
        }
        if(campaignList.size() > 0){
            update campaignList;
        }
    }
    
}