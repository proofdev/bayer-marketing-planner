/************************************************************************************************************************************
* Developer         Date(MM/DD/YYYY)    Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              06/05/2020          Created for W-000339 (Deleted Existing Data Variable Job and Data Variable Item records and 
create new Data Variable Job and Data Variable Item records as per the updated values if applicable)
* Sowmya              01/07/2022          Update for W-000390 to created Forecast when Allocation object is not available
**************************************************************************************************************************************/
trigger CampaignTrigger on Campaign (after insert, after update, before insert,before update) {
    DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance();
    if(!disableTrigger.musqotmp__Campaign__c){
        set<string> dbNameSet = new set<string>();
        set<string> dbCECId = new set<string>();
        set<string> dbTCId = new set<string>();
        if(trigger.isInsert){
            if(trigger.isBefore){
                for(Schema.Campaign camObj : [Select id, Name,musqotmp__CEC_ID_text__c,musqotmp__TC_ID_text__c  from Campaign]){
                    dbNameSet.add(camObj.Name);
                    dbCECId.add(camObj.musqotmp__CEC_ID_text__c);
                    dbTCId.add(camObj.musqotmp__TC_ID_text__c);
                }
                for(Schema.Campaign newCampaign : trigger.new){
                    if(dbNameSet.contains(newCampaign.name)){
                        newCampaign.addError('Campaign name should be unique. Campaign with this name already exists.');
                    }
                    string recordTypeName = Schema.getGlobalDescribe().get('Campaign').getDescribe().getRecordTypeInfosById().get(newCampaign.RecordTypeId).getName();
                    
                    if(recordTypeName == 'Customer Evolution Campaign (CEC)'){
                        if(dbCECId.contains(newCampaign.musqotmp__CEC_ID_text__c) ){
                            newCampaign.addError('CEC ID should be unique. A campaign with this ID already exists.');
                        }
                    } 
                    if(recordTypeName == 'Tactical Campaign (TC)'){
                        if(dbTCId.contains(newCampaign.musqotmp__TC_ID_text__c) ){
                            newCampaign.addError('TC ID should be unique. A campaign with this ID already exists.');
                        }
                    }
                }
            }
            if(trigger.isAfter){
                //Added for W-000390
                if(!DataVariableJobForecastUtility.isObjectExist('musqotmp__Allocation__c')){
                    //CampaignTriggerHelper.createCampaignForecast(Trigger.new);
                }  
            }
        }
        if(trigger.isUpdate){
            if(trigger.isAfter){
                //Added for W-000390
                if(!DataVariableJobForecastUtility.isObjectExist('musqotmp__Allocation__c')){
                    //CampaignTriggerHelper.updateCampaignForecast(Trigger.new,Trigger.oldMap);
                } else {
                    //Added for W-000339
                    //CampaignTriggerHelper.processDataVarForecastsAfterUpdate(Trigger.new,Trigger.oldMap);    
                }
            }
        }
    }
}