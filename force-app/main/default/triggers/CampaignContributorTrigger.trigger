/************************************************************************************************************************************
* Developer         Date                Description
* ---------------------------------------------------------------------------------------------------------------------------------------
* Alka              05/05/2020          Created for W-000307 - (Update Campaign.WorkingTime__c as sum of all child musqotmp__CampaignContributor__c.WorkingTime__c)
**************************************************************************************************************************************/

trigger CampaignContributorTrigger on CampaignContributor__c(after insert,after update,after delete) {
    DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance();
    Set<Id> campaignIdsToUpdate = new Set<Id>();
  //  system.debug('disableTrigger.musqotmp__CampaignContributor__c ***'+disableTrigger.musqotmp__CampaignContributor__c );
    if(!disableTrigger.musqotmp__CampaignContributor__c){
        if(trigger.isInsert){
            if(trigger.isAfter){
            	//Added for W-000307 - start
                List<musqotmp__CampaignContributor__c> campaignContributorListWithWT = new List<musqotmp__CampaignContributor__c>();
                for(musqotmp__CampaignContributor__c campContri : Trigger.new){
                    if(campContri.musqotmp__WorkingTime__c > 0 && campContri.musqotmp__Campaign__c != null){
                        campaignIdsToUpdate.add(campContri.musqotmp__Campaign__c); 
                        campaignContributorListWithWT.add(campContri);
                    }
                }
                if(campaignIdsToUpdate.size() > 0)
                    CampaignContributorTriggerHelper.rollUpWorkingTimeAfterInsert(campaignIdsToUpdate,campaignContributorListWithWT);
                //Added for W-000307 - end
            }
        }
        if(trigger.isUpdate){
            if(trigger.isAfter){
                //Added for W-000307 - start
                List<musqotmp__CampaignContributor__c> campaignContributorListWithWT = new List<musqotmp__CampaignContributor__c>();
                for(musqotmp__CampaignContributor__c cc : Trigger.new){
                  //  System.debug('cc.musqotmp__WorkingTime__c'+cc.musqotmp__WorkingTime__c);
                 //   System.debug('Trigger.oldMap.get(cc.Id).musqotmp__WorkingTime__c'+Trigger.oldMap.get(cc.Id).musqotmp__WorkingTime__c);
                    if(cc.musqotmp__Campaign__c != null && 
                       cc.musqotmp__WorkingTime__c != Trigger.oldMap.get(cc.Id).musqotmp__WorkingTime__c){
                           campaignIdsToUpdate.add(cc.musqotmp__Campaign__c); 
                           campaignContributorListWithWT.add(cc);
                    }
                }
                if(campaignIdsToUpdate.size()>0)
                	CampaignContributorTriggerHelper.rollUpWorkingTimeAfterUpdate(campaignIdsToUpdate,campaignContributorListWithWT,Trigger.oldMap);
                //Added for W-000307 - end
            }
        }
        if(trigger.isDelete){
            if(trigger.isAfter){
                //Added for W-000307 - start
                //List<musqotmp__CampaignContributor__c> allocationListWithApprovedAmt = new List<musqotmp__CampaignContributor__c>();
                for(musqotmp__CampaignContributor__c cc : Trigger.old){
                    if(cc.musqotmp__Campaign__c != null){
                        //allocationListWithApprovedAmt.add(allocation);
                        campaignIdsToUpdate.add(cc.musqotmp__Campaign__c); 
                    }
                }
                if(campaignIdsToUpdate.size() > 0)
                    CampaignContributorTriggerHelper.rollUpWorkingTimeAftereDelete(campaignIdsToUpdate,Trigger.old);
                //Added for W-000307 - end
            }
        }
    }
}