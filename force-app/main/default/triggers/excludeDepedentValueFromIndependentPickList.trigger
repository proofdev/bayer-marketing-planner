trigger excludeDepedentValueFromIndependentPickList on GanttPerformance__c (before insert,before update) {
  for(GanttPerformance__c g : Trigger.New) {
    if(g.musqotmp__DependentData__c != null && g.musqotmp__IndependentData__c!= null){
      String depentdentValue =  g.musqotmp__DependentData__c;
      Set<String> multiSelectIndepentdentValue =  new set<String>(g.musqotmp__IndependentData__c.split(';'));
      //System.debug(multiSelectIndepentdentValue);
      if(multiSelectIndepentdentValue.contains(depentdentValue)) {
          g.addError('Please remove the dependent data from independent data list!');
      }
    }
  }
}