/************************************************************************************************************************************
* Developer			Date				Description
* ---------------------------------------------------------------------------------------------------------------------------------------	                                                               												campaigncontributorrelatedlist 
* Abhishek          28/12/2020			Trigger for Plan Member creation.
**************************************************************************************************************************************/
trigger PlanTrigger on musqotmp__MarketingPlan__c (after insert, after update) {
	try{
        DisableTrigger__c disableTrigger = DisableTrigger__c.getInstance();
        if(!disableTrigger.musqotmp__MarketingPlan__c){
            if(trigger.isInsert){
                if(trigger.isAfter){
                   // System.debug('In After Insert' + Trigger.new);
                    PlanTriggerHelper.afterInsert(Trigger.new);
                }
            }
            if(trigger.isUpdate){
                if(trigger.isAfter){
                    PlanTriggerHelper.afterUpdate(Trigger.new, Trigger.oldMap);
                }
            }
        }
    }Catch(Exception ex){
        ExceptionHandler.logError('Trigger','PlanTrigger','',ex);
    }
}