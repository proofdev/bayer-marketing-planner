trigger KeyInsightDashboardTrigger on KeyInsightDashboard__c (before insert, before update) {
    for(KeyInsightDashboard__c kid: Trigger.New){
        kid.musqotmp__KIDUniqId__c  = kid.musqotmp__keyInsightDashboardId__c +''+kid.musqotmp__KIDUser__c;
    }
}